/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["getNpgpjsApi"];

var Services = Components.utils.import("resource://gre/modules/Services.jsm").Services;
const XPCOMUtils = ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm").XPCOMUtils;

if (typeof CryptoAPI === "undefined") {
  Services.scriptloader.loadSubScript("chrome://enigmail/content/modules/cryptoAPI/interface.js",
    null, "UTF-8"); /* global CryptoAPI */
}

/* eslint no-invalid-this: 0 */
XPCOMUtils.defineLazyModuleGetter(this, "EnigmailKeyRing", "chrome://enigmail/content/modules/keyRing.jsm", "EnigmailKeyRing"); /* global EnigmailKeyRing: false */
XPCOMUtils.defineLazyModuleGetter(this, "EnigmailDialog", "chrome://enigmail/content/modules/dialog.jsm", "EnigmailDialog"); /* global EnigmailDialog: false */
XPCOMUtils.defineLazyModuleGetter(this, "EnigmailData", "chrome://enigmail/content/modules/data.jsm", "EnigmailData"); /* global EnigmailData: false */
XPCOMUtils.defineLazyModuleGetter(this, "EnigmailKeyEditor", "chrome://enigmail/content/modules/cryptoAPI/gnupg-keyEditor.jsm", "EnigmailKeyEditor"); /* global EnigmailKeyEditor: false */

const EnigmailLog = ChromeUtils.import("chrome://enigmail/content/modules/log.jsm").EnigmailLog;
const EnigmailExecution = ChromeUtils.import("chrome://enigmail/content/modules/execution.jsm").EnigmailExecution;
const EnigmailFiles = ChromeUtils.import("chrome://enigmail/content/modules/files.jsm").EnigmailFiles;
const EnigmailConstants = ChromeUtils.import("chrome://enigmail/content/modules/constants.jsm").EnigmailConstants;
const EnigmailTime = ChromeUtils.import("chrome://enigmail/content/modules/time.jsm").EnigmailTime;
const EnigmailLocale = ChromeUtils.import("chrome://enigmail/content/modules/locale.jsm").EnigmailLocale;
const EnigmailOS = ChromeUtils.import("chrome://enigmail/content/modules/os.jsm").EnigmailOS;
const EnigmailVersioning = ChromeUtils.import("chrome://enigmail/content/modules/versioning.jsm").EnigmailVersioning;
const EnigmailPrefs = ChromeUtils.import("chrome://enigmail/content/modules/prefs.jsm").EnigmailPrefs;
const EnigmailTrust = ChromeUtils.import("chrome://enigmail/content/modules/trust.jsm").EnigmailTrust;
const AddonManager = Components.utils.import("resource://gre/modules/AddonManager.jsm").AddonManager;

var inspector;

/**
 * npgp.js implementation of CryptoAPI
 */

class NpgpJSCryptoAPI extends CryptoAPI {
  constructor() {
    super();
    this.api_name = "Npgp.JS";
    this._npgpjsPath = "";

    if (!inspector) {
      inspector = Cc["@mozilla.org/jsinspector;1"].createInstance(Ci.nsIJSInspector);
    }
  }

  /**
   * Initialize the tools/functions required to run the API
   *
   * @param {nsIWindow} parentWindow: parent window, may be NULL
   * @param {Object} esvc: Enigmail service object
   */
  initialize(parentWindow, esvc) {
    EnigmailLog.DEBUG(`npgp.js: initialize()\n`);
    if (!esvc) {
      esvc = {
        environment: Cc["@mozilla.org/process/environment;1"].getService(Ci.nsIEnvironment)
      };
    }

    this._esvc = esvc;

    EnigmailLog.DEBUG(`npgp.js: Environment: ${EnigmailCore.getEnvList().join(", ")}\n`);

    this._npgpjsPath = this.sync(findNpgpjs());
    this._parentWindow = parentWindow;
    if (!this._npgpjsPath) {
      throw new Error(`npgp.js: initialize: npgp.js executable not found`);
    }

    this._version = this.sync(this.determineNpgpVersion());

    EnigmailLog.DEBUG(`npgp.js: initialization complete\n`);
  }


  /**
   * Close/shutdown anything related to the functionality
   */
  finalize() {
    return null;
  }


  // /**
  //  * Obtain signatures for a given set of key IDs.
  //  *
  //  * @param {String}  fpr:            key fingerprint. Separate multiple keys by spaces.
  //  * @param {Boolean} ignoreUnknownUid: if true, filter out unknown signer's UIDs
  //  *
  //  * @return {Promise<Array of Object>}
  //  *     - {String} userId
  //  *     - {String} rawUserId
  //  *     - {String} keyId
  //  *     - {String} fpr
  //  *     - {String} created
  //  *     - {Array} sigList:
  //  *            - {String} userId
  //  *            - {String} created
  //  *            - {Number} createdTime
  //  *            - {String} signerKeyId
  //  *            - {String} sigType
  //  *            - {Boolean} sigKnown
  //  */
  // async getKeySignatures(fpr, ignoreUnknownUid = false) {
  //   EnigmailLog.DEBUG(`npgp.js: getKeySignatures(${fpr}, ${ignoreUnknownUid})\n`);
  //   let cmdObj = {
  //     "op": "keylist",
  //     "sigs": true,
  //     "keys": fpr.split(/[ ,]+/)
  //   };

  //   let keysObj = await this.execJsonCmd(cmdObj);
  //   let signatureList = [];

  //   if ("keys" in keysObj && keysObj.keys.length > 0) {
  //     for (let key of keysObj.keys) {
  //       for (let uid of key.userids) {
  //         const sig = {
  //           userId: EnigmailData.convertGpgToUnicode(uid.uid),
  //           rawUserId: EnigmailData.convertGpgToUnicode(uid.uid),
  //           keyId: key.subKeys[0].keyid,
  //           fpr: key.fingerprint,
  //           created: EnigmailTime.getDateTime(key.subKeys[0].timestamp, true, false),
  //           sigList: []
  //         };

  //         for (let s of uid.signatures) {
  //           let uid = s.name ? s.name : "";
  //           let sigKnown = s.status === "Success";
  //           if (sigKnown) {
  //             if (s.email) {
  //               if (uid.length > 0) {
  //                 uid += " <" + s.email + ">";
  //               }
  //               else {
  //                 uid = s.email;
  //               }
  //             }

  //             if (s.comment.length > 0) {
  //               if (uid.length > 0) {
  //                 uid += "(" + s.comment + ")";
  //               }
  //               else {
  //                 uid = s.comment;
  //               }
  //             }
  //           }

  //           if (sigKnown || ignoreUnknownUid) {
  //             sig.sigList.push({
  //               userId: EnigmailData.convertGpgToUnicode(uid),
  //               created: EnigmailTime.getDateTime(s.timestamp, true, false),
  //               createdTime: s.timestamp,
  //               signerKeyId: s.keyid,
  //               sigType: s.exportable ? "x" : "l",
  //               sigKnown: sigKnown
  //             });
  //           }
  //         }
  //         signatureList.push(sig);
  //       }
  //     }
  //   }

  //   return signatureList;
  // }

  /**
   * Get the list of all konwn keys (including their secret keys)
   * @param {Array of String} onlyKeys: [optional] only load data for specified key IDs
   *
   * @return {Promise<Array of Object>}
   */
  async getKeys(onlyKeys = null) {
    EnigmailLog.DEBUG(`npgp.js: getKeys(${onlyKeys})\n`);
    let cmdObj = {
      "op": "list"
    };

    if (onlyKeys && typeof (onlyKeys) === "string") {
      onlyKeys = onlyKeys.split(/[, ]+/);
    }
    if (onlyKeys) {
      cmdObj.data = {
        fpr: onlyKeys
      };
    }

    let keysObj = await this.execJsonCmd(cmdObj);

    let keyArr = [];
    if ("result" in keysObj) {
      for (let key of keysObj.result) {
        keyArr.push(createKeyObj(key));
      }
    }

    return keyArr;
  }

  // /**
  //  * Return an array containing the aliases and the email addresses
  //  *
  //  * @return {Array<{alias,keylist}>} <{String,String}>
  //  */
  // getGroups() {
  //   EnigmailLog.DEBUG(`npgp.js: getGroups()\n`);
  //   let cfg = this.sync(this.execJsonCmd({
  //     op: "config_opt",
  //     component: "gpg",
  //     option: "group"
  //   }));
  //   let groups = null;
  //   if ("option" in cfg) {
  //     groups = cfg.option.value;
  //   }

  //   if (!groups) return [];

  //   let groupList = [];
  //   for (let g of groups) {
  //     let parts = g.string.match(/^([^=]+)=(.+)$/);
  //     if (parts && parts.length > 2) {
  //       parts[1] = parts[1].toLowerCase();
  //       if (parts[1] in groupList) {
  //         groupList[parts[1]] += ` ${parts[2]}`;
  //       }
  //       else
  //         groupList[parts[1]] = parts[2];
  //     }
  //   }

  //   let ret = [];
  //   for (let g in groupList) {
  //     ret.push({
  //       alias: g,
  //       keylist: groupList[g]
  //     });
  //   }

  //   return ret;
  // }

  // /**
  //  * Get groups defined in gpg.conf in the same structure as KeyObject
  //  * [synchronous]
  //  *
  //  * @return {Array of KeyObject} with type = "grp"
  //  */
  // getGroupList() {
  //   EnigmailLog.DEBUG(`npgp.js: getGroupList()\n`);

  //   let groupList = this.getGroups();
  //   let retList = [];
  //   for (let grp of groupList) {

  //     let grpObj = {
  //       type: "grp",
  //       keyUseFor: "G",
  //       userIds: [],
  //       subKeys: [],
  //       keyTrust: "g",
  //       userId: grp.alias,
  //       keyId: grp.alias
  //     };

  //     let rcpt = grp.keylist.split(/[,; ]+/);
  //     for (let r of rcpt) {
  //       grpObj.userIds.push({
  //         userId: EnigmailData.convertGpgToUnicode(r),
  //         keyTrust: "q"
  //       });
  //     }

  //     retList.push(grpObj);
  //   }

  //   return retList;
  // }

  // /**
  //  * Extract a photo ID from a key, store it as file and return the file object.
  //  *
  //  * @param {String} keyId:       Key Fingerprint
  //  * @param {Number} photoNumber: number of the photo on the key, starting with 0
  //  *
  //  * @return {nsIFile} object or null in case no data / error.
  //  */

  // async getPhotoFile(keyId, photoNumber) {
  //   // not available via this API
  //   return null;
  // }


  /**
   * Import key(s) from a file
   *
   * @param {nsIFile} inputFile:  the file holding the keys
   *
   * @return {Object} or null in case no data / error:
   *   - {Number}          exitCode:        result code (0: OK)
   *   - {Array of String) importedKeys:    imported fingerprints
   *   - {Number}          importSum:       total number of processed keys
   *   - {Number}          importUnchanged: number of unchanged keys
   */

  async importKeyFromFile(inputFile) {
    EnigmailLog.DEBUG(`npgp.js: importKeyFromFile(${inputFile.path})\n`);

    let fileData = EnigmailFiles.readBinaryFile(inputFile);
    return this.importKeyData(fileData, false);
  }

  /**
   * Import key(s) from a string
   *
   * @param {String} keyData:  the key data to be imported (ASCII armored)
   * @param {Boolean} minimizeKey: import the minimum key without any 3rd-party signatures
   * @param {Array of String} limitedUids: skip UIDs that were not specified
   *
   * @return {Object} or null in case no data / error:
   *   - {Number}          exitCode:        result code (0: OK)
   *   - {Array of String) importedKeys:    imported fingerprints
   *   - {Number}          importSum:       total number of processed keys
   *   - {Number}          importUnchanged: number of unchanged keys
   */

  async importKeyData(keyData, minimizeKey = false, limitedUids = []) {
    EnigmailLog.DEBUG(`npgp.js: importKeyData(${keyData.length}, ${minimizeKey})\n`);

    let req = {
      op: "import",
      data: {
        key: keyData,
        minimize: minimizeKey ? true : false
      }
    };

    if (limitedUids && limitedUids.length > 0) {
      req.data.limitUid = limitedUids;
    }

    const res = await this.execJsonCmd(req);

    if ("result" in res) {
      return {
        exitCode: 0,
        errorMsg: "",
        importedKeys: res.result.importedFpr,
        importSum: res.result.importedFpr.length,
        importUnchanged: 0,
        secCount: 0,
        secImported: 0,
        secDups: 0
      };
    }
    else
      return {
        exitCode: 1,
        errorMsg: res.error.message,
        errorCode: res.error.code
      };
  }

  // /**
  //  * Delete keys from keyring
  //  *
  //  * @param {Array<String>} fpr: fingerprint(s) to delete
  //  * @param {Boolean} deleteSecretKey: if true, also delete secret keys
  //  * @param {nsIWindow} parentWindow: parent window for displaying modal dialogs
  //  *
  //  * @return {Promise<Object>}:
  //  *      - {Number} exitCode: 0 if successful, other values indicate error
  //  *      - {String} errorMsg: error message if deletion not successful
  //  */
  // async deleteKeys(fpr, deleteSecretKey, parentWindow) {
  //   EnigmailLog.DEBUG(`npgp.js: deleteKeys(${fpr.join("+")}, ${deleteSecretKey})\n`);
  //   let args = ["--no-verbose", "--status-fd", "2", "--batch", "--yes"];

  //   if (deleteSecretKey) {
  //     args.push("--delete-secret-and-public-key");
  //   }
  //   else {
  //     args.push("--delete-keys");
  //   }

  //   args = args.concat(fpr);
  //   const res = await EnigmailExecution.execAsync(this._gpgPath, args, "");
  //   const deletedKeys = [];

  //   let lines = res.statusMsg.split(/[\r\n]+/);
  //   for (let l of lines) {
  //     if (l.search(/^KEY_CONSIDERED /) === 0) {
  //       deletedKeys.push(l.split(/ /)[1]);
  //     }
  //   }

  //   let exitCode = (deletedKeys.length >= fpr.length) ? 0 : 1;

  //   return {
  //     exitCode: exitCode,
  //     errorMsg: exitCode !== 0 ? res.errorMsg : ""
  //   };
  // }

  // /**
  //  * Export the minimum key for the public key object:
  //  * public key, user ID, newest encryption subkey
  //  *
  //  * @param {String} fpr  : a single FPR
  //  * @param {String} email: [optional] the email address of the desired user ID.
  //  *                        If the desired user ID cannot be found or is not valid, use the primary UID instead
  //  * @param {Array<Number>} subkeyDates: [optional] remove subKeys with sepcific creation Dates
  //  *
  //  * @return {Promise<Object>}:
  //  *    - exitCode (0 = success)
  //  *    - errorMsg (if exitCode != 0)
  //  *    - keyData: BASE64-encded string of key data
  //  */
  // async getMinimalPubKey(fpr, email, subkeyDates) {
  //   return exportKeyFromGnuPG(this._gpgPath, fpr, false, false, true, email, subkeyDates);
  // }

  // /**
  //  * Export secret key(s) as ASCII armored data
  //  *
  //  * @param {String}  keyId       Specification by fingerprint or keyID, separate mutliple keys with spaces
  //  * @param {Boolean} minimalKey  if true, reduce key to minimum required
  //  *
  //  * @return {Object}:
  //  *   - {Number} exitCode:  result code (0: OK)
  //  *   - {String} keyData:   ASCII armored key data material
  //  *   - {String} errorMsg:  error message in case exitCode !== 0
  //  */

  // async extractSecretKey(keyId, minimalKey = false) {
  //   return exportKeyFromGnuPG(this._gpgPath, keyId, true, true, minimalKey);
  // }

  // /**
  //  * Export public key(s) as ASCII armored data
  //  *
  //  * @param {String}  keyId       Specification by fingerprint or keyID, separate mutliple keys with spaces
  //  *
  //  * @return {Object}:
  //  *   - {Number} exitCode:  result code (0: OK)
  //  *   - {String} keyData:   ASCII armored key data material
  //  *   - {String} errorMsg:  error message in case exitCode !== 0
  //  */

  // async extractPublicKey(keyId) {
  //   return exportKeyFromGnuPG(this._gpgPath, keyId, false, true, false);
  // }

  // /**
  //  * Generate a new key pair
  //  *
  //  * @param {String} name:       name part of UID
  //  * @param {String} comment:    comment part of UID (brackets are added)
  //  * @param {String} email:      email part of UID (<> will be added)
  //  * @param {Number} expiryDate: number of days after now; 0 if no expiry
  //  * @param {Number} keyLength:  size of key in bytes (not supported in this API)
  //  * @param {String} keyType:    'RSA' or 'ECC'
  //  * @param {String} passphrase: password; use null if no password (not supported in this API)
  //  *
  //  * @return {Object}: Handle to key creation
  //  *    - {function} cancel(): abort key creation
  //  *    - {Promise<exitCode, generatedKeyId>} promise: resolved when key creation is complete
  //  *                 - {Number} exitCode:       result code (0: OK)
  //  *                 - {String} generatedKeyId: generated key ID
  //  */

  // generateKey(name, comment, email, expiryDate = 0, keyLength, keyType, passphrase) {
  //   EnigmailLog.DEBUG(`npgp.js: generateKey(${name}, ${email}, ${expiryDate}, ${keyType})\n`);
  //   let canceled = false;

  //   let promise = new Promise((resolve, reject) => {
  //     let uid = (name + (comment ? ` (${comment})` : "") + (email ? ` <${email}>` : "")).trim();
  //     this.execJsonCmd({
  //       op: "createkey",
  //       userid: uid,
  //       algo: (keyType === "RSA" ? "default" : "future-default"),
  //       expires: expiryDate * 86400
  //     }).then(async (result) => {
  //       if ("fingerprint" in result) {
  //         resolve({
  //           exitCode: 0,
  //           generatedKeyId: "0x" + result.fingerprint
  //         });
  //       }
  //       else {
  //         let r = getErrorMessage(result.code);
  //         reject(r.errorMessage);
  //       }

  //     }).catch(err => {
  //       reject(err);
  //     });
  //   });

  //   return {
  //     cancel: function () {
  //       canceled = true;
  //     },
  //     promise: promise
  //   };
  // }


  // /**
  //  * Determine the file name from OpenPGP data.
  //  *
  //  * @param {byte} byteData    The encrypted data
  //  *
  //  * @return {String} - the name of the attached file
  //  */

  // async getFileName(byteData) {
  //   let r = await this.decrypt(byteData, {
  //     noOutput: true
  //   });

  //   if (r.exitCode === 0) {
  //     return r.encryptedFileName;
  //   }
  //   return null;
  // }

  // /**
  //  * Verify the detached signature of an attachment (or in other words,
  //  * check the signature of a file, given the file and the signature).
  //  *
  //  * @param {String} filePath    Path specification for the signed file
  //  * @param {String} sigPath     Path specification for the signature file
  //  *
  //  * @return {Promise<String>} - A message from the verification.
  //  *
  //  * Use Promise.catch to handle failed verifications.
  //  * The message will be an error message in this case.
  //  */

  // async verifyAttachment(filePath, sigPath) {
  //   let dataFile = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
  //   let sigFile = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
  //   EnigmailFiles.initPath(dataFile, filePath);
  //   EnigmailFiles.initPath(sigFile, sigPath);

  //   if (!dataFile.exists()) {
  //     throw new Error(`Data file ${filePath} does not exist`);
  //   }
  //   if (!sigFile.exists()) {
  //     throw new Error(`Signature file ${sigPath} does not exist`);
  //   }

  //   let data = EnigmailFiles.readBinaryFile(dataFile);
  //   let sig = EnigmailFiles.readBinaryFile(sigFile);

  //   let r = await this.verifyMime(data, sig, null);
  //   if (r.statusFlags & (EnigmailConstants.BAD_SIGNATURE | EnigmailConstants.UNVERIFIED_SIGNATURE)) {
  //     throw r.errorMsg ? r.errorMsg : EnigmailLocale.getString("unverifiedSig") + " - " + EnigmailLocale.getString("msgSignedUnkownKey");
  //   }

  //   const detailArr = r.sigDetails.split(/ /);
  //   const dateTime = EnigmailTime.getDateTime(detailArr[2], true, true);
  //   return r.errorMsg + "\n" + EnigmailLocale.getString("keyAndSigDate", [r.keyId, dateTime]);
  // }

  // /**
  //  * Decrypt an attachment.
  //  *
  //  * @param {Bytes}  encrypted     The encrypted data
  //  *
  //  * @return {Promise<Object>} - Return object with decryptedData and
  //  * status information
  //  *
  //  * Use Promise.catch to handle failed decryption.
  //  * retObj.errorMsg will be an error message in this case.
  //  */

  // async decryptAttachment(encrypted) {
  //   let r = await this.decrypt(encrypted, {});
  //   r.stdoutData = r.decryptedData;
  //   delete r.decryptedData;
  //   return r;
  // }

  /**
   * Generic function to decrypt and/or verify an OpenPGP message.
   *
   * @param {String} encrypted     The encrypted data
   * @param {String} encrypted     The encrypted data
   * @param {Object} options       Decryption options
   *      - logFile (the actual file)
   *      - keyserver
   *      - keyserverProxy
   *      - fromAddr
   *      - noOutput
   *      - verifyOnly
   *      - uiFlags
   *      - mimeSignatureFile
   *      - maxOutputLength
   *
   * @return {Promise<Object>} - Return object with decryptedData and status information:
   *     - {String} decryptedData
   *     - {Number} exitCode
   *     - {Number} statusFlags
   *     - {String} errorMsg
   *     - {String} blockSeparation
   *     - {String} userId: signature user Id
   *     - {String} keyId: signature key ID
   *     - {String} sigDetails: as printed by GnuPG for VALIDSIG pattern
   *     - {String} encToDetails: \n  keyId1 (userId1),\n  keyId1 (userId2),\n  ...
   *     - {String} encryptedFileName
   *
   * Use Promise.catch to handle failed decryption.
   * retObj.errorMsg will be an error message in this case.
   */

  async decrypt(encrypted, options) {
    EnigmailLog.DEBUG(`npgp.js: decrypt()\n`);

    let r = await this.execJsonCmd({
      op: options.verifyOnly ? "verify" : "decrypt",
      data: {
        cryptoData: encrypted,
        password: ("password" in options) ? options.password : undefined
      }
    });

    EnigmailLog.DEBUG(`npgp.js: decrypt: result: ${"result" in r ? "OK" : "Error"}\n`);
    let ret = {
      decryptedData: "",
      exitCode: 2,
      statusFlags: 0,
      errorMsg: "err",
      blockSeparation: "",
      userId: "",
      keyId: "",
      sigDetails: "",
      encToDetails: ""
    };

    if ("result" in r) {
      ret.errorMsg = "";
      ret.decryptedData = r.result.decryptedData;
      if (options.uiFlags & EnigmailConstants.UI_PGP_MIME) {
        ret.statusFlags |= EnigmailConstants.PGP_MIME_ENCRYPTED;
      }

      if ("decryptionResult" in r.result) {
        if (r.result.decryptionResult.indexOf("DECRYPTION_OKAY") >= 0) {
          ret.statusFlags |= EnigmailConstants.DECRYPTION_OKAY;
          ret.exitCode = 0;
        }
        else {
          if (r.result.decryptionResult.indexOf("INVALID_OR_MISSING_MDC,") >= 0) {
            ret.statusFlags |= EnigmailConstants.MISSING_MDC;
            ret.decryptedData = "";
          }
          if (r.result.decryptionResult.indexOf("DECRYPTION_FAILED") >= 0) {
            ret.statusFlags |= EnigmailConstants.DECRYPTION_FAILED;
            ret.decryptedData = "";
          }
          if (r.result.decryptionResult.indexOf("NO_PRIVATE_KEY") >= 0) {
            ret.statusFlags |= EnigmailConstants.NO_SECKEY;
          }
          if (r.result.decryptionResult.indexOf("BAD_PASSPHRASE") >= 0) {
            ret.statusFlags |= EnigmailConstants.BAD_PASSPHRASE;
          }
          if (r.result.decryptionResult.indexOf("INVALID_MESSAGE") >= 0) {
            ret.statusFlags |= EnigmailConstants.BAD_ARMOR;
          }
        }

        if (r.result.encryptedFileName) ret.encryptedFileName = r.result.encryptedFileName;

        if ("encryptionKeys" in r.result) {
          let encToArr = [];
          for (let keyid of r.result.encryptionKeys) {
            // except for ID 00000000, which signals hidden keys
            if (keyid.search(/^0+$/) < 0) {
              let localKey = EnigmailKeyRing.getKeyById(`0x${keyid}`);
              encToArr.push(keyid + (localKey ? ` (${localKey.userId})` : ""));
            }
            else {
              encToArr.push(EnigmailLocale.getString("hiddenKey"));
            }
          }
          ret.encToDetails = "\n  " + encToArr.join(",\n  ") + "\n";
        }
      }

      if ("signatures" in r.result) {
        await this._interpetSignatureData(r.result, ret);
      }
    }
    else {
      EnigmailLog.DEBUG(`npgp.js: decrypt: result= ${JSON.stringify(r)}\n`);

      let msg = {
        statusFlags: EnigmailConstants.DECRYPTION_FAILED
      };

      if ("error" in r) {
        msg = getErrorMessage(r.error.code, r.error.message);
      }
      else {
        msg.errorMessage = EnigmailLocale.getString("failedDecrypt");
      }
      ret.exitCode = 1;
      ret.errorMsg = msg.errorMessage;
      ret.statusFlags = msg.statusFlags | EnigmailConstants.DECRYPTION_FAILED;
    }

    return ret;
  }

  // /**
  //  * Decrypt a PGP/MIME-encrypted message
  //  *
  //  * @param {String} encrypted     The encrypted data
  //  * @param {Object} options       Decryption options (see decrypt() for details)
  //  *
  //  * @return {Promise<Object>} - Return object with decryptedData and status information
  //  *                             (see decrypt() for details)
  //  *
  //  * Use Promise.catch to handle failed decryption.
  //  * retObj.errorMsg will be an error message in this case.
  //  */

  // async decryptMime(encrypted, options) {
  //   options.noOutput = false;
  //   options.verifyOnly = false;
  //   options.uiFlags = EnigmailConstants.UI_PGP_MIME;

  //   return this.decrypt(encrypted, options);
  // }

  // /**
  //  * Verify a PGP/MIME-signed message
  //  *
  //  * @param {String} signedData    The signed data
  //  * @param {String} signature     The signature data
  //  * @param {Object} options       Decryption options
  //  *
  //  * @return {Promise<Object>} - Return object with decryptedData and
  //  *                             status information
  //  *
  //  * Use Promise.catch to handle failed decryption.
  //  * retObj.errorMsg will be an error message in this case.
  //  */

  // async verifyMime(signedData, signature, options) {
  //   EnigmailLog.DEBUG(`npgp.js: verifyMime()\n`);
  //   let result = await this.execJsonCmd({
  //     op: "verify",
  //     data: btoa(signedData),
  //     signature: btoa(signature),
  //     base64: true
  //   });

  //   let ret = {};

  //   if ("info" in result) {
  //     await this._interpetSignatureData(result, ret);
  //   }
  //   else {
  //     EnigmailLog.DEBUG(`npgp.js: verifyMime: result.type: ${result.type}\n`);
  //     ret.errorMsg = result.msg;
  //     ret.statusFlags = EnigmailConstants.DECRYPTION_FAILED;
  //   }

  //   return ret;
  // }

  /**
   * private function to read/intperet the signature data returned by gpgme
   */
  async _interpetSignatureData(resultData, retObj) {
    if (!retObj) return;
    if (!("signatures" in resultData)) return;
    if (resultData.signatures.length < 1) return;

    const
      undetermined = 0,
      none = 1,
      keyMissing = 2,
      red = 3,
      expired = 4,
      green = 5,
      valid = 99;

    let overallSigStatus = undetermined,
      iSig = null;

    // determine "best" signature
    for (let sig of resultData.signatures) {
      let sigStatus = 0;
      switch (sig.signatureStatus) {
        case "VALID_SIGNATURE":
          sigStatus = green;
          if (retObj.exitCode === 2) retObj.exitCode = 0;
          break;
        case "BAD_SIGNATURE":
          sigStatus = red;
          break;
        case "UNKNOWN_CERT":
          sigStatus = keyMissing;
          break;
        case "GOOD_SIG_EXPIRED_CERT":
        case "GOOD_SIG_INVALID_CERT":
          sigStatus = expired;
          if (retObj.exitCode === 2) retObj.exitCode = 0;
          break;
      }
      if (sigStatus > overallSigStatus) {
        overallSigStatus = sigStatus;
        iSig = sig;
      }
    }

    // interpret the "best" signature
    if (iSig) {
      if (overallSigStatus === red) {
        retObj.statusFlags |= EnigmailConstants.BAD_SIGNATURE;
      }
      else if (overallSigStatus === keyMissing) {
        retObj.statusFlags |= (EnigmailConstants.UNVERIFIED_SIGNATURE | EnigmailConstants.NO_PUBKEY);
      }
      else {
        retObj.statusFlags |= EnigmailConstants.GOOD_SIGNATURE;
        if (overallSigStatus === green) retObj.statusFlags |= EnigmailConstants.TRUSTED_IDENTITY;
        //if (iSig.summary.revoked) retObj.statusFlags |= EnigmailConstants.REVOKED_KEY; // FIXME
        if (overallSigStatus === expired) retObj.statusFlags |= (EnigmailConstants.EXPIRED_KEY | EnigmailConstants.EXPIRED_KEY_SIGNATURE);
      }

      if (iSig.fpr) {
        retObj.keyId = iSig.fpr;
        let keys = await this.getKeys(`0x${iSig.fpr}`);
        if (keys && keys.length > 0) {
          if (retObj.statusFlags & EnigmailConstants.GOOD_SIGNATURE) {
            retObj.errorMsg = EnigmailLocale.getString("prefGood", [keys[0].userId]);
          }
          else if (retObj.statusFlags & EnigmailConstants.BAD_SIGNATURE) {
            retObj.errorMsg = EnigmailLocale.getString("prefBad", [keys[0].userId]);
          }
          retObj.userId = keys[0].userId;
        }
        else {
          keys = null;
        }

        let sigDate = iSig.created.substring(0, 10);
        /*  VALIDSIG args are (separated by space):
            - <fingerprint_in_hex> 4F9F89F5505AC1D1A260631CDB1187B9DD5F693B
            - <sig_creation_date> 2020-03-21
            - <sig-timestamp> 1584805187
            - <expire-timestamp> 0
            - <sig-version> 4
            - <reserved> 0
            - <pubkey-algo> 1
            - <hash-algo> 8
            - <sig-class> 00
            - [ <primary-key-fpr> ] 4F9F89F5505AC1D1A260631CDB1187B9DD5F693B
        */
        retObj.sigDetails = `${iSig.fpr} ${sigDate} ${getTimeFromDateStr(iSig.created)} ${getTimeFromDateStr(iSig.signatureExpirationTime)} 4 0 ${iSig.pubkeyAlgorithm} ${iSig.hashAlgorithm} 00 ${keys ? keys[0].fpr : ""}`;
      }
    }
  }

  // /**
  //  * Get details (key ID, UID) of the data contained in a OpenPGP key block
  //  *
  //  * @param {String} keyBlockStr  String: the contents of one or more public keys
  //  *
  //  * @return {Promise<Array>}: array of objects with the following structure:
  //  *          - id (key ID)
  //  *          - fpr
  //  *          - name (the UID of the key)
  //  */

  // async getKeyListFromKeyBlock(keyBlockStr) {
  //   EnigmailLog.DEBUG(`npgp.js: getKeyListFromKeyBlock()\n`);

  //   const args = ["--no-tty", "--batch", "--no-verbose", "--with-fingerprint", "--with-colons", "--import-options", "import-show", "--dry-run", "--import"];
  //   const ENTRY_ID = 0;
  //   const KEY_ID = 4;
  //   const CREATED_ID = 5;
  //   const USERID_ID = 9;


  //   let res = await EnigmailExecution.execAsync(this._gpgPath, args, keyBlockStr);
  //   let lines = res.stdoutData.split(/\n/);

  //   let key = {};
  //   let keyId = "";
  //   let keyList = [];

  //   for (let i = 0; i < lines.length; i++) {
  //     const lineTokens = lines[i].split(/:/);

  //     switch (lineTokens[ENTRY_ID]) {
  //       case "pub":
  //       case "sec":
  //         key = {
  //           id: lineTokens[KEY_ID],
  //           fpr: null,
  //           name: null,
  //           isSecret: false,
  //           created: EnigmailTime.getDateTime(lineTokens[CREATED_ID], true, false),
  //           uids: []
  //         };

  //         if (!(key.id in keyList)) {
  //           keyList[key.id] = key;
  //         }

  //         if (lineTokens[ENTRY_ID] === "sec") {
  //           keyList[key.id].isSecret = true;
  //         }
  //         break;
  //       case "fpr":
  //         if (!key.fpr) {
  //           key.fpr = lineTokens[USERID_ID];
  //         }
  //         break;
  //       case "uid":
  //         if (!key.name) {
  //           key.name = lineTokens[USERID_ID];
  //         }
  //         else {
  //           key.uids.push(lineTokens[USERID_ID]);
  //         }
  //         break;
  //       case "rvs":
  //         keyId = lineTokens[KEY_ID];
  //         if (keyId) {
  //           if (keyId in keyList) {
  //             keyList[keyId].revoke = true;
  //           }
  //           else {
  //             keyList[keyId] = {
  //               revoke: true,
  //               id: keyId
  //             };
  //           }
  //         }
  //         break;
  //     }
  //   }

  //   return keyList;
  // }


  // /**
  //  * Export the ownertrust database
  //  * @param {String or nsIFile} outputFile: Output file name or Object - or NULL if trust data
  //  *                                        should be returned as string
  //  *
  //  * @return {Object}:
  //  *          - ownerTrustData {String}: if outputFile is NULL, the key block data; "" if a file is written
  //  *          - exitCode {Number}: exit code
  //  *          - errorMsg {String}: error message
  //  */
  // async getOwnerTrust(outputFile) {
  //   const DEFAULT_FILE_PERMS = 0o600;
  //   const GPG_ARGS = ["--no-tty", "--batch", "--no-verbose", "--export-ownertrust"];

  //   let res = await EnigmailExecution.execAsync(this._gpgPath, GPG_ARGS, "");
  //   let exitCode = res.exitCode;
  //   let errorMsg = res.errorMsg;

  //   if (outputFile) {
  //     if (!EnigmailFiles.writeFileContents(outputFile, res.stdoutData, DEFAULT_FILE_PERMS)) {
  //       exitCode = -1;
  //       errorMsg = EnigmailLocale.getString("fileWriteFailed", [outputFile]);
  //     }

  //     return {
  //       ownerTrustData: "",
  //       exitCode: exitCode,
  //       errorMsg: errorMsg
  //     };
  //   }

  //   return {
  //     ownerTrustData: res.stdoutData,
  //     exitCode: exitCode,
  //     errorMsg: errorMsg
  //   };

  // }


  // /**
  //  * Import the ownertrust database
  //  *
  //  * @param {String or nsIFile} inputFile: input file name or Object
  //  *
  //  * @return {Object}:
  //  *         - exitCode {Number}: exit code
  //  *         - errorMsg {String}: error message
  //  */
  // async importOwnerTrust(inputFile) {
  //   const GPG_ARGS = ["--no-tty", "--batch", "--no-verbose", "--import-ownertrust"];
  //   let res = {
  //     exitCode: -1,
  //     errorMsg: ""
  //   };

  //   try {
  //     let trustData = EnigmailFiles.readFile(inputFile);
  //     res = await EnigmailExecution.execAsync(this._gpgPath, GPG_ARGS, trustData);
  //   }
  //   catch (ex) { }

  //   return res;
  // }


  // /**
  //  * Encrypt messages
  //  *
  //  * @param {String} from: keyID of sender/signer
  //  * @param {String} recipients: keyIDs or email addresses of recipients, separated by spaces
  //  * @param {String} hiddenRecipients: keyIDs or email addresses of hidden recipients (bcc), separated by spaces
  //  * @param {Number} encryptionFlags: Flags for Signed/encrypted/PGP-MIME etc.
  //  * @param {String} plainText: data to encrypt
  //  * @param {String} hashAlgorithm: [OPTIONAL] hash algorithm (ignored for this API)
  //  * @param {nsIWindow} parentWindow: [OPTIONAL] window on top of which to display modal dialogs
  //  *
  //  * @return {Object}:
  //  *     - {Number} exitCode:    0 = success / other values: error
  //  *     - {String} data:        encrypted data
  //  *     - {String} errorMsg:    error message in case exitCode !== 0
  //  *     - {Number} statusFlags: Status flags for result
  //  */

  // async encryptMessage(from, recipients, hiddenRecipients, encryptionFlags, plainText, hashAlgorithm = null, parentWindow = null) {
  //   EnigmailLog.DEBUG(`npgp.js: encryptMessage(${from}, ${recipients}, ${encryptionFlags}, ${plainText.length})\n`);
  //   let reqOp;

  //   if (encryptionFlags & EnigmailConstants.SEND_ENCRYPTED) {
  //     if (hiddenRecipients.length > 0) {
  //       recipients += " " + hiddenRecipients;
  //     }

  //     reqOp = {
  //       op: "encrypt",
  //       keys: (from + " " + recipients).split(/[ ,]+/),
  //       data: btoa(plainText),
  //       sender: from,
  //       base64: true,
  //       armor: true,
  //       'always-trust': encryptionFlags & EnigmailConstants.SEND_ALWAYS_TRUST ? true : false,
  //       mime: encryptionFlags & EnigmailConstants.SEND_PGP_MIME ? true : false
  //     };

  //     if (encryptionFlags & EnigmailConstants.SEND_SIGNED) {
  //       reqOp.signing_keys = from;
  //     }
  //   }
  //   else {
  //     reqOp = {
  //       op: "sign",
  //       keys: from,
  //       data: btoa(plainText),
  //       sender: from,
  //       base64: true,
  //       armor: true,
  //       mode: encryptionFlags & EnigmailConstants.SEND_PGP_MIME ? "detached" : "clearsign"
  //     };
  //   }
  //   let result = await this.execJsonCmd(reqOp);
  //   EnigmailLog.DEBUG(`npgp.js: encryptMessage: result.type: ${result.type}\n`);

  //   if (result.type === "ciphertext" || result.type === "signature") {
  //     result.exitCode = 0;
  //     result.statusFlags = 0;
  //     result.errorMsg = "";
  //     if (result.base64) {
  //       result.data = atob(result.data);
  //     }
  //   }
  //   else {
  //     EnigmailLog.DEBUG(`npgp.js: encryptMessage: result=${JSON.stringify(result)}\n`);

  //     let r = getErrorMessage(result.code);
  //     result.errorMsg = r.errorMessage;
  //     result.exitCode = 1;
  //     result.statusFlags = r.statusFlags;
  //     result.data = "";
  //   }

  //   return result;
  // }

  // /**
  //  * Clear any cached passwords
  //  *
  //  * @return {Boolean} true if successful, false otherwise
  //  */
  // async clearPassphrase() {
  //   const input = "RELOADAGENT\n/bye\n";
  //   let gpgConnPath = resolveToolPath(this._gpgAgentPath, "gpg-connect-agent");

  //   let res = await EnigmailExecution.execAsync(gpgConnPath, [], input);
  //   return (res.stdoutData.search(/^ERR/m) < 0);
  // }

  /***
   * Determine if a specific feature is available by the used toolset
   *
   * @param {String} featureName:  String; one of the following values:
   *    version-supported    - is the gpg version supported at all (true for gpg >= 2.0.10)
   *    supports-gpg-agent   - is gpg-agent is auto-started (true for gpg >= 2.0.16)
   *    keygen-passphrase    - can the passphrase be specified when generating keys (false for gpg 2.1 and 2.1.1)
   *    windows-photoid-bug  - is there a bug in gpg with the output of photoid on Windows (true for gpg < 2.0.16)
   *    genkey-no-protection - is "%no-protection" supported for generting keys (true for gpg >= 2.1)
   *    search-keys-cmd      - what command to use to terminate the --search-key operation. ("save" for gpg > 2.1; "quit" otherwise)
   *    socks-on-windows     - is SOCKS proxy supported on Windows (true for gpg >= 2.0.20)
   *    supports-dirmngr     - is dirmngr supported (true for gpg >= 2.1)
   *    supports-ecc-keys    - are ECC (elliptic curve) keys supported (true for gpg >= 2.1)
   *    supports-sender      - does gnupg understand the --sender argument (true for gpg >= 2.1.15)
   *    supports-wkd         - does gpg support wkd (web key directory) (true for gpg >= 2.1.19)
   *    export-result        - does gpg print EXPORTED when exporting keys (true for gpg >= 2.1.10)
   *    decryption-info      - does gpg print DECRYPTION_INFO (true for gpg >= 2.0.19)
   *    export-specific-uid  - does gpg support exporting a key with a specific UID (true for gpg >= 2.2.8)
   *    supports-show-only   - does gpg support --import-options show-only (true for gpg >= 2.1.14)
   *    handles-huge-keys    - can gpg deal with huge keys without aborting (true for gpg >= 2.2.17)
   *    smartcard            - does the library support smartcards
   *    uid-management       - implementation supports adding, removing etc. of UIDs (true for GnuPG)

   * @return: depending on featureName - Boolean unless specified differently:
   *    (true if feature is available / false otherwise)
   *   If the feature cannot be found, undefined is returned
   */
  supportsFeature(featureName) {
    switch (featureName) {
      case "supports-ecc-keys":
      case "export-specific-uid":
      case "keygen-passphrase":
        return true;
    }
    return false;
  }

  /**
   * Get a human readable version string
   *
   * @returns {String}
   */
  getVersionString() {
    return `npgp.js ${this._version.version} / OpenPGP.js ${this._version.libraryVersion}`;
  }


  /**
   *
   * @param {String} trustCode
   * @return {String}   Localized label
   */
  getTrustLabel(trustCode) {
    let keyTrust = trustCode;
    switch (trustCode) {
      case 'r':
        keyTrust = EnigmailLocale.getString("keyValid.revoked");
        break;
      case 'e':
        keyTrust = EnigmailLocale.getString("keyValid.expired");
        break;
      case 'f':
        keyTrust = EnigmailLocale.getString("keyTrust.full");
        break;
      case 'u':
        keyTrust = EnigmailLocale.getString("keyTrust.ultimate");
        break;
      default:
        keyTrust = "";
    }

    return keyTrust;
  }

  // /**
  //  * Return the key management functions (sub-API)
  //  */
  // getKeyManagement() {
  //   EnigmailKeyEditor.gpgPath = this._gpgPath;
  //   return EnigmailKeyEditor;
  // }

  // /**
  //  * Return the OpenPGP configuration directory (if any)
  //  *
  //  * @return {String}: config directory or null if none
  //  */
  // getConfigDir() {
  //   if (!this._gpgConfPath) return null;

  //   const args = ["--list-dirs"];

  //   let result = this.sync(EnigmailExecution.execAsync(this._gpgConfPath, args, ""));

  //   let m = result.stdoutData.match(/^(homedir:)(.*)$/mi);
  //   if (m && m.length > 2) {
  //     return EnigmailData.convertGpgToUnicode(unescape(m[2]));
  //   }

  //   return null;
  // }

  async execJsonCmd(paramsObj) {
    let jsonStr = JSON.stringify(paramsObj) + ";";
    EnigmailLog.DEBUG(`npgp.js: execJsonCmd(${jsonStr.substring(0, 40)})\n`);

    const ek = this._esvc.environment.get("ENIGMAILKEYS");
    if (ek && ek.length > 0) {
      EnigmailCore.setEnvVariable("ENIGMAILKEYS", ek);
    }
    let result;

    try {
      let result = await EnigmailExecution.execAsync(this._npgpjsPath, [], jsonStr);

      if (!result.stdoutData) throw "no data";
      if (result.exitCode !== 0) throw "error";
      let retObj = JSON.parse(result.stdoutData.replace(/;[ \t\r\n]*$/, ""));
      return retObj;
    }
    catch (ex) {
      EnigmailLog.DEBUG(`npgp.js: execJsonCmd: error ${ex.toString()}\n`);

      return {
        "error": result.stderrData
      };
    }
  }

  async determineNpgpVersion() {
    EnigmailLog.DEBUG(`npgp.js: determineNppgVersion(${this._npgpjsPath.path})\n`);

    return getNpgpVersion(this._npgpjsPath);
  }


  // async getAgentMaxIdle() {
  //   EnigmailLog.DEBUG("npgp.js: getAgentMaxIdle()\n");
  //   let maxIdle = -1;

  //   if (!this._gpgConfPath) return maxIdle;

  //   const DEFAULT = 7;
  //   const CFGVALUE = 9;

  //   let result = await EnigmailExecution.execAsync(this._gpgConfPath, ["--list-options", "gpg-agent"], "");
  //   const lines = result.stdoutData.split(/[\r\n]/);

  //   for (let i = 0; i < lines.length; i++) {
  //     EnigmailLog.DEBUG("npgp.js: getAgentMaxIdle: line: " + lines[i] + "\n");

  //     if (lines[i].search(/^default-cache-ttl:/) === 0) {
  //       const m = lines[i].split(/:/);
  //       if (m[CFGVALUE].length === 0) {
  //         maxIdle = Math.round(m[DEFAULT] / 60);
  //       }
  //       else {
  //         maxIdle = Math.round(m[CFGVALUE] / 60);
  //       }

  //       break;
  //     }
  //   }
  //   return maxIdle;
  // }

  // async setAgentMaxIdle(idleMinutes) {
  //   EnigmailLog.DEBUG("npgp.js: setAgentMaxIdle()\n");
  //   if (!this._gpgConfPath) return;

  //   const RUNTIME = 8;

  //   const stdinData = "default-cache-ttl:" + RUNTIME + ":" + (idleMinutes * 60) + "\n" +
  //     "max-cache-ttl:" + RUNTIME + ":" + (idleMinutes * 600) + "\n";

  //   let result = await EnigmailExecution.execAsync(this._gpgConfPath, ["--runtime", "--change-options", "gpg-agent"], stdinData);
  //   EnigmailLog.DEBUG("npgp.js: setAgentMaxIdle.stdout: " + result.stdoutData + "\n");
  // }

  // async getMaxIdlePref() {
  //   let maxIdle = EnigmailPrefs.getPref("maxIdleMinutes");

  //   try {
  //     if (this._gpgConfPath) {
  //       const m = await this.getAgentMaxIdle();
  //       if (m > -1) maxIdle = m;
  //     }
  //   }
  //   catch (ex) { }

  //   return maxIdle;
  // }

  // async setMaxIdlePref(minutes) {
  //   EnigmailPrefs.setPref("maxIdleMinutes", minutes);
  //   try {
  //     await this.setAgentMaxIdle(minutes);
  //   }
  //   catch (ex) { }
  // }
}


function getNpgpjsApi() {
  return new NpgpJSCryptoAPI();
}


function getAddonPath() {
  let p = new Promise((resolve) => {
    AddonManager.getAddonByID("{847b3a00-7ab1-11d4-8f02-006008948af5}", addonData => {
      addonData.getDataDirectory(dir => {
        resolve(dir);
      });
    });
  });

  return p;
}


async function findNpgpjs() {
  EnigmailLog.DEBUG("npgp.js: findNpgpjs()\n");

  let filePath = await getNpgpjsPath();

  if (!filePath.exists()) {
    await installNpgpjs();
  }
  else {
    let versionStr = EnigmailFiles.readUrlContent("chrome://enigmail/content/bin/npgpjs-version.json");
    let expectedVersion = JSON.parse(versionStr);

    let currentVersion = await getNpgpVersion(filePath.path);

    if (expectedVersion.version !== currentVersion.version) {
      await installNpgpjs();
    }
  }

  return filePath.path;
}

async function getNpgpjsPath() {
  EnigmailLog.DEBUG("npgp.js: getNpgpjsPath()\n");
  let addonPath = await getAddonPath();

  let filePath = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
  EnigmailFiles.initPath(filePath, addonPath);

  filePath.append("bin");
  if (!filePath.exists()) {
    createDirWithParents(filePath);
  }

  filePath.append("npgp.js");

  return filePath;
}


async function installNpgpjs() {
  EnigmailLog.DEBUG("npgp.js: installNpgpjs()\n");

  let filePath = await getNpgpjsPath();

  let fileContent = EnigmailFiles.readUrlContent("chrome://enigmail/content/bin/npgp.js");

  EnigmailLog.DEBUG(`npgp.js: installNpgpjs: writing ${fileContent.length} bytes to ${filePath.path}\n`);
  EnigmailFiles.writeFileContents(filePath, fileContent, 0o700);
}

async function getNpgpVersion(path) {
  EnigmailLog.DEBUG(`npgp.js: getNpgpVersion(${path})\n`);

  const req = { op: "version" };
  let v = { version: null, openpgp: null };

  const res = await EnigmailExecution.execAsync(path, [], JSON.stringify(req) + ";");

  if (res.exitCode !== 0) {
    EnigmailLog.ERROR(`npgp.js: getNpgpVersion: npgp.js failed with exitCode ${res.exitCode} msg='${res.stdoutData} ${res.stderrData}'\n`);
    throw Components.results.NS_ERROR_FAILURE;
  }

  res.stdoutData = res.stdoutData.replace(/;[\t \n\r]*$/g, "");
  try {
    v = JSON.parse(res.stdoutData);

    EnigmailLog.DEBUG(`npgp.js: detected Npgp.js version '${v.version}'\n`);

  }
  catch (x) {
    EnigmailLog.DEBUG(`npgp.js: no valid JSON output: ${res.stdoutData}'\n`);
  }


  return {
    npgpjs: v.version,
    openpgp: v.libraryVersion
  };
}


/**
 * Determine the location of the GnuPG executable
 *
 * @param env: Object: nsIEnvironment to use
 *
 * @return Object: nsIFile pointing to gpg, or NULL
 */
function resolvePath(env) {
  EnigmailLog.DEBUG("npgp.js: resolvePath()\n");

  let toolName = EnigmailOS.isDosLike ? "npgpjs.exe" : "npgp.js";

  // Resolve relative path using PATH environment variable
  const envPath = env.get("PATH");
  let toolPath = EnigmailFiles.resolvePath(toolName, envPath, EnigmailOS.isDosLike);

  if (!toolPath && EnigmailOS.isDosLike) {
    // DOS-like systems: FIXME
  }


  if (!toolPath && !EnigmailOS.isDosLike) {
    // Unix-like systems: check /usr/bin and /usr/local/bin
    let gpgPath = "/usr/bin:/usr/local/bin";
    toolPath = EnigmailFiles.resolvePath(toolName, gpgPath, EnigmailOS.isDosLike);
  }

  if (!toolPath) {
    return null;
  }

  return toolPath.QueryInterface(Ci.nsIFile);
}

function resolveToolPath(parentPath, fileName) {
  let filePath = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);

  EnigmailFiles.initPath(filePath, parentPath);

  if (filePath) {
    // try to get the install directory of gpg/gpg2 executable
    filePath.normalize();
    filePath = filePath.parent;
  }

  if (filePath) {
    filePath.append(EnigmailFiles.potentialWindowsExecutable(fileName));
    if (filePath.exists()) {
      filePath.normalize();
      return filePath;
    }
  }

  return EnigmailFiles.resolvePathWithEnv(fileName);
}

function getSecondsFromDate(str) {
  if (!str || str.length === 0) return 0;

  return Math.floor(new Date(str).getTime() / 1000);
}

function createKeyObj(keyData) {
  if (!("validity" in keyData)) {
    keyData.validity = "f";
  }

  keyData.keyCreated = getSecondsFromDate(keyData.created);
  keyData.created = EnigmailTime.getDateTime(keyData.keyCreated, true, false);
  //keyData.keyUseFor = keyData.keyUseFor + keyData.keyUseFor.toUpperCase();
  keyData.ownerTrust = keyData.validity;
  keyData.photoAvailable = false; // FIXME
  keyData.type = "pub";
  keyData.keyTrust = keyData.validity;
  keyData.expiryTime = getSecondsFromDate(keyData.expiryTime);

  for (let i = 0; i < keyData.subKeys.length; i++) {
    let s = keyData.subKeys[i];
    s.expiryTime = getSecondsFromDate(s.expiryTime);
    s.expiry = EnigmailTime.getDateTime(s.expiryTime, true, false);
    s.keyTrust = s.validity;
    s.keyUseFor = s.keyUseFor + s.keyUseFor.toUpperCase();
    s.keyCreated = getSecondsFromDate(s.created);
    s.created = EnigmailTime.getDateTime(s.keyCreated, true, false);
    s.type = "sub";
  }

  if (keyData.userIds.length > 0) {
    keyData.userId = keyData.userIds[0].userId;

    for (let u of keyData.userIds) {
      u.keyTrust = u.validity;
      u.uidFpr = "0";
      u.type = "uid";
    }
  }

  return keyData;
}

/**
 * Recursively create a directory 
 * @param {nsIFile} dirObj 
 */

function createDirWithParents(dirObj) {
  EnigmailLog.DEBUG(`npgp.js: createDirWithParents(${dirObj.path}})\n`);
  if (!dirObj.parent.exists()) {
    createDirWithParents(dirObj.parent);
  }
  dirObj.create(dirObj.DIRECTORY_TYPE, 493);
}

function getErrorMessage(errorCode, errorMessage) {
  let statusFlags = 0;

  switch (errorCode) {
    case "WRONG_PASSWORD":
      errorMessage = EnigmailLocale.getString("badPhrase");
      statusFlags = EnigmailConstants.BAD_PASSPHRASE;
      break;
    case "NEED_PASSWORD": // no passphrase
      errorMessage = EnigmailLocale.getString("missingPassphrase");
      statusFlags = EnigmailConstants.MISSING_PASSPHRASE;
      break;
  }

  return {
    errorMessage: errorMessage,
    statusFlags: statusFlags
  };
}

function getTimeFromDateStr(str) {
  if (!str || str.length === 0) return 0;

  return Math.floor(new Date(str).getTime() / 1000);
}