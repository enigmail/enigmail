#!/usr/bin/env -S node --no-warnings
"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __esm = (fn2, res) => function __init() {
  return fn2 && (res = (0, fn2[__getOwnPropNames(fn2)[0]])(fn2 = 0)), res;
};
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to2, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to2, key) && key !== except)
        __defProp(to2, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to2;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// lib/logData.js
var require_logData = __commonJS({
  "lib/logData.js"(exports2, module2) {
    "use strict";
    var DEBUG_LEVEL = 0;
    var LogData2 = {
      /**
       * Generate an error message object
       * @param {String} code 
       * @param {String} message 
       * @param {Object} data
       */
      genError: function(code, message, data) {
        let r2 = {
          error: {
            code,
            message
          }
        };
        if (data) {
          r2.error.data = data;
        }
        ;
        return r2;
      },
      DEBUG: function(str) {
        if (DEBUG_LEVEL >= 5) {
          console.error(str);
        }
      },
      ERROR: function(code, message) {
        console.error(JSON.stringify(this.genError(code, message)));
      },
      DIE: function(code, message) {
        console.log(JSON.stringify(this.genError(code, message)));
        process.exit(1);
      }
    };
    module2.exports = LogData2;
  }
});

// lib/funcs.js
var require_funcs = __commonJS({
  "lib/funcs.js"(exports2, module2) {
    "use strict";
    var Funcs = {
      /**
       * get a list of plain email addresses without name or surrounding <>
       * @param mailAddrs |string| - address-list encdoded in Unicode as specified in RFC 2822, 3.4
       *                             separated by , or ;
       *
       * @return |string|          - list of pure email addresses separated by ","
       */
      stripEmail: function(mailAddresses) {
        const SIMPLE = "[^<>,]+";
        const COMPLEX = "[^<>,]*<[^<>, ]+>";
        const MatchAddr = new RegExp("^(" + SIMPLE + "|" + COMPLEX + ")(," + SIMPLE + "|," + COMPLEX + ")*$");
        let mailAddrs = mailAddresses;
        try {
          let qStart, qEnd;
          while ((qStart = mailAddrs.indexOf('"')) >= 0) {
            qEnd = mailAddrs.indexOf('"', qStart + 1);
            if (qEnd < 0) {
              throw new Error("Failure");
            }
            mailAddrs = mailAddrs.substring(0, qStart) + mailAddrs.substring(qEnd + 1);
          }
          mailAddrs = mailAddrs.replace(/[,;]+/g, ",").replace(/^,/, "").replace(/,$/, "");
          if (mailAddrs.length === 0) return "";
          if (mailAddrs.search(MatchAddr) < 0) {
            EnigmailLog.ERROR("funcs.jsm: stripEmail: Invalid <..> brackets in mail address: '" + mailAddresses + "'\n");
            throw new Error("Failure");
          }
          let addrList = mailAddrs.split(/,/);
          for (let i2 in addrList) {
            addrList[i2] = addrList[i2].replace(/^([^<>]*<)([^<>]+)(>)$/, "$2").replace(/\s/g, "");
          }
          mailAddrs = addrList.join(",").replace(/,,/g, ",").replace(/^,/, "").replace(/,$/, "");
          return mailAddrs;
        } catch (ex) {
          return "";
        }
      },
      /**
      * Convert an ArrayBuffer (or Uint8Array) object into a string
      */
      arrayBufferToString: function(buffer) {
        const MAXLEN = 102400;
        let uArr = new Uint8Array(buffer);
        let ret = "";
        let len = buffer.byteLength;
        for (let j2 = 0; j2 < Math.floor(len / MAXLEN) + 1; j2++) {
          ret += String.fromCharCode.apply(null, uArr.subarray(j2 * MAXLEN, (j2 + 1) * MAXLEN));
        }
        return ret;
      },
      /**
      * Convert an array containing numbers into a Hexadecimal string
      *
      * @param {Array} arr: input array
      *
      * @return {String} Hex-String
      */
      arrayToHex: function(arr) {
        return arr.reduce((previousValue, i2) => {
          if (typeof previousValue === "number") {
            previousValue = ("0" + previousValue.toString(16)).substr(-2);
          }
          return previousValue + ("0" + i2.toString(16)).substr(-2);
        }).toUpperCase();
      }
    };
    module2.exports = Funcs;
  }
});

// lib/openpgp.min.mjs
var openpgp_min_exports = {};
__export(openpgp_min_exports, {
  AEADEncryptedDataPacket: () => za,
  CleartextMessage: () => Vo,
  CompressedDataPacket: () => xa,
  LiteralDataPacket: () => ba,
  MarkerPacket: () => Ya,
  Message: () => Ho,
  OnePassSignaturePacket: () => Ca,
  PacketList: () => Ua,
  PaddingPacket: () => no,
  PrivateKey: () => Uo,
  PublicKey: () => Do,
  PublicKeyEncryptedSessionKeyPacket: () => Ga,
  PublicKeyPacket: () => qa,
  PublicSubkeyPacket: () => Za,
  SecretKeyPacket: () => $a,
  SecretSubkeyPacket: () => to,
  Signature: () => so,
  SignaturePacket: () => Ba,
  Subkey: () => Io,
  SymEncryptedIntegrityProtectedDataPacket: () => Fa,
  SymEncryptedSessionKeyPacket: () => ja,
  SymmetricallyEncryptedDataPacket: () => Ja,
  TrustPacket: () => ro,
  UnparseablePacket: () => Wt,
  UserAttributePacket: () => Wa,
  UserIDPacket: () => eo,
  armor: () => X,
  config: () => T,
  createCleartextMessage: () => Yo,
  createMessage: () => jo,
  decrypt: () => rc,
  decryptKey: () => Xo,
  decryptSessionKeys: () => oc,
  encrypt: () => tc,
  encryptKey: () => ec,
  encryptSessionKey: () => ac,
  enums: () => R,
  generateKey: () => Zo,
  generateSessionKey: () => sc,
  readCleartextMessage: () => Jo,
  readKey: () => Ro,
  readKeys: () => Lo,
  readMessage: () => _o,
  readPrivateKey: () => To,
  readPrivateKeys: () => Mo,
  readSignature: () => ao,
  reformatKey: () => Wo,
  revokeKey: () => $o,
  sign: () => nc,
  unarmor: () => $,
  verify: () => ic
});
function t(e2, t2) {
  return t2.forEach(function(t3) {
    t3 && "string" != typeof t3 && !Array.isArray(t3) && Object.keys(t3).forEach(function(r2) {
      if ("default" !== r2 && !(r2 in e2)) {
        var n2 = Object.getOwnPropertyDescriptor(t3, r2);
        Object.defineProperty(e2, r2, n2.get ? n2 : { enumerable: true, get: function() {
          return t3[r2];
        } });
      }
    });
  }), Object.freeze(e2);
}
function o(e2) {
  return e2 && e2.getReader && Array.isArray(e2);
}
function c(e2) {
  if (!o(e2)) {
    const t2 = e2.getWriter(), r2 = t2.releaseLock;
    return t2.releaseLock = () => {
      t2.closed.catch(function() {
      }), r2.call(t2);
    }, t2;
  }
  this.stream = e2;
}
function h(t2) {
  if (o(t2)) return "array";
  if (e.ReadableStream && e.ReadableStream.prototype.isPrototypeOf(t2)) return "web";
  if (t2 && !(e.ReadableStream && t2 instanceof e.ReadableStream) && "function" == typeof t2._read && "object" == typeof t2._readableState) throw Error("Native Node streams are no longer supported: please manually convert the stream to a WebStream, using e.g. `stream.Readable.toWeb`");
  return !(!t2 || !t2.getReader) && "web-like";
}
function u(e2) {
  return Uint8Array.prototype.isPrototypeOf(e2);
}
function l(e2) {
  if (1 === e2.length) return e2[0];
  let t2 = 0;
  for (let r3 = 0; r3 < e2.length; r3++) {
    if (!u(e2[r3])) throw Error("concatUint8Array: Data must be in the form of a Uint8Array");
    t2 += e2[r3].length;
  }
  const r2 = new Uint8Array(t2);
  let n2 = 0;
  return e2.forEach(function(e3) {
    r2.set(e3, n2), n2 += e3.length;
  }), r2;
}
function g(e2) {
  if (this.stream = e2, e2[f] && (this[f] = e2[f].slice()), o(e2)) {
    const t3 = e2.getReader();
    return this._read = t3.read.bind(t3), this._releaseLock = () => {
    }, void (this._cancel = () => {
    });
  }
  if (h(e2)) {
    const t3 = e2.getReader();
    return this._read = t3.read.bind(t3), this._releaseLock = () => {
      t3.closed.catch(function() {
      }), t3.releaseLock();
    }, void (this._cancel = t3.cancel.bind(t3));
  }
  let t2 = false;
  this._read = async () => t2 || y.has(e2) ? { value: void 0, done: true } : (t2 = true, { value: e2, done: false }), this._releaseLock = () => {
    if (t2) try {
      y.add(e2);
    } catch (e3) {
    }
  };
}
function p(e2) {
  return h(e2) ? e2 : new ReadableStream({ start(t2) {
    t2.enqueue(e2), t2.close();
  } });
}
function d(e2) {
  if (h(e2)) return e2;
  const t2 = new a();
  return (async () => {
    const r2 = x(t2);
    await r2.write(e2), await r2.close();
  })(), t2;
}
function A(e2) {
  return e2.some((e3) => h(e3) && !o(e3)) ? function(e3) {
    e3 = e3.map(p);
    const t2 = b(async function(e4) {
      await Promise.all(n2.map((t3) => D(t3, e4)));
    });
    let r2 = Promise.resolve();
    const n2 = e3.map((n3, i2) => E(n3, (n4, s2) => (r2 = r2.then(() => w(n4, t2.writable, { preventClose: i2 !== e3.length - 1 })), r2)));
    return t2.readable;
  }(e2) : e2.some((e3) => o(e3)) ? function(e3) {
    const t2 = new a();
    let r2 = Promise.resolve();
    return e3.forEach((n2, i2) => (r2 = r2.then(() => w(n2, t2, { preventClose: i2 !== e3.length - 1 })), r2)), t2;
  }(e2) : "string" == typeof e2[0] ? e2.join("") : l(e2);
}
async function w(e2, t2, { preventClose: r2 = false, preventAbort: n2 = false, preventCancel: i2 = false } = {}) {
  if (h(e2) && !o(e2)) {
    e2 = p(e2);
    try {
      if (e2[f]) {
        const r3 = x(t2);
        for (let t3 = 0; t3 < e2[f].length; t3++) await r3.ready, await r3.write(e2[f][t3]);
        r3.releaseLock();
      }
      await e2.pipeTo(t2, { preventClose: r2, preventAbort: n2, preventCancel: i2 });
    } catch (e3) {
    }
    return;
  }
  const s2 = P(e2 = d(e2)), a2 = x(t2);
  try {
    for (; ; ) {
      await a2.ready;
      const { done: e3, value: t3 } = await s2.read();
      if (e3) {
        r2 || await a2.close();
        break;
      }
      await a2.write(t3);
    }
  } catch (e3) {
    n2 || await a2.abort(e3);
  } finally {
    s2.releaseLock(), a2.releaseLock();
  }
}
function m(e2, t2) {
  const r2 = new TransformStream(t2);
  return w(e2, r2.writable), r2.readable;
}
function b(e2) {
  let t2, r2, n2, i2 = false, s2 = false;
  return { readable: new ReadableStream({ start(e3) {
    n2 = e3;
  }, pull() {
    t2 ? t2() : i2 = true;
  }, async cancel(t3) {
    s2 = true, e2 && await e2(t3), r2 && r2(t3);
  } }, { highWaterMark: 0 }), writable: new WritableStream({ write: async function(e3) {
    if (s2) throw Error("Stream is cancelled");
    n2.enqueue(e3), i2 ? i2 = false : (await new Promise((e4, n3) => {
      t2 = e4, r2 = n3;
    }), t2 = null, r2 = null);
  }, close: n2.close.bind(n2), abort: n2.error.bind(n2) }) };
}
function k(e2, t2 = () => {
}, r2 = () => {
}) {
  if (o(e2)) {
    const n3 = new a();
    return (async () => {
      const i3 = x(n3);
      try {
        const n4 = await C(e2), s2 = t2(n4), a2 = r2();
        let o2;
        o2 = void 0 !== s2 && void 0 !== a2 ? A([s2, a2]) : void 0 !== s2 ? s2 : a2, await i3.write(o2), await i3.close();
      } catch (e3) {
        await i3.abort(e3);
      }
    })(), n3;
  }
  if (h(e2)) return m(e2, { async transform(e3, r3) {
    try {
      const n3 = await t2(e3);
      void 0 !== n3 && r3.enqueue(n3);
    } catch (e4) {
      r3.error(e4);
    }
  }, async flush(e3) {
    try {
      const t3 = await r2();
      void 0 !== t3 && e3.enqueue(t3);
    } catch (t3) {
      e3.error(t3);
    }
  } });
  const n2 = t2(e2), i2 = r2();
  return void 0 !== n2 && void 0 !== i2 ? A([n2, i2]) : void 0 !== n2 ? n2 : i2;
}
function E(e2, t2) {
  if (h(e2) && !o(e2)) {
    let r3;
    const n2 = new TransformStream({ start(e3) {
      r3 = e3;
    } }), i2 = w(e2, n2.writable), s2 = b(async function(e3) {
      r3.error(e3), await i2, await new Promise(setTimeout);
    });
    return t2(n2.readable, s2.writable), s2.readable;
  }
  e2 = d(e2);
  const r2 = new a();
  return t2(e2, r2), r2;
}
function v(e2, t2) {
  let r2;
  const n2 = E(e2, (e3, i2) => {
    const s2 = P(e3);
    s2.remainder = () => (s2.releaseLock(), w(e3, i2), n2), r2 = t2(s2);
  });
  return r2;
}
function I(e2) {
  if (o(e2)) return e2.clone();
  if (h(e2)) {
    const t2 = function(e3) {
      if (o(e3)) throw Error("ArrayStream cannot be tee()d, use clone() instead");
      if (h(e3)) {
        const t3 = p(e3).tee();
        return t3[0][f] = t3[1][f] = e3[f], t3;
      }
      return [K(e3), K(e3)];
    }(e2);
    return S(e2, t2[0]), t2[1];
  }
  return K(e2);
}
function B(e2) {
  return o(e2) ? I(e2) : h(e2) ? new ReadableStream({ start(t2) {
    const r2 = E(e2, async (e3, r3) => {
      const n2 = P(e3), i2 = x(r3);
      try {
        for (; ; ) {
          await i2.ready;
          const { done: e4, value: r4 } = await n2.read();
          if (e4) {
            try {
              t2.close();
            } catch (e5) {
            }
            return void await i2.close();
          }
          try {
            t2.enqueue(r4);
          } catch (e5) {
          }
          await i2.write(r4);
        }
      } catch (e4) {
        t2.error(e4), await i2.abort(e4);
      }
    });
    S(e2, r2);
  } }) : K(e2);
}
function S(e2, t2) {
  Object.entries(Object.getOwnPropertyDescriptors(e2.constructor.prototype)).forEach(([r2, n2]) => {
    "constructor" !== r2 && (n2.value ? n2.value = n2.value.bind(t2) : n2.get = n2.get.bind(t2), Object.defineProperty(e2, r2, n2));
  });
}
function K(e2, t2 = 0, r2 = 1 / 0) {
  if (o(e2)) throw Error("Not implemented");
  if (h(e2)) {
    if (t2 >= 0 && r2 >= 0) {
      let n2 = 0;
      return m(e2, { transform(e3, i2) {
        n2 < r2 ? (n2 + e3.length >= t2 && i2.enqueue(K(e3, Math.max(t2 - n2, 0), r2 - n2)), n2 += e3.length) : i2.terminate();
      } });
    }
    if (t2 < 0 && (r2 < 0 || r2 === 1 / 0)) {
      let n2 = [];
      return k(e2, (e3) => {
        e3.length >= -t2 ? n2 = [e3] : n2.push(e3);
      }, () => K(A(n2), t2, r2));
    }
    if (0 === t2 && r2 < 0) {
      let n2;
      return k(e2, (e3) => {
        const i2 = n2 ? A([n2, e3]) : e3;
        if (i2.length >= -r2) return n2 = K(i2, r2), K(i2, t2, r2);
        n2 = i2;
      });
    }
    return console.warn(`stream.slice(input, ${t2}, ${r2}) not implemented efficiently.`), U(async () => K(await C(e2), t2, r2));
  }
  return e2[f] && (e2 = A(e2[f].concat([e2]))), u(e2) ? e2.subarray(t2, r2 === 1 / 0 ? e2.length : r2) : e2.slice(t2, r2);
}
async function C(e2, t2 = A) {
  return o(e2) ? e2.readToEnd(t2) : h(e2) ? P(e2).readToEnd(t2) : e2;
}
async function D(e2, t2) {
  if (h(e2)) {
    if (e2.cancel) {
      const r2 = await e2.cancel(t2);
      return await new Promise(setTimeout), r2;
    }
    if (e2.destroy) return e2.destroy(t2), await new Promise(setTimeout), t2;
  }
}
function U(e2) {
  const t2 = new a();
  return (async () => {
    const r2 = x(t2);
    try {
      await r2.write(await e2()), await r2.close();
    } catch (e3) {
      await r2.abort(e3);
    }
  })(), t2;
}
function P(e2) {
  return new g(e2);
}
function x(e2) {
  return new c(e2);
}
function H(e2) {
  let t2 = new Uint8Array();
  return k(e2, (e3) => {
    t2 = M.concatUint8Array([t2, e3]);
    const r2 = [], n2 = Math.floor(t2.length / 45), i2 = 45 * n2, s2 = F(t2.subarray(0, i2));
    for (let e4 = 0; e4 < n2; e4++) r2.push(s2.substr(60 * e4, 60)), r2.push("\n");
    return t2 = t2.subarray(i2), r2.join("");
  }, () => t2.length ? F(t2) + "\n" : "");
}
function z(e2) {
  let t2 = "";
  return k(e2, (e3) => {
    t2 += e3;
    let r2 = 0;
    const n2 = [" ", "	", "\r", "\n"];
    for (let e4 = 0; e4 < n2.length; e4++) {
      const i3 = n2[e4];
      for (let e5 = t2.indexOf(i3); -1 !== e5; e5 = t2.indexOf(i3, e5 + 1)) r2++;
    }
    let i2 = t2.length;
    for (; i2 > 0 && (i2 - r2) % 4 != 0; i2--) n2.includes(t2[i2]) && r2--;
    const s2 = O(t2.substr(0, i2));
    return t2 = t2.substr(i2), s2;
  }, () => O(t2));
}
function G(e2) {
  return z(e2.replace(/-/g, "+").replace(/_/g, "/"));
}
function _(e2, t2) {
  let r2 = H(e2).replace(/[\r\n]/g, "");
  return r2 = r2.replace(/[+]/g, "-").replace(/[/]/g, "_").replace(/[=]/g, ""), r2;
}
function j(e2) {
  const t2 = e2.match(/^-----BEGIN PGP (MESSAGE, PART \d+\/\d+|MESSAGE, PART \d+|SIGNED MESSAGE|MESSAGE|PUBLIC KEY BLOCK|PRIVATE KEY BLOCK|SIGNATURE)-----$/m);
  if (!t2) throw Error("Unknown ASCII armor type");
  return /MESSAGE, PART \d+\/\d+/.test(t2[1]) ? R.armor.multipartSection : /MESSAGE, PART \d+/.test(t2[1]) ? R.armor.multipartLast : /SIGNED MESSAGE/.test(t2[1]) ? R.armor.signed : /MESSAGE/.test(t2[1]) ? R.armor.message : /PUBLIC KEY BLOCK/.test(t2[1]) ? R.armor.publicKey : /PRIVATE KEY BLOCK/.test(t2[1]) ? R.armor.privateKey : /SIGNATURE/.test(t2[1]) ? R.armor.signature : void 0;
}
function q(e2, t2) {
  let r2 = "";
  return t2.showVersion && (r2 += "Version: " + t2.versionString + "\n"), t2.showComment && (r2 += "Comment: " + t2.commentString + "\n"), e2 && (r2 += "Comment: " + e2 + "\n"), r2 += "\n", r2;
}
function V(e2) {
  const t2 = function(e3) {
    let t3 = 13501623;
    return k(e3, (e4) => {
      const r2 = Y ? Math.floor(e4.length / 4) : 0, n2 = new Uint32Array(e4.buffer, e4.byteOffset, r2);
      for (let e5 = 0; e5 < r2; e5++) t3 ^= n2[e5], t3 = J[0][t3 >> 24 & 255] ^ J[1][t3 >> 16 & 255] ^ J[2][t3 >> 8 & 255] ^ J[3][255 & t3];
      for (let n3 = 4 * r2; n3 < e4.length; n3++) t3 = t3 >> 8 ^ J[0][255 & t3 ^ e4[n3]];
    }, () => new Uint8Array([t3, t3 >> 8, t3 >> 16]));
  }(e2);
  return H(t2);
}
function Z(e2) {
  for (let t2 = 0; t2 < e2.length; t2++) /^([^\s:]|[^\s:][^:]*[^\s:]): .+$/.test(e2[t2]) || M.printDebugError(Error("Improperly formatted armor header: " + e2[t2])), /^(Version|Comment|MessageID|Hash|Charset): .+$/.test(e2[t2]) || M.printDebugError(Error("Unknown header: " + e2[t2]));
}
function W(e2) {
  let t2 = e2;
  const r2 = e2.lastIndexOf("=");
  return r2 >= 0 && r2 !== e2.length - 1 && (t2 = e2.slice(0, r2)), t2;
}
function $(e2) {
  return new Promise(async (t2, r2) => {
    try {
      const n2 = /^-----[^-]+-----$/m, i2 = /^[ \f\r\t\u00a0\u2000-\u200a\u202f\u205f\u3000]*$/;
      let s2;
      const a2 = [];
      let o2, c2, h2 = a2, u2 = [];
      const l2 = z(E(e2, async (e3, y2) => {
        const f2 = P(e3);
        try {
          for (; ; ) {
            let e4 = await f2.readLine();
            if (void 0 === e4) throw Error("Misformed armored text");
            if (e4 = M.removeTrailingSpaces(e4.replace(/[\r\n]/g, "")), s2) if (o2) c2 || s2 !== R.armor.signed || (n2.test(e4) ? (u2 = u2.join("\r\n"), c2 = true, Z(h2), h2 = [], o2 = false) : u2.push(e4.replace(/^- /, "")));
            else if (n2.test(e4) && r2(Error("Mandatory blank line missing between armor headers and armor data")), i2.test(e4)) {
              if (Z(h2), o2 = true, c2 || s2 !== R.armor.signed) {
                t2({ text: u2, data: l2, headers: a2, type: s2 });
                break;
              }
            } else h2.push(e4);
            else n2.test(e4) && (s2 = j(e4));
          }
        } catch (e4) {
          return void r2(e4);
        }
        const g2 = x(y2);
        try {
          for (; ; ) {
            await g2.ready;
            const { done: e4, value: t3 } = await f2.read();
            if (e4) throw Error("Misformed armored text");
            const r3 = t3 + "";
            if (-1 !== r3.indexOf("=") || -1 !== r3.indexOf("-")) {
              let e5 = await f2.readToEnd();
              e5.length || (e5 = ""), e5 = r3 + e5, e5 = M.removeTrailingSpaces(e5.replace(/\r/g, ""));
              const t4 = e5.split(n2);
              if (1 === t4.length) throw Error("Misformed armored text");
              const i3 = W(t4[0].slice(0, -1));
              await g2.write(i3);
              break;
            }
            await g2.write(r3);
          }
          await g2.ready, await g2.close();
        } catch (e4) {
          await g2.abort(e4);
        }
      }));
    } catch (e3) {
      r2(e3);
    }
  }).then(async (e3) => (o(e3.data) && (e3.data = await C(e3.data)), e3));
}
function X(e2, t2, r2, n2, i2, s2 = false, a2 = T) {
  let o2, c2;
  e2 === R.armor.signed && (o2 = t2.text, c2 = t2.hash, t2 = t2.data);
  const h2 = s2 && B(t2), u2 = [];
  switch (e2) {
    case R.armor.multipartSection:
      u2.push("-----BEGIN PGP MESSAGE, PART " + r2 + "/" + n2 + "-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP MESSAGE, PART " + r2 + "/" + n2 + "-----\n");
      break;
    case R.armor.multipartLast:
      u2.push("-----BEGIN PGP MESSAGE, PART " + r2 + "-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP MESSAGE, PART " + r2 + "-----\n");
      break;
    case R.armor.signed:
      u2.push("-----BEGIN PGP SIGNED MESSAGE-----\n"), u2.push(c2 ? `Hash: ${c2}

` : "\n"), u2.push(o2.replace(/^-/gm, "- -")), u2.push("\n-----BEGIN PGP SIGNATURE-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP SIGNATURE-----\n");
      break;
    case R.armor.message:
      u2.push("-----BEGIN PGP MESSAGE-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP MESSAGE-----\n");
      break;
    case R.armor.publicKey:
      u2.push("-----BEGIN PGP PUBLIC KEY BLOCK-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP PUBLIC KEY BLOCK-----\n");
      break;
    case R.armor.privateKey:
      u2.push("-----BEGIN PGP PRIVATE KEY BLOCK-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP PRIVATE KEY BLOCK-----\n");
      break;
    case R.armor.signature:
      u2.push("-----BEGIN PGP SIGNATURE-----\n"), u2.push(q(i2, a2)), u2.push(H(t2)), h2 && u2.push("=", V(h2)), u2.push("-----END PGP SIGNATURE-----\n");
  }
  return M.concat(u2);
}
function re(e2) {
  const t2 = "0123456789ABCDEF";
  let r2 = "";
  return e2.forEach((e3) => {
    r2 += t2[e3 >> 4] + t2[15 & e3];
  }), BigInt("0x0" + r2);
}
function ne(e2, t2) {
  const r2 = e2 % t2;
  return r2 < ee ? r2 + t2 : r2;
}
function ie(e2, t2, r2) {
  if (r2 === ee) throw Error("Modulo cannot be zero");
  if (r2 === te) return BigInt(0);
  if (t2 < ee) throw Error("Unsopported negative exponent");
  let n2 = t2, i2 = e2;
  i2 %= r2;
  let s2 = BigInt(1);
  for (; n2 > ee; ) {
    const e3 = n2 & te;
    n2 >>= te;
    s2 = e3 ? s2 * i2 % r2 : s2, i2 = i2 * i2 % r2;
  }
  return s2;
}
function se(e2) {
  return e2 >= ee ? e2 : -e2;
}
function ae(e2, t2) {
  const { gcd: r2, x: n2 } = function(e3, t3) {
    let r3 = BigInt(0), n3 = BigInt(1), i2 = BigInt(1), s2 = BigInt(0), a2 = se(e3), o2 = se(t3);
    const c2 = e3 < ee, h2 = t3 < ee;
    for (; o2 !== ee; ) {
      const e4 = a2 / o2;
      let t4 = r3;
      r3 = i2 - e4 * r3, i2 = t4, t4 = n3, n3 = s2 - e4 * n3, s2 = t4, t4 = o2, o2 = a2 % o2, a2 = t4;
    }
    return { x: c2 ? -i2 : i2, y: h2 ? -s2 : s2, gcd: a2 };
  }(e2, t2);
  if (r2 !== te) throw Error("Inverse does not exist");
  return ne(n2 + t2, t2);
}
function oe(e2) {
  const t2 = Number(e2);
  if (t2 > Number.MAX_SAFE_INTEGER) throw Error("Number can only safely store up to 53 bits");
  return t2;
}
function ce(e2, t2) {
  return (e2 >> BigInt(t2) & te) === ee ? 0 : 1;
}
function he(e2) {
  const t2 = e2 < ee ? BigInt(-1) : ee;
  let r2 = 1, n2 = e2;
  for (; (n2 >>= te) !== t2; ) r2++;
  return r2;
}
function ue(e2) {
  const t2 = e2 < ee ? BigInt(-1) : ee, r2 = BigInt(8);
  let n2 = 1, i2 = e2;
  for (; (i2 >>= r2) !== t2; ) n2++;
  return n2;
}
function le(e2, t2 = "be", r2) {
  let n2 = e2.toString(16);
  n2.length % 2 == 1 && (n2 = "0" + n2);
  const i2 = n2.length / 2, s2 = new Uint8Array(r2 || i2), a2 = r2 ? r2 - i2 : 0;
  let o2 = 0;
  for (; o2 < i2; ) s2[o2 + a2] = parseInt(n2.slice(2 * o2, 2 * o2 + 2), 16), o2++;
  return "be" !== t2 && s2.reverse(), s2;
}
function fe(e2) {
  const t2 = "undefined" != typeof crypto ? crypto : ye?.webcrypto;
  if (t2?.getRandomValues) {
    const r2 = new Uint8Array(e2);
    return t2.getRandomValues(r2);
  }
  throw Error("No secure random number generator available.");
}
function ge(e2, t2) {
  if (t2 < e2) throw Error("Illegal parameter value: max <= min");
  const r2 = t2 - e2;
  return ne(re(fe(ue(r2) + 8)), r2) + e2;
}
function de(e2, t2, r2) {
  const n2 = BigInt(30), i2 = pe << BigInt(e2 - 1), s2 = [1, 6, 5, 4, 3, 2, 1, 4, 3, 2, 1, 2, 1, 4, 3, 2, 1, 2, 1, 4, 3, 2, 1, 6, 5, 4, 3, 2, 1, 2];
  let a2 = ge(i2, i2 << pe), o2 = oe(ne(a2, n2));
  do {
    a2 += BigInt(s2[o2]), o2 = (o2 + s2[o2]) % s2.length, he(a2) > e2 && (a2 = ne(a2, i2 << pe), a2 += i2, o2 = oe(ne(a2, n2)));
  } while (!Ae(a2, t2, r2));
  return a2;
}
function Ae(e2, t2, r2) {
  return (!t2 || function(e3, t3) {
    let r3 = e3, n2 = t3;
    for (; n2 !== ee; ) {
      const e4 = n2;
      n2 = r3 % n2, r3 = e4;
    }
    return r3;
  }(e2 - pe, t2) === pe) && (!!function(e3) {
    const t3 = BigInt(0);
    return we.every((r3) => ne(e3, r3) !== t3);
  }(e2) && (!!function(e3, t3 = BigInt(2)) {
    return ie(t3, e3 - pe, e3) === pe;
  }(e2) && !!function(e3, t3) {
    const r3 = he(e3);
    t3 || (t3 = Math.max(1, r3 / 48 | 0));
    const n2 = e3 - pe;
    let i2 = 0;
    for (; !ce(n2, i2); ) i2++;
    const s2 = e3 >> BigInt(i2);
    for (; t3 > 0; t3--) {
      let t4, r4 = ie(ge(BigInt(2), n2), s2, e3);
      if (r4 !== pe && r4 !== n2) {
        for (t4 = 1; t4 < i2; t4++) {
          if (r4 = ne(r4 * r4, e3), r4 === pe) return false;
          if (r4 === n2) break;
        }
        if (t4 === i2) return false;
      }
    }
    return true;
  }(e2, r2)));
}
function Ee(e2) {
  if (be && ke.includes(e2)) return async function(t2) {
    const r2 = be.createHash(e2);
    return k(t2, (e3) => {
      r2.update(e3);
    }, () => new Uint8Array(r2.digest()));
  };
}
function ve(e2, t2) {
  const r2 = async () => {
    const { nobleHashes: t3 } = await Promise.resolve().then(function() {
      return ty;
    }), r3 = t3.get(e2);
    if (!r3) throw Error("Unsupported hash");
    return r3;
  };
  return async function(e3) {
    if (o(e3) && (e3 = await C(e3)), M.isStream(e3)) {
      const t3 = (await r2()).create();
      return k(e3, (e4) => {
        t3.update(e4);
      }, () => t3.digest());
    }
    if (me && t2) return new Uint8Array(await me.digest(t2, e3));
    return (await r2())(e3);
  };
}
function Qe(e2, t2) {
  switch (e2) {
    case R.hash.md5:
      return Ie(t2);
    case R.hash.sha1:
      return Be(t2);
    case R.hash.ripemd:
      return Ue(t2);
    case R.hash.sha256:
      return Ke(t2);
    case R.hash.sha384:
      return Ce(t2);
    case R.hash.sha512:
      return De(t2);
    case R.hash.sha224:
      return Se(t2);
    case R.hash.sha3_256:
      return Pe(t2);
    case R.hash.sha3_512:
      return xe(t2);
    default:
      throw Error("Unsupported hash function");
  }
}
function Re(e2) {
  switch (e2) {
    case R.hash.md5:
      return 16;
    case R.hash.sha1:
    case R.hash.ripemd:
      return 20;
    case R.hash.sha256:
      return 32;
    case R.hash.sha384:
      return 48;
    case R.hash.sha512:
      return 64;
    case R.hash.sha224:
      return 28;
    case R.hash.sha3_256:
      return 32;
    case R.hash.sha3_512:
      return 64;
    default:
      throw Error("Invalid hash algorithm.");
  }
}
function Le(e2, t2) {
  const r2 = e2.length;
  if (r2 > t2 - 11) throw Error("Message too long");
  const n2 = function(e3) {
    const t3 = new Uint8Array(e3);
    let r3 = 0;
    for (; r3 < e3; ) {
      const n3 = fe(e3 - r3);
      for (let e4 = 0; e4 < n3.length; e4++) 0 !== n3[e4] && (t3[r3++] = n3[e4]);
    }
    return t3;
  }(t2 - r2 - 3), i2 = new Uint8Array(t2);
  return i2[1] = 2, i2.set(n2, 2), i2.set(e2, t2 - r2), i2;
}
function Me(e2, t2) {
  let r2 = 2, n2 = 1;
  for (let t3 = r2; t3 < e2.length; t3++) n2 &= 0 !== e2[t3], r2 += n2;
  const i2 = r2 - 2, s2 = e2.subarray(r2 + 1), a2 = 0 === e2[0] & 2 === e2[1] & i2 >= 8 & !n2;
  if (t2) return M.selectUint8Array(a2, s2, t2);
  if (a2) return s2;
  throw Error("Decryption error");
}
function Ne(e2, t2, r2) {
  let n2;
  if (t2.length !== Re(e2)) throw Error("Invalid hash length");
  const i2 = new Uint8Array(Te[e2].length);
  for (n2 = 0; n2 < Te[e2].length; n2++) i2[n2] = Te[e2][n2];
  const s2 = i2.length + t2.length;
  if (r2 < s2 + 11) throw Error("Intended encoded message length too short");
  const a2 = new Uint8Array(r2 - s2 - 3).fill(255), o2 = new Uint8Array(r2);
  return o2[1] = 1, o2.set(a2, 2), o2.set(i2, r2 - s2), o2.set(t2, r2 - t2.length), o2;
}
async function ze(e2, t2, r2, n2, i2, s2, a2, o2, c2) {
  if (Re(e2) >= r2.length) throw Error("Digest size cannot exceed key modulus size");
  if (t2 && !M.isStream(t2)) {
    if (M.getWebCrypto()) try {
      return await async function(e3, t3, r3, n3, i3, s3, a3, o3) {
        const c3 = await qe(r3, n3, i3, s3, a3, o3), h2 = { name: "RSASSA-PKCS1-v1_5", hash: { name: e3 } }, u2 = await Fe.importKey("jwk", c3, h2, false, ["sign"]);
        return new Uint8Array(await Fe.sign("RSASSA-PKCS1-v1_5", u2, t3));
      }(R.read(R.webHash, e2), t2, r2, n2, i2, s2, a2, o2);
    } catch (e3) {
      M.printDebugError(e3);
    }
    else if (M.getNodeCrypto()) return async function(e3, t3, r3, n3, i3, s3, a3, o3) {
      const c3 = Oe.createSign(R.read(R.hash, e3));
      c3.write(t3), c3.end();
      const h2 = await qe(r3, n3, i3, s3, a3, o3);
      return new Uint8Array(c3.sign({ key: h2, format: "jwk", type: "pkcs1" }));
    }(e2, t2, r2, n2, i2, s2, a2, o2);
  }
  return async function(e3, t3, r3, n3) {
    t3 = re(t3);
    const i3 = re(Ne(e3, n3, ue(t3)));
    return r3 = re(r3), le(ie(i3, r3, t3), "be", ue(t3));
  }(e2, r2, i2, c2);
}
async function Ge(e2, t2, r2, n2, i2, s2) {
  if (t2 && !M.isStream(t2)) {
    if (M.getWebCrypto()) try {
      return await async function(e3, t3, r3, n3, i3) {
        const s3 = Ve(n3, i3), a2 = await Fe.importKey("jwk", s3, { name: "RSASSA-PKCS1-v1_5", hash: { name: e3 } }, false, ["verify"]);
        return Fe.verify("RSASSA-PKCS1-v1_5", a2, r3, t3);
      }(R.read(R.webHash, e2), t2, r2, n2, i2);
    } catch (e3) {
      M.printDebugError(e3);
    }
    else if (M.getNodeCrypto()) return async function(e3, t3, r3, n3, i3) {
      const s3 = Ve(n3, i3), a2 = { key: s3, format: "jwk", type: "pkcs1" }, o2 = Oe.createVerify(R.read(R.hash, e3));
      o2.write(t3), o2.end();
      try {
        return o2.verify(a2, r3);
      } catch (e4) {
        return false;
      }
    }(e2, t2, r2, n2, i2);
  }
  return async function(e3, t3, r3, n3, i3) {
    if (r3 = re(r3), t3 = re(t3), n3 = re(n3), t3 >= r3) throw Error("Signature size cannot exceed modulus size");
    const s3 = le(ie(t3, n3, r3), "be", ue(r3)), a2 = Ne(e3, i3, ue(r3));
    return M.equalsUint8Array(s3, a2);
  }(e2, r2, n2, i2, s2);
}
async function _e(e2, t2, r2) {
  return M.getNodeCrypto() ? async function(e3, t3, r3) {
    const n2 = Ve(t3, r3), i2 = { key: n2, format: "jwk", type: "pkcs1", padding: Oe.constants.RSA_PKCS1_PADDING };
    return new Uint8Array(Oe.publicEncrypt(i2, e3));
  }(e2, t2, r2) : async function(e3, t3, r3) {
    if (t3 = re(t3), e3 = re(Le(e3, ue(t3))), r3 = re(r3), e3 >= t3) throw Error("Message size cannot exceed modulus size");
    return le(ie(e3, r3, t3), "be", ue(t3));
  }(e2, t2, r2);
}
async function je(e2, t2, r2, n2, i2, s2, a2, o2) {
  if (M.getNodeCrypto() && !o2) try {
    return await async function(e3, t3, r3, n3, i3, s3, a3) {
      const o3 = await qe(t3, r3, n3, i3, s3, a3), c2 = { key: o3, format: "jwk", type: "pkcs1", padding: Oe.constants.RSA_PKCS1_PADDING };
      try {
        return new Uint8Array(Oe.privateDecrypt(c2, e3));
      } catch (e4) {
        throw Error("Decryption error");
      }
    }(e2, t2, r2, n2, i2, s2, a2);
  } catch (e3) {
    M.printDebugError(e3);
  }
  return async function(e3, t3, r3, n3, i3, s3, a3, o3) {
    if (e3 = re(e3), t3 = re(t3), r3 = re(r3), n3 = re(n3), i3 = re(i3), s3 = re(s3), a3 = re(a3), e3 >= t3) throw Error("Data too large.");
    const c2 = ne(n3, s3 - He), h2 = ne(n3, i3 - He), u2 = ge(BigInt(2), t3), l2 = ie(ae(u2, t3), r3, t3);
    e3 = ne(e3 * l2, t3);
    const y2 = ie(e3, h2, i3), f2 = ie(e3, c2, s3), g2 = ne(a3 * (f2 - y2), s3);
    let p2 = g2 * i3 + y2;
    return p2 = ne(p2 * u2, t3), Me(le(p2, "be", ue(t3)), o3);
  }(e2, t2, r2, n2, i2, s2, a2, o2);
}
async function qe(e2, t2, r2, n2, i2, s2) {
  const a2 = re(n2), o2 = re(i2), c2 = re(r2);
  let h2 = ne(c2, o2 - He), u2 = ne(c2, a2 - He);
  return u2 = le(u2), h2 = le(h2), { kty: "RSA", n: _(e2), e: _(t2), d: _(r2), p: _(i2), q: _(n2), dp: _(h2), dq: _(u2), qi: _(s2), ext: true };
}
function Ve(e2, t2) {
  return { kty: "RSA", n: _(e2), e: _(t2), ext: true };
}
function Je(e2, t2) {
  return { n: G(e2.n), e: le(t2), d: G(e2.d), p: G(e2.q), q: G(e2.p), u: G(e2.qi) };
}
function ht(e2, t2, r2, n2) {
  e2[t2] = r2 >> 24 & 255, e2[t2 + 1] = r2 >> 16 & 255, e2[t2 + 2] = r2 >> 8 & 255, e2[t2 + 3] = 255 & r2, e2[t2 + 4] = n2 >> 24 & 255, e2[t2 + 5] = n2 >> 16 & 255, e2[t2 + 6] = n2 >> 8 & 255, e2[t2 + 7] = 255 & n2;
}
function ut(e2, t2, r2, n2) {
  return function(e3, t3, r3, n3, i2) {
    var s2, a2 = 0;
    for (s2 = 0; s2 < i2; s2++) a2 |= e3[t3 + s2] ^ r3[n3 + s2];
    return (1 & a2 - 1 >>> 8) - 1;
  }(e2, t2, r2, n2, 32);
}
function lt(e2, t2) {
  var r2;
  for (r2 = 0; r2 < 16; r2++) e2[r2] = 0 | t2[r2];
}
function yt(e2) {
  var t2, r2, n2 = 1;
  for (t2 = 0; t2 < 16; t2++) r2 = e2[t2] + n2 + 65535, n2 = Math.floor(r2 / 65536), e2[t2] = r2 - 65536 * n2;
  e2[0] += n2 - 1 + 37 * (n2 - 1);
}
function ft(e2, t2, r2) {
  for (var n2, i2 = ~(r2 - 1), s2 = 0; s2 < 16; s2++) n2 = i2 & (e2[s2] ^ t2[s2]), e2[s2] ^= n2, t2[s2] ^= n2;
}
function gt(e2, t2) {
  var r2, n2, i2, s2 = $e(), a2 = $e();
  for (r2 = 0; r2 < 16; r2++) a2[r2] = t2[r2];
  for (yt(a2), yt(a2), yt(a2), n2 = 0; n2 < 2; n2++) {
    for (s2[0] = a2[0] - 65517, r2 = 1; r2 < 15; r2++) s2[r2] = a2[r2] - 65535 - (s2[r2 - 1] >> 16 & 1), s2[r2 - 1] &= 65535;
    s2[15] = a2[15] - 32767 - (s2[14] >> 16 & 1), i2 = s2[15] >> 16 & 1, s2[14] &= 65535, ft(a2, s2, 1 - i2);
  }
  for (r2 = 0; r2 < 16; r2++) e2[2 * r2] = 255 & a2[r2], e2[2 * r2 + 1] = a2[r2] >> 8;
}
function pt(e2, t2) {
  var r2 = new Uint8Array(32), n2 = new Uint8Array(32);
  return gt(r2, e2), gt(n2, t2), ut(r2, 0, n2, 0);
}
function dt(e2) {
  var t2 = new Uint8Array(32);
  return gt(t2, e2), 1 & t2[0];
}
function At(e2, t2) {
  var r2;
  for (r2 = 0; r2 < 16; r2++) e2[r2] = t2[2 * r2] + (t2[2 * r2 + 1] << 8);
  e2[15] &= 32767;
}
function wt(e2, t2, r2) {
  for (var n2 = 0; n2 < 16; n2++) e2[n2] = t2[n2] + r2[n2];
}
function mt(e2, t2, r2) {
  for (var n2 = 0; n2 < 16; n2++) e2[n2] = t2[n2] - r2[n2];
}
function bt(e2, t2, r2) {
  var n2, i2, s2 = 0, a2 = 0, o2 = 0, c2 = 0, h2 = 0, u2 = 0, l2 = 0, y2 = 0, f2 = 0, g2 = 0, p2 = 0, d2 = 0, A2 = 0, w2 = 0, m2 = 0, b2 = 0, k2 = 0, E2 = 0, v2 = 0, I2 = 0, B2 = 0, S2 = 0, K2 = 0, C2 = 0, D2 = 0, U2 = 0, P2 = 0, x2 = 0, Q2 = 0, R2 = 0, T2 = 0, L2 = r2[0], M2 = r2[1], N2 = r2[2], F2 = r2[3], O2 = r2[4], H2 = r2[5], z2 = r2[6], G2 = r2[7], _2 = r2[8], j2 = r2[9], q2 = r2[10], V2 = r2[11], J2 = r2[12], Y2 = r2[13], Z2 = r2[14], W2 = r2[15];
  s2 += (n2 = t2[0]) * L2, a2 += n2 * M2, o2 += n2 * N2, c2 += n2 * F2, h2 += n2 * O2, u2 += n2 * H2, l2 += n2 * z2, y2 += n2 * G2, f2 += n2 * _2, g2 += n2 * j2, p2 += n2 * q2, d2 += n2 * V2, A2 += n2 * J2, w2 += n2 * Y2, m2 += n2 * Z2, b2 += n2 * W2, a2 += (n2 = t2[1]) * L2, o2 += n2 * M2, c2 += n2 * N2, h2 += n2 * F2, u2 += n2 * O2, l2 += n2 * H2, y2 += n2 * z2, f2 += n2 * G2, g2 += n2 * _2, p2 += n2 * j2, d2 += n2 * q2, A2 += n2 * V2, w2 += n2 * J2, m2 += n2 * Y2, b2 += n2 * Z2, k2 += n2 * W2, o2 += (n2 = t2[2]) * L2, c2 += n2 * M2, h2 += n2 * N2, u2 += n2 * F2, l2 += n2 * O2, y2 += n2 * H2, f2 += n2 * z2, g2 += n2 * G2, p2 += n2 * _2, d2 += n2 * j2, A2 += n2 * q2, w2 += n2 * V2, m2 += n2 * J2, b2 += n2 * Y2, k2 += n2 * Z2, E2 += n2 * W2, c2 += (n2 = t2[3]) * L2, h2 += n2 * M2, u2 += n2 * N2, l2 += n2 * F2, y2 += n2 * O2, f2 += n2 * H2, g2 += n2 * z2, p2 += n2 * G2, d2 += n2 * _2, A2 += n2 * j2, w2 += n2 * q2, m2 += n2 * V2, b2 += n2 * J2, k2 += n2 * Y2, E2 += n2 * Z2, v2 += n2 * W2, h2 += (n2 = t2[4]) * L2, u2 += n2 * M2, l2 += n2 * N2, y2 += n2 * F2, f2 += n2 * O2, g2 += n2 * H2, p2 += n2 * z2, d2 += n2 * G2, A2 += n2 * _2, w2 += n2 * j2, m2 += n2 * q2, b2 += n2 * V2, k2 += n2 * J2, E2 += n2 * Y2, v2 += n2 * Z2, I2 += n2 * W2, u2 += (n2 = t2[5]) * L2, l2 += n2 * M2, y2 += n2 * N2, f2 += n2 * F2, g2 += n2 * O2, p2 += n2 * H2, d2 += n2 * z2, A2 += n2 * G2, w2 += n2 * _2, m2 += n2 * j2, b2 += n2 * q2, k2 += n2 * V2, E2 += n2 * J2, v2 += n2 * Y2, I2 += n2 * Z2, B2 += n2 * W2, l2 += (n2 = t2[6]) * L2, y2 += n2 * M2, f2 += n2 * N2, g2 += n2 * F2, p2 += n2 * O2, d2 += n2 * H2, A2 += n2 * z2, w2 += n2 * G2, m2 += n2 * _2, b2 += n2 * j2, k2 += n2 * q2, E2 += n2 * V2, v2 += n2 * J2, I2 += n2 * Y2, B2 += n2 * Z2, S2 += n2 * W2, y2 += (n2 = t2[7]) * L2, f2 += n2 * M2, g2 += n2 * N2, p2 += n2 * F2, d2 += n2 * O2, A2 += n2 * H2, w2 += n2 * z2, m2 += n2 * G2, b2 += n2 * _2, k2 += n2 * j2, E2 += n2 * q2, v2 += n2 * V2, I2 += n2 * J2, B2 += n2 * Y2, S2 += n2 * Z2, K2 += n2 * W2, f2 += (n2 = t2[8]) * L2, g2 += n2 * M2, p2 += n2 * N2, d2 += n2 * F2, A2 += n2 * O2, w2 += n2 * H2, m2 += n2 * z2, b2 += n2 * G2, k2 += n2 * _2, E2 += n2 * j2, v2 += n2 * q2, I2 += n2 * V2, B2 += n2 * J2, S2 += n2 * Y2, K2 += n2 * Z2, C2 += n2 * W2, g2 += (n2 = t2[9]) * L2, p2 += n2 * M2, d2 += n2 * N2, A2 += n2 * F2, w2 += n2 * O2, m2 += n2 * H2, b2 += n2 * z2, k2 += n2 * G2, E2 += n2 * _2, v2 += n2 * j2, I2 += n2 * q2, B2 += n2 * V2, S2 += n2 * J2, K2 += n2 * Y2, C2 += n2 * Z2, D2 += n2 * W2, p2 += (n2 = t2[10]) * L2, d2 += n2 * M2, A2 += n2 * N2, w2 += n2 * F2, m2 += n2 * O2, b2 += n2 * H2, k2 += n2 * z2, E2 += n2 * G2, v2 += n2 * _2, I2 += n2 * j2, B2 += n2 * q2, S2 += n2 * V2, K2 += n2 * J2, C2 += n2 * Y2, D2 += n2 * Z2, U2 += n2 * W2, d2 += (n2 = t2[11]) * L2, A2 += n2 * M2, w2 += n2 * N2, m2 += n2 * F2, b2 += n2 * O2, k2 += n2 * H2, E2 += n2 * z2, v2 += n2 * G2, I2 += n2 * _2, B2 += n2 * j2, S2 += n2 * q2, K2 += n2 * V2, C2 += n2 * J2, D2 += n2 * Y2, U2 += n2 * Z2, P2 += n2 * W2, A2 += (n2 = t2[12]) * L2, w2 += n2 * M2, m2 += n2 * N2, b2 += n2 * F2, k2 += n2 * O2, E2 += n2 * H2, v2 += n2 * z2, I2 += n2 * G2, B2 += n2 * _2, S2 += n2 * j2, K2 += n2 * q2, C2 += n2 * V2, D2 += n2 * J2, U2 += n2 * Y2, P2 += n2 * Z2, x2 += n2 * W2, w2 += (n2 = t2[13]) * L2, m2 += n2 * M2, b2 += n2 * N2, k2 += n2 * F2, E2 += n2 * O2, v2 += n2 * H2, I2 += n2 * z2, B2 += n2 * G2, S2 += n2 * _2, K2 += n2 * j2, C2 += n2 * q2, D2 += n2 * V2, U2 += n2 * J2, P2 += n2 * Y2, x2 += n2 * Z2, Q2 += n2 * W2, m2 += (n2 = t2[14]) * L2, b2 += n2 * M2, k2 += n2 * N2, E2 += n2 * F2, v2 += n2 * O2, I2 += n2 * H2, B2 += n2 * z2, S2 += n2 * G2, K2 += n2 * _2, C2 += n2 * j2, D2 += n2 * q2, U2 += n2 * V2, P2 += n2 * J2, x2 += n2 * Y2, Q2 += n2 * Z2, R2 += n2 * W2, b2 += (n2 = t2[15]) * L2, a2 += 38 * (E2 += n2 * N2), o2 += 38 * (v2 += n2 * F2), c2 += 38 * (I2 += n2 * O2), h2 += 38 * (B2 += n2 * H2), u2 += 38 * (S2 += n2 * z2), l2 += 38 * (K2 += n2 * G2), y2 += 38 * (C2 += n2 * _2), f2 += 38 * (D2 += n2 * j2), g2 += 38 * (U2 += n2 * q2), p2 += 38 * (P2 += n2 * V2), d2 += 38 * (x2 += n2 * J2), A2 += 38 * (Q2 += n2 * Y2), w2 += 38 * (R2 += n2 * Z2), m2 += 38 * (T2 += n2 * W2), s2 = (n2 = (s2 += 38 * (k2 += n2 * M2)) + (i2 = 1) + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), a2 = (n2 = a2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), o2 = (n2 = o2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), c2 = (n2 = c2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), h2 = (n2 = h2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), u2 = (n2 = u2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), l2 = (n2 = l2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), y2 = (n2 = y2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), f2 = (n2 = f2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), g2 = (n2 = g2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), p2 = (n2 = p2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), d2 = (n2 = d2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), A2 = (n2 = A2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), w2 = (n2 = w2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), m2 = (n2 = m2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), b2 = (n2 = b2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), s2 = (n2 = (s2 += i2 - 1 + 37 * (i2 - 1)) + (i2 = 1) + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), a2 = (n2 = a2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), o2 = (n2 = o2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), c2 = (n2 = c2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), h2 = (n2 = h2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), u2 = (n2 = u2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), l2 = (n2 = l2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), y2 = (n2 = y2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), f2 = (n2 = f2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), g2 = (n2 = g2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), p2 = (n2 = p2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), d2 = (n2 = d2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), A2 = (n2 = A2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), w2 = (n2 = w2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), m2 = (n2 = m2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), b2 = (n2 = b2 + i2 + 65535) - 65536 * (i2 = Math.floor(n2 / 65536)), s2 += i2 - 1 + 37 * (i2 - 1), e2[0] = s2, e2[1] = a2, e2[2] = o2, e2[3] = c2, e2[4] = h2, e2[5] = u2, e2[6] = l2, e2[7] = y2, e2[8] = f2, e2[9] = g2, e2[10] = p2, e2[11] = d2, e2[12] = A2, e2[13] = w2, e2[14] = m2, e2[15] = b2;
}
function kt(e2, t2) {
  bt(e2, t2, t2);
}
function Et(e2, t2) {
  var r2, n2 = $e();
  for (r2 = 0; r2 < 16; r2++) n2[r2] = t2[r2];
  for (r2 = 253; r2 >= 0; r2--) kt(n2, n2), 2 !== r2 && 4 !== r2 && bt(n2, n2, t2);
  for (r2 = 0; r2 < 16; r2++) e2[r2] = n2[r2];
}
function vt(e2, t2, r2) {
  var n2, i2, s2 = new Uint8Array(32), a2 = new Float64Array(80), o2 = $e(), c2 = $e(), h2 = $e(), u2 = $e(), l2 = $e(), y2 = $e();
  for (i2 = 0; i2 < 31; i2++) s2[i2] = t2[i2];
  for (s2[31] = 127 & t2[31] | 64, s2[0] &= 248, At(a2, r2), i2 = 0; i2 < 16; i2++) c2[i2] = a2[i2], u2[i2] = o2[i2] = h2[i2] = 0;
  for (o2[0] = u2[0] = 1, i2 = 254; i2 >= 0; --i2) ft(o2, c2, n2 = s2[i2 >>> 3] >>> (7 & i2) & 1), ft(h2, u2, n2), wt(l2, o2, h2), mt(o2, o2, h2), wt(h2, c2, u2), mt(c2, c2, u2), kt(u2, l2), kt(y2, o2), bt(o2, h2, o2), bt(h2, c2, l2), wt(l2, o2, h2), mt(o2, o2, h2), kt(c2, o2), mt(h2, u2, y2), bt(o2, h2, nt), wt(o2, o2, u2), bt(h2, h2, o2), bt(o2, u2, y2), bt(u2, c2, a2), kt(c2, l2), ft(o2, c2, n2), ft(h2, u2, n2);
  for (i2 = 0; i2 < 16; i2++) a2[i2 + 16] = o2[i2], a2[i2 + 32] = h2[i2], a2[i2 + 48] = c2[i2], a2[i2 + 64] = u2[i2];
  var f2 = a2.subarray(32), g2 = a2.subarray(16);
  return Et(f2, f2), bt(g2, g2, f2), gt(e2, g2), 0;
}
function It(e2, t2) {
  return vt(e2, t2, et);
}
function St(e2, t2, r2, n2) {
  for (var i2, s2, a2, o2, c2, h2, u2, l2, y2, f2, g2, p2, d2, A2, w2, m2, b2, k2, E2, v2, I2, B2, S2, K2, C2, D2, U2 = new Int32Array(16), P2 = new Int32Array(16), x2 = e2[0], Q2 = e2[1], R2 = e2[2], T2 = e2[3], L2 = e2[4], M2 = e2[5], N2 = e2[6], F2 = e2[7], O2 = t2[0], H2 = t2[1], z2 = t2[2], G2 = t2[3], _2 = t2[4], j2 = t2[5], q2 = t2[6], V2 = t2[7], J2 = 0; n2 >= 128; ) {
    for (E2 = 0; E2 < 16; E2++) v2 = 8 * E2 + J2, U2[E2] = r2[v2 + 0] << 24 | r2[v2 + 1] << 16 | r2[v2 + 2] << 8 | r2[v2 + 3], P2[E2] = r2[v2 + 4] << 24 | r2[v2 + 5] << 16 | r2[v2 + 6] << 8 | r2[v2 + 7];
    for (E2 = 0; E2 < 80; E2++) if (i2 = x2, s2 = Q2, a2 = R2, o2 = T2, c2 = L2, h2 = M2, u2 = N2, F2, y2 = O2, f2 = H2, g2 = z2, p2 = G2, d2 = _2, A2 = j2, w2 = q2, V2, S2 = 65535 & (B2 = V2), K2 = B2 >>> 16, C2 = 65535 & (I2 = F2), D2 = I2 >>> 16, S2 += 65535 & (B2 = (_2 >>> 14 | L2 << 18) ^ (_2 >>> 18 | L2 << 14) ^ (L2 >>> 9 | _2 << 23)), K2 += B2 >>> 16, C2 += 65535 & (I2 = (L2 >>> 14 | _2 << 18) ^ (L2 >>> 18 | _2 << 14) ^ (_2 >>> 9 | L2 << 23)), D2 += I2 >>> 16, S2 += 65535 & (B2 = _2 & j2 ^ ~_2 & q2), K2 += B2 >>> 16, C2 += 65535 & (I2 = L2 & M2 ^ ~L2 & N2), D2 += I2 >>> 16, I2 = Bt[2 * E2], S2 += 65535 & (B2 = Bt[2 * E2 + 1]), K2 += B2 >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, I2 = U2[E2 % 16], K2 += (B2 = P2[E2 % 16]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16, S2 = 65535 & (B2 = k2 = 65535 & S2 | K2 << 16), K2 = B2 >>> 16, C2 = 65535 & (I2 = b2 = 65535 & C2 | (D2 += C2 >>> 16) << 16), D2 = I2 >>> 16, S2 += 65535 & (B2 = (O2 >>> 28 | x2 << 4) ^ (x2 >>> 2 | O2 << 30) ^ (x2 >>> 7 | O2 << 25)), K2 += B2 >>> 16, C2 += 65535 & (I2 = (x2 >>> 28 | O2 << 4) ^ (O2 >>> 2 | x2 << 30) ^ (O2 >>> 7 | x2 << 25)), D2 += I2 >>> 16, K2 += (B2 = O2 & H2 ^ O2 & z2 ^ H2 & z2) >>> 16, C2 += 65535 & (I2 = x2 & Q2 ^ x2 & R2 ^ Q2 & R2), D2 += I2 >>> 16, l2 = 65535 & (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) | (D2 += C2 >>> 16) << 16, m2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = p2), K2 = B2 >>> 16, C2 = 65535 & (I2 = o2), D2 = I2 >>> 16, K2 += (B2 = k2) >>> 16, C2 += 65535 & (I2 = b2), D2 += I2 >>> 16, Q2 = i2, R2 = s2, T2 = a2, L2 = o2 = 65535 & (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) | (D2 += C2 >>> 16) << 16, M2 = c2, N2 = h2, F2 = u2, x2 = l2, H2 = y2, z2 = f2, G2 = g2, _2 = p2 = 65535 & S2 | K2 << 16, j2 = d2, q2 = A2, V2 = w2, O2 = m2, E2 % 16 == 15) for (v2 = 0; v2 < 16; v2++) I2 = U2[v2], S2 = 65535 & (B2 = P2[v2]), K2 = B2 >>> 16, C2 = 65535 & I2, D2 = I2 >>> 16, I2 = U2[(v2 + 9) % 16], S2 += 65535 & (B2 = P2[(v2 + 9) % 16]), K2 += B2 >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, b2 = U2[(v2 + 1) % 16], S2 += 65535 & (B2 = ((k2 = P2[(v2 + 1) % 16]) >>> 1 | b2 << 31) ^ (k2 >>> 8 | b2 << 24) ^ (k2 >>> 7 | b2 << 25)), K2 += B2 >>> 16, C2 += 65535 & (I2 = (b2 >>> 1 | k2 << 31) ^ (b2 >>> 8 | k2 << 24) ^ b2 >>> 7), D2 += I2 >>> 16, b2 = U2[(v2 + 14) % 16], K2 += (B2 = ((k2 = P2[(v2 + 14) % 16]) >>> 19 | b2 << 13) ^ (b2 >>> 29 | k2 << 3) ^ (k2 >>> 6 | b2 << 26)) >>> 16, C2 += 65535 & (I2 = (b2 >>> 19 | k2 << 13) ^ (k2 >>> 29 | b2 << 3) ^ b2 >>> 6), D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, U2[v2] = 65535 & C2 | D2 << 16, P2[v2] = 65535 & S2 | K2 << 16;
    S2 = 65535 & (B2 = O2), K2 = B2 >>> 16, C2 = 65535 & (I2 = x2), D2 = I2 >>> 16, I2 = e2[0], K2 += (B2 = t2[0]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[0] = x2 = 65535 & C2 | D2 << 16, t2[0] = O2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = H2), K2 = B2 >>> 16, C2 = 65535 & (I2 = Q2), D2 = I2 >>> 16, I2 = e2[1], K2 += (B2 = t2[1]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[1] = Q2 = 65535 & C2 | D2 << 16, t2[1] = H2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = z2), K2 = B2 >>> 16, C2 = 65535 & (I2 = R2), D2 = I2 >>> 16, I2 = e2[2], K2 += (B2 = t2[2]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[2] = R2 = 65535 & C2 | D2 << 16, t2[2] = z2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = G2), K2 = B2 >>> 16, C2 = 65535 & (I2 = T2), D2 = I2 >>> 16, I2 = e2[3], K2 += (B2 = t2[3]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[3] = T2 = 65535 & C2 | D2 << 16, t2[3] = G2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = _2), K2 = B2 >>> 16, C2 = 65535 & (I2 = L2), D2 = I2 >>> 16, I2 = e2[4], K2 += (B2 = t2[4]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[4] = L2 = 65535 & C2 | D2 << 16, t2[4] = _2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = j2), K2 = B2 >>> 16, C2 = 65535 & (I2 = M2), D2 = I2 >>> 16, I2 = e2[5], K2 += (B2 = t2[5]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[5] = M2 = 65535 & C2 | D2 << 16, t2[5] = j2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = q2), K2 = B2 >>> 16, C2 = 65535 & (I2 = N2), D2 = I2 >>> 16, I2 = e2[6], K2 += (B2 = t2[6]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[6] = N2 = 65535 & C2 | D2 << 16, t2[6] = q2 = 65535 & S2 | K2 << 16, S2 = 65535 & (B2 = V2), K2 = B2 >>> 16, C2 = 65535 & (I2 = F2), D2 = I2 >>> 16, I2 = e2[7], K2 += (B2 = t2[7]) >>> 16, C2 += 65535 & I2, D2 += I2 >>> 16, D2 += (C2 += (K2 += (S2 += 65535 & B2) >>> 16) >>> 16) >>> 16, e2[7] = F2 = 65535 & C2 | D2 << 16, t2[7] = V2 = 65535 & S2 | K2 << 16, J2 += 128, n2 -= 128;
  }
  return n2;
}
function Kt(e2, t2, r2) {
  var n2, i2 = new Int32Array(8), s2 = new Int32Array(8), a2 = new Uint8Array(256), o2 = r2;
  for (i2[0] = 1779033703, i2[1] = 3144134277, i2[2] = 1013904242, i2[3] = 2773480762, i2[4] = 1359893119, i2[5] = 2600822924, i2[6] = 528734635, i2[7] = 1541459225, s2[0] = 4089235720, s2[1] = 2227873595, s2[2] = 4271175723, s2[3] = 1595750129, s2[4] = 2917565137, s2[5] = 725511199, s2[6] = 4215389547, s2[7] = 327033209, St(i2, s2, t2, r2), r2 %= 128, n2 = 0; n2 < r2; n2++) a2[n2] = t2[o2 - r2 + n2];
  for (a2[r2] = 128, a2[(r2 = 256 - 128 * (r2 < 112 ? 1 : 0)) - 9] = 0, ht(a2, r2 - 8, o2 / 536870912 | 0, o2 << 3), St(i2, s2, a2, r2), n2 = 0; n2 < 8; n2++) ht(e2, 8 * n2, i2[n2], s2[n2]);
  return 0;
}
function Ct(e2, t2) {
  var r2 = $e(), n2 = $e(), i2 = $e(), s2 = $e(), a2 = $e(), o2 = $e(), c2 = $e(), h2 = $e(), u2 = $e();
  mt(r2, e2[1], e2[0]), mt(u2, t2[1], t2[0]), bt(r2, r2, u2), wt(n2, e2[0], e2[1]), wt(u2, t2[0], t2[1]), bt(n2, n2, u2), bt(i2, e2[3], t2[3]), bt(i2, i2, st), bt(s2, e2[2], t2[2]), wt(s2, s2, s2), mt(a2, n2, r2), mt(o2, s2, i2), wt(c2, s2, i2), wt(h2, n2, r2), bt(e2[0], a2, o2), bt(e2[1], h2, c2), bt(e2[2], c2, o2), bt(e2[3], a2, h2);
}
function Dt(e2, t2, r2) {
  var n2;
  for (n2 = 0; n2 < 4; n2++) ft(e2[n2], t2[n2], r2);
}
function Ut(e2, t2) {
  var r2 = $e(), n2 = $e(), i2 = $e();
  Et(i2, t2[2]), bt(r2, t2[0], i2), bt(n2, t2[1], i2), gt(e2, n2), e2[31] ^= dt(r2) << 7;
}
function Pt(e2, t2, r2) {
  var n2, i2;
  for (lt(e2[0], tt), lt(e2[1], rt), lt(e2[2], rt), lt(e2[3], tt), i2 = 255; i2 >= 0; --i2) Dt(e2, t2, n2 = r2[i2 / 8 | 0] >> (7 & i2) & 1), Ct(t2, e2), Ct(e2, e2), Dt(e2, t2, n2);
}
function xt(e2, t2) {
  var r2 = [$e(), $e(), $e(), $e()];
  lt(r2[0], at), lt(r2[1], ot), lt(r2[2], rt), bt(r2[3], at, ot), Pt(e2, r2, t2);
}
function Qt(e2, t2, r2) {
  var n2, i2 = new Uint8Array(64), s2 = [$e(), $e(), $e(), $e()];
  for (r2 || Xe(t2, 32), Kt(i2, t2, 32), i2[0] &= 248, i2[31] &= 127, i2[31] |= 64, xt(s2, i2), Ut(e2, s2), n2 = 0; n2 < 32; n2++) t2[n2 + 32] = e2[n2];
  return 0;
}
function Tt(e2, t2) {
  var r2, n2, i2, s2;
  for (n2 = 63; n2 >= 32; --n2) {
    for (r2 = 0, i2 = n2 - 32, s2 = n2 - 12; i2 < s2; ++i2) t2[i2] += r2 - 16 * t2[n2] * Rt[i2 - (n2 - 32)], r2 = Math.floor((t2[i2] + 128) / 256), t2[i2] -= 256 * r2;
    t2[i2] += r2, t2[n2] = 0;
  }
  for (r2 = 0, i2 = 0; i2 < 32; i2++) t2[i2] += r2 - (t2[31] >> 4) * Rt[i2], r2 = t2[i2] >> 8, t2[i2] &= 255;
  for (i2 = 0; i2 < 32; i2++) t2[i2] -= r2 * Rt[i2];
  for (n2 = 0; n2 < 32; n2++) t2[n2 + 1] += t2[n2] >> 8, e2[n2] = 255 & t2[n2];
}
function Lt(e2) {
  var t2, r2 = new Float64Array(64);
  for (t2 = 0; t2 < 64; t2++) r2[t2] = e2[t2];
  for (t2 = 0; t2 < 64; t2++) e2[t2] = 0;
  Tt(e2, r2);
}
function Mt(e2, t2) {
  var r2 = $e(), n2 = $e(), i2 = $e(), s2 = $e(), a2 = $e(), o2 = $e(), c2 = $e();
  return lt(e2[2], rt), At(e2[1], t2), kt(i2, e2[1]), bt(s2, i2, it), mt(i2, i2, e2[2]), wt(s2, e2[2], s2), kt(a2, s2), kt(o2, a2), bt(c2, o2, a2), bt(r2, c2, i2), bt(r2, r2, s2), function(e3, t3) {
    var r3, n3 = $e();
    for (r3 = 0; r3 < 16; r3++) n3[r3] = t3[r3];
    for (r3 = 250; r3 >= 0; r3--) kt(n3, n3), 1 !== r3 && bt(n3, n3, t3);
    for (r3 = 0; r3 < 16; r3++) e3[r3] = n3[r3];
  }(r2, r2), bt(r2, r2, i2), bt(r2, r2, s2), bt(r2, r2, s2), bt(e2[0], r2, s2), kt(n2, e2[0]), bt(n2, n2, s2), pt(n2, i2) && bt(e2[0], e2[0], ct), kt(n2, e2[0]), bt(n2, n2, s2), pt(n2, i2) ? -1 : (dt(e2[0]) === t2[31] >> 7 && mt(e2[0], tt, e2[0]), bt(e2[3], e2[0], e2[1]), 0);
}
function Ft() {
  for (var e2 = 0; e2 < arguments.length; e2++) if (!(arguments[e2] instanceof Uint8Array)) throw new TypeError("unexpected type, use Uint8Array");
}
function zt(e2) {
  let t2, r2 = 0;
  const n2 = e2[0];
  return n2 < 192 ? ([r2] = e2, t2 = 1) : n2 < 255 ? (r2 = (e2[0] - 192 << 8) + e2[1] + 192, t2 = 2) : 255 === n2 && (r2 = M.readNumber(e2.subarray(1, 5)), t2 = 5), { len: r2, offset: t2 };
}
function Gt(e2) {
  return e2 < 192 ? new Uint8Array([e2]) : e2 > 191 && e2 < 8384 ? new Uint8Array([192 + (e2 - 192 >> 8), e2 - 192 & 255]) : M.concatUint8Array([new Uint8Array([255]), M.writeNumber(e2, 4)]);
}
function _t(e2) {
  if (e2 < 0 || e2 > 30) throw Error("Partial Length power must be between 1 and 30");
  return new Uint8Array([224 + e2]);
}
function jt(e2) {
  return new Uint8Array([192 | e2]);
}
function qt(e2, t2) {
  return M.concatUint8Array([jt(e2), Gt(t2)]);
}
function Vt(e2) {
  return [R.packet.literalData, R.packet.compressedData, R.packet.symmetricallyEncryptedData, R.packet.symEncryptedIntegrityProtectedData, R.packet.aeadEncryptedData].includes(e2);
}
async function Jt(e2, t2) {
  const r2 = P(e2);
  let n2, i2;
  try {
    const s2 = await r2.peekBytes(2);
    if (!s2 || s2.length < 2 || !(128 & s2[0])) throw Error("Error during parsing. This message / key probably does not conform to a valid OpenPGP format.");
    const o2 = await r2.readByte();
    let c2, h2, u2 = -1, l2 = -1;
    l2 = 0, 64 & o2 && (l2 = 1), l2 ? u2 = 63 & o2 : (u2 = (63 & o2) >> 2, h2 = 3 & o2);
    const y2 = Vt(u2);
    let f2, g2 = null;
    if (y2) {
      if ("array" === M.isStream(e2)) {
        const e3 = new a();
        n2 = x(e3), g2 = e3;
      } else {
        const e3 = new TransformStream();
        n2 = x(e3.writable), g2 = e3.readable;
      }
      i2 = t2({ tag: u2, packet: g2 });
    } else g2 = [];
    do {
      if (l2) {
        const e3 = await r2.readByte();
        if (f2 = false, e3 < 192) c2 = e3;
        else if (e3 >= 192 && e3 < 224) c2 = (e3 - 192 << 8) + await r2.readByte() + 192;
        else if (e3 > 223 && e3 < 255) {
          if (c2 = 1 << (31 & e3), f2 = true, !y2) throw new TypeError("This packet type does not support partial lengths.");
        } else c2 = await r2.readByte() << 24 | await r2.readByte() << 16 | await r2.readByte() << 8 | await r2.readByte();
      } else switch (h2) {
        case 0:
          c2 = await r2.readByte();
          break;
        case 1:
          c2 = await r2.readByte() << 8 | await r2.readByte();
          break;
        case 2:
          c2 = await r2.readByte() << 24 | await r2.readByte() << 16 | await r2.readByte() << 8 | await r2.readByte();
          break;
        default:
          c2 = 1 / 0;
      }
      if (c2 > 0) {
        let e3 = 0;
        for (; ; ) {
          n2 && await n2.ready;
          const { done: t3, value: i3 } = await r2.read();
          if (t3) {
            if (c2 === 1 / 0) break;
            throw Error("Unexpected end of packet");
          }
          const s3 = c2 === 1 / 0 ? i3 : i3.subarray(0, c2 - e3);
          if (n2 ? await n2.write(s3) : g2.push(s3), e3 += i3.length, e3 >= c2) {
            r2.unshift(i3.subarray(c2 - e3 + i3.length));
            break;
          }
        }
      }
    } while (f2);
    const p2 = await r2.peekBytes(y2 ? 1 / 0 : 2);
    return n2 ? (await n2.ready, await n2.close()) : (g2 = M.concatUint8Array(g2), await t2({ tag: u2, packet: g2 })), !p2 || !p2.length;
  } catch (e3) {
    if (n2) return await n2.abort(e3), true;
    throw e3;
  } finally {
    n2 && await i2, r2.releaseLock();
  }
}
async function $t(e2) {
  switch (e2) {
    case R.publicKey.ed25519:
      try {
        const e3 = M.getWebCrypto(), t2 = await e3.generateKey("Ed25519", true, ["sign", "verify"]), r2 = await e3.exportKey("jwk", t2.privateKey), n2 = await e3.exportKey("jwk", t2.publicKey);
        return { A: new Uint8Array(G(n2.x)), seed: G(r2.d) };
      } catch (t2) {
        if ("NotSupportedError" !== t2.name && "OperationError" !== t2.name) throw t2;
        const r2 = fe(rr(e2)), { publicKey: n2 } = We.sign.keyPair.fromSeed(r2);
        return { A: n2, seed: r2 };
      }
    case R.publicKey.ed448: {
      const e3 = await M.getNobleCurve(R.publicKey.ed448), t2 = e3.utils.randomPrivateKey();
      return { A: e3.getPublicKey(t2), seed: t2 };
    }
    default:
      throw Error("Unsupported EdDSA algorithm");
  }
}
async function Xt(e2, t2, r2, n2, i2, s2) {
  if (Re(t2) < Re(nr(e2))) throw Error("Hash algorithm too weak for EdDSA.");
  switch (e2) {
    case R.publicKey.ed25519:
      try {
        const t3 = M.getWebCrypto(), r3 = sr(e2, n2, i2), a2 = await t3.importKey("jwk", r3, "Ed25519", false, ["sign"]);
        return { RS: new Uint8Array(await t3.sign("Ed25519", a2, s2)) };
      } catch (e3) {
        if ("NotSupportedError" !== e3.name) throw e3;
        const t3 = M.concatUint8Array([i2, n2]);
        return { RS: We.sign.detached(s2, t3) };
      }
    case R.publicKey.ed448:
      return { RS: (await M.getNobleCurve(R.publicKey.ed448)).sign(s2, i2) };
    default:
      throw Error("Unsupported EdDSA algorithm");
  }
}
async function er(e2, t2, { RS: r2 }, n2, i2, s2) {
  if (Re(t2) < Re(nr(e2))) throw Error("Hash algorithm too weak for EdDSA.");
  switch (e2) {
    case R.publicKey.ed25519:
      try {
        const t3 = M.getWebCrypto(), n3 = ir(e2, i2), a2 = await t3.importKey("jwk", n3, "Ed25519", false, ["verify"]);
        return await t3.verify("Ed25519", a2, r2, s2);
      } catch (e3) {
        if ("NotSupportedError" !== e3.name) throw e3;
        return We.sign.detached.verify(s2, r2, i2);
      }
    case R.publicKey.ed448:
      return (await M.getNobleCurve(R.publicKey.ed448)).verify(r2, s2, i2);
    default:
      throw Error("Unsupported EdDSA algorithm");
  }
}
async function tr(e2, t2, r2) {
  switch (e2) {
    case R.publicKey.ed25519: {
      const { publicKey: e3 } = We.sign.keyPair.fromSeed(r2);
      return M.equalsUint8Array(t2, e3);
    }
    case R.publicKey.ed448: {
      const e3 = (await M.getNobleCurve(R.publicKey.ed448)).getPublicKey(r2);
      return M.equalsUint8Array(t2, e3);
    }
    default:
      return false;
  }
}
function rr(e2) {
  switch (e2) {
    case R.publicKey.ed25519:
      return 32;
    case R.publicKey.ed448:
      return 57;
    default:
      throw Error("Unsupported EdDSA algorithm");
  }
}
function nr(e2) {
  switch (e2) {
    case R.publicKey.ed25519:
      return R.hash.sha256;
    case R.publicKey.ed448:
      return R.hash.sha512;
    default:
      throw Error("Unknown EdDSA algo");
  }
}
function or(e2) {
  return e2 instanceof Uint8Array || null != e2 && "object" == typeof e2 && "Uint8Array" === e2.constructor.name;
}
function cr(e2, ...t2) {
  if (!or(e2)) throw Error("Uint8Array expected");
  if (t2.length > 0 && !t2.includes(e2.length)) throw Error(`Uint8Array expected of length ${t2}, not of length=${e2.length}`);
}
function hr(e2, t2 = true) {
  if (e2.destroyed) throw Error("Hash instance has been destroyed");
  if (t2 && e2.finished) throw Error("Hash#digest() has already been called");
}
function ur(e2, t2) {
  cr(e2);
  const r2 = t2.outputLen;
  if (e2.length < r2) throw Error("digestInto() expects output buffer of length at least " + r2);
}
function gr(e2) {
  if ("string" == typeof e2) e2 = function(e3) {
    if ("string" != typeof e3) throw Error("string expected, got " + typeof e3);
    return new Uint8Array(new TextEncoder().encode(e3));
  }(e2);
  else {
    if (!or(e2)) throw Error("Uint8Array expected, got " + typeof e2);
    e2 = mr(e2);
  }
  return e2;
}
function pr(e2, t2) {
  if (e2.length !== t2.length) return false;
  let r2 = 0;
  for (let n2 = 0; n2 < e2.length; n2++) r2 |= e2[n2] ^ t2[n2];
  return 0 === r2;
}
function Ar(e2, t2, r2, n2) {
  if ("function" == typeof e2.setBigUint64) return e2.setBigUint64(t2, r2, n2);
  const i2 = BigInt(32), s2 = BigInt(4294967295), a2 = Number(r2 >> i2 & s2), o2 = Number(r2 & s2);
  e2.setUint32(t2 + 0, a2, n2), e2.setUint32(t2 + 4, o2, n2);
}
function wr(e2) {
  return e2.byteOffset % 4 == 0;
}
function mr(e2) {
  return Uint8Array.from(e2);
}
function br(...e2) {
  for (let t2 = 0; t2 < e2.length; t2++) e2[t2].fill(0);
}
function Kr(e2) {
  const t2 = (t3, r3) => e2(r3, t3.length).update(gr(t3)).digest(), r2 = e2(new Uint8Array(16), 0);
  return t2.outputLen = r2.outputLen, t2.blockLen = r2.blockLen, t2.create = (t3, r3) => e2(t3, r3), t2;
}
function xr(e2) {
  return e2 << 1 ^ Pr & -(e2 >> 7);
}
function Qr(e2, t2) {
  let r2 = 0;
  for (; t2 > 0; t2 >>= 1) r2 ^= e2 & -(1 & t2), e2 = xr(e2);
  return r2;
}
function Fr(e2, t2) {
  if (256 !== e2.length) throw Error("Wrong sbox length");
  const r2 = new Uint32Array(256).map((r3, n3) => t2(e2[n3])), n2 = r2.map(Mr), i2 = n2.map(Mr), s2 = i2.map(Mr), a2 = new Uint32Array(65536), o2 = new Uint32Array(65536), c2 = new Uint16Array(65536);
  for (let t3 = 0; t3 < 256; t3++) for (let h2 = 0; h2 < 256; h2++) {
    const u2 = 256 * t3 + h2;
    a2[u2] = r2[t3] ^ n2[h2], o2[u2] = i2[t3] ^ s2[h2], c2[u2] = e2[t3] << 8 | e2[h2];
  }
  return { sbox: e2, sbox2: c2, T0: r2, T1: n2, T2: i2, T3: s2, T01: a2, T23: o2 };
}
function Gr(e2) {
  cr(e2);
  const t2 = e2.length;
  if (![16, 24, 32].includes(t2)) throw Error("aes: wrong key size: should be 16, 24 or 32, got: " + t2);
  const { sbox2: r2 } = Or, n2 = [];
  wr(e2) || n2.push(e2 = mr(e2));
  const i2 = yr(e2), s2 = i2.length, a2 = (e3) => qr(r2, e3, e3, e3, e3), o2 = new Uint32Array(t2 + 28);
  o2.set(i2);
  for (let e3 = s2; e3 < o2.length; e3++) {
    let t3 = o2[e3 - 1];
    e3 % s2 == 0 ? t3 = a2(Lr(t3)) ^ zr[e3 / s2 - 1] : s2 > 6 && e3 % s2 == 4 && (t3 = a2(t3)), o2[e3] = o2[e3 - s2] ^ t3;
  }
  return br(...n2), o2;
}
function _r(e2) {
  const t2 = Gr(e2), r2 = t2.slice(), n2 = t2.length, { sbox2: i2 } = Or, { T0: s2, T1: a2, T2: o2, T3: c2 } = Hr;
  for (let e3 = 0; e3 < n2; e3 += 4) for (let i3 = 0; i3 < 4; i3++) r2[e3 + i3] = t2[n2 - e3 - 4 + i3];
  br(t2);
  for (let e3 = 4; e3 < n2 - 4; e3++) {
    const t3 = r2[e3], n3 = qr(i2, t3, t3, t3, t3);
    r2[e3] = s2[255 & n3] ^ a2[n3 >>> 8 & 255] ^ o2[n3 >>> 16 & 255] ^ c2[n3 >>> 24];
  }
  return r2;
}
function jr(e2, t2, r2, n2, i2, s2) {
  return e2[r2 << 8 & 65280 | n2 >>> 8 & 255] ^ t2[i2 >>> 8 & 65280 | s2 >>> 24 & 255];
}
function qr(e2, t2, r2, n2, i2) {
  return e2[255 & t2 | 65280 & r2] | e2[n2 >>> 16 & 255 | i2 >>> 16 & 65280] << 16;
}
function Vr(e2, t2, r2, n2, i2) {
  const { sbox2: s2, T01: a2, T23: o2 } = Or;
  let c2 = 0;
  t2 ^= e2[c2++], r2 ^= e2[c2++], n2 ^= e2[c2++], i2 ^= e2[c2++];
  const h2 = e2.length / 4 - 2;
  for (let s3 = 0; s3 < h2; s3++) {
    const s4 = e2[c2++] ^ jr(a2, o2, t2, r2, n2, i2), h3 = e2[c2++] ^ jr(a2, o2, r2, n2, i2, t2), u2 = e2[c2++] ^ jr(a2, o2, n2, i2, t2, r2), l2 = e2[c2++] ^ jr(a2, o2, i2, t2, r2, n2);
    t2 = s4, r2 = h3, n2 = u2, i2 = l2;
  }
  return { s0: e2[c2++] ^ qr(s2, t2, r2, n2, i2), s1: e2[c2++] ^ qr(s2, r2, n2, i2, t2), s2: e2[c2++] ^ qr(s2, n2, i2, t2, r2), s3: e2[c2++] ^ qr(s2, i2, t2, r2, n2) };
}
function Jr(e2, t2, r2, n2, i2) {
  const { sbox2: s2, T01: a2, T23: o2 } = Hr;
  let c2 = 0;
  t2 ^= e2[c2++], r2 ^= e2[c2++], n2 ^= e2[c2++], i2 ^= e2[c2++];
  const h2 = e2.length / 4 - 2;
  for (let s3 = 0; s3 < h2; s3++) {
    const s4 = e2[c2++] ^ jr(a2, o2, t2, i2, n2, r2), h3 = e2[c2++] ^ jr(a2, o2, r2, t2, i2, n2), u2 = e2[c2++] ^ jr(a2, o2, n2, r2, t2, i2), l2 = e2[c2++] ^ jr(a2, o2, i2, n2, r2, t2);
    t2 = s4, r2 = h3, n2 = u2, i2 = l2;
  }
  return { s0: e2[c2++] ^ qr(s2, t2, i2, n2, r2), s1: e2[c2++] ^ qr(s2, r2, t2, i2, n2), s2: e2[c2++] ^ qr(s2, n2, r2, t2, i2), s3: e2[c2++] ^ qr(s2, i2, n2, r2, t2) };
}
function Yr(e2, t2) {
  if (void 0 === t2) return new Uint8Array(e2);
  if (cr(t2), t2.length < e2) throw Error(`aes: wrong destination length, expected at least ${e2}, got: ${t2.length}`);
  if (!wr(t2)) throw Error("unaligned dst");
  return t2;
}
function Zr(e2, t2, r2, n2) {
  cr(t2, Dr), cr(r2);
  const i2 = r2.length;
  n2 = Yr(i2, n2);
  const s2 = t2, a2 = yr(s2);
  let { s0: o2, s1: c2, s2: h2, s3: u2 } = Vr(e2, a2[0], a2[1], a2[2], a2[3]);
  const l2 = yr(r2), y2 = yr(n2);
  for (let t3 = 0; t3 + 4 <= l2.length; t3 += 4) {
    y2[t3 + 0] = l2[t3 + 0] ^ o2, y2[t3 + 1] = l2[t3 + 1] ^ c2, y2[t3 + 2] = l2[t3 + 2] ^ h2, y2[t3 + 3] = l2[t3 + 3] ^ u2;
    let r3 = 1;
    for (let e3 = s2.length - 1; e3 >= 0; e3--) r3 = r3 + (255 & s2[e3]) | 0, s2[e3] = 255 & r3, r3 >>>= 8;
    ({ s0: o2, s1: c2, s2: h2, s3: u2 } = Vr(e2, a2[0], a2[1], a2[2], a2[3]));
  }
  const f2 = Dr * Math.floor(l2.length / 4);
  if (f2 < i2) {
    const e3 = new Uint32Array([o2, c2, h2, u2]), t3 = lr(e3);
    for (let e4 = f2, s3 = 0; e4 < i2; e4++, s3++) n2[e4] = r2[e4] ^ t3[s3];
    br(e3);
  }
  return n2;
}
function Wr(e2, t2, r2, n2, i2) {
  cr(r2, Dr), cr(n2), i2 = Yr(n2.length, i2);
  const s2 = r2, a2 = yr(s2), o2 = fr(s2), c2 = yr(n2), h2 = yr(i2), u2 = t2 ? 0 : 12, l2 = n2.length;
  let y2 = o2.getUint32(u2, t2), { s0: f2, s1: g2, s2: p2, s3: d2 } = Vr(e2, a2[0], a2[1], a2[2], a2[3]);
  for (let r3 = 0; r3 + 4 <= c2.length; r3 += 4) h2[r3 + 0] = c2[r3 + 0] ^ f2, h2[r3 + 1] = c2[r3 + 1] ^ g2, h2[r3 + 2] = c2[r3 + 2] ^ p2, h2[r3 + 3] = c2[r3 + 3] ^ d2, y2 = y2 + 1 >>> 0, o2.setUint32(u2, y2, t2), { s0: f2, s1: g2, s2: p2, s3: d2 } = Vr(e2, a2[0], a2[1], a2[2], a2[3]);
  const A2 = Dr * Math.floor(c2.length / 4);
  if (A2 < l2) {
    const e3 = new Uint32Array([f2, g2, p2, d2]), t3 = lr(e3);
    for (let e4 = A2, r3 = 0; e4 < l2; e4++, r3++) i2[e4] = n2[e4] ^ t3[r3];
    br(e3);
  }
  return i2;
}
function rn(e2) {
  return null != e2 && "object" == typeof e2 && (e2 instanceof Uint32Array || "Uint32Array" === e2.constructor.name);
}
function nn(e2, t2) {
  if (cr(t2, 16), !rn(e2)) throw Error("_encryptBlock accepts result of expandKeyLE");
  const r2 = yr(t2);
  let { s0: n2, s1: i2, s2, s3: a2 } = Vr(e2, r2[0], r2[1], r2[2], r2[3]);
  return r2[0] = n2, r2[1] = i2, r2[2] = s2, r2[3] = a2, t2;
}
function sn(e2, t2) {
  if (cr(t2, 16), !rn(e2)) throw Error("_decryptBlock accepts result of expandKeyLE");
  const r2 = yr(t2);
  let { s0: n2, s1: i2, s2, s3: a2 } = Jr(e2, r2[0], r2[1], r2[2], r2[3]);
  return r2[0] = n2, r2[1] = i2, r2[2] = s2, r2[3] = a2, t2;
}
async function un(e2) {
  switch (e2) {
    case R.symmetric.aes128:
    case R.symmetric.aes192:
    case R.symmetric.aes256:
      throw Error("Not a legacy cipher");
    case R.symmetric.cast5:
    case R.symmetric.blowfish:
    case R.symmetric.twofish:
    case R.symmetric.tripledes: {
      const { legacyCiphers: t2 } = await Promise.resolve().then(function() {
        return dy;
      }), r2 = R.read(R.symmetric, e2), n2 = t2.get(r2);
      if (!n2) throw Error("Unsupported cipher algorithm");
      return n2;
    }
    default:
      throw Error("Unsupported cipher algorithm");
  }
}
function ln(e2) {
  switch (e2) {
    case R.symmetric.aes128:
    case R.symmetric.aes192:
    case R.symmetric.aes256:
    case R.symmetric.twofish:
      return 16;
    case R.symmetric.blowfish:
    case R.symmetric.cast5:
    case R.symmetric.tripledes:
      return 8;
    default:
      throw Error("Unsupported cipher");
  }
}
function yn(e2) {
  switch (e2) {
    case R.symmetric.aes128:
    case R.symmetric.blowfish:
    case R.symmetric.cast5:
      return 16;
    case R.symmetric.aes192:
    case R.symmetric.tripledes:
      return 24;
    case R.symmetric.aes256:
    case R.symmetric.twofish:
      return 32;
    default:
      throw Error("Unsupported cipher");
  }
}
function fn(e2) {
  return { keySize: yn(e2), blockSize: ln(e2) };
}
async function pn(e2, t2, r2) {
  const { keySize: n2 } = fn(e2);
  if (!M.isAES(e2) || t2.length !== n2) throw Error("Unexpected algorithm or key size");
  try {
    const e3 = await gn.importKey("raw", t2, { name: "AES-KW" }, false, ["wrapKey"]), n3 = await gn.importKey("raw", r2, { name: "HMAC", hash: "SHA-256" }, true, ["sign"]), i2 = await gn.wrapKey("raw", n3, e3, { name: "AES-KW" });
    return new Uint8Array(i2);
  } catch (e3) {
    if ("NotSupportedError" !== e3.name && (24 !== t2.length || "OperationError" !== e3.name)) throw e3;
    M.printDebugError("Browser did not support operation: " + e3.message);
  }
  return cn(t2).encrypt(r2);
}
async function dn(e2, t2, r2) {
  const { keySize: n2 } = fn(e2);
  if (!M.isAES(e2) || t2.length !== n2) throw Error("Unexpected algorithm or key size");
  let i2;
  try {
    i2 = await gn.importKey("raw", t2, { name: "AES-KW" }, false, ["unwrapKey"]);
  } catch (e3) {
    if ("NotSupportedError" !== e3.name && (24 !== t2.length || "OperationError" !== e3.name)) throw e3;
    return M.printDebugError("Browser did not support operation: " + e3.message), cn(t2).decrypt(r2);
  }
  try {
    const e3 = await gn.unwrapKey("raw", r2, i2, { name: "AES-KW" }, { name: "HMAC", hash: "SHA-256" }, true, ["sign"]);
    return new Uint8Array(await gn.exportKey("raw", e3));
  } catch (e3) {
    if ("OperationError" === e3.name) throw Error("Key Data Integrity failed");
    throw e3;
  }
}
async function wn(e2, t2, r2, n2, i2) {
  const s2 = R.read(R.webHash, e2);
  if (!s2) throw Error("Hash algo not supported with HKDF");
  const a2 = await An.importKey("raw", t2, "HKDF", false, ["deriveBits"]), o2 = await An.deriveBits({ name: "HKDF", hash: s2, salt: r2, info: n2 }, a2, 8 * i2);
  return new Uint8Array(o2);
}
async function bn(e2) {
  switch (e2) {
    case R.publicKey.x25519: {
      const e3 = fe(32), { publicKey: t2 } = We.box.keyPair.fromSecretKey(e3);
      return { A: t2, k: e3 };
    }
    case R.publicKey.x448: {
      const e3 = await M.getNobleCurve(R.publicKey.x448), t2 = e3.utils.randomPrivateKey();
      return { A: e3.getPublicKey(t2), k: t2 };
    }
    default:
      throw Error("Unsupported ECDH algorithm");
  }
}
async function kn(e2, t2, r2) {
  switch (e2) {
    case R.publicKey.x25519: {
      const { publicKey: e3 } = We.box.keyPair.fromSecretKey(r2);
      return M.equalsUint8Array(t2, e3);
    }
    case R.publicKey.x448: {
      const e3 = (await M.getNobleCurve(R.publicKey.x448)).getPublicKey(r2);
      return M.equalsUint8Array(t2, e3);
    }
    default:
      return false;
  }
}
async function En(e2, t2, r2) {
  const { ephemeralPublicKey: n2, sharedSecret: i2 } = await Bn(e2, r2), s2 = M.concatUint8Array([n2, r2, i2]);
  switch (e2) {
    case R.publicKey.x25519: {
      const e3 = R.symmetric.aes128, { keySize: r3 } = fn(e3), i3 = await wn(R.hash.sha256, s2, new Uint8Array(), mn.x25519, r3);
      return { ephemeralPublicKey: n2, wrappedKey: await pn(e3, i3, t2) };
    }
    case R.publicKey.x448: {
      const e3 = R.symmetric.aes256, { keySize: r3 } = fn(R.symmetric.aes256), i3 = await wn(R.hash.sha512, s2, new Uint8Array(), mn.x448, r3);
      return { ephemeralPublicKey: n2, wrappedKey: await pn(e3, i3, t2) };
    }
    default:
      throw Error("Unsupported ECDH algorithm");
  }
}
async function vn(e2, t2, r2, n2, i2) {
  const s2 = await Sn(e2, t2, n2, i2), a2 = M.concatUint8Array([t2, n2, s2]);
  switch (e2) {
    case R.publicKey.x25519: {
      const e3 = R.symmetric.aes128, { keySize: t3 } = fn(e3);
      return dn(e3, await wn(R.hash.sha256, a2, new Uint8Array(), mn.x25519, t3), r2);
    }
    case R.publicKey.x448: {
      const e3 = R.symmetric.aes256, { keySize: t3 } = fn(R.symmetric.aes256);
      return dn(e3, await wn(R.hash.sha512, a2, new Uint8Array(), mn.x448, t3), r2);
    }
    default:
      throw Error("Unsupported ECDH algorithm");
  }
}
function In(e2) {
  switch (e2) {
    case R.publicKey.x25519:
      return 32;
    case R.publicKey.x448:
      return 56;
    default:
      throw Error("Unsupported ECDH algorithm");
  }
}
async function Bn(e2, t2) {
  switch (e2) {
    case R.publicKey.x25519: {
      const r2 = fe(In(e2)), n2 = We.scalarMult(r2, t2);
      Kn(n2);
      const { publicKey: i2 } = We.box.keyPair.fromSecretKey(r2);
      return { ephemeralPublicKey: i2, sharedSecret: n2 };
    }
    case R.publicKey.x448: {
      const e3 = await M.getNobleCurve(R.publicKey.x448), r2 = e3.utils.randomPrivateKey(), n2 = e3.getSharedSecret(r2, t2);
      Kn(n2);
      return { ephemeralPublicKey: e3.getPublicKey(r2), sharedSecret: n2 };
    }
    default:
      throw Error("Unsupported ECDH algorithm");
  }
}
async function Sn(e2, t2, r2, n2) {
  switch (e2) {
    case R.publicKey.x25519: {
      const e3 = We.scalarMult(n2, t2);
      return Kn(e3), e3;
    }
    case R.publicKey.x448: {
      const e3 = (await M.getNobleCurve(R.publicKey.x448)).getSharedSecret(n2, t2);
      return Kn(e3), e3;
    }
    default:
      throw Error("Unsupported ECDH algorithm");
  }
}
function Kn(e2) {
  let t2 = 0;
  for (let r2 = 0; r2 < e2.length; r2++) t2 |= e2[r2];
  if (0 === t2) throw Error("Unexpected low order point");
}
async function Ln(e2) {
  const t2 = new Tn(e2), { oid: r2, hash: n2, cipher: i2 } = t2, s2 = await t2.genKeyPair();
  return { oid: r2, Q: s2.publicKey, secret: M.leftPad(s2.privateKey, t2.payloadSize), hash: n2, cipher: i2 };
}
function Mn(e2) {
  return Rn[e2.getName()].hash;
}
async function Nn(e2, t2, r2, n2) {
  const i2 = { [R.curve.nistP256]: true, [R.curve.nistP384]: true, [R.curve.nistP521]: true, [R.curve.secp256k1]: true, [R.curve.curve25519Legacy]: e2 === R.publicKey.ecdh, [R.curve.brainpoolP256r1]: true, [R.curve.brainpoolP384r1]: true, [R.curve.brainpoolP512r1]: true }, s2 = t2.getName();
  if (!i2[s2]) return false;
  if (s2 === R.curve.curve25519Legacy) {
    n2 = n2.slice().reverse();
    const { publicKey: e3 } = We.box.keyPair.fromSecretKey(n2);
    r2 = new Uint8Array(r2);
    const t3 = new Uint8Array([64, ...e3]);
    return !!M.equalsUint8Array(t3, r2);
  }
  const a2 = (await M.getNobleCurve(R.publicKey.ecdsa, s2)).getPublicKey(n2, false);
  return !!M.equalsUint8Array(a2, r2);
}
function Fn(e2, t2) {
  const { payloadSize: r2, wireFormatLeadingByte: n2, name: i2 } = e2, s2 = i2 === R.curve.curve25519Legacy || i2 === R.curve.ed25519Legacy ? r2 : 2 * r2;
  if (t2[0] !== n2 || t2.length !== s2 + 1) throw Error("Invalid point encoding");
}
async function On(e2) {
  const t2 = await M.getNobleCurve(R.publicKey.ecdsa, e2), r2 = t2.utils.randomPrivateKey();
  return { publicKey: t2.getPublicKey(r2, false), privateKey: r2 };
}
function Hn(e2, t2) {
  const r2 = G(e2.x), n2 = G(e2.y), i2 = new Uint8Array(r2.length + n2.length + 1);
  return i2[0] = t2, i2.set(r2, 1), i2.set(n2, r2.length + 1), i2;
}
function zn(e2, t2, r2) {
  const n2 = e2, i2 = r2.slice(1, n2 + 1), s2 = r2.slice(n2 + 1, 2 * n2 + 1);
  return { kty: "EC", crv: t2, x: _(i2), y: _(s2), ext: true };
}
function Gn(e2, t2, r2, n2) {
  const i2 = zn(e2, t2, r2);
  return i2.d = _(n2), i2;
}
async function qn(e2, t2, r2, n2, i2, s2) {
  const a2 = new Tn(e2);
  if (Fn(a2, n2), r2 && !M.isStream(r2)) {
    const e3 = { publicKey: n2, privateKey: i2 };
    switch (a2.type) {
      case "web":
        try {
          return await async function(e4, t3, r3, n3) {
            const i3 = e4.payloadSize, s3 = Gn(e4.payloadSize, Pn[e4.name], n3.publicKey, n3.privateKey), a3 = await _n.importKey("jwk", s3, { name: "ECDSA", namedCurve: Pn[e4.name], hash: { name: R.read(R.webHash, e4.hash) } }, false, ["sign"]), o3 = new Uint8Array(await _n.sign({ name: "ECDSA", namedCurve: Pn[e4.name], hash: { name: R.read(R.webHash, t3) } }, a3, r3));
            return { r: o3.slice(0, i3), s: o3.slice(i3, i3 << 1) };
          }(a2, t2, r2, e3);
        } catch (e4) {
          if ("nistP521" !== a2.name && ("DataError" === e4.name || "OperationError" === e4.name)) throw e4;
          M.printDebugError("Browser did not support signing: " + e4.message);
        }
        break;
      case "node":
        return async function(e4, t3, r3, n3) {
          const i3 = M.nodeRequire("eckey-utils"), s3 = M.getNodeBuffer(), { privateKey: a3 } = i3.generateDer({ curveName: Qn[e4.name], privateKey: s3.from(n3) }), o3 = jn.createSign(R.read(R.hash, t3));
          o3.write(r3), o3.end();
          const c2 = new Uint8Array(o3.sign({ key: a3, format: "der", type: "sec1", dsaEncoding: "ieee-p1363" })), h2 = e4.payloadSize;
          return { r: c2.subarray(0, h2), s: c2.subarray(h2, h2 << 1) };
        }(a2, t2, r2, i2);
    }
  }
  const o2 = (await M.getNobleCurve(R.publicKey.ecdsa, a2.name)).sign(s2, i2, { lowS: false });
  return { r: le(o2.r, "be", a2.payloadSize), s: le(o2.s, "be", a2.payloadSize) };
}
async function Vn(e2, t2, r2, n2, i2, s2) {
  const a2 = new Tn(e2);
  Fn(a2, i2);
  const o2 = async () => 0 === s2[0] && Jn(a2, r2, s2.subarray(1), i2);
  if (n2 && !M.isStream(n2)) switch (a2.type) {
    case "web":
      try {
        const e3 = await async function(e4, t3, { r: r3, s: n3 }, i3, s3) {
          const a3 = zn(e4.payloadSize, Pn[e4.name], s3), o3 = await _n.importKey("jwk", a3, { name: "ECDSA", namedCurve: Pn[e4.name], hash: { name: R.read(R.webHash, e4.hash) } }, false, ["verify"]), c2 = M.concatUint8Array([r3, n3]).buffer;
          return _n.verify({ name: "ECDSA", namedCurve: Pn[e4.name], hash: { name: R.read(R.webHash, t3) } }, o3, c2, i3);
        }(a2, t2, r2, n2, i2);
        return e3 || o2();
      } catch (e3) {
        if ("nistP521" !== a2.name && ("DataError" === e3.name || "OperationError" === e3.name)) throw e3;
        M.printDebugError("Browser did not support verifying: " + e3.message);
      }
      break;
    case "node": {
      const e3 = await async function(e4, t3, { r: r3, s: n3 }, i3, s3) {
        const a3 = M.nodeRequire("eckey-utils"), o3 = M.getNodeBuffer(), { publicKey: c2 } = a3.generateDer({ curveName: Qn[e4.name], publicKey: o3.from(s3) }), h2 = jn.createVerify(R.read(R.hash, t3));
        h2.write(i3), h2.end();
        const u2 = M.concatUint8Array([r3, n3]);
        try {
          return h2.verify({ key: c2, format: "der", type: "spki", dsaEncoding: "ieee-p1363" }, u2);
        } catch (e5) {
          return false;
        }
      }(a2, t2, r2, n2, i2);
      return e3 || o2();
    }
  }
  return await Jn(a2, r2, s2, i2) || o2();
}
async function Jn(e2, t2, r2, n2) {
  return (await M.getNobleCurve(R.publicKey.ecdsa, e2.name)).verify(M.concatUint8Array([t2.r, t2.s]), r2, n2, { lowS: false });
}
async function Zn(e2, t2, r2, n2, i2, s2) {
  if (Fn(new Tn(e2), n2), Re(t2) < Re(R.hash.sha256)) throw Error("Hash algorithm too weak for EdDSA.");
  const { RS: a2 } = await Xt(R.publicKey.ed25519, t2, 0, n2.subarray(1), i2, s2);
  return { r: a2.subarray(0, 32), s: a2.subarray(32) };
}
async function Wn(e2, t2, { r: r2, s: n2 }, i2, s2, a2) {
  if (Fn(new Tn(e2), s2), Re(t2) < Re(R.hash.sha256)) throw Error("Hash algorithm too weak for EdDSA.");
  const o2 = M.concatUint8Array([r2, n2]);
  return er(R.publicKey.ed25519, t2, { RS: o2 }, 0, s2.subarray(1), a2);
}
async function $n(e2, t2, r2) {
  if (e2.getName() !== R.curve.ed25519Legacy) return false;
  const { publicKey: n2 } = We.sign.keyPair.fromSeed(r2), i2 = new Uint8Array([64, ...n2]);
  return M.equalsUint8Array(t2, i2);
}
function ei(e2) {
  const t2 = e2.length;
  if (t2 > 0) {
    const r2 = e2[t2 - 1];
    if (r2 >= 1) {
      const n2 = e2.subarray(t2 - r2), i2 = new Uint8Array(r2).fill(r2);
      if (M.equalsUint8Array(n2, i2)) return e2.subarray(0, t2 - r2);
    }
  }
  throw Error("Invalid padding");
}
function ni(e2, t2, r2, n2) {
  return M.concatUint8Array([t2.write(), new Uint8Array([e2]), r2.write(), M.stringToUint8Array("Anonymous Sender    "), n2]);
}
async function ii(e2, t2, r2, n2, i2 = false, s2 = false) {
  let a2;
  if (i2) {
    for (a2 = 0; a2 < t2.length && 0 === t2[a2]; a2++) ;
    t2 = t2.subarray(a2);
  }
  if (s2) {
    for (a2 = t2.length - 1; a2 >= 0 && 0 === t2[a2]; a2--) ;
    t2 = t2.subarray(0, a2 + 1);
  }
  return (await Qe(e2, M.concatUint8Array([new Uint8Array([0, 0, 0, 1]), t2, n2]))).subarray(0, r2);
}
async function si(e2, t2) {
  switch (e2.type) {
    case "curve25519Legacy": {
      const { sharedSecret: r2, ephemeralPublicKey: n2 } = await Bn(R.publicKey.x25519, t2.subarray(1));
      return { publicKey: M.concatUint8Array([new Uint8Array([e2.wireFormatLeadingByte]), n2]), sharedKey: r2 };
    }
    case "web":
      if (e2.web && M.getWebCrypto()) try {
        return await async function(e3, t3) {
          const r2 = zn(e3.payloadSize, e3.web, t3);
          let n2 = ti.generateKey({ name: "ECDH", namedCurve: e3.web }, true, ["deriveKey", "deriveBits"]), i2 = ti.importKey("jwk", r2, { name: "ECDH", namedCurve: e3.web }, false, []);
          [n2, i2] = await Promise.all([n2, i2]);
          let s2 = ti.deriveBits({ name: "ECDH", namedCurve: e3.web, public: i2 }, n2.privateKey, e3.sharedSize), a2 = ti.exportKey("jwk", n2.publicKey);
          [s2, a2] = await Promise.all([s2, a2]);
          const o2 = new Uint8Array(s2), c2 = new Uint8Array(Hn(a2, e3.wireFormatLeadingByte));
          return { publicKey: c2, sharedKey: o2 };
        }(e2, t2);
      } catch (r2) {
        return M.printDebugError(r2), ui(e2, t2);
      }
      break;
    case "node":
      return async function(e3, t3) {
        const r2 = ri.createECDH(e3.node);
        r2.generateKeys();
        const n2 = new Uint8Array(r2.computeSecret(t3));
        return { publicKey: new Uint8Array(r2.getPublicKey()), sharedKey: n2 };
      }(e2, t2);
    default:
      return ui(e2, t2);
  }
}
async function ai(e2, t2, r2, n2, i2) {
  const s2 = function(e3) {
    const t3 = 8 - e3.length % 8, r3 = new Uint8Array(e3.length + t3).fill(t3);
    return r3.set(e3), r3;
  }(r2), a2 = new Tn(e2);
  Fn(a2, n2);
  const { publicKey: o2, sharedKey: c2 } = await si(a2, n2), h2 = ni(R.publicKey.ecdh, e2, t2, i2), { keySize: u2 } = fn(t2.cipher), l2 = await ii(t2.hash, c2, u2, h2);
  return { publicKey: o2, wrappedKey: await pn(t2.cipher, l2, s2) };
}
async function oi(e2, t2, r2, n2) {
  if (n2.length !== e2.payloadSize) {
    const t3 = new Uint8Array(e2.payloadSize);
    t3.set(n2, e2.payloadSize - n2.length), n2 = t3;
  }
  switch (e2.type) {
    case "curve25519Legacy": {
      const e3 = n2.slice().reverse();
      return { secretKey: e3, sharedKey: await Sn(R.publicKey.x25519, t2.subarray(1), r2.subarray(1), e3) };
    }
    case "web":
      if (e2.web && M.getWebCrypto()) try {
        return await async function(e3, t3, r3, n3) {
          const i2 = Gn(e3.payloadSize, e3.web, r3, n3);
          let s2 = ti.importKey("jwk", i2, { name: "ECDH", namedCurve: e3.web }, true, ["deriveKey", "deriveBits"]);
          const a2 = zn(e3.payloadSize, e3.web, t3);
          let o2 = ti.importKey("jwk", a2, { name: "ECDH", namedCurve: e3.web }, true, []);
          [s2, o2] = await Promise.all([s2, o2]);
          let c2 = ti.deriveBits({ name: "ECDH", namedCurve: e3.web, public: o2 }, s2, e3.sharedSize), h2 = ti.exportKey("jwk", s2);
          [c2, h2] = await Promise.all([c2, h2]);
          const u2 = new Uint8Array(c2);
          return { secretKey: G(h2.d), sharedKey: u2 };
        }(e2, t2, r2, n2);
      } catch (r3) {
        return M.printDebugError(r3), hi(e2, t2, n2);
      }
      break;
    case "node":
      return async function(e3, t3, r3) {
        const n3 = ri.createECDH(e3.node);
        n3.setPrivateKey(r3);
        const i2 = new Uint8Array(n3.computeSecret(t3));
        return { secretKey: new Uint8Array(n3.getPrivateKey()), sharedKey: i2 };
      }(e2, t2, n2);
    default:
      return hi(e2, t2, n2);
  }
}
async function ci(e2, t2, r2, n2, i2, s2, a2) {
  const o2 = new Tn(e2);
  Fn(o2, i2), Fn(o2, r2);
  const { sharedKey: c2 } = await oi(o2, r2, i2, s2), h2 = ni(R.publicKey.ecdh, e2, t2, a2), { keySize: u2 } = fn(t2.cipher);
  let l2;
  for (let e3 = 0; e3 < 3; e3++) try {
    const r3 = await ii(t2.hash, c2, u2, h2, 1 === e3, 2 === e3);
    return ei(await dn(t2.cipher, r3, n2));
  } catch (e4) {
    l2 = e4;
  }
  throw l2;
}
async function hi(e2, t2, r2) {
  return { secretKey: r2, sharedKey: (await M.getNobleCurve(R.publicKey.ecdh, e2.name)).getSharedSecret(r2, t2).subarray(1) };
}
async function ui(e2, t2) {
  const r2 = await M.getNobleCurve(R.publicKey.ecdh, e2.name), { publicKey: n2, privateKey: i2 } = await e2.genKeyPair();
  return { publicKey: n2, sharedKey: r2.getSharedSecret(i2, t2).subarray(1) };
}
async function Ai(e2, t2, r2, n2, i2) {
  switch (e2) {
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaEncryptSign: {
      const { n: e3, e: t3 } = r2;
      return { c: await _e(n2, e3, t3) };
    }
    case R.publicKey.elgamal: {
      const { p: e3, g: t3, y: i3 } = r2;
      return async function(e4, t4, r3, n3) {
        t4 = re(t4), r3 = re(r3), n3 = re(n3);
        const i4 = re(Le(e4, ue(t4))), s2 = ge(Ye, t4 - Ye);
        return { c1: le(ie(r3, s2, t4)), c2: le(ne(ie(n3, s2, t4) * i4, t4)) };
      }(n2, e3, t3, i3);
    }
    case R.publicKey.ecdh: {
      const { oid: e3, Q: t3, kdfParams: s2 } = r2, { publicKey: a2, wrappedKey: o2 } = await ai(e3, s2, n2, t3, i2);
      return { V: a2, C: new gi(o2) };
    }
    case R.publicKey.x25519:
    case R.publicKey.x448: {
      if (t2 && !M.isAES(t2)) throw Error("X25519 and X448 keys can only encrypt AES session keys");
      const { A: i3 } = r2, { ephemeralPublicKey: s2, wrappedKey: a2 } = await En(e2, n2, i3);
      return { ephemeralPublicKey: s2, C: di.fromObject({ algorithm: t2, wrappedKey: a2 }) };
    }
    default:
      return [];
  }
}
async function wi(e2, t2, r2, n2, i2, s2) {
  switch (e2) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaEncrypt: {
      const { c: e3 } = n2, { n: i3, e: a2 } = t2, { d: o2, p: c2, q: h2, u: u2 } = r2;
      return je(e3, i3, a2, o2, c2, h2, u2, s2);
    }
    case R.publicKey.elgamal: {
      const { c1: e3, c2: i3 } = n2;
      return async function(e4, t3, r3, n3, i4) {
        return e4 = re(e4), t3 = re(t3), r3 = re(r3), Me(le(ne(ae(ie(e4, n3 = re(n3), r3), r3) * t3, r3), "be", ue(r3)), i4);
      }(e3, i3, t2.p, r2.x, s2);
    }
    case R.publicKey.ecdh: {
      const { oid: e3, Q: s3, kdfParams: a2 } = t2, { d: o2 } = r2, { V: c2, C: h2 } = n2;
      return ci(e3, a2, c2, h2.data, s3, o2, i2);
    }
    case R.publicKey.x25519:
    case R.publicKey.x448: {
      const { A: i3 } = t2, { k: s3 } = r2, { ephemeralPublicKey: a2, C: o2 } = n2;
      if (null !== o2.algorithm && !M.isAES(o2.algorithm)) throw Error("AES session key expected");
      return vn(e2, a2, o2.wrappedKey, i3, s3);
    }
    default:
      throw Error("Unknown public key encryption algorithm.");
  }
}
function mi(e2, t2, r2) {
  let n2 = 0;
  switch (e2) {
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaSign: {
      const e3 = M.readMPI(t2.subarray(n2));
      n2 += e3.length + 2;
      const r3 = M.readMPI(t2.subarray(n2));
      n2 += r3.length + 2;
      const i2 = M.readMPI(t2.subarray(n2));
      n2 += i2.length + 2;
      const s2 = M.readMPI(t2.subarray(n2));
      return n2 += s2.length + 2, { read: n2, privateParams: { d: e3, p: r3, q: i2, u: s2 } };
    }
    case R.publicKey.dsa:
    case R.publicKey.elgamal: {
      const e3 = M.readMPI(t2.subarray(n2));
      return n2 += e3.length + 2, { read: n2, privateParams: { x: e3 } };
    }
    case R.publicKey.ecdsa:
    case R.publicKey.ecdh: {
      const i2 = Bi(e2, r2.oid);
      let s2 = M.readMPI(t2.subarray(n2));
      return n2 += s2.length + 2, s2 = M.leftPad(s2, i2), { read: n2, privateParams: { d: s2 } };
    }
    case R.publicKey.eddsaLegacy: {
      const i2 = Bi(e2, r2.oid);
      if (r2.oid.getName() !== R.curve.ed25519Legacy) throw Error("Unexpected OID for eddsaLegacy");
      let s2 = M.readMPI(t2.subarray(n2));
      return n2 += s2.length + 2, s2 = M.leftPad(s2, i2), { read: n2, privateParams: { seed: s2 } };
    }
    case R.publicKey.ed25519:
    case R.publicKey.ed448: {
      const r3 = Bi(e2), i2 = M.readExactSubarray(t2, n2, n2 + r3);
      return n2 += i2.length, { read: n2, privateParams: { seed: i2 } };
    }
    case R.publicKey.x25519:
    case R.publicKey.x448: {
      const r3 = Bi(e2), i2 = M.readExactSubarray(t2, n2, n2 + r3);
      return n2 += i2.length, { read: n2, privateParams: { k: i2 } };
    }
    default:
      throw new Yt("Unknown public key encryption algorithm.");
  }
}
function bi(e2, t2) {
  const r2 = /* @__PURE__ */ new Set([R.publicKey.ed25519, R.publicKey.x25519, R.publicKey.ed448, R.publicKey.x448]), n2 = Object.keys(t2).map((n3) => {
    const i2 = t2[n3];
    return M.isUint8Array(i2) ? r2.has(e2) ? i2 : M.uint8ArrayToMPI(i2) : i2.write();
  });
  return M.concatUint8Array(n2);
}
function ki(e2, t2, r2) {
  switch (e2) {
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaSign:
      return async function(e3, t3) {
        if (t3 = BigInt(t3), M.getWebCrypto()) {
          const r4 = { name: "RSASSA-PKCS1-v1_5", modulusLength: e3, publicExponent: le(t3), hash: { name: "SHA-1" } }, n3 = await Fe.generateKey(r4, true, ["sign", "verify"]);
          return Je(await Fe.exportKey("jwk", n3.privateKey), t3);
        }
        if (M.getNodeCrypto()) {
          const r4 = { modulusLength: e3, publicExponent: oe(t3), publicKeyEncoding: { type: "pkcs1", format: "jwk" }, privateKeyEncoding: { type: "pkcs1", format: "jwk" } }, n3 = await new Promise((e4, t4) => {
            Oe.generateKeyPair("rsa", r4, (r5, n4, i3) => {
              r5 ? t4(r5) : e4(i3);
            });
          });
          return Je(n3, t3);
        }
        let r3, n2, i2;
        do {
          n2 = de(e3 - (e3 >> 1), t3, 40), r3 = de(e3 >> 1, t3, 40), i2 = r3 * n2;
        } while (he(i2) !== e3);
        const s2 = (r3 - He) * (n2 - He);
        return n2 < r3 && ([r3, n2] = [n2, r3]), { n: le(i2), e: le(t3), d: le(ae(t3, s2)), p: le(r3), q: le(n2), u: le(ae(r3, n2)) };
      }(t2, 65537).then(({ n: e3, e: t3, d: r3, p: n2, q: i2, u: s2 }) => ({ privateParams: { d: r3, p: n2, q: i2, u: s2 }, publicParams: { n: e3, e: t3 } }));
    case R.publicKey.ecdsa:
      return Ln(r2).then(({ oid: e3, Q: t3, secret: r3 }) => ({ privateParams: { d: r3 }, publicParams: { oid: new Ht(e3), Q: t3 } }));
    case R.publicKey.eddsaLegacy:
      return Ln(r2).then(({ oid: e3, Q: t3, secret: r3 }) => ({ privateParams: { seed: r3 }, publicParams: { oid: new Ht(e3), Q: t3 } }));
    case R.publicKey.ecdh:
      return Ln(r2).then(({ oid: e3, Q: t3, secret: r3, hash: n2, cipher: i2 }) => ({ privateParams: { d: r3 }, publicParams: { oid: new Ht(e3), Q: t3, kdfParams: new pi({ hash: n2, cipher: i2 }) } }));
    case R.publicKey.ed25519:
    case R.publicKey.ed448:
      return $t(e2).then(({ A: e3, seed: t3 }) => ({ privateParams: { seed: t3 }, publicParams: { A: e3 } }));
    case R.publicKey.x25519:
    case R.publicKey.x448:
      return bn(e2).then(({ A: e3, k: t3 }) => ({ privateParams: { k: t3 }, publicParams: { A: e3 } }));
    case R.publicKey.dsa:
    case R.publicKey.elgamal:
      throw Error("Unsupported algorithm for key generation.");
    default:
      throw Error("Unknown public key algorithm.");
  }
}
async function Ei(e2, t2, r2) {
  if (!t2 || !r2) throw Error("Missing key parameters");
  switch (e2) {
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaSign: {
      const { n: e3, e: n2 } = t2, { d: i2, p: s2, q: a2, u: o2 } = r2;
      return async function(e4, t3, r3, n3, i3, s3) {
        if (e4 = re(e4), (n3 = re(n3)) * (i3 = re(i3)) !== e4) return false;
        const a3 = BigInt(2);
        if (ne(n3 * (s3 = re(s3)), i3) !== BigInt(1)) return false;
        t3 = re(t3), r3 = re(r3);
        const o3 = ge(a3, a3 << BigInt(Math.floor(he(e4) / 3))), c2 = o3 * r3 * t3;
        return !(ne(c2, n3 - He) !== o3 || ne(c2, i3 - He) !== o3);
      }(e3, n2, i2, s2, a2, o2);
    }
    case R.publicKey.dsa: {
      const { p: e3, q: n2, g: i2, y: s2 } = t2, { x: a2 } = r2;
      return async function(e4, t3, r3, n3, i3) {
        if (e4 = re(e4), t3 = re(t3), r3 = re(r3), n3 = re(n3), r3 <= fi || r3 >= e4) return false;
        if (ne(e4 - fi, t3) !== yi) return false;
        if (ie(r3, t3, e4) !== fi) return false;
        const s3 = BigInt(he(t3));
        if (s3 < BigInt(150) || !Ae(t3, null, 32)) return false;
        i3 = re(i3);
        const a3 = BigInt(2);
        return n3 === ie(r3, t3 * ge(a3 << s3 - fi, a3 << s3) + i3, e4);
      }(e3, n2, i2, s2, a2);
    }
    case R.publicKey.elgamal: {
      const { p: e3, g: n2, y: i2 } = t2, { x: s2 } = r2;
      return async function(e4, t3, r3, n3) {
        if (e4 = re(e4), t3 = re(t3), r3 = re(r3), t3 <= Ye || t3 >= e4) return false;
        const i3 = BigInt(he(e4));
        if (i3 < BigInt(1023)) return false;
        if (ie(t3, e4 - Ye, e4) !== Ye) return false;
        let s3 = t3, a2 = BigInt(1);
        const o2 = BigInt(2), c2 = o2 << BigInt(17);
        for (; a2 < c2; ) {
          if (s3 = ne(s3 * t3, e4), s3 === Ye) return false;
          a2++;
        }
        n3 = re(n3);
        const h2 = ge(o2 << i3 - Ye, o2 << i3);
        return r3 === ie(t3, (e4 - Ye) * h2 + n3, e4);
      }(e3, n2, i2, s2);
    }
    case R.publicKey.ecdsa:
    case R.publicKey.ecdh: {
      const n2 = li[R.read(R.publicKey, e2)], { oid: i2, Q: s2 } = t2, { d: a2 } = r2;
      return n2.validateParams(i2, s2, a2);
    }
    case R.publicKey.eddsaLegacy: {
      const { Q: e3, oid: n2 } = t2, { seed: i2 } = r2;
      return $n(n2, e3, i2);
    }
    case R.publicKey.ed25519:
    case R.publicKey.ed448: {
      const { A: n2 } = t2, { seed: i2 } = r2;
      return tr(e2, n2, i2);
    }
    case R.publicKey.x25519:
    case R.publicKey.x448: {
      const { A: n2 } = t2, { k: i2 } = r2;
      return kn(e2, n2, i2);
    }
    default:
      throw Error("Unknown public key algorithm.");
  }
}
function vi(e2) {
  const { keySize: t2 } = fn(e2);
  return fe(t2);
}
function Ii(e2) {
  try {
    e2.getName();
  } catch (e3) {
    throw new Yt("Unknown curve OID");
  }
}
function Bi(e2, t2) {
  switch (e2) {
    case R.publicKey.ecdsa:
    case R.publicKey.ecdh:
    case R.publicKey.eddsaLegacy:
      return new Tn(t2).payloadSize;
    case R.publicKey.ed25519:
    case R.publicKey.ed448:
      return rr(e2);
    case R.publicKey.x25519:
    case R.publicKey.x448:
      return In(e2);
    default:
      throw Error("Unknown elliptic algo");
  }
}
async function Ui(e2) {
  const { blockSize: t2 } = fn(e2), r2 = await fe(t2), n2 = new Uint8Array([r2[r2.length - 2], r2[r2.length - 1]]);
  return M.concat([r2, n2]);
}
async function Pi(e2, t2, r2, n2, i2) {
  const s2 = R.read(R.symmetric, e2);
  if (M.getNodeCrypto() && Di[s2]) return function(e3, t3, r3, n3) {
    const i3 = R.read(R.symmetric, e3), s3 = new Ki.createCipheriv(Di[i3], t3, n3);
    return k(r3, (e4) => new Uint8Array(s3.update(e4)));
  }(e2, t2, r2, n2);
  if (M.isAES(e2)) return async function(e3, t3, r3, n3) {
    if (Si && await Qi.isSupported(e3)) {
      const i3 = new Qi(e3, t3, n3);
      return M.isStream(r3) ? k(r3, (e4) => i3.encryptChunk(e4), () => i3.finish()) : i3.encrypt(r3);
    }
    if (M.isStream(r3)) {
      const i3 = new Ri(true, e3, t3, n3);
      return k(r3, (e4) => i3.processChunk(e4), () => i3.finish());
    }
    return en(t3, n3).encrypt(r3);
  }(e2, t2, r2, n2);
  const a2 = new (await un(e2))(t2), o2 = a2.blockSize, c2 = n2.slice();
  let h2 = new Uint8Array();
  const u2 = (e3) => {
    e3 && (h2 = M.concatUint8Array([h2, e3]));
    const t3 = new Uint8Array(h2.length);
    let r3, n3 = 0;
    for (; e3 ? h2.length >= o2 : h2.length; ) {
      const e4 = a2.encrypt(c2);
      for (r3 = 0; r3 < o2; r3++) c2[r3] = h2[r3] ^ e4[r3], t3[n3++] = c2[r3];
      h2 = h2.subarray(o2);
    }
    return t3.subarray(0, n3);
  };
  return k(r2, u2, u2);
}
async function xi(e2, t2, r2, n2) {
  const i2 = R.read(R.symmetric, e2);
  if (Ki && Di[i2]) return function(e3, t3, r3, n3) {
    const i3 = R.read(R.symmetric, e3), s3 = new Ki.createDecipheriv(Di[i3], t3, n3);
    return k(r3, (e4) => new Uint8Array(s3.update(e4)));
  }(e2, t2, r2, n2);
  if (M.isAES(e2)) return async function(e3, t3, r3, n3) {
    if (M.isStream(r3)) {
      const i3 = new Ri(false, e3, t3, n3);
      return k(r3, (e4) => i3.processChunk(e4), () => i3.finish());
    }
    return en(t3, n3).decrypt(r3);
  }(e2, t2, r2, n2);
  const s2 = new (await un(e2))(t2), a2 = s2.blockSize;
  let o2 = n2, c2 = new Uint8Array();
  const h2 = (e3) => {
    e3 && (c2 = M.concatUint8Array([c2, e3]));
    const t3 = new Uint8Array(c2.length);
    let r3, n3 = 0;
    for (; e3 ? c2.length >= a2 : c2.length; ) {
      const e4 = s2.encrypt(o2);
      for (o2 = c2.subarray(0, a2), r3 = 0; r3 < a2; r3++) t3[n3++] = o2[r3] ^ e4[r3];
      c2 = c2.subarray(a2);
    }
    return t3.subarray(0, n3);
  };
  return k(r2, h2, h2);
}
function Ti(e2, t2) {
  const r2 = Math.min(e2.length, t2.length);
  for (let n2 = 0; n2 < r2; n2++) e2[n2] = e2[n2] ^ t2[n2];
}
function Oi(e2, t2) {
  const r2 = e2.length - Fi;
  for (let n2 = 0; n2 < Fi; n2++) e2[n2 + r2] ^= t2[n2];
  return e2;
}
async function zi(e2) {
  const t2 = await Gi(e2), r2 = M.double(await t2(Hi)), n2 = M.double(r2);
  return async function(e3) {
    return (await t2(function(e4, t3, r3) {
      if (e4.length && e4.length % Fi == 0) return Oi(e4, t3);
      const n3 = new Uint8Array(e4.length + (Fi - e4.length % Fi));
      return n3.set(e4), n3[e4.length] = 128, Oi(n3, r3);
    }(e3, r2, n2))).subarray(-Fi);
  };
}
async function Gi(e2) {
  if (M.getNodeCrypto()) return async function(t2) {
    const r2 = new Ni.createCipheriv("aes-" + 8 * e2.length + "-cbc", e2, Hi).update(t2);
    return new Uint8Array(r2);
  };
  if (M.getWebCrypto()) try {
    return e2 = await Mi.importKey("raw", e2, { name: "AES-CBC", length: 8 * e2.length }, false, ["encrypt"]), async function(t2) {
      const r2 = await Mi.encrypt({ name: "AES-CBC", iv: Hi, length: 8 * Fi }, e2, t2);
      return new Uint8Array(r2).subarray(0, r2.byteLength - Fi);
    };
  } catch (t2) {
    if ("NotSupportedError" !== t2.name && (24 !== e2.length || "OperationError" !== t2.name)) throw t2;
    M.printDebugError("Browser did not support operation: " + t2.message);
  }
  return async function(t2) {
    return Xr(e2, Hi, { disablePadding: true }).encrypt(t2);
  };
}
async function Xi(e2) {
  const t2 = await zi(e2);
  return function(e3, r2) {
    return t2(M.concatUint8Array([e3, r2]));
  };
}
async function es(e2) {
  if (M.getNodeCrypto()) return async function(t2, r2) {
    const n2 = new ji.createCipheriv("aes-" + 8 * e2.length + "-ctr", e2, r2), i2 = qi.concat([n2.update(t2), n2.final()]);
    return new Uint8Array(i2);
  };
  if (M.getWebCrypto()) try {
    const t2 = await _i.importKey("raw", e2, { name: "AES-CTR", length: 8 * e2.length }, false, ["encrypt"]);
    return async function(e3, r2) {
      const n2 = await _i.encrypt({ name: "AES-CTR", counter: r2, length: 8 * Vi }, t2, e3);
      return new Uint8Array(n2);
    };
  } catch (t2) {
    if ("NotSupportedError" !== t2.name && (24 !== e2.length || "OperationError" !== t2.name)) throw t2;
    M.printDebugError("Browser did not support operation: " + t2.message);
  }
  return async function(t2, r2) {
    return $r(e2, r2).encrypt(t2);
  };
}
async function ts(e2, t2) {
  if (e2 !== R.symmetric.aes128 && e2 !== R.symmetric.aes192 && e2 !== R.symmetric.aes256) throw Error("EAX mode supports only AES cipher");
  const [r2, n2] = await Promise.all([Xi(t2), es(t2)]);
  return { encrypt: async function(e3, t3, i2) {
    const [s2, a2] = await Promise.all([r2(Zi, t3), r2(Wi, i2)]), o2 = await n2(e3, s2), c2 = await r2($i, o2);
    for (let e4 = 0; e4 < Yi; e4++) c2[e4] ^= a2[e4] ^ s2[e4];
    return M.concatUint8Array([o2, c2]);
  }, decrypt: async function(e3, t3, i2) {
    if (e3.length < Yi) throw Error("Invalid EAX ciphertext");
    const s2 = e3.subarray(0, -Yi), a2 = e3.subarray(-Yi), [o2, c2, h2] = await Promise.all([r2(Zi, t3), r2(Wi, i2), r2($i, s2)]), u2 = h2;
    for (let e4 = 0; e4 < Yi; e4++) u2[e4] ^= c2[e4] ^ o2[e4];
    if (!M.equalsUint8Array(a2, u2)) throw Error("Authentication tag mismatch");
    return await n2(s2, o2);
  } };
}
function ss(e2) {
  let t2 = 0;
  for (let r2 = 1; !(e2 & r2); r2 <<= 1) t2++;
  return t2;
}
function as(e2, t2) {
  for (let r2 = 0; r2 < e2.length; r2++) e2[r2] ^= t2[r2];
  return e2;
}
function os(e2, t2) {
  return as(e2.slice(), t2);
}
async function us(e2, t2) {
  const { keySize: r2 } = fn(e2);
  if (!M.isAES(e2) || t2.length !== r2) throw Error("Unexpected algorithm or key size");
  let n2 = 0;
  const i2 = Xr(t2, cs, { disablePadding: true }), s2 = (e3) => i2.encrypt(e3), a2 = (e3) => i2.decrypt(e3);
  let o2;
  function c2(e3, t3, r3, i3) {
    const a3 = t3.length / rs | 0;
    !function(e4, t4) {
      const r4 = M.nbits(Math.max(e4.length, t4.length) / rs | 0) - 1;
      for (let e5 = n2 + 1; e5 <= r4; e5++) o2[e5] = M.double(o2[e5 - 1]);
      n2 = r4;
    }(t3, i3);
    const c3 = M.concatUint8Array([cs.subarray(0, ns - r3.length), hs, r3]), h2 = 63 & c3[rs - 1];
    c3[rs - 1] &= 192;
    const u2 = s2(c3), l2 = M.concatUint8Array([u2, os(u2.subarray(0, 8), u2.subarray(1, 9))]), y2 = M.shiftRight(l2.subarray(0 + (h2 >> 3), 17 + (h2 >> 3)), 8 - (7 & h2)).subarray(1), f2 = new Uint8Array(rs), g2 = new Uint8Array(t3.length + is);
    let p2, d2 = 0;
    for (p2 = 0; p2 < a3; p2++) as(y2, o2[ss(p2 + 1)]), g2.set(as(e3(os(y2, t3)), y2), d2), as(f2, e3 === s2 ? t3 : g2.subarray(d2)), t3 = t3.subarray(rs), d2 += rs;
    if (t3.length) {
      as(y2, o2.x);
      const r4 = s2(y2);
      g2.set(os(t3, r4), d2);
      const n3 = new Uint8Array(rs);
      n3.set(e3 === s2 ? t3 : g2.subarray(d2, -is), 0), n3[t3.length] = 128, as(f2, n3), d2 += t3.length;
    }
    const A2 = as(s2(as(as(f2, y2), o2.$)), function(e4) {
      if (!e4.length) return cs;
      const t4 = e4.length / rs | 0, r4 = new Uint8Array(rs), n3 = new Uint8Array(rs);
      for (let i4 = 0; i4 < t4; i4++) as(r4, o2[ss(i4 + 1)]), as(n3, s2(os(r4, e4))), e4 = e4.subarray(rs);
      if (e4.length) {
        as(r4, o2.x);
        const t5 = new Uint8Array(rs);
        t5.set(e4, 0), t5[e4.length] = 128, as(t5, r4), as(n3, s2(t5));
      }
      return n3;
    }(i3));
    return g2.set(A2, d2), g2;
  }
  return function() {
    const e3 = s2(cs), t3 = M.double(e3);
    o2 = [], o2[0] = M.double(t3), o2.x = e3, o2.$ = t3;
  }(), { encrypt: async function(e3, t3, r3) {
    return c2(s2, e3, t3, r3);
  }, decrypt: async function(e3, t3, r3) {
    if (e3.length < is) throw Error("Invalid OCB ciphertext");
    const n3 = e3.subarray(-is);
    e3 = e3.subarray(0, -is);
    const i3 = c2(a2, e3, t3, r3);
    if (M.equalsUint8Array(n3, i3.subarray(-is))) return i3.subarray(0, -is);
    throw Error("Authentication tag mismatch");
  } };
}
async function ds(e2, t2) {
  if (e2 !== R.symmetric.aes128 && e2 !== R.symmetric.aes192 && e2 !== R.symmetric.aes256) throw Error("GCM mode supports only AES cipher");
  if (M.getNodeCrypto()) return { encrypt: async function(e3, r2, n2 = new Uint8Array()) {
    const i2 = new ys.createCipheriv("aes-" + 8 * t2.length + "-gcm", t2, r2);
    i2.setAAD(n2);
    const s2 = fs.concat([i2.update(e3), i2.final(), i2.getAuthTag()]);
    return new Uint8Array(s2);
  }, decrypt: async function(e3, r2, n2 = new Uint8Array()) {
    const i2 = new ys.createDecipheriv("aes-" + 8 * t2.length + "-gcm", t2, r2);
    i2.setAAD(n2), i2.setAuthTag(e3.slice(e3.length - gs, e3.length));
    const s2 = fs.concat([i2.update(e3.slice(0, e3.length - gs)), i2.final()]);
    return new Uint8Array(s2);
  } };
  if (M.getWebCrypto()) try {
    const e3 = await ls.importKey("raw", t2, { name: ps }, false, ["encrypt", "decrypt"]), r2 = navigator.userAgent.match(/Version\/13\.\d(\.\d)* Safari/) || navigator.userAgent.match(/Version\/(13|14)\.\d(\.\d)* Mobile\/\S* Safari/);
    return { encrypt: async function(n2, i2, s2 = new Uint8Array()) {
      if (r2 && !n2.length) return tn(t2, i2, s2).encrypt(n2);
      const a2 = await ls.encrypt({ name: ps, iv: i2, additionalData: s2, tagLength: 8 * gs }, e3, n2);
      return new Uint8Array(a2);
    }, decrypt: async function(n2, i2, s2 = new Uint8Array()) {
      if (r2 && n2.length === gs) return tn(t2, i2, s2).decrypt(n2);
      try {
        const t3 = await ls.decrypt({ name: ps, iv: i2, additionalData: s2, tagLength: 8 * gs }, e3, n2);
        return new Uint8Array(t3);
      } catch (e4) {
        if ("OperationError" === e4.name) throw Error("Authentication tag mismatch");
      }
    } };
  } catch (e3) {
    if ("NotSupportedError" !== e3.name && (24 !== t2.length || "OperationError" !== e3.name)) throw e3;
    M.printDebugError("Browser did not support operation: " + e3.message);
  }
  return { encrypt: async function(e3, r2, n2) {
    return tn(t2, r2, n2).encrypt(e3);
  }, decrypt: async function(e3, r2, n2) {
    return tn(t2, r2, n2).decrypt(e3);
  } };
}
function As(e2, t2 = false) {
  switch (e2) {
    case R.aead.eax:
      return ts;
    case R.aead.ocb:
      return us;
    case R.aead.gcm:
      return ds;
    case R.aead.experimentalGCM:
      if (!t2) throw Error("Unexpected non-standard `experimentalGCM` AEAD algorithm provided in `config.preferredAEADAlgorithm`: use `gcm` instead");
      return ds;
    default:
      throw Error("Unsupported AEAD mode");
  }
}
async function ws(e2, t2, r2, n2, i2, s2) {
  switch (e2) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaSign: {
      const { n: e3, e: a2 } = n2;
      return Ge(t2, i2, M.leftPad(r2.s, e3.length), e3, a2, s2);
    }
    case R.publicKey.dsa: {
      const { g: e3, p: t3, q: i3, y: a2 } = n2, { r: o2, s: c2 } = r2;
      return async function(e4, t4, r3, n3, i4, s3, a3, o3) {
        if (t4 = re(t4), r3 = re(r3), s3 = re(s3), a3 = re(a3), i4 = re(i4), o3 = re(o3), t4 <= yi || t4 >= a3 || r3 <= yi || r3 >= a3) return M.printDebug("invalid DSA Signature"), false;
        const c3 = ne(re(n3.subarray(0, ue(a3))), a3), h2 = ae(r3, a3);
        if (h2 === yi) return M.printDebug("invalid DSA Signature"), false;
        i4 = ne(i4, s3), o3 = ne(o3, s3);
        const u2 = ne(c3 * h2, a3), l2 = ne(t4 * h2, a3);
        return ne(ne(ie(i4, u2, s3) * ie(o3, l2, s3), s3), a3) === t4;
      }(0, o2, c2, s2, e3, t3, i3, a2);
    }
    case R.publicKey.ecdsa: {
      const { oid: e3, Q: a2 } = n2, o2 = new Tn(e3).payloadSize;
      return Vn(e3, t2, { r: M.leftPad(r2.r, o2), s: M.leftPad(r2.s, o2) }, i2, a2, s2);
    }
    case R.publicKey.eddsaLegacy: {
      const { oid: e3, Q: i3 } = n2, a2 = new Tn(e3).payloadSize;
      return Wn(e3, t2, { r: M.leftPad(r2.r, a2), s: M.leftPad(r2.s, a2) }, 0, i3, s2);
    }
    case R.publicKey.ed25519:
    case R.publicKey.ed448: {
      const { A: i3 } = n2;
      return er(e2, t2, r2, 0, i3, s2);
    }
    default:
      throw Error("Unknown signature algorithm.");
  }
}
async function ms(e2, t2, r2, n2, i2, s2) {
  if (!r2 || !n2) throw Error("Missing key parameters");
  switch (e2) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaSign: {
      const { n: e3, e: a2 } = r2, { d: o2, p: c2, q: h2, u: u2 } = n2;
      return { s: await ze(t2, i2, e3, a2, o2, c2, h2, u2, s2) };
    }
    case R.publicKey.dsa: {
      const { g: e3, p: t3, q: i3 } = r2, { x: a2 } = n2;
      return async function(e4, t4, r3, n3, i4, s3) {
        const a3 = BigInt(0);
        let o2, c2, h2, u2;
        n3 = re(n3), i4 = re(i4), r3 = re(r3), s3 = re(s3), r3 = ne(r3, n3), s3 = ne(s3, i4);
        const l2 = ne(re(t4.subarray(0, ue(i4))), i4);
        for (; ; ) {
          if (o2 = ge(fi, i4), c2 = ne(ie(r3, o2, n3), i4), c2 === a3) continue;
          const e5 = ne(s3 * c2, i4);
          if (u2 = ne(l2 + e5, i4), h2 = ne(ae(o2, i4) * u2, i4), h2 !== a3) break;
        }
        return { r: le(c2, "be", ue(n3)), s: le(h2, "be", ue(n3)) };
      }(0, s2, e3, t3, i3, a2);
    }
    case R.publicKey.elgamal:
      throw Error("Signing with Elgamal is not defined in the OpenPGP standard.");
    case R.publicKey.ecdsa: {
      const { oid: e3, Q: a2 } = r2, { d: o2 } = n2;
      return qn(e3, t2, i2, a2, o2, s2);
    }
    case R.publicKey.eddsaLegacy: {
      const { oid: e3, Q: i3 } = r2, { seed: a2 } = n2;
      return Zn(e3, t2, 0, i3, a2, s2);
    }
    case R.publicKey.ed25519:
    case R.publicKey.ed448: {
      const { A: i3 } = r2, { seed: a2 } = n2;
      return Xt(e2, t2, 0, i3, a2, s2);
    }
    default:
      throw Error("Unknown signature algorithm.");
  }
}
function Ss(e2, t2 = T) {
  switch (e2) {
    case R.s2k.argon2:
      return new vs(t2);
    case R.s2k.iterated:
    case R.s2k.gnu:
    case R.s2k.salted:
    case R.s2k.simple:
      return new Is(e2, t2);
    default:
      throw new Yt("Unsupported S2K type");
  }
}
function Ks(e2) {
  const { s2kType: t2 } = e2;
  if (!Bs.has(t2)) throw Error("The provided `config.s2kType` value is not allowed");
  return Ss(t2, e2);
}
function Sa(e2, t2, r2) {
  const n2 = [];
  return n2.push(Gt(r2.length + 1)), n2.push(new Uint8Array([(t2 ? 128 : 0) | e2])), n2.push(r2), M.concat(n2);
}
function Ka(e2) {
  switch (e2) {
    case R.hash.sha256:
      return 16;
    case R.hash.sha384:
      return 24;
    case R.hash.sha512:
      return 32;
    case R.hash.sha224:
    case R.hash.sha3_256:
      return 16;
    case R.hash.sha3_512:
      return 32;
    default:
      throw Error("Unsupported hash function");
  }
}
function Da(e2, t2) {
  if (!t2[e2]) {
    let t3;
    try {
      t3 = R.read(R.packet, e2);
    } catch (t4) {
      throw new Zt("Unknown packet type with tag: " + e2);
    }
    throw Error("Packet not allowed in this context: " + t3);
  }
  return new t2[e2]();
}
function Qa(e2, t2) {
  return (r2) => {
    if (!M.isStream(r2) || o(r2)) return U(() => C(r2).then((e3) => new Promise((r3, n3) => {
      const i3 = new t2();
      i3.ondata = (e4) => {
        r3(e4);
      };
      try {
        i3.push(e3, true);
      } catch (e4) {
        n3(e4);
      }
    })));
    if (e2) try {
      const t3 = e2();
      return r2.pipeThrough(t3);
    } catch (e3) {
      if ("TypeError" !== e3.name) throw e3;
    }
    const n2 = r2.getReader(), i2 = new t2();
    return new ReadableStream({ async start(e3) {
      for (i2.ondata = async (t3, r3) => {
        e3.enqueue(t3), r3 && e3.close();
      }; ; ) {
        const { done: e4, value: t3 } = await n2.read();
        if (e4) return void i2.push(new Uint8Array(), true);
        t3.length && i2.push(t3);
      }
    } });
  };
}
function Ra() {
  return async function(e2) {
    const { decode: t2 } = await Promise.resolve().then(function() {
      return bf;
    });
    return U(async () => t2(await C(e2)));
  };
}
async function Oa(e2, t2, r2, n2) {
  const i2 = e2 instanceof Fa && 2 === e2.version, s2 = !i2 && e2.constructor.tag === R.packet.aeadEncryptedData;
  if (!i2 && !s2) throw Error("Unexpected packet type");
  const a2 = As(e2.aeadAlgorithm, s2), o2 = "decrypt" === t2 ? a2.tagLength : 0, c2 = "encrypt" === t2 ? a2.tagLength : 0, h2 = 2 ** (e2.chunkSizeByte + 6) + o2, u2 = s2 ? 8 : 0, l2 = new ArrayBuffer(13 + u2), y2 = new Uint8Array(l2, 0, 5 + u2), f2 = new Uint8Array(l2), g2 = new DataView(l2), p2 = new Uint8Array(l2, 5, 8);
  y2.set([192 | e2.constructor.tag, e2.version, e2.cipherAlgorithm, e2.aeadAlgorithm, e2.chunkSizeByte], 0);
  let d2, A2, m2 = 0, b2 = Promise.resolve(), k2 = 0, v2 = 0;
  if (i2) {
    const { keySize: t3 } = fn(e2.cipherAlgorithm), { ivLength: n3 } = a2, i3 = new Uint8Array(l2, 0, 5), s3 = await wn(R.hash.sha256, r2, e2.salt, i3, t3 + n3);
    r2 = s3.subarray(0, t3), d2 = s3.subarray(t3), d2.fill(0, d2.length - 8), A2 = new DataView(d2.buffer, d2.byteOffset, d2.byteLength);
  } else d2 = e2.iv;
  const I2 = await a2(e2.cipherAlgorithm, r2);
  return E(n2, async (r3, n3) => {
    if ("array" !== M.isStream(r3)) {
      const t3 = new TransformStream({}, { highWaterMark: M.getHardwareConcurrency() * 2 ** (e2.chunkSizeByte + 6), size: (e3) => e3.length });
      w(t3.readable, n3), n3 = t3.writable;
    }
    const s3 = P(r3), a3 = x(n3);
    try {
      for (; ; ) {
        let e3 = await s3.readBytes(h2 + o2) || new Uint8Array();
        const r4 = e3.subarray(e3.length - o2);
        let n4, l3, w2;
        if (e3 = e3.subarray(0, e3.length - o2), i2) w2 = d2;
        else {
          w2 = d2.slice();
          for (let e4 = 0; e4 < 8; e4++) w2[d2.length - 8 + e4] ^= p2[e4];
        }
        if (!m2 || e3.length ? (s3.unshift(r4), n4 = I2[t2](e3, w2, y2), n4.catch(() => {
        }), v2 += e3.length - o2 + c2) : (g2.setInt32(5 + u2 + 4, k2), n4 = I2[t2](r4, w2, f2), n4.catch(() => {
        }), v2 += c2, l3 = true), k2 += e3.length - o2, b2 = b2.then(() => n4).then(async (e4) => {
          await a3.ready, await a3.write(e4), v2 -= e4.length;
        }).catch((e4) => a3.abort(e4)), (l3 || v2 > a3.desiredSize) && await b2, l3) {
          await a3.close();
          break;
        }
        i2 ? A2.setInt32(d2.length - 4, ++m2) : g2.setInt32(9, ++m2);
      }
    } catch (e3) {
      await a3.ready.catch(() => {
      }), await a3.abort(e3);
    }
  });
}
function _a(e2, t2, r2, n2) {
  switch (t2) {
    case R.publicKey.rsaEncrypt:
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.elgamal:
    case R.publicKey.ecdh:
      return M.concatUint8Array([new Uint8Array(6 === e2 ? [] : [r2]), n2, M.writeChecksum(n2.subarray(n2.length % 8))]);
    case R.publicKey.x25519:
    case R.publicKey.x448:
      return n2;
    default:
      throw Error("Unsupported public key algorithm");
  }
}
async function Xa(e2, t2, r2, n2, i2, s2, a2) {
  if ("argon2" === t2.type && !i2) throw Error("Using Argon2 S2K without AEAD is not allowed");
  if ("simple" === t2.type && 6 === e2) throw Error("Using Simple S2K with version 6 keys is not allowed");
  const { keySize: o2 } = fn(n2), c2 = await t2.produceKey(r2, o2);
  if (!i2 || 5 === e2 || a2) return c2;
  const h2 = M.concatUint8Array([s2, new Uint8Array([e2, n2, i2])]);
  return wn(R.hash.sha256, c2, new Uint8Array(), h2, o2);
}
async function ao({ armoredSignature: e2, binarySignature: t2, config: r2, ...n2 }) {
  r2 = { ...T, ...r2 };
  let i2 = e2 || t2;
  if (!i2) throw Error("readSignature: must pass options object containing `armoredSignature` or `binarySignature`");
  if (e2 && !M.isString(e2)) throw Error("readSignature: options.armoredSignature must be a string");
  if (t2 && !M.isUint8Array(t2)) throw Error("readSignature: options.binarySignature must be a Uint8Array");
  const s2 = Object.keys(n2);
  if (s2.length > 0) throw Error("Unknown option: " + s2.join(", "));
  if (e2) {
    const { type: e3, data: t3 } = await $(i2);
    if (e3 !== R.armor.signature) throw Error("Armored text not of type signature");
    i2 = t3;
  }
  const a2 = await Ua.fromBinary(i2, io, r2);
  return new so(a2);
}
async function oo(e2, t2) {
  const r2 = new to(e2.date, t2);
  return r2.packets = null, r2.algorithm = R.write(R.publicKey, e2.algorithm), await r2.generate(e2.rsaBits, e2.curve), await r2.computeFingerprintAndKeyID(), r2;
}
async function co(e2, t2) {
  const r2 = new $a(e2.date, t2);
  return r2.packets = null, r2.algorithm = R.write(R.publicKey, e2.algorithm), await r2.generate(e2.rsaBits, e2.curve, e2.config), await r2.computeFingerprintAndKeyID(), r2;
}
async function ho(e2, t2, r2, n2, i2 = /* @__PURE__ */ new Date(), s2) {
  let a2, o2;
  for (let c2 = e2.length - 1; c2 >= 0; c2--) try {
    (!a2 || e2[c2].created >= a2.created) && (await e2[c2].verify(t2, r2, n2, i2, void 0, s2), a2 = e2[c2]);
  } catch (e3) {
    o2 = e3;
  }
  if (!a2) throw M.wrapError(`Could not find valid ${R.read(R.signature, r2)} signature in key ${t2.getKeyID().toHex()}`.replace("certGeneric ", "self-").replace(/([a-z])([A-Z])/g, (e3, t3, r3) => t3 + " " + r3.toLowerCase()), o2);
  return a2;
}
function uo(e2, t2, r2 = /* @__PURE__ */ new Date()) {
  const n2 = M.normalizeDate(r2);
  if (null !== n2) {
    const r3 = Ao(e2, t2);
    return !(e2.created <= n2 && n2 < r3);
  }
  return false;
}
async function lo(e2, t2, r2, n2) {
  const i2 = {};
  i2.key = t2, i2.bind = e2;
  const s2 = { signatureType: R.signature.subkeyBinding };
  r2.sign ? (s2.keyFlags = [R.keyFlags.signData], s2.embeddedSignature = await fo(i2, [], e2, { signatureType: R.signature.keyBinding }, r2.date, void 0, void 0, void 0, n2)) : s2.keyFlags = [R.keyFlags.encryptCommunication | R.keyFlags.encryptStorage], r2.keyExpirationTime > 0 && (s2.keyExpirationTime = r2.keyExpirationTime, s2.keyNeverExpires = false);
  return await fo(i2, [], t2, s2, r2.date, void 0, void 0, void 0, n2);
}
async function yo(e2, t2, r2 = /* @__PURE__ */ new Date(), n2 = [], i2) {
  const s2 = R.hash.sha256, a2 = i2.preferredHashAlgorithm, o2 = await Promise.all(e2.map(async (e3, t3) => (await e3.getPrimarySelfSignature(r2, n2[t3], i2)).preferredHashAlgorithms || [])), c2 = /* @__PURE__ */ new Map();
  for (const e3 of o2) for (const t3 of e3) try {
    const e4 = R.write(R.hash, t3);
    c2.set(e4, c2.has(e4) ? c2.get(e4) + 1 : 1);
  } catch {
  }
  const h2 = (t3) => 0 === e2.length || c2.get(t3) === e2.length || t3 === s2, u2 = () => {
    if (0 === c2.size) return s2;
    const e3 = Array.from(c2.keys()).filter((e4) => h2(e4)).sort((e4, t3) => Re(e4) - Re(t3))[0];
    return Re(e3) >= Re(s2) ? e3 : s2;
  };
  if ((/* @__PURE__ */ new Set([R.publicKey.ecdsa, R.publicKey.eddsaLegacy, R.publicKey.ed25519, R.publicKey.ed448])).has(t2.algorithm)) {
    const e3 = function(e4, t3) {
      switch (e4) {
        case R.publicKey.ecdsa:
        case R.publicKey.eddsaLegacy:
          return Mn(t3);
        case R.publicKey.ed25519:
        case R.publicKey.ed448:
          return nr(e4);
        default:
          throw Error("Unknown elliptic signing algo");
      }
    }(t2.algorithm, t2.publicParams.oid), r3 = h2(a2), n3 = Re(a2) >= Re(e3);
    if (r3 && n3) return a2;
    {
      const t3 = u2();
      return Re(t3) >= Re(e3) ? t3 : e3;
    }
  }
  return h2(a2) ? a2 : u2();
}
async function fo(e2, t2, r2, n2, i2, s2, a2 = [], o2 = false, c2) {
  if (r2.isDummy()) throw Error("Cannot sign with a gnu-dummy key.");
  if (!r2.isDecrypted()) throw Error("Signing key is not decrypted.");
  const h2 = new Ba();
  return Object.assign(h2, n2), h2.publicKeyAlgorithm = r2.algorithm, h2.hashAlgorithm = await yo(t2, r2, i2, s2, c2), h2.rawNotations = [...a2], await h2.sign(r2, e2, i2, o2, c2), h2;
}
async function go(e2, t2, r2, n2 = /* @__PURE__ */ new Date(), i2) {
  (e2 = e2[r2]) && (t2[r2].length ? await Promise.all(e2.map(async function(e3) {
    e3.isExpired(n2) || i2 && !await i2(e3) || t2[r2].some(function(t3) {
      return M.equalsUint8Array(t3.writeParams(), e3.writeParams());
    }) || t2[r2].push(e3);
  })) : t2[r2] = e2);
}
async function po(e2, t2, r2, n2, i2, s2, a2 = /* @__PURE__ */ new Date(), o2) {
  s2 = s2 || e2;
  const c2 = [];
  return await Promise.all(n2.map(async function(e3) {
    try {
      if (!i2 || e3.issuerKeyID.equals(i2.issuerKeyID)) {
        const n3 = ![R.reasonForRevocation.keyRetired, R.reasonForRevocation.keySuperseded, R.reasonForRevocation.userIDInvalid].includes(e3.reasonForRevocationFlag);
        await e3.verify(s2, t2, r2, n3 ? null : a2, false, o2), c2.push(e3.issuerKeyID);
      }
    } catch (e4) {
    }
  })), i2 ? (i2.revoked = !!c2.some((e3) => e3.equals(i2.issuerKeyID)) || (i2.revoked || false), i2.revoked) : c2.length > 0;
}
function Ao(e2, t2) {
  let r2;
  return false === t2.keyNeverExpires && (r2 = e2.created.getTime() + 1e3 * t2.keyExpirationTime), r2 ? new Date(r2) : 1 / 0;
}
function wo(e2, t2 = {}) {
  switch (e2.type = e2.type || t2.type, e2.curve = e2.curve || t2.curve, e2.rsaBits = e2.rsaBits || t2.rsaBits, e2.keyExpirationTime = void 0 !== e2.keyExpirationTime ? e2.keyExpirationTime : t2.keyExpirationTime, e2.passphrase = M.isString(e2.passphrase) ? e2.passphrase : t2.passphrase, e2.date = e2.date || t2.date, e2.sign = e2.sign || false, e2.type) {
    case "ecc":
      try {
        e2.curve = R.write(R.curve, e2.curve);
      } catch (e3) {
        throw Error("Unknown curve");
      }
      e2.curve !== R.curve.ed25519Legacy && e2.curve !== R.curve.curve25519Legacy && "ed25519" !== e2.curve && "curve25519" !== e2.curve || (e2.curve = e2.sign ? R.curve.ed25519Legacy : R.curve.curve25519Legacy), e2.sign ? e2.algorithm = e2.curve === R.curve.ed25519Legacy ? R.publicKey.eddsaLegacy : R.publicKey.ecdsa : e2.algorithm = R.publicKey.ecdh;
      break;
    case "curve25519":
      e2.algorithm = e2.sign ? R.publicKey.ed25519 : R.publicKey.x25519;
      break;
    case "curve448":
      e2.algorithm = e2.sign ? R.publicKey.ed448 : R.publicKey.x448;
      break;
    case "rsa":
      e2.algorithm = R.publicKey.rsaEncryptSign;
      break;
    default:
      throw Error("Unsupported key type " + e2.type);
  }
  return e2;
}
function mo(e2, t2, r2) {
  switch (e2.algorithm) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaSign:
    case R.publicKey.dsa:
    case R.publicKey.ecdsa:
    case R.publicKey.eddsaLegacy:
    case R.publicKey.ed25519:
    case R.publicKey.ed448:
      if (!t2.keyFlags && !r2.allowMissingKeyFlags) throw Error("None of the key flags is set: consider passing `config.allowMissingKeyFlags`");
      return !t2.keyFlags || !!(t2.keyFlags[0] & R.keyFlags.signData);
    default:
      return false;
  }
}
function bo(e2, t2, r2) {
  switch (e2.algorithm) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaEncrypt:
    case R.publicKey.elgamal:
    case R.publicKey.ecdh:
    case R.publicKey.x25519:
    case R.publicKey.x448:
      if (!t2.keyFlags && !r2.allowMissingKeyFlags) throw Error("None of the key flags is set: consider passing `config.allowMissingKeyFlags`");
      return !t2.keyFlags || !!(t2.keyFlags[0] & R.keyFlags.encryptCommunication) || !!(t2.keyFlags[0] & R.keyFlags.encryptStorage);
    default:
      return false;
  }
}
function ko(e2, t2, r2) {
  if (!t2.keyFlags && !r2.allowMissingKeyFlags) throw Error("None of the key flags is set: consider passing `config.allowMissingKeyFlags`");
  switch (e2.algorithm) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaEncrypt:
    case R.publicKey.elgamal:
    case R.publicKey.ecdh:
    case R.publicKey.x25519:
    case R.publicKey.x448:
      return !(!(!t2.keyFlags || !!(t2.keyFlags[0] & R.keyFlags.signData)) || !r2.allowInsecureDecryptionWithSigningKeys) || (!t2.keyFlags || !!(t2.keyFlags[0] & R.keyFlags.encryptCommunication) || !!(t2.keyFlags[0] & R.keyFlags.encryptStorage));
    default:
      return false;
  }
}
function Eo(e2, t2) {
  const r2 = R.write(R.publicKey, e2.algorithm), n2 = e2.getAlgorithmInfo();
  if (t2.rejectPublicKeyAlgorithms.has(r2)) throw Error(n2.algorithm + " keys are considered too weak.");
  switch (r2) {
    case R.publicKey.rsaEncryptSign:
    case R.publicKey.rsaSign:
    case R.publicKey.rsaEncrypt:
      if (n2.bits < t2.minRSABits) throw Error(`RSA keys shorter than ${t2.minRSABits} bits are considered too weak.`);
      break;
    case R.publicKey.ecdsa:
    case R.publicKey.eddsaLegacy:
    case R.publicKey.ecdh:
      if (t2.rejectCurves.has(n2.curve)) throw Error(`Support for ${n2.algorithm} keys using curve ${n2.curve} is disabled.`);
  }
}
function xo(e2) {
  for (const t2 of e2) switch (t2.constructor.tag) {
    case R.packet.secretKey:
      return new Uo(e2);
    case R.packet.publicKey:
      return new Do(e2);
  }
  throw Error("No key packet found");
}
async function Qo(e2, t2, r2, n2) {
  r2.passphrase && await e2.encrypt(r2.passphrase, n2), await Promise.all(t2.map(async function(e3, t3) {
    const i3 = r2.subkeys[t3].passphrase;
    i3 && await e3.encrypt(i3, n2);
  }));
  const i2 = new Ua();
  function s2(e3, t3) {
    return [t3, ...e3.filter((e4) => e4 !== t3)];
  }
  function a2() {
    const e3 = {};
    e3.keyFlags = [R.keyFlags.certifyKeys | R.keyFlags.signData];
    const t3 = s2([R.symmetric.aes256, R.symmetric.aes128], n2.preferredSymmetricAlgorithm);
    if (e3.preferredSymmetricAlgorithms = t3, n2.aeadProtect) {
      const r3 = s2([R.aead.gcm, R.aead.eax, R.aead.ocb], n2.preferredAEADAlgorithm);
      e3.preferredCipherSuites = r3.flatMap((e4) => t3.map((t4) => [t4, e4]));
    }
    return e3.preferredHashAlgorithms = s2([R.hash.sha512, R.hash.sha256, R.hash.sha3_512, R.hash.sha3_256], n2.preferredHashAlgorithm), e3.preferredCompressionAlgorithms = s2([R.compression.uncompressed, R.compression.zlib, R.compression.zip], n2.preferredCompressionAlgorithm), e3.features = [0], e3.features[0] |= R.features.modificationDetection, n2.aeadProtect && (e3.features[0] |= R.features.seipdv2), r2.keyExpirationTime > 0 && (e3.keyExpirationTime = r2.keyExpirationTime, e3.keyNeverExpires = false), e3;
  }
  if (i2.push(e2), 6 === e2.version) {
    const t3 = { key: e2 }, s3 = a2();
    s3.signatureType = R.signature.key;
    const o3 = await fo(t3, [], e2, s3, r2.date, void 0, void 0, void 0, n2);
    i2.push(o3);
  }
  await Promise.all(r2.userIDs.map(async function(t3, i3) {
    const s3 = eo.fromObject(t3), o3 = { userID: s3, key: e2 }, c2 = 6 !== e2.version ? a2() : {};
    c2.signatureType = R.signature.certPositive, 0 === i3 && (c2.isPrimaryUserID = true);
    return { userIDPacket: s3, signaturePacket: await fo(o3, [], e2, c2, r2.date, void 0, void 0, void 0, n2) };
  })).then((e3) => {
    e3.forEach(({ userIDPacket: e4, signaturePacket: t3 }) => {
      i2.push(e4), i2.push(t3);
    });
  }), await Promise.all(t2.map(async function(t3, i3) {
    const s3 = r2.subkeys[i3];
    return { secretSubkeyPacket: t3, subkeySignaturePacket: await lo(t3, e2, s3, n2) };
  })).then((e3) => {
    e3.forEach(({ secretSubkeyPacket: e4, subkeySignaturePacket: t3 }) => {
      i2.push(e4), i2.push(t3);
    });
  });
  const o2 = { key: e2 };
  return i2.push(await fo(o2, [], e2, { signatureType: R.signature.keyRevocation, reasonForRevocationFlag: R.reasonForRevocation.noReason, reasonForRevocationString: "" }, r2.date, void 0, void 0, void 0, n2)), r2.passphrase && e2.clearPrivateParams(), await Promise.all(t2.map(async function(e3, t3) {
    r2.subkeys[t3].passphrase && e3.clearPrivateParams();
  })), new Uo(i2);
}
async function Ro({ armoredKey: e2, binaryKey: t2, config: r2, ...n2 }) {
  if (r2 = { ...T, ...r2 }, !e2 && !t2) throw Error("readKey: must pass options object containing `armoredKey` or `binaryKey`");
  if (e2 && !M.isString(e2)) throw Error("readKey: options.armoredKey must be a string");
  if (t2 && !M.isUint8Array(t2)) throw Error("readKey: options.binaryKey must be a Uint8Array");
  const i2 = Object.keys(n2);
  if (i2.length > 0) throw Error("Unknown option: " + i2.join(", "));
  let s2;
  if (e2) {
    const { type: t3, data: r3 } = await $(e2);
    if (t3 !== R.armor.publicKey && t3 !== R.armor.privateKey) throw Error("Armored text not of type key");
    s2 = r3;
  } else s2 = t2;
  const a2 = await Ua.fromBinary(s2, Po, r2), o2 = a2.indexOfTag(R.packet.publicKey, R.packet.secretKey);
  if (0 === o2.length) throw Error("No key packet found");
  return xo(a2.slice(o2[0], o2[1]));
}
async function To({ armoredKey: e2, binaryKey: t2, config: r2, ...n2 }) {
  if (r2 = { ...T, ...r2 }, !e2 && !t2) throw Error("readPrivateKey: must pass options object containing `armoredKey` or `binaryKey`");
  if (e2 && !M.isString(e2)) throw Error("readPrivateKey: options.armoredKey must be a string");
  if (t2 && !M.isUint8Array(t2)) throw Error("readPrivateKey: options.binaryKey must be a Uint8Array");
  const i2 = Object.keys(n2);
  if (i2.length > 0) throw Error("Unknown option: " + i2.join(", "));
  let s2;
  if (e2) {
    const { type: t3, data: r3 } = await $(e2);
    if (t3 !== R.armor.privateKey) throw Error("Armored text not of type private key");
    s2 = r3;
  } else s2 = t2;
  const a2 = await Ua.fromBinary(s2, Po, r2), o2 = a2.indexOfTag(R.packet.publicKey, R.packet.secretKey);
  for (let e3 = 0; e3 < o2.length; e3++) {
    if (a2[o2[e3]].constructor.tag === R.packet.publicKey) continue;
    const t3 = a2.slice(o2[e3], o2[e3 + 1]);
    return new Uo(t3);
  }
  throw Error("No secret key packet found");
}
async function Lo({ armoredKeys: e2, binaryKeys: t2, config: r2, ...n2 }) {
  r2 = { ...T, ...r2 };
  let i2 = e2 || t2;
  if (!i2) throw Error("readKeys: must pass options object containing `armoredKeys` or `binaryKeys`");
  if (e2 && !M.isString(e2)) throw Error("readKeys: options.armoredKeys must be a string");
  if (t2 && !M.isUint8Array(t2)) throw Error("readKeys: options.binaryKeys must be a Uint8Array");
  const s2 = Object.keys(n2);
  if (s2.length > 0) throw Error("Unknown option: " + s2.join(", "));
  if (e2) {
    const { type: t3, data: r3 } = await $(e2);
    if (t3 !== R.armor.publicKey && t3 !== R.armor.privateKey) throw Error("Armored text not of type key");
    i2 = r3;
  }
  const a2 = [], o2 = await Ua.fromBinary(i2, Po, r2), c2 = o2.indexOfTag(R.packet.publicKey, R.packet.secretKey);
  if (0 === c2.length) throw Error("No key packet found");
  for (let e3 = 0; e3 < c2.length; e3++) {
    const t3 = xo(o2.slice(c2[e3], c2[e3 + 1]));
    a2.push(t3);
  }
  return a2;
}
async function Mo({ armoredKeys: e2, binaryKeys: t2, config: r2 }) {
  r2 = { ...T, ...r2 };
  let n2 = e2 || t2;
  if (!n2) throw Error("readPrivateKeys: must pass options object containing `armoredKeys` or `binaryKeys`");
  if (e2 && !M.isString(e2)) throw Error("readPrivateKeys: options.armoredKeys must be a string");
  if (t2 && !M.isUint8Array(t2)) throw Error("readPrivateKeys: options.binaryKeys must be a Uint8Array");
  if (e2) {
    const { type: t3, data: r3 } = await $(e2);
    if (t3 !== R.armor.privateKey) throw Error("Armored text not of type private key");
    n2 = r3;
  }
  const i2 = [], s2 = await Ua.fromBinary(n2, Po, r2), a2 = s2.indexOfTag(R.packet.publicKey, R.packet.secretKey);
  for (let e3 = 0; e3 < a2.length; e3++) {
    if (s2[a2[e3]].constructor.tag === R.packet.publicKey) continue;
    const t3 = s2.slice(a2[e3], a2[e3 + 1]), r3 = new Uo(t3);
    i2.push(r3);
  }
  if (0 === i2.length) throw Error("No secret key packet found");
  return i2;
}
async function zo(e2, t2, r2 = [], n2 = null, i2 = [], s2 = /* @__PURE__ */ new Date(), a2 = [], o2 = [], c2 = [], h2 = false, u2 = T) {
  const l2 = new Ua(), y2 = null === e2.text ? R.signature.binary : R.signature.text;
  if (await Promise.all(t2.map(async (t3, n3) => {
    const l3 = a2[n3];
    if (!t3.isPrivate()) throw Error("Need private key for signing");
    const f2 = await t3.getSigningKey(i2[n3], s2, l3, u2);
    return fo(e2, r2.length ? r2 : [t3], f2.keyPacket, { signatureType: y2 }, s2, o2, c2, h2, u2);
  })).then((e3) => {
    l2.push(...e3);
  }), n2) {
    const e3 = n2.packets.filterByTag(R.packet.signature);
    l2.push(...e3);
  }
  return l2;
}
async function Go(e2, t2, r2, n2 = /* @__PURE__ */ new Date(), i2 = false, s2 = T) {
  return Promise.all(e2.filter(function(e3) {
    return ["text", "binary"].includes(R.read(R.signature, e3.signatureType));
  }).map(async function(e3) {
    return async function(e4, t3, r3, n3 = /* @__PURE__ */ new Date(), i3 = false, s3 = T) {
      let a2, o2;
      for (const t4 of r3) {
        const r4 = t4.getKeys(e4.issuerKeyID);
        if (r4.length > 0) {
          a2 = t4, o2 = r4[0];
          break;
        }
      }
      const c2 = e4 instanceof Ca ? e4.correspondingSig : e4, h2 = { keyID: e4.issuerKeyID, verified: (async () => {
        if (!o2) throw Error("Could not find signing key with key ID " + e4.issuerKeyID.toHex());
        await e4.verify(o2.keyPacket, e4.signatureType, t3[0], n3, i3, s3);
        const r4 = await c2;
        if (o2.getCreationTime() > r4.created) throw Error("Key is newer than the signature");
        try {
          await a2.getSigningKey(o2.getKeyID(), r4.created, void 0, s3);
        } catch (e5) {
          if (!s3.allowInsecureVerificationWithReformattedKeys || !e5.message.match(/Signature creation time is in the future/)) throw e5;
          await a2.getSigningKey(o2.getKeyID(), n3, void 0, s3);
        }
        return true;
      })(), signature: (async () => {
        const e5 = await c2, t4 = new Ua();
        return e5 && t4.push(e5), new so(t4);
      })() };
      return h2.signature.catch(() => {
      }), h2.verified.catch(() => {
      }), h2;
    }(e3, t2, r2, n2, i2, s2);
  }));
}
async function _o({ armoredMessage: e2, binaryMessage: t2, config: r2, ...n2 }) {
  r2 = { ...T, ...r2 };
  let i2 = e2 || t2;
  if (!i2) throw Error("readMessage: must pass options object containing `armoredMessage` or `binaryMessage`");
  if (e2 && !M.isString(e2) && !M.isStream(e2)) throw Error("readMessage: options.armoredMessage must be a string or stream");
  if (t2 && !M.isUint8Array(t2) && !M.isStream(t2)) throw Error("readMessage: options.binaryMessage must be a Uint8Array or stream");
  const s2 = Object.keys(n2);
  if (s2.length > 0) throw Error("Unknown option: " + s2.join(", "));
  const a2 = M.isStream(i2);
  if (e2) {
    const { type: e3, data: t3 } = await $(i2);
    if (e3 !== R.armor.message) throw Error("Armored text not of type message");
    i2 = t3;
  }
  const o2 = await Ua.fromBinary(i2, No, r2), c2 = new Ho(o2);
  return c2.fromStream = a2, c2;
}
async function jo({ text: e2, binary: t2, filename: r2, date: n2 = /* @__PURE__ */ new Date(), format: i2 = void 0 !== e2 ? "utf8" : "binary", ...s2 }) {
  const a2 = void 0 !== e2 ? e2 : t2;
  if (void 0 === a2) throw Error("createMessage: must pass options object containing `text` or `binary`");
  if (e2 && !M.isString(e2) && !M.isStream(e2)) throw Error("createMessage: options.text must be a string or stream");
  if (t2 && !M.isUint8Array(t2) && !M.isStream(t2)) throw Error("createMessage: options.binary must be a Uint8Array or stream");
  const o2 = Object.keys(s2);
  if (o2.length > 0) throw Error("Unknown option: " + o2.join(", "));
  const c2 = M.isStream(a2), h2 = new ba(n2);
  void 0 !== e2 ? h2.setText(a2, R.write(R.literal, i2)) : h2.setBytes(a2, R.write(R.literal, i2)), void 0 !== r2 && h2.setFilename(r2);
  const u2 = new Ua();
  u2.push(h2);
  const l2 = new Ho(u2);
  return l2.fromStream = c2, l2;
}
async function Jo({ cleartextMessage: e2, config: t2, ...r2 }) {
  if (t2 = { ...T, ...t2 }, !e2) throw Error("readCleartextMessage: must pass options object containing `cleartextMessage`");
  if (!M.isString(e2)) throw Error("readCleartextMessage: options.cleartextMessage must be a string");
  const n2 = Object.keys(r2);
  if (n2.length > 0) throw Error("Unknown option: " + n2.join(", "));
  const i2 = await $(e2);
  if (i2.type !== R.armor.signed) throw Error("No cleartext signed message.");
  const s2 = await Ua.fromBinary(i2.data, qo, t2);
  !function(e3, t3) {
    const r3 = function(e4) {
      const r4 = (e5) => (t4) => e5.hashAlgorithm === t4;
      for (let n4 = 0; n4 < t3.length; n4++) if (t3[n4].constructor.tag === R.packet.signature && !e4.some(r4(t3[n4]))) return false;
      return true;
    }, n3 = [];
    if (e3.forEach((e4) => {
      const t4 = e4.match(/^Hash: (.+)$/);
      if (!t4) throw Error('Only "Hash" header allowed in cleartext signed message');
      {
        const e5 = t4[1].replace(/\s/g, "").split(",").map((e6) => {
          try {
            return R.write(R.hash, e6.toLowerCase());
          } catch (t5) {
            throw Error("Unknown hash algorithm in armor header: " + e6.toLowerCase());
          }
        });
        n3.push(...e5);
      }
    }), n3.length && !r3(n3)) throw Error("Hash algorithm mismatch in armor header and signature");
  }(i2.headers, s2);
  const a2 = new so(s2);
  return new Vo(i2.text, a2);
}
async function Yo({ text: e2, ...t2 }) {
  if (!e2) throw Error("createCleartextMessage: must pass options object containing `text`");
  if (!M.isString(e2)) throw Error("createCleartextMessage: options.text must be a string");
  const r2 = Object.keys(t2);
  if (r2.length > 0) throw Error("Unknown option: " + r2.join(", "));
  return new Vo(e2);
}
async function Zo({ userIDs: e2 = [], passphrase: t2, type: r2, curve: n2, rsaBits: i2 = 4096, keyExpirationTime: s2 = 0, date: a2 = /* @__PURE__ */ new Date(), subkeys: o2 = [{}], format: c2 = "armored", config: h2, ...u2 }) {
  yc(h2 = { ...T, ...h2 }), r2 || n2 ? (r2 = r2 || "ecc", n2 = n2 || "curve25519Legacy") : (r2 = h2.v6Keys ? "curve25519" : "ecc", n2 = "curve25519Legacy"), e2 = fc(e2);
  const l2 = Object.keys(u2);
  if (l2.length > 0) throw Error("Unknown option: " + l2.join(", "));
  if (0 === e2.length && !h2.v6Keys) throw Error("UserIDs are required for V4 keys");
  if ("rsa" === r2 && i2 < h2.minRSABits) throw Error(`rsaBits should be at least ${h2.minRSABits}, got: ${i2}`);
  const y2 = { userIDs: e2, passphrase: t2, type: r2, rsaBits: i2, curve: n2, keyExpirationTime: s2, date: a2, subkeys: o2 };
  try {
    const { key: e3, revocationCertificate: t3 } = await async function(e4, t4) {
      e4.sign = true, (e4 = wo(e4)).subkeys = e4.subkeys.map((t5, r4) => wo(e4.subkeys[r4], e4));
      let r3 = [co(e4, t4)];
      r3 = r3.concat(e4.subkeys.map((e5) => oo(e5, t4)));
      const n3 = await Promise.all(r3), i3 = await Qo(n3[0], n3.slice(1), e4, t4), s3 = await i3.getRevocationCertificate(e4.date, t4);
      return i3.revocationSignatures = [], { key: i3, revocationCertificate: s3 };
    }(y2, h2);
    return e3.getKeys().forEach(({ keyPacket: e4 }) => Eo(e4, h2)), { privateKey: dc(e3, c2, h2), publicKey: dc(e3.toPublic(), c2, h2), revocationCertificate: t3 };
  } catch (e3) {
    throw M.wrapError("Error generating keypair", e3);
  }
}
async function Wo({ privateKey: e2, userIDs: t2 = [], passphrase: r2, keyExpirationTime: n2 = 0, date: i2, format: s2 = "armored", config: a2, ...o2 }) {
  yc(a2 = { ...T, ...a2 }), t2 = fc(t2);
  const c2 = Object.keys(o2);
  if (c2.length > 0) throw Error("Unknown option: " + c2.join(", "));
  if (0 === t2.length && 6 !== e2.keyPacket.version) throw Error("UserIDs are required for V4 keys");
  const h2 = { privateKey: e2, userIDs: t2, passphrase: r2, keyExpirationTime: n2, date: i2 };
  try {
    const { key: e3, revocationCertificate: t3 } = await async function(e4, t4) {
      e4 = o3(e4);
      const { privateKey: r3 } = e4;
      if (!r3.isPrivate()) throw Error("Cannot reformat a public key");
      if (r3.keyPacket.isDummy()) throw Error("Cannot reformat a gnu-dummy primary key");
      if (!r3.getKeys().every(({ keyPacket: e5 }) => e5.isDecrypted())) throw Error("Key is not decrypted");
      const n3 = r3.keyPacket;
      e4.subkeys || (e4.subkeys = await Promise.all(r3.subkeys.map(async (e5) => {
        const r4 = e5.keyPacket, i4 = { key: n3, bind: r4 }, s4 = await ho(e5.bindingSignatures, n3, R.signature.subkeyBinding, i4, null, t4).catch(() => ({}));
        return { sign: s4.keyFlags && s4.keyFlags[0] & R.keyFlags.signData };
      })));
      const i3 = r3.subkeys.map((e5) => e5.keyPacket);
      if (e4.subkeys.length !== i3.length) throw Error("Number of subkey options does not match number of subkeys");
      e4.subkeys = e4.subkeys.map((t5) => o3(t5, e4));
      const s3 = await Qo(n3, i3, e4, t4), a3 = await s3.getRevocationCertificate(e4.date, t4);
      return s3.revocationSignatures = [], { key: s3, revocationCertificate: a3 };
      function o3(e5, t5 = {}) {
        return e5.keyExpirationTime = e5.keyExpirationTime || t5.keyExpirationTime, e5.passphrase = M.isString(e5.passphrase) ? e5.passphrase : t5.passphrase, e5.date = e5.date || t5.date, e5;
      }
    }(h2, a2);
    return { privateKey: dc(e3, s2, a2), publicKey: dc(e3.toPublic(), s2, a2), revocationCertificate: t3 };
  } catch (e3) {
    throw M.wrapError("Error reformatting keypair", e3);
  }
}
async function $o({ key: e2, revocationCertificate: t2, reasonForRevocation: r2, date: n2 = /* @__PURE__ */ new Date(), format: i2 = "armored", config: s2, ...a2 }) {
  yc(s2 = { ...T, ...s2 });
  const o2 = Object.keys(a2);
  if (o2.length > 0) throw Error("Unknown option: " + o2.join(", "));
  try {
    const a3 = t2 ? await e2.applyRevocationCertificate(t2, n2, s2) : await e2.revoke(r2, n2, s2);
    return a3.isPrivate() ? { privateKey: dc(a3, i2, s2), publicKey: dc(a3.toPublic(), i2, s2) } : { privateKey: null, publicKey: dc(a3, i2, s2) };
  } catch (e3) {
    throw M.wrapError("Error revoking key", e3);
  }
}
async function Xo({ privateKey: e2, passphrase: t2, config: r2, ...n2 }) {
  yc(r2 = { ...T, ...r2 });
  const i2 = Object.keys(n2);
  if (i2.length > 0) throw Error("Unknown option: " + i2.join(", "));
  if (!e2.isPrivate()) throw Error("Cannot decrypt a public key");
  const s2 = e2.clone(true), a2 = M.isArray(t2) ? t2 : [t2];
  try {
    return await Promise.all(s2.getKeys().map((e3) => M.anyPromise(a2.map((t3) => e3.keyPacket.decrypt(t3))))), await s2.validate(r2), s2;
  } catch (e3) {
    throw s2.clearPrivateParams(), M.wrapError("Error decrypting private key", e3);
  }
}
async function ec({ privateKey: e2, passphrase: t2, config: r2, ...n2 }) {
  yc(r2 = { ...T, ...r2 });
  const i2 = Object.keys(n2);
  if (i2.length > 0) throw Error("Unknown option: " + i2.join(", "));
  if (!e2.isPrivate()) throw Error("Cannot encrypt a public key");
  const s2 = e2.clone(true), a2 = s2.getKeys(), o2 = M.isArray(t2) ? t2 : Array(a2.length).fill(t2);
  if (o2.length !== a2.length) throw Error("Invalid number of passphrases given for key encryption");
  try {
    return await Promise.all(a2.map(async (e3, t3) => {
      const { keyPacket: n3 } = e3;
      await n3.encrypt(o2[t3], r2), n3.clearPrivateParams();
    })), s2;
  } catch (e3) {
    throw s2.clearPrivateParams(), M.wrapError("Error encrypting private key", e3);
  }
}
async function tc({ message: e2, encryptionKeys: t2, signingKeys: r2, passwords: n2, sessionKey: i2, format: s2 = "armored", signature: a2 = null, wildcard: o2 = false, signingKeyIDs: c2 = [], encryptionKeyIDs: h2 = [], date: u2 = /* @__PURE__ */ new Date(), signingUserIDs: l2 = [], encryptionUserIDs: y2 = [], signatureNotations: f2 = [], config: g2, ...p2 }) {
  if (yc(g2 = { ...T, ...g2 }), cc(e2), uc(s2), t2 = fc(t2), r2 = fc(r2), n2 = fc(n2), c2 = fc(c2), h2 = fc(h2), l2 = fc(l2), y2 = fc(y2), f2 = fc(f2), p2.detached) throw Error("The `detached` option has been removed from openpgp.encrypt, separately call openpgp.sign instead. Don't forget to remove the `privateKeys` option as well.");
  if (p2.publicKeys) throw Error("The `publicKeys` option has been removed from openpgp.encrypt, pass `encryptionKeys` instead");
  if (p2.privateKeys) throw Error("The `privateKeys` option has been removed from openpgp.encrypt, pass `signingKeys` instead");
  if (void 0 !== p2.armor) throw Error("The `armor` option has been removed from openpgp.encrypt, pass `format` instead.");
  const d2 = Object.keys(p2);
  if (d2.length > 0) throw Error("Unknown option: " + d2.join(", "));
  r2 || (r2 = []);
  try {
    if ((r2.length || a2) && (e2 = await e2.sign(r2, t2, a2, c2, u2, l2, h2, f2, g2)), e2 = e2.compress(await async function(e3 = [], t3 = /* @__PURE__ */ new Date(), r3 = [], n3 = T) {
      const i3 = R.compression.uncompressed, s3 = n3.preferredCompressionAlgorithm, a3 = await Promise.all(e3.map(async function(e4, i4) {
        const a4 = (await e4.getPrimarySelfSignature(t3, r3[i4], n3)).preferredCompressionAlgorithms;
        return !!a4 && a4.indexOf(s3) >= 0;
      }));
      return a3.every(Boolean) ? s3 : i3;
    }(t2, u2, y2, g2), g2), e2 = await e2.encrypt(t2, n2, i2, o2, h2, u2, y2, g2), "object" === s2) return e2;
    const p3 = "armored" === s2 ? e2.armor(g2) : e2.write();
    return await gc(p3);
  } catch (e3) {
    throw M.wrapError("Error encrypting message", e3);
  }
}
async function rc({ message: e2, decryptionKeys: t2, passwords: r2, sessionKeys: n2, verificationKeys: i2, expectSigned: s2 = false, format: a2 = "utf8", signature: o2 = null, date: c2 = /* @__PURE__ */ new Date(), config: h2, ...u2 }) {
  if (yc(h2 = { ...T, ...h2 }), cc(e2), i2 = fc(i2), t2 = fc(t2), r2 = fc(r2), n2 = fc(n2), u2.privateKeys) throw Error("The `privateKeys` option has been removed from openpgp.decrypt, pass `decryptionKeys` instead");
  if (u2.publicKeys) throw Error("The `publicKeys` option has been removed from openpgp.decrypt, pass `verificationKeys` instead");
  const l2 = Object.keys(u2);
  if (l2.length > 0) throw Error("Unknown option: " + l2.join(", "));
  try {
    const u3 = await e2.decrypt(t2, r2, n2, c2, h2);
    i2 || (i2 = []);
    const l3 = {};
    if (l3.signatures = o2 ? await u3.verifyDetached(o2, i2, c2, h2) : await u3.verify(i2, c2, h2), l3.data = "binary" === a2 ? u3.getLiteralData() : u3.getText(), l3.filename = u3.getFilename(), pc(l3, e2), s2) {
      if (0 === i2.length) throw Error("Verification keys are required to verify message signatures");
      if (0 === l3.signatures.length) throw Error("Message is not signed");
      l3.data = A([l3.data, U(async () => (await M.anyPromise(l3.signatures.map((e3) => e3.verified)), "binary" === a2 ? new Uint8Array() : ""))]);
    }
    return l3.data = await gc(l3.data), l3;
  } catch (e3) {
    throw M.wrapError("Error decrypting message", e3);
  }
}
async function nc({ message: e2, signingKeys: t2, recipientKeys: r2 = [], format: n2 = "armored", detached: i2 = false, signingKeyIDs: s2 = [], date: a2 = /* @__PURE__ */ new Date(), signingUserIDs: o2 = [], recipientUserIDs: c2 = [], signatureNotations: h2 = [], config: u2, ...l2 }) {
  if (yc(u2 = { ...T, ...u2 }), hc(e2), uc(n2), t2 = fc(t2), s2 = fc(s2), o2 = fc(o2), r2 = fc(r2), c2 = fc(c2), h2 = fc(h2), l2.privateKeys) throw Error("The `privateKeys` option has been removed from openpgp.sign, pass `signingKeys` instead");
  if (void 0 !== l2.armor) throw Error("The `armor` option has been removed from openpgp.sign, pass `format` instead.");
  const y2 = Object.keys(l2);
  if (y2.length > 0) throw Error("Unknown option: " + y2.join(", "));
  if (e2 instanceof Vo && "binary" === n2) throw Error("Cannot return signed cleartext message in binary format");
  if (e2 instanceof Vo && i2) throw Error("Cannot detach-sign a cleartext message");
  if (!t2 || 0 === t2.length) throw Error("No signing keys provided");
  try {
    let l3;
    if (l3 = i2 ? await e2.signDetached(t2, r2, void 0, s2, a2, o2, c2, h2, u2) : await e2.sign(t2, r2, void 0, s2, a2, o2, c2, h2, u2), "object" === n2) return l3;
    return l3 = "armored" === n2 ? l3.armor(u2) : l3.write(), i2 && (l3 = E(e2.packets.write(), async (e3, t3) => {
      await Promise.all([w(l3, t3), C(e3).catch(() => {
      })]);
    })), await gc(l3);
  } catch (e3) {
    throw M.wrapError("Error signing message", e3);
  }
}
async function ic({ message: e2, verificationKeys: t2, expectSigned: r2 = false, format: n2 = "utf8", signature: i2 = null, date: s2 = /* @__PURE__ */ new Date(), config: a2, ...o2 }) {
  if (yc(a2 = { ...T, ...a2 }), hc(e2), t2 = fc(t2), o2.publicKeys) throw Error("The `publicKeys` option has been removed from openpgp.verify, pass `verificationKeys` instead");
  const c2 = Object.keys(o2);
  if (c2.length > 0) throw Error("Unknown option: " + c2.join(", "));
  if (e2 instanceof Vo && "binary" === n2) throw Error("Can't return cleartext message data as binary");
  if (e2 instanceof Vo && i2) throw Error("Can't verify detached cleartext signature");
  try {
    const o3 = {};
    if (o3.signatures = i2 ? await e2.verifyDetached(i2, t2, s2, a2) : await e2.verify(t2, s2, a2), o3.data = "binary" === n2 ? e2.getLiteralData() : e2.getText(), e2.fromStream && !i2 && pc(o3, e2), r2) {
      if (0 === o3.signatures.length) throw Error("Message is not signed");
      o3.data = A([o3.data, U(async () => (await M.anyPromise(o3.signatures.map((e3) => e3.verified)), "binary" === n2 ? new Uint8Array() : ""))]);
    }
    return o3.data = await gc(o3.data), o3;
  } catch (e3) {
    throw M.wrapError("Error verifying signed message", e3);
  }
}
async function sc({ encryptionKeys: e2, date: t2 = /* @__PURE__ */ new Date(), encryptionUserIDs: r2 = [], config: n2, ...i2 }) {
  if (yc(n2 = { ...T, ...n2 }), e2 = fc(e2), r2 = fc(r2), i2.publicKeys) throw Error("The `publicKeys` option has been removed from openpgp.generateSessionKey, pass `encryptionKeys` instead");
  const s2 = Object.keys(i2);
  if (s2.length > 0) throw Error("Unknown option: " + s2.join(", "));
  try {
    return await Ho.generateSessionKey(e2, t2, r2, n2);
  } catch (e3) {
    throw M.wrapError("Error generating session key", e3);
  }
}
async function ac({ data: e2, algorithm: t2, aeadAlgorithm: r2, encryptionKeys: n2, passwords: i2, format: s2 = "armored", wildcard: a2 = false, encryptionKeyIDs: o2 = [], date: c2 = /* @__PURE__ */ new Date(), encryptionUserIDs: h2 = [], config: u2, ...l2 }) {
  if (yc(u2 = { ...T, ...u2 }), function(e3) {
    if (!M.isUint8Array(e3)) throw Error("Parameter [data] must be of type Uint8Array");
  }(e2), function(e3, t3) {
    if (!M.isString(e3)) throw Error("Parameter [" + t3 + "] must be of type String");
  }(t2, "algorithm"), uc(s2), n2 = fc(n2), i2 = fc(i2), o2 = fc(o2), h2 = fc(h2), l2.publicKeys) throw Error("The `publicKeys` option has been removed from openpgp.encryptSessionKey, pass `encryptionKeys` instead");
  const y2 = Object.keys(l2);
  if (y2.length > 0) throw Error("Unknown option: " + y2.join(", "));
  if (!(n2 && 0 !== n2.length || i2 && 0 !== i2.length)) throw Error("No encryption keys or passwords provided.");
  try {
    return dc(await Ho.encryptSessionKey(e2, t2, r2, n2, i2, a2, o2, c2, h2, u2), s2, u2);
  } catch (e3) {
    throw M.wrapError("Error encrypting session key", e3);
  }
}
async function oc({ message: e2, decryptionKeys: t2, passwords: r2, date: n2 = /* @__PURE__ */ new Date(), config: i2, ...s2 }) {
  if (yc(i2 = { ...T, ...i2 }), cc(e2), t2 = fc(t2), r2 = fc(r2), s2.privateKeys) throw Error("The `privateKeys` option has been removed from openpgp.decryptSessionKeys, pass `decryptionKeys` instead");
  const a2 = Object.keys(s2);
  if (a2.length > 0) throw Error("Unknown option: " + a2.join(", "));
  try {
    return await e2.decryptSessionKeys(t2, r2, void 0, n2, i2);
  } catch (e3) {
    throw M.wrapError("Error decrypting session keys", e3);
  }
}
function cc(e2) {
  if (!(e2 instanceof Ho)) throw Error("Parameter [message] needs to be of type Message");
}
function hc(e2) {
  if (!(e2 instanceof Vo || e2 instanceof Ho)) throw Error("Parameter [message] needs to be of type Message or CleartextMessage");
}
function uc(e2) {
  if ("armored" !== e2 && "binary" !== e2 && "object" !== e2) throw Error("Unsupported format " + e2);
}
function yc(e2) {
  const t2 = Object.keys(e2);
  if (t2.length !== lc) {
    for (const e3 of t2) if (void 0 === T[e3]) throw Error("Unknown config property: " + e3);
  }
}
function fc(e2) {
  return e2 && !M.isArray(e2) && (e2 = [e2]), e2;
}
async function gc(e2) {
  return "array" === M.isStream(e2) ? C(e2) : e2;
}
function pc(e2, t2) {
  e2.data = E(t2.packets.stream, async (t3, r2) => {
    await w(e2.data, r2, { preventClose: true });
    const n2 = x(r2);
    try {
      await C(t3, (e3) => e3), await n2.close();
    } catch (e3) {
      await n2.abort(e3);
    }
  });
}
function dc(e2, t2, r2) {
  switch (t2) {
    case "object":
      return e2;
    case "armored":
      return e2.armor(r2);
    case "binary":
      return e2.write();
    default:
      throw Error("Unsupported format " + t2);
  }
}
function Ac(e2) {
  if (!Number.isSafeInteger(e2) || e2 < 0) throw Error("positive integer expected, not " + e2);
}
function wc(e2, ...t2) {
  if (!((r2 = e2) instanceof Uint8Array || null != r2 && "object" == typeof r2 && "Uint8Array" === r2.constructor.name)) throw Error("Uint8Array expected");
  var r2;
  if (t2.length > 0 && !t2.includes(e2.length)) throw Error(`Uint8Array expected of length ${t2}, not of length=${e2.length}`);
}
function mc(e2, t2 = true) {
  if (e2.destroyed) throw Error("Hash instance has been destroyed");
  if (t2 && e2.finished) throw Error("Hash#digest() has already been called");
}
function bc(e2, t2) {
  wc(e2);
  const r2 = t2.outputLen;
  if (e2.length < r2) throw Error("digestInto() expects output buffer of length at least " + r2);
}
function Sc(e2) {
  for (let r2 = 0; r2 < e2.length; r2++) e2[r2] = (t2 = e2[r2]) << 24 & 4278190080 | t2 << 8 & 16711680 | t2 >>> 8 & 65280 | t2 >>> 24 & 255;
  var t2;
}
function Kc(e2) {
  if ("string" != typeof e2) throw Error("utf8ToBytes expected string, got " + typeof e2);
  return new Uint8Array(new TextEncoder().encode(e2));
}
function Cc(e2) {
  return "string" == typeof e2 && (e2 = Kc(e2)), wc(e2), e2;
}
function Dc(...e2) {
  let t2 = 0;
  for (let r3 = 0; r3 < e2.length; r3++) {
    const n2 = e2[r3];
    wc(n2), t2 += n2.length;
  }
  const r2 = new Uint8Array(t2);
  for (let t3 = 0, n2 = 0; t3 < e2.length; t3++) {
    const i2 = e2[t3];
    r2.set(i2, n2), n2 += i2.length;
  }
  return r2;
}
function Pc(e2) {
  const t2 = (t3) => e2().update(Cc(t3)).digest(), r2 = e2();
  return t2.outputLen = r2.outputLen, t2.blockLen = r2.blockLen, t2.create = () => e2(), t2;
}
function xc(e2 = 32) {
  if (kc && "function" == typeof kc.getRandomValues) return kc.getRandomValues(new Uint8Array(e2));
  if (kc && "function" == typeof kc.randomBytes) return kc.randomBytes(e2);
  throw Error("crypto.getRandomValues must be defined");
}
function Jc(e2) {
  return e2 instanceof Uint8Array || null != e2 && "object" == typeof e2 && "Uint8Array" === e2.constructor.name;
}
function Yc(e2) {
  if (!Jc(e2)) throw Error("Uint8Array expected");
}
function Zc(e2, t2) {
  if ("boolean" != typeof t2) throw Error(`${e2} must be valid boolean, got "${t2}".`);
}
function $c(e2) {
  Yc(e2);
  let t2 = "";
  for (let r2 = 0; r2 < e2.length; r2++) t2 += Wc[e2[r2]];
  return t2;
}
function Xc(e2) {
  const t2 = e2.toString(16);
  return 1 & t2.length ? "0" + t2 : t2;
}
function eh(e2) {
  if ("string" != typeof e2) throw Error("hex string expected, got " + typeof e2);
  return BigInt("" === e2 ? "0" : "0x" + e2);
}
function rh(e2) {
  return e2 >= th._0 && e2 <= th._9 ? e2 - th._0 : e2 >= th._A && e2 <= th._F ? e2 - (th._A - 10) : e2 >= th._a && e2 <= th._f ? e2 - (th._a - 10) : void 0;
}
function nh(e2) {
  if ("string" != typeof e2) throw Error("hex string expected, got " + typeof e2);
  const t2 = e2.length, r2 = t2 / 2;
  if (t2 % 2) throw Error("padded hex string expected, got unpadded hex of length " + t2);
  const n2 = new Uint8Array(r2);
  for (let t3 = 0, i2 = 0; t3 < r2; t3++, i2 += 2) {
    const r3 = rh(e2.charCodeAt(i2)), s2 = rh(e2.charCodeAt(i2 + 1));
    if (void 0 === r3 || void 0 === s2) {
      const t4 = e2[i2] + e2[i2 + 1];
      throw Error('hex string expected, got non-hex character "' + t4 + '" at index ' + i2);
    }
    n2[t3] = 16 * r3 + s2;
  }
  return n2;
}
function ih(e2) {
  return eh($c(e2));
}
function sh(e2) {
  return Yc(e2), eh($c(Uint8Array.from(e2).reverse()));
}
function ah(e2, t2) {
  return nh(e2.toString(16).padStart(2 * t2, "0"));
}
function oh(e2, t2) {
  return ah(e2, t2).reverse();
}
function ch(e2, t2, r2) {
  let n2;
  if ("string" == typeof t2) try {
    n2 = nh(t2);
  } catch (r3) {
    throw Error(`${e2} must be valid hex string, got "${t2}". Cause: ${r3}`);
  }
  else {
    if (!Jc(t2)) throw Error(e2 + " must be hex string or Uint8Array");
    n2 = Uint8Array.from(t2);
  }
  const i2 = n2.length;
  if ("number" == typeof r2 && i2 !== r2) throw Error(`${e2} expected ${r2} bytes, got ${i2}`);
  return n2;
}
function hh(...e2) {
  let t2 = 0;
  for (let r3 = 0; r3 < e2.length; r3++) {
    const n2 = e2[r3];
    Yc(n2), t2 += n2.length;
  }
  const r2 = new Uint8Array(t2);
  for (let t3 = 0, n2 = 0; t3 < e2.length; t3++) {
    const i2 = e2[t3];
    r2.set(i2, n2), n2 += i2.length;
  }
  return r2;
}
function lh(e2, t2, r2) {
  return uh(e2) && uh(t2) && uh(r2) && t2 <= e2 && e2 < r2;
}
function yh(e2, t2, r2, n2) {
  if (!lh(t2, r2, n2)) throw Error(`expected valid ${e2}: ${r2} <= n < ${n2}, got ${typeof t2} ${t2}`);
}
function fh(e2) {
  let t2;
  for (t2 = 0; e2 > jc; e2 >>= qc, t2 += 1) ;
  return t2;
}
function Ah(e2, t2, r2) {
  if ("number" != typeof e2 || e2 < 2) throw Error("hashLen must be a number");
  if ("number" != typeof t2 || t2 < 2) throw Error("qByteLen must be a number");
  if ("function" != typeof r2) throw Error("hmacFn must be a function");
  let n2 = ph(e2), i2 = ph(e2), s2 = 0;
  const a2 = () => {
    n2.fill(1), i2.fill(0), s2 = 0;
  }, o2 = (...e3) => r2(i2, n2, ...e3), c2 = (e3 = ph()) => {
    i2 = o2(dh([0]), e3), n2 = o2(), 0 !== e3.length && (i2 = o2(dh([1]), e3), n2 = o2());
  }, h2 = () => {
    if (s2++ >= 1e3) throw Error("drbg: tried 1000 values");
    let e3 = 0;
    const r3 = [];
    for (; e3 < t2; ) {
      n2 = o2();
      const t3 = n2.slice();
      r3.push(t3), e3 += n2.length;
    }
    return hh(...r3);
  };
  return (e3, t3) => {
    let r3;
    for (a2(), c2(e3); !(r3 = t3(h2())); ) c2();
    return a2(), r3;
  };
}
function mh(e2, t2, r2 = {}) {
  const n2 = (t3, r3, n3) => {
    const i2 = wh[r3];
    if ("function" != typeof i2) throw Error(`Invalid validator "${r3}", expected function`);
    const s2 = e2[t3];
    if (!(n3 && void 0 === s2 || i2(s2, e2))) throw Error(`Invalid param ${t3 + ""}=${s2} (${typeof s2}), expected ${r3}`);
  };
  for (const [e3, r3] of Object.entries(t2)) n2(e3, r3, false);
  for (const [e3, t3] of Object.entries(r2)) n2(e3, t3, true);
  return e2;
}
function bh(e2) {
  const t2 = /* @__PURE__ */ new WeakMap();
  return (r2, ...n2) => {
    const i2 = t2.get(r2);
    if (void 0 !== i2) return i2;
    const s2 = e2(r2, ...n2);
    return t2.set(r2, s2), s2;
  };
}
function Dh(e2, t2) {
  const r2 = e2 % t2;
  return r2 >= Eh ? r2 : t2 + r2;
}
function Uh(e2, t2, r2) {
  if (r2 <= Eh || t2 < Eh) throw Error("Expected power/modulo > 0");
  if (r2 === vh) return Eh;
  let n2 = vh;
  for (; t2 > Eh; ) t2 & vh && (n2 = n2 * e2 % r2), e2 = e2 * e2 % r2, t2 >>= vh;
  return n2;
}
function Ph(e2, t2, r2) {
  let n2 = e2;
  for (; t2-- > Eh; ) n2 *= n2, n2 %= r2;
  return n2;
}
function xh(e2, t2) {
  if (e2 === Eh || t2 <= Eh) throw Error(`invert: expected positive integers, got n=${e2} mod=${t2}`);
  let r2 = Dh(e2, t2), n2 = t2, i2 = Eh, s2 = vh;
  for (; r2 !== Eh; ) {
    const e3 = n2 % r2, t3 = i2 - s2 * (n2 / r2);
    n2 = r2, r2 = e3, i2 = s2, s2 = t3;
  }
  if (n2 !== vh) throw Error("invert: does not exist");
  return Dh(i2, t2);
}
function Qh(e2) {
  if (e2 % Sh === Bh) {
    const t2 = (e2 + vh) / Sh;
    return function(e3, r2) {
      const n2 = e3.pow(r2, t2);
      if (!e3.eql(e3.sqr(n2), r2)) throw Error("Cannot find square root");
      return n2;
    };
  }
  if (e2 % Ch === Kh) {
    const t2 = (e2 - Kh) / Ch;
    return function(e3, r2) {
      const n2 = e3.mul(r2, Ih), i2 = e3.pow(n2, t2), s2 = e3.mul(r2, i2), a2 = e3.mul(e3.mul(s2, Ih), i2), o2 = e3.mul(s2, e3.sub(a2, e3.ONE));
      if (!e3.eql(e3.sqr(o2), r2)) throw Error("Cannot find square root");
      return o2;
    };
  }
  return function(e3) {
    const t2 = (e3 - vh) / Ih;
    let r2, n2, i2;
    for (r2 = e3 - vh, n2 = 0; r2 % Ih === Eh; r2 /= Ih, n2++) ;
    for (i2 = Ih; i2 < e3 && Uh(i2, t2, e3) !== e3 - vh; i2++) ;
    if (1 === n2) {
      const t3 = (e3 + vh) / Sh;
      return function(e4, r3) {
        const n3 = e4.pow(r3, t3);
        if (!e4.eql(e4.sqr(n3), r3)) throw Error("Cannot find square root");
        return n3;
      };
    }
    const s2 = (r2 + vh) / Ih;
    return function(e4, a2) {
      if (e4.pow(a2, t2) === e4.neg(e4.ONE)) throw Error("Cannot find square root");
      let o2 = n2, c2 = e4.pow(e4.mul(e4.ONE, i2), r2), h2 = e4.pow(a2, s2), u2 = e4.pow(a2, r2);
      for (; !e4.eql(u2, e4.ONE); ) {
        if (e4.eql(u2, e4.ZERO)) return e4.ZERO;
        let t3 = 1;
        for (let r4 = e4.sqr(u2); t3 < o2 && !e4.eql(r4, e4.ONE); t3++) r4 = e4.sqr(r4);
        const r3 = e4.pow(c2, vh << BigInt(o2 - t3 - 1));
        c2 = e4.sqr(r3), h2 = e4.mul(h2, r3), u2 = e4.mul(u2, c2), o2 = t3;
      }
      return h2;
    };
  }(e2);
}
function Th(e2, t2) {
  const r2 = void 0 !== t2 ? t2 : e2.toString(2).length;
  return { nBitLength: r2, nByteLength: Math.ceil(r2 / 8) };
}
function Lh(e2, t2, r2 = false, n2 = {}) {
  if (e2 <= Eh) throw Error("Expected Field ORDER > 0, got " + e2);
  const { nBitLength: i2, nByteLength: s2 } = Th(e2, t2);
  if (s2 > 2048) throw Error("Field lengths over 2048 bytes are not supported");
  const a2 = Qh(e2), o2 = Object.freeze({ ORDER: e2, BITS: i2, BYTES: s2, MASK: gh(i2), ZERO: Eh, ONE: vh, create: (t3) => Dh(t3, e2), isValid: (t3) => {
    if ("bigint" != typeof t3) throw Error("Invalid field element: expected bigint, got " + typeof t3);
    return Eh <= t3 && t3 < e2;
  }, is0: (e3) => e3 === Eh, isOdd: (e3) => (e3 & vh) === vh, neg: (t3) => Dh(-t3, e2), eql: (e3, t3) => e3 === t3, sqr: (t3) => Dh(t3 * t3, e2), add: (t3, r3) => Dh(t3 + r3, e2), sub: (t3, r3) => Dh(t3 - r3, e2), mul: (t3, r3) => Dh(t3 * r3, e2), pow: (e3, t3) => function(e4, t4, r3) {
    if (r3 < Eh) throw Error("Expected power > 0");
    if (r3 === Eh) return e4.ONE;
    if (r3 === vh) return t4;
    let n3 = e4.ONE, i3 = t4;
    for (; r3 > Eh; ) r3 & vh && (n3 = e4.mul(n3, i3)), i3 = e4.sqr(i3), r3 >>= vh;
    return n3;
  }(o2, e3, t3), div: (t3, r3) => Dh(t3 * xh(r3, e2), e2), sqrN: (e3) => e3 * e3, addN: (e3, t3) => e3 + t3, subN: (e3, t3) => e3 - t3, mulN: (e3, t3) => e3 * t3, inv: (t3) => xh(t3, e2), sqrt: n2.sqrt || ((e3) => a2(o2, e3)), invertBatch: (e3) => function(e4, t3) {
    const r3 = Array(t3.length), n3 = t3.reduce((t4, n4, i4) => e4.is0(n4) ? t4 : (r3[i4] = t4, e4.mul(t4, n4)), e4.ONE), i3 = e4.inv(n3);
    return t3.reduceRight((t4, n4, i4) => e4.is0(n4) ? t4 : (r3[i4] = e4.mul(t4, r3[i4]), e4.mul(t4, n4)), i3), r3;
  }(o2, e3), cmov: (e3, t3, r3) => r3 ? t3 : e3, toBytes: (e3) => r2 ? oh(e3, s2) : ah(e3, s2), fromBytes: (e3) => {
    if (e3.length !== s2) throw Error(`Fp.fromBytes: expected ${s2}, got ${e3.length}`);
    return r2 ? sh(e3) : ih(e3);
  } });
  return Object.freeze(o2);
}
function Mh(e2) {
  if ("bigint" != typeof e2) throw Error("field order must be bigint");
  const t2 = e2.toString(2).length;
  return Math.ceil(t2 / 8);
}
function Nh(e2) {
  const t2 = Mh(e2);
  return t2 + Math.ceil(t2 / 2);
}
function Gh(e2, t2) {
  const r2 = (e3, t3) => {
    const r3 = t3.negate();
    return e3 ? r3 : t3;
  }, n2 = (e3) => {
    if (!Number.isSafeInteger(e3) || e3 <= 0 || e3 > t2) throw Error(`Wrong window size=${e3}, should be [1..${t2}]`);
  }, i2 = (e3) => {
    n2(e3);
    return { windows: Math.ceil(t2 / e3) + 1, windowSize: 2 ** (e3 - 1) };
  };
  return { constTimeNegate: r2, unsafeLadder(t3, r3) {
    let n3 = e2.ZERO, i3 = t3;
    for (; r3 > Fh; ) r3 & Oh && (n3 = n3.add(i3)), i3 = i3.double(), r3 >>= Oh;
    return n3;
  }, precomputeWindow(e3, t3) {
    const { windows: r3, windowSize: n3 } = i2(t3), s2 = [];
    let a2 = e3, o2 = a2;
    for (let e4 = 0; e4 < r3; e4++) {
      o2 = a2, s2.push(o2);
      for (let e5 = 1; e5 < n3; e5++) o2 = o2.add(a2), s2.push(o2);
      a2 = o2.double();
    }
    return s2;
  }, wNAF(t3, n3, s2) {
    const { windows: a2, windowSize: o2 } = i2(t3);
    let c2 = e2.ZERO, h2 = e2.BASE;
    const u2 = BigInt(2 ** t3 - 1), l2 = 2 ** t3, y2 = BigInt(t3);
    for (let e3 = 0; e3 < a2; e3++) {
      const t4 = e3 * o2;
      let i3 = Number(s2 & u2);
      s2 >>= y2, i3 > o2 && (i3 -= l2, s2 += Oh);
      const a3 = t4, f2 = t4 + Math.abs(i3) - 1, g2 = e3 % 2 != 0, p2 = i3 < 0;
      0 === i3 ? h2 = h2.add(r2(g2, n3[a3])) : c2 = c2.add(r2(p2, n3[f2]));
    }
    return { p: c2, f: h2 };
  }, wNAFCached(e3, t3, r3) {
    const n3 = zh.get(e3) || 1;
    let i3 = Hh.get(e3);
    return i3 || (i3 = this.precomputeWindow(e3, n3), 1 !== n3 && Hh.set(e3, r3(i3))), this.wNAF(n3, i3, t3);
  }, setWindowSize(e3, t3) {
    n2(t3), zh.set(e3, t3), Hh.delete(e3);
  } };
}
function _h(e2, t2, r2, n2) {
  if (!Array.isArray(r2) || !Array.isArray(n2) || n2.length !== r2.length) throw Error("arrays of points and scalars must have equal length");
  n2.forEach((e3, r3) => {
    if (!t2.isValid(e3)) throw Error("wrong scalar at index " + r3);
  }), r2.forEach((t3, r3) => {
    if (!(t3 instanceof e2)) throw Error("wrong point at index " + r3);
  });
  const i2 = fh(BigInt(r2.length)), s2 = i2 > 12 ? i2 - 3 : i2 > 4 ? i2 - 2 : i2 ? 2 : 1, a2 = (1 << s2) - 1, o2 = Array(a2 + 1).fill(e2.ZERO), c2 = Math.floor((t2.BITS - 1) / s2) * s2;
  let h2 = e2.ZERO;
  for (let t3 = c2; t3 >= 0; t3 -= s2) {
    o2.fill(e2.ZERO);
    for (let e3 = 0; e3 < n2.length; e3++) {
      const i4 = n2[e3], s3 = Number(i4 >> BigInt(t3) & BigInt(a2));
      o2[s3] = o2[s3].add(r2[e3]);
    }
    let i3 = e2.ZERO;
    for (let t4 = o2.length - 1, r3 = e2.ZERO; t4 > 0; t4--) r3 = r3.add(o2[t4]), i3 = i3.add(r3);
    if (h2 = h2.add(i3), 0 !== t3) for (let e3 = 0; e3 < s2; e3++) h2 = h2.double();
  }
  return h2;
}
function jh(e2) {
  return mh(e2.Fp, Rh.reduce((e3, t2) => (e3[t2] = "function", e3), { ORDER: "bigint", MASK: "bigint", BYTES: "isSafeInteger", BITS: "isSafeInteger" })), mh(e2, { n: "bigint", h: "bigint", Gx: "field", Gy: "field" }, { nBitLength: "isSafeInteger", nByteLength: "isSafeInteger" }), Object.freeze({ ...Th(e2.n, e2.nBitLength), ...e2, p: e2.Fp.ORDER });
}
function qh(e2) {
  void 0 !== e2.lowS && Zc("lowS", e2.lowS), void 0 !== e2.prehash && Zc("prehash", e2.prehash);
}
function Xh(e2) {
  const t2 = function(e3) {
    const t3 = jh(e3);
    mh(t3, { a: "field", b: "field" }, { allowedPrivateKeyLengths: "array", wrapPrivateKey: "boolean", isTorsionFree: "function", clearCofactor: "function", allowInfinityPoint: "boolean", fromBytes: "function", toBytes: "function" });
    const { endo: r3, Fp: n3, a: i3 } = t3;
    if (r3) {
      if (!n3.eql(i3, n3.ZERO)) throw Error("Endomorphism can only be defined for Koblitz curves that have a=0");
      if ("object" != typeof r3 || "bigint" != typeof r3.beta || "function" != typeof r3.splitScalar) throw Error("Expected endomorphism with beta: bigint and splitScalar: function");
    }
    return Object.freeze({ ...t3 });
  }(e2), { Fp: r2 } = t2, n2 = Lh(t2.n, t2.nBitLength), i2 = t2.toBytes || ((e3, t3, n3) => {
    const i3 = t3.toAffine();
    return hh(Uint8Array.from([4]), r2.toBytes(i3.x), r2.toBytes(i3.y));
  }), s2 = t2.fromBytes || ((e3) => {
    const t3 = e3.subarray(1);
    return { x: r2.fromBytes(t3.subarray(0, r2.BYTES)), y: r2.fromBytes(t3.subarray(r2.BYTES, 2 * r2.BYTES)) };
  });
  function a2(e3) {
    const { a: n3, b: i3 } = t2, s3 = r2.sqr(e3), a3 = r2.mul(s3, e3);
    return r2.add(r2.add(a3, r2.mul(e3, n3)), i3);
  }
  if (!r2.eql(r2.sqr(t2.Gy), a2(t2.Gx))) throw Error("bad generator point: equation left != right");
  function o2(e3) {
    const { allowedPrivateKeyLengths: r3, nByteLength: n3, wrapPrivateKey: i3, n: s3 } = t2;
    if (r3 && "bigint" != typeof e3) {
      if (Jc(e3) && (e3 = $c(e3)), "string" != typeof e3 || !r3.includes(e3.length)) throw Error("Invalid key");
      e3 = e3.padStart(2 * n3, "0");
    }
    let a3;
    try {
      a3 = "bigint" == typeof e3 ? e3 : ih(ch("private key", e3, n3));
    } catch (t3) {
      throw Error(`private key must be ${n3} bytes, hex or bigint, not ${typeof e3}`);
    }
    return i3 && (a3 = Dh(a3, s3)), yh("private key", a3, Wh, s3), a3;
  }
  function c2(e3) {
    if (!(e3 instanceof l2)) throw Error("ProjectivePoint expected");
  }
  const h2 = bh((e3, t3) => {
    const { px: n3, py: i3, pz: s3 } = e3;
    if (r2.eql(s3, r2.ONE)) return { x: n3, y: i3 };
    const a3 = e3.is0();
    null == t3 && (t3 = a3 ? r2.ONE : r2.inv(s3));
    const o3 = r2.mul(n3, t3), c3 = r2.mul(i3, t3), h3 = r2.mul(s3, t3);
    if (a3) return { x: r2.ZERO, y: r2.ZERO };
    if (!r2.eql(h3, r2.ONE)) throw Error("invZ was invalid");
    return { x: o3, y: c3 };
  }), u2 = bh((e3) => {
    if (e3.is0()) {
      if (t2.allowInfinityPoint && !r2.is0(e3.py)) return;
      throw Error("bad point: ZERO");
    }
    const { x: n3, y: i3 } = e3.toAffine();
    if (!r2.isValid(n3) || !r2.isValid(i3)) throw Error("bad point: x or y not FE");
    const s3 = r2.sqr(i3), o3 = a2(n3);
    if (!r2.eql(s3, o3)) throw Error("bad point: equation left != right");
    if (!e3.isTorsionFree()) throw Error("bad point: not in prime-order subgroup");
    return true;
  });
  class l2 {
    constructor(e3, t3, n3) {
      if (this.px = e3, this.py = t3, this.pz = n3, null == e3 || !r2.isValid(e3)) throw Error("x required");
      if (null == t3 || !r2.isValid(t3)) throw Error("y required");
      if (null == n3 || !r2.isValid(n3)) throw Error("z required");
      Object.freeze(this);
    }
    static fromAffine(e3) {
      const { x: t3, y: n3 } = e3 || {};
      if (!e3 || !r2.isValid(t3) || !r2.isValid(n3)) throw Error("invalid affine point");
      if (e3 instanceof l2) throw Error("projective point not allowed");
      const i3 = (e4) => r2.eql(e4, r2.ZERO);
      return i3(t3) && i3(n3) ? l2.ZERO : new l2(t3, n3, r2.ONE);
    }
    get x() {
      return this.toAffine().x;
    }
    get y() {
      return this.toAffine().y;
    }
    static normalizeZ(e3) {
      const t3 = r2.invertBatch(e3.map((e4) => e4.pz));
      return e3.map((e4, r3) => e4.toAffine(t3[r3])).map(l2.fromAffine);
    }
    static fromHex(e3) {
      const t3 = l2.fromAffine(s2(ch("pointHex", e3)));
      return t3.assertValidity(), t3;
    }
    static fromPrivateKey(e3) {
      return l2.BASE.multiply(o2(e3));
    }
    static msm(e3, t3) {
      return _h(l2, n2, e3, t3);
    }
    _setWindowSize(e3) {
      f2.setWindowSize(this, e3);
    }
    assertValidity() {
      u2(this);
    }
    hasEvenY() {
      const { y: e3 } = this.toAffine();
      if (r2.isOdd) return !r2.isOdd(e3);
      throw Error("Field doesn't support isOdd");
    }
    equals(e3) {
      c2(e3);
      const { px: t3, py: n3, pz: i3 } = this, { px: s3, py: a3, pz: o3 } = e3, h3 = r2.eql(r2.mul(t3, o3), r2.mul(s3, i3)), u3 = r2.eql(r2.mul(n3, o3), r2.mul(a3, i3));
      return h3 && u3;
    }
    negate() {
      return new l2(this.px, r2.neg(this.py), this.pz);
    }
    double() {
      const { a: e3, b: n3 } = t2, i3 = r2.mul(n3, $h), { px: s3, py: a3, pz: o3 } = this;
      let c3 = r2.ZERO, h3 = r2.ZERO, u3 = r2.ZERO, y3 = r2.mul(s3, s3), f3 = r2.mul(a3, a3), g2 = r2.mul(o3, o3), p2 = r2.mul(s3, a3);
      return p2 = r2.add(p2, p2), u3 = r2.mul(s3, o3), u3 = r2.add(u3, u3), c3 = r2.mul(e3, u3), h3 = r2.mul(i3, g2), h3 = r2.add(c3, h3), c3 = r2.sub(f3, h3), h3 = r2.add(f3, h3), h3 = r2.mul(c3, h3), c3 = r2.mul(p2, c3), u3 = r2.mul(i3, u3), g2 = r2.mul(e3, g2), p2 = r2.sub(y3, g2), p2 = r2.mul(e3, p2), p2 = r2.add(p2, u3), u3 = r2.add(y3, y3), y3 = r2.add(u3, y3), y3 = r2.add(y3, g2), y3 = r2.mul(y3, p2), h3 = r2.add(h3, y3), g2 = r2.mul(a3, o3), g2 = r2.add(g2, g2), y3 = r2.mul(g2, p2), c3 = r2.sub(c3, y3), u3 = r2.mul(g2, f3), u3 = r2.add(u3, u3), u3 = r2.add(u3, u3), new l2(c3, h3, u3);
    }
    add(e3) {
      c2(e3);
      const { px: n3, py: i3, pz: s3 } = this, { px: a3, py: o3, pz: h3 } = e3;
      let u3 = r2.ZERO, y3 = r2.ZERO, f3 = r2.ZERO;
      const g2 = t2.a, p2 = r2.mul(t2.b, $h);
      let d2 = r2.mul(n3, a3), A2 = r2.mul(i3, o3), w2 = r2.mul(s3, h3), m2 = r2.add(n3, i3), b2 = r2.add(a3, o3);
      m2 = r2.mul(m2, b2), b2 = r2.add(d2, A2), m2 = r2.sub(m2, b2), b2 = r2.add(n3, s3);
      let k2 = r2.add(a3, h3);
      return b2 = r2.mul(b2, k2), k2 = r2.add(d2, w2), b2 = r2.sub(b2, k2), k2 = r2.add(i3, s3), u3 = r2.add(o3, h3), k2 = r2.mul(k2, u3), u3 = r2.add(A2, w2), k2 = r2.sub(k2, u3), f3 = r2.mul(g2, b2), u3 = r2.mul(p2, w2), f3 = r2.add(u3, f3), u3 = r2.sub(A2, f3), f3 = r2.add(A2, f3), y3 = r2.mul(u3, f3), A2 = r2.add(d2, d2), A2 = r2.add(A2, d2), w2 = r2.mul(g2, w2), b2 = r2.mul(p2, b2), A2 = r2.add(A2, w2), w2 = r2.sub(d2, w2), w2 = r2.mul(g2, w2), b2 = r2.add(b2, w2), d2 = r2.mul(A2, b2), y3 = r2.add(y3, d2), d2 = r2.mul(k2, b2), u3 = r2.mul(m2, u3), u3 = r2.sub(u3, d2), d2 = r2.mul(m2, A2), f3 = r2.mul(k2, f3), f3 = r2.add(f3, d2), new l2(u3, y3, f3);
    }
    subtract(e3) {
      return this.add(e3.negate());
    }
    is0() {
      return this.equals(l2.ZERO);
    }
    wNAF(e3) {
      return f2.wNAFCached(this, e3, l2.normalizeZ);
    }
    multiplyUnsafe(e3) {
      yh("scalar", e3, Zh, t2.n);
      const n3 = l2.ZERO;
      if (e3 === Zh) return n3;
      if (e3 === Wh) return this;
      const { endo: i3 } = t2;
      if (!i3) return f2.unsafeLadder(this, e3);
      let { k1neg: s3, k1: a3, k2neg: o3, k2: c3 } = i3.splitScalar(e3), h3 = n3, u3 = n3, y3 = this;
      for (; a3 > Zh || c3 > Zh; ) a3 & Wh && (h3 = h3.add(y3)), c3 & Wh && (u3 = u3.add(y3)), y3 = y3.double(), a3 >>= Wh, c3 >>= Wh;
      return s3 && (h3 = h3.negate()), o3 && (u3 = u3.negate()), u3 = new l2(r2.mul(u3.px, i3.beta), u3.py, u3.pz), h3.add(u3);
    }
    multiply(e3) {
      const { endo: n3, n: i3 } = t2;
      let s3, a3;
      if (yh("scalar", e3, Wh, i3), n3) {
        const { k1neg: t3, k1: i4, k2neg: o3, k2: c3 } = n3.splitScalar(e3);
        let { p: h3, f: u3 } = this.wNAF(i4), { p: y3, f: g2 } = this.wNAF(c3);
        h3 = f2.constTimeNegate(t3, h3), y3 = f2.constTimeNegate(o3, y3), y3 = new l2(r2.mul(y3.px, n3.beta), y3.py, y3.pz), s3 = h3.add(y3), a3 = u3.add(g2);
      } else {
        const { p: t3, f: r3 } = this.wNAF(e3);
        s3 = t3, a3 = r3;
      }
      return l2.normalizeZ([s3, a3])[0];
    }
    multiplyAndAddUnsafe(e3, t3, r3) {
      const n3 = l2.BASE, i3 = (e4, t4) => t4 !== Zh && t4 !== Wh && e4.equals(n3) ? e4.multiply(t4) : e4.multiplyUnsafe(t4), s3 = i3(this, t3).add(i3(e3, r3));
      return s3.is0() ? void 0 : s3;
    }
    toAffine(e3) {
      return h2(this, e3);
    }
    isTorsionFree() {
      const { h: e3, isTorsionFree: r3 } = t2;
      if (e3 === Wh) return true;
      if (r3) return r3(l2, this);
      throw Error("isTorsionFree() has not been declared for the elliptic curve");
    }
    clearCofactor() {
      const { h: e3, clearCofactor: r3 } = t2;
      return e3 === Wh ? this : r3 ? r3(l2, this) : this.multiplyUnsafe(t2.h);
    }
    toRawBytes(e3 = true) {
      return Zc("isCompressed", e3), this.assertValidity(), i2(l2, this, e3);
    }
    toHex(e3 = true) {
      return Zc("isCompressed", e3), $c(this.toRawBytes(e3));
    }
  }
  l2.BASE = new l2(t2.Gx, t2.Gy, r2.ONE), l2.ZERO = new l2(r2.ZERO, r2.ONE, r2.ZERO);
  const y2 = t2.nBitLength, f2 = Gh(l2, t2.endo ? Math.ceil(y2 / 2) : y2);
  return { CURVE: t2, ProjectivePoint: l2, normPrivateKeyToScalar: o2, weierstrassEquation: a2, isWithinCurveOrder: function(e3) {
    return lh(e3, Wh, t2.n);
  } };
}
function eu(e2) {
  const t2 = function(e3) {
    const t3 = jh(e3);
    return mh(t3, { hash: "hash", hmac: "function", randomBytes: "function" }, { bits2int: "function", bits2int_modN: "function", lowS: "boolean" }), Object.freeze({ lowS: true, ...t3 });
  }(e2), { Fp: r2, n: n2 } = t2, i2 = r2.BYTES + 1, s2 = 2 * r2.BYTES + 1;
  function a2(e3) {
    return Dh(e3, n2);
  }
  function o2(e3) {
    return xh(e3, n2);
  }
  const { ProjectivePoint: c2, normPrivateKeyToScalar: h2, weierstrassEquation: u2, isWithinCurveOrder: l2 } = Xh({ ...t2, toBytes(e3, t3, n3) {
    const i3 = t3.toAffine(), s3 = r2.toBytes(i3.x), a3 = hh;
    return Zc("isCompressed", n3), n3 ? a3(Uint8Array.from([t3.hasEvenY() ? 2 : 3]), s3) : a3(Uint8Array.from([4]), s3, r2.toBytes(i3.y));
  }, fromBytes(e3) {
    const t3 = e3.length, n3 = e3[0], a3 = e3.subarray(1);
    if (t3 !== i2 || 2 !== n3 && 3 !== n3) {
      if (t3 === s2 && 4 === n3) {
        return { x: r2.fromBytes(a3.subarray(0, r2.BYTES)), y: r2.fromBytes(a3.subarray(r2.BYTES, 2 * r2.BYTES)) };
      }
      throw Error(`Point of length ${t3} was invalid. Expected ${i2} compressed bytes or ${s2} uncompressed bytes`);
    }
    {
      const e4 = ih(a3);
      if (!lh(e4, Wh, r2.ORDER)) throw Error("Point is not on curve");
      const t4 = u2(e4);
      let i3;
      try {
        i3 = r2.sqrt(t4);
      } catch (e5) {
        const t5 = e5 instanceof Error ? ": " + e5.message : "";
        throw Error("Point is not on curve" + t5);
      }
      return !(1 & ~n3) !== ((i3 & Wh) === Wh) && (i3 = r2.neg(i3)), { x: e4, y: i3 };
    }
  } }), y2 = (e3) => $c(ah(e3, t2.nByteLength));
  function f2(e3) {
    return e3 > n2 >> Wh;
  }
  const g2 = (e3, t3, r3) => ih(e3.slice(t3, r3));
  class p2 {
    constructor(e3, t3, r3) {
      this.r = e3, this.s = t3, this.recovery = r3, this.assertValidity();
    }
    static fromCompact(e3) {
      const r3 = t2.nByteLength;
      return e3 = ch("compactSignature", e3, 2 * r3), new p2(g2(e3, 0, r3), g2(e3, r3, 2 * r3));
    }
    static fromDER(e3) {
      const { r: t3, s: r3 } = Yh.toSig(ch("DER", e3));
      return new p2(t3, r3);
    }
    assertValidity() {
      yh("r", this.r, Wh, n2), yh("s", this.s, Wh, n2);
    }
    addRecoveryBit(e3) {
      return new p2(this.r, this.s, e3);
    }
    recoverPublicKey(e3) {
      const { r: n3, s: i3, recovery: s3 } = this, h3 = m2(ch("msgHash", e3));
      if (null == s3 || ![0, 1, 2, 3].includes(s3)) throw Error("recovery id invalid");
      const u3 = 2 === s3 || 3 === s3 ? n3 + t2.n : n3;
      if (u3 >= r2.ORDER) throw Error("recovery id 2 or 3 invalid");
      const l3 = 1 & s3 ? "03" : "02", f3 = c2.fromHex(l3 + y2(u3)), g3 = o2(u3), p3 = a2(-h3 * g3), d3 = a2(i3 * g3), A3 = c2.BASE.multiplyAndAddUnsafe(f3, p3, d3);
      if (!A3) throw Error("point at infinify");
      return A3.assertValidity(), A3;
    }
    hasHighS() {
      return f2(this.s);
    }
    normalizeS() {
      return this.hasHighS() ? new p2(this.r, a2(-this.s), this.recovery) : this;
    }
    toDERRawBytes() {
      return nh(this.toDERHex());
    }
    toDERHex() {
      return Yh.hexFromSig({ r: this.r, s: this.s });
    }
    toCompactRawBytes() {
      return nh(this.toCompactHex());
    }
    toCompactHex() {
      return y2(this.r) + y2(this.s);
    }
  }
  const d2 = { isValidPrivateKey(e3) {
    try {
      return h2(e3), true;
    } catch (e4) {
      return false;
    }
  }, normPrivateKeyToScalar: h2, randomPrivateKey: () => {
    const e3 = Nh(t2.n);
    return function(e4, t3, r3 = false) {
      const n3 = e4.length, i3 = Mh(t3), s3 = Nh(t3);
      if (n3 < 16 || n3 < s3 || n3 > 1024) throw Error(`expected ${s3}-1024 bytes of input, got ${n3}`);
      const a3 = Dh(r3 ? ih(e4) : sh(e4), t3 - vh) + vh;
      return r3 ? oh(a3, i3) : ah(a3, i3);
    }(t2.randomBytes(e3), t2.n);
  }, precompute: (e3 = 8, t3 = c2.BASE) => (t3._setWindowSize(e3), t3.multiply(BigInt(3)), t3) };
  function A2(e3) {
    const t3 = Jc(e3), r3 = "string" == typeof e3, n3 = (t3 || r3) && e3.length;
    return t3 ? n3 === i2 || n3 === s2 : r3 ? n3 === 2 * i2 || n3 === 2 * s2 : e3 instanceof c2;
  }
  const w2 = t2.bits2int || function(e3) {
    const r3 = ih(e3), n3 = 8 * e3.length - t2.nBitLength;
    return n3 > 0 ? r3 >> BigInt(n3) : r3;
  }, m2 = t2.bits2int_modN || function(e3) {
    return a2(w2(e3));
  }, b2 = gh(t2.nBitLength);
  function k2(e3) {
    return yh("num < 2^" + t2.nBitLength, e3, Zh, b2), ah(e3, t2.nByteLength);
  }
  function E2(e3, n3, i3 = v2) {
    if (["recovered", "canonical"].some((e4) => e4 in i3)) throw Error("sign() legacy options not supported");
    const { hash: s3, randomBytes: u3 } = t2;
    let { lowS: y3, prehash: g3, extraEntropy: d3 } = i3;
    null == y3 && (y3 = true), e3 = ch("msgHash", e3), qh(i3), g3 && (e3 = ch("prehashed msgHash", s3(e3)));
    const A3 = m2(e3), b3 = h2(n3), E3 = [k2(b3), k2(A3)];
    if (null != d3 && false !== d3) {
      const e4 = true === d3 ? u3(r2.BYTES) : d3;
      E3.push(ch("extraEntropy", e4));
    }
    const I3 = hh(...E3), B2 = A3;
    return { seed: I3, k2sig: function(e4) {
      const t3 = w2(e4);
      if (!l2(t3)) return;
      const r3 = o2(t3), n4 = c2.BASE.multiply(t3).toAffine(), i4 = a2(n4.x);
      if (i4 === Zh) return;
      const s4 = a2(r3 * a2(B2 + i4 * b3));
      if (s4 === Zh) return;
      let h3 = (n4.x === i4 ? 0 : 2) | Number(n4.y & Wh), u4 = s4;
      return y3 && f2(s4) && (u4 = function(e5) {
        return f2(e5) ? a2(-e5) : e5;
      }(s4), h3 ^= 1), new p2(i4, u4, h3);
    } };
  }
  const v2 = { lowS: t2.lowS, prehash: false }, I2 = { lowS: t2.lowS, prehash: false };
  return c2.BASE._setWindowSize(8), { CURVE: t2, getPublicKey: function(e3, t3 = true) {
    return c2.fromPrivateKey(e3).toRawBytes(t3);
  }, getSharedSecret: function(e3, t3, r3 = true) {
    if (A2(e3)) throw Error("first arg must be private key");
    if (!A2(t3)) throw Error("second arg must be public key");
    return c2.fromHex(t3).multiply(h2(e3)).toRawBytes(r3);
  }, sign: function(e3, r3, n3 = v2) {
    const { seed: i3, k2sig: s3 } = E2(e3, r3, n3), a3 = t2;
    return Ah(a3.hash.outputLen, a3.nByteLength, a3.hmac)(i3, s3);
  }, verify: function(e3, r3, n3, i3 = I2) {
    const s3 = e3;
    if (r3 = ch("msgHash", r3), n3 = ch("publicKey", n3), "strict" in i3) throw Error("options.strict was renamed to lowS");
    qh(i3);
    const { lowS: h3, prehash: u3 } = i3;
    let l3, y3;
    try {
      if ("string" == typeof s3 || Jc(s3)) try {
        l3 = p2.fromDER(s3);
      } catch (e4) {
        if (!(e4 instanceof Yh.Err)) throw e4;
        l3 = p2.fromCompact(s3);
      }
      else {
        if ("object" != typeof s3 || "bigint" != typeof s3.r || "bigint" != typeof s3.s) throw Error("PARSE");
        {
          const { r: e4, s: t3 } = s3;
          l3 = new p2(e4, t3);
        }
      }
      y3 = c2.fromHex(n3);
    } catch (e4) {
      if ("PARSE" === e4.message) throw Error("signature must be Signature instance, Uint8Array or hex string");
      return false;
    }
    if (h3 && l3.hasHighS()) return false;
    u3 && (r3 = t2.hash(r3));
    const { r: f3, s: g3 } = l3, d3 = m2(r3), A3 = o2(g3), w3 = a2(d3 * A3), b3 = a2(f3 * A3), k3 = c2.BASE.multiplyAndAddUnsafe(y3, w3, b3)?.toAffine();
    return !!k3 && a2(k3.x) === f3;
  }, ProjectivePoint: c2, Signature: p2, utils: d2 };
}
function tu(e2) {
  return { hash: e2, hmac: (t2, ...r2) => _c(e2, t2, Dc(...r2)), randomBytes: xc };
}
function ru(e2, t2) {
  const r2 = (t3) => eu({ ...e2, ...tu(t3) });
  return Object.freeze({ ...r2(t2), create: r2 });
}
function ou(e2, t2 = false) {
  return t2 ? { h: Number(e2 & su), l: Number(e2 >> au & su) } : { h: 0 | Number(e2 >> au & su), l: 0 | Number(e2 & su) };
}
function cu(e2, t2 = false) {
  let r2 = new Uint32Array(e2.length), n2 = new Uint32Array(e2.length);
  for (let i2 = 0; i2 < e2.length; i2++) {
    const { h: s2, l: a2 } = ou(e2[i2], t2);
    [r2[i2], n2[i2]] = [s2, a2];
  }
  return [r2, n2];
}
function Zu(e2) {
  const t2 = function(e3) {
    const t3 = jh(e3);
    return mh(e3, { hash: "function", a: "bigint", d: "bigint", randomBytes: "function" }, { adjustScalarBytes: "function", domain: "function", uvRatio: "function", mapToCurve: "function" }), Object.freeze({ ...t3 });
  }(e2), { Fp: r2, n: n2, prehash: i2, hash: s2, randomBytes: a2, nByteLength: o2, h: c2 } = t2, h2 = Vu << BigInt(8 * o2) - qu, u2 = r2.create, l2 = Lh(t2.n, t2.nBitLength), y2 = t2.uvRatio || ((e3, t3) => {
    try {
      return { isValid: true, value: r2.sqrt(e3 * r2.inv(t3)) };
    } catch (e4) {
      return { isValid: false, value: ju };
    }
  }), f2 = t2.adjustScalarBytes || ((e3) => e3), g2 = t2.domain || ((e3, t3, r3) => {
    if (Zc("phflag", r3), t3.length || r3) throw Error("Contexts/pre-hash are not supported");
    return e3;
  });
  function p2(e3, t3) {
    yh("coordinate " + e3, t3, ju, h2);
  }
  function d2(e3) {
    if (!(e3 instanceof m2)) throw Error("ExtendedPoint expected");
  }
  const A2 = bh((e3, t3) => {
    const { ex: n3, ey: i3, ez: s3 } = e3, a3 = e3.is0();
    null == t3 && (t3 = a3 ? Ju : r2.inv(s3));
    const o3 = u2(n3 * t3), c3 = u2(i3 * t3), h3 = u2(s3 * t3);
    if (a3) return { x: ju, y: qu };
    if (h3 !== qu) throw Error("invZ was invalid");
    return { x: o3, y: c3 };
  }), w2 = bh((e3) => {
    const { a: r3, d: n3 } = t2;
    if (e3.is0()) throw Error("bad point: ZERO");
    const { ex: i3, ey: s3, ez: a3, et: o3 } = e3, c3 = u2(i3 * i3), h3 = u2(s3 * s3), l3 = u2(a3 * a3), y3 = u2(l3 * l3), f3 = u2(c3 * r3);
    if (u2(l3 * u2(f3 + h3)) !== u2(y3 + u2(n3 * u2(c3 * h3)))) throw Error("bad point: equation left != right (1)");
    if (u2(i3 * s3) !== u2(a3 * o3)) throw Error("bad point: equation left != right (2)");
    return true;
  });
  class m2 {
    constructor(e3, t3, r3, n3) {
      this.ex = e3, this.ey = t3, this.ez = r3, this.et = n3, p2("x", e3), p2("y", t3), p2("z", r3), p2("t", n3), Object.freeze(this);
    }
    get x() {
      return this.toAffine().x;
    }
    get y() {
      return this.toAffine().y;
    }
    static fromAffine(e3) {
      if (e3 instanceof m2) throw Error("extended point not allowed");
      const { x: t3, y: r3 } = e3 || {};
      return p2("x", t3), p2("y", r3), new m2(t3, r3, qu, u2(t3 * r3));
    }
    static normalizeZ(e3) {
      const t3 = r2.invertBatch(e3.map((e4) => e4.ez));
      return e3.map((e4, r3) => e4.toAffine(t3[r3])).map(m2.fromAffine);
    }
    static msm(e3, t3) {
      return _h(m2, l2, e3, t3);
    }
    _setWindowSize(e3) {
      E2.setWindowSize(this, e3);
    }
    assertValidity() {
      w2(this);
    }
    equals(e3) {
      d2(e3);
      const { ex: t3, ey: r3, ez: n3 } = this, { ex: i3, ey: s3, ez: a3 } = e3, o3 = u2(t3 * a3), c3 = u2(i3 * n3), h3 = u2(r3 * a3), l3 = u2(s3 * n3);
      return o3 === c3 && h3 === l3;
    }
    is0() {
      return this.equals(m2.ZERO);
    }
    negate() {
      return new m2(u2(-this.ex), this.ey, this.ez, u2(-this.et));
    }
    double() {
      const { a: e3 } = t2, { ex: r3, ey: n3, ez: i3 } = this, s3 = u2(r3 * r3), a3 = u2(n3 * n3), o3 = u2(Vu * u2(i3 * i3)), c3 = u2(e3 * s3), h3 = r3 + n3, l3 = u2(u2(h3 * h3) - s3 - a3), y3 = c3 + a3, f3 = y3 - o3, g3 = c3 - a3, p3 = u2(l3 * f3), d3 = u2(y3 * g3), A3 = u2(l3 * g3), w3 = u2(f3 * y3);
      return new m2(p3, d3, w3, A3);
    }
    add(e3) {
      d2(e3);
      const { a: r3, d: n3 } = t2, { ex: i3, ey: s3, ez: a3, et: o3 } = this, { ex: c3, ey: h3, ez: l3, et: y3 } = e3;
      if (r3 === BigInt(-1)) {
        const e4 = u2((s3 - i3) * (h3 + c3)), t3 = u2((s3 + i3) * (h3 - c3)), r4 = u2(t3 - e4);
        if (r4 === ju) return this.double();
        const n4 = u2(a3 * Vu * y3), f4 = u2(o3 * Vu * l3), g4 = f4 + n4, p4 = t3 + e4, d3 = f4 - n4, A4 = u2(g4 * r4), w4 = u2(p4 * d3), b4 = u2(g4 * d3), k4 = u2(r4 * p4);
        return new m2(A4, w4, k4, b4);
      }
      const f3 = u2(i3 * c3), g3 = u2(s3 * h3), p3 = u2(o3 * n3 * y3), A3 = u2(a3 * l3), w3 = u2((i3 + s3) * (c3 + h3) - f3 - g3), b3 = A3 - p3, k3 = A3 + p3, E3 = u2(g3 - r3 * f3), v3 = u2(w3 * b3), I3 = u2(k3 * E3), B3 = u2(w3 * E3), S3 = u2(b3 * k3);
      return new m2(v3, I3, S3, B3);
    }
    subtract(e3) {
      return this.add(e3.negate());
    }
    wNAF(e3) {
      return E2.wNAFCached(this, e3, m2.normalizeZ);
    }
    multiply(e3) {
      const t3 = e3;
      yh("scalar", t3, qu, n2);
      const { p: r3, f: i3 } = this.wNAF(t3);
      return m2.normalizeZ([r3, i3])[0];
    }
    multiplyUnsafe(e3) {
      const t3 = e3;
      return yh("scalar", t3, ju, n2), t3 === ju ? k2 : this.equals(k2) || t3 === qu ? this : this.equals(b2) ? this.wNAF(t3).p : E2.unsafeLadder(this, t3);
    }
    isSmallOrder() {
      return this.multiplyUnsafe(c2).is0();
    }
    isTorsionFree() {
      return E2.unsafeLadder(this, n2).is0();
    }
    toAffine(e3) {
      return A2(this, e3);
    }
    clearCofactor() {
      const { h: e3 } = t2;
      return e3 === qu ? this : this.multiplyUnsafe(e3);
    }
    static fromHex(e3, n3 = false) {
      const { d: i3, a: s3 } = t2, a3 = r2.BYTES;
      e3 = ch("pointHex", e3, a3), Zc("zip215", n3);
      const o3 = e3.slice(), c3 = e3[a3 - 1];
      o3[a3 - 1] = -129 & c3;
      const l3 = sh(o3), f3 = n3 ? h2 : r2.ORDER;
      yh("pointHex.y", l3, ju, f3);
      const g3 = u2(l3 * l3), p3 = u2(g3 - qu), d3 = u2(i3 * g3 - s3);
      let { isValid: A3, value: w3 } = y2(p3, d3);
      if (!A3) throw Error("Point.fromHex: invalid y coordinate");
      const b3 = (w3 & qu) === qu, k3 = !!(128 & c3);
      if (!n3 && w3 === ju && k3) throw Error("Point.fromHex: x=0 and x_0=1");
      return k3 !== b3 && (w3 = u2(-w3)), m2.fromAffine({ x: w3, y: l3 });
    }
    static fromPrivateKey(e3) {
      return B2(e3).point;
    }
    toRawBytes() {
      const { x: e3, y: t3 } = this.toAffine(), n3 = oh(t3, r2.BYTES);
      return n3[n3.length - 1] |= e3 & qu ? 128 : 0, n3;
    }
    toHex() {
      return $c(this.toRawBytes());
    }
  }
  m2.BASE = new m2(t2.Gx, t2.Gy, qu, u2(t2.Gx * t2.Gy)), m2.ZERO = new m2(ju, qu, qu, ju);
  const { BASE: b2, ZERO: k2 } = m2, E2 = Gh(m2, 8 * o2);
  function v2(e3) {
    return Dh(e3, n2);
  }
  function I2(e3) {
    return v2(sh(e3));
  }
  function B2(e3) {
    const t3 = o2;
    e3 = ch("private key", e3, t3);
    const r3 = ch("hashed private key", s2(e3), 2 * t3), n3 = f2(r3.slice(0, t3)), i3 = r3.slice(t3, 2 * t3), a3 = I2(n3), c3 = b2.multiply(a3), h3 = c3.toRawBytes();
    return { head: n3, prefix: i3, scalar: a3, point: c3, pointBytes: h3 };
  }
  function S2(e3 = new Uint8Array(), ...t3) {
    const r3 = hh(...t3);
    return I2(s2(g2(r3, ch("context", e3), !!i2)));
  }
  const K2 = Yu;
  b2._setWindowSize(8);
  return { CURVE: t2, getPublicKey: function(e3) {
    return B2(e3).pointBytes;
  }, sign: function(e3, t3, s3 = {}) {
    e3 = ch("message", e3), i2 && (e3 = i2(e3));
    const { prefix: a3, scalar: c3, pointBytes: h3 } = B2(t3), u3 = S2(s3.context, a3, e3), l3 = b2.multiply(u3).toRawBytes(), y3 = v2(u3 + S2(s3.context, l3, h3, e3) * c3);
    return yh("signature.s", y3, ju, n2), ch("result", hh(l3, oh(y3, r2.BYTES)), 2 * o2);
  }, verify: function(e3, t3, n3, s3 = K2) {
    const { context: a3, zip215: o3 } = s3, c3 = r2.BYTES;
    e3 = ch("signature", e3, 2 * c3), t3 = ch("message", t3), void 0 !== o3 && Zc("zip215", o3), i2 && (t3 = i2(t3));
    const h3 = sh(e3.slice(c3, 2 * c3));
    let u3, l3, y3;
    try {
      u3 = m2.fromHex(n3, o3), l3 = m2.fromHex(e3.slice(0, c3), o3), y3 = b2.multiplyUnsafe(h3);
    } catch (e4) {
      return false;
    }
    if (!o3 && u3.isSmallOrder()) return false;
    const f3 = S2(a3, l3.toRawBytes(), u3.toRawBytes(), t3);
    return l3.add(u3.multiplyUnsafe(f3)).subtract(y3).clearCofactor().equals(m2.ZERO);
  }, ExtendedPoint: m2, utils: { getExtendedPublicKey: B2, randomPrivateKey: () => a2(r2.BYTES), precompute: (e3 = 8, t3 = m2.BASE) => (t3._setWindowSize(e3), t3.multiply(BigInt(3)), t3) } };
}
function Xu(e2) {
  const t2 = (mh(r2 = e2, { a: "bigint" }, { montgomeryBits: "isSafeInteger", nByteLength: "isSafeInteger", adjustScalarBytes: "function", domain: "function", powPminus2: "function", Gu: "bigint" }), Object.freeze({ ...r2 }));
  var r2;
  const { P: n2 } = t2, i2 = (e3) => Dh(e3, n2), s2 = t2.montgomeryBits, a2 = Math.ceil(s2 / 8), o2 = t2.nByteLength, c2 = t2.adjustScalarBytes || ((e3) => e3), h2 = t2.powPminus2 || ((e3) => Uh(e3, n2 - BigInt(2), n2));
  function u2(e3, t3, r3) {
    const n3 = i2(e3 * (t3 - r3));
    return [t3 = i2(t3 - n3), r3 = i2(r3 + n3)];
  }
  const l2 = (t2.a - BigInt(2)) / BigInt(4);
  function y2(e3) {
    return oh(i2(e3), a2);
  }
  function f2(e3, t3) {
    const r3 = function(e4) {
      const t4 = ch("u coordinate", e4, a2);
      return 32 === o2 && (t4[31] &= 127), sh(t4);
    }(t3), f3 = function(e4) {
      const t4 = ch("scalar", e4), r4 = t4.length;
      if (r4 !== a2 && r4 !== o2) throw Error(`Expected ${a2} or ${o2} bytes, got ${r4}`);
      return sh(c2(t4));
    }(e3), g3 = function(e4, t4) {
      yh("u", e4, Wu, n2), yh("scalar", t4, Wu, n2);
      const r4 = t4, a3 = e4;
      let o3, c3 = $u, y3 = Wu, f4 = e4, g4 = $u, p3 = Wu;
      for (let e5 = BigInt(s2 - 1); e5 >= Wu; e5--) {
        const t5 = r4 >> e5 & $u;
        p3 ^= t5, o3 = u2(p3, c3, f4), c3 = o3[0], f4 = o3[1], o3 = u2(p3, y3, g4), y3 = o3[0], g4 = o3[1], p3 = t5;
        const n3 = c3 + y3, s3 = i2(n3 * n3), h3 = c3 - y3, d3 = i2(h3 * h3), A2 = s3 - d3, w2 = f4 + g4, m2 = i2((f4 - g4) * n3), b2 = i2(w2 * h3), k2 = m2 + b2, E2 = m2 - b2;
        f4 = i2(k2 * k2), g4 = i2(a3 * i2(E2 * E2)), c3 = i2(s3 * d3), y3 = i2(A2 * (s3 + i2(l2 * A2)));
      }
      o3 = u2(p3, c3, f4), c3 = o3[0], f4 = o3[1], o3 = u2(p3, y3, g4), y3 = o3[0], g4 = o3[1];
      const d2 = h2(y3);
      return i2(c3 * d2);
    }(r3, f3);
    if (g3 === Wu) throw Error("Invalid private or public key received");
    return y2(g3);
  }
  const g2 = y2(t2.Gu);
  function p2(e3) {
    return f2(e3, g2);
  }
  return { scalarMult: f2, scalarMultBase: p2, getSharedSecret: (e3, t3) => f2(e3, t3), getPublicKey: (e3) => p2(e3), utils: { randomPrivateKey: () => t2.randomBytes(t2.nByteLength) }, GuBytes: g2 };
}
function ul(e2) {
  const t2 = tl, r2 = e2 * e2 * e2 % t2, n2 = r2 * r2 * e2 % t2, i2 = Ph(n2, il, t2) * n2 % t2, s2 = Ph(i2, il, t2) * n2 % t2, a2 = Ph(s2, nl, t2) * r2 % t2, o2 = Ph(a2, sl, t2) * a2 % t2, c2 = Ph(o2, al, t2) * o2 % t2, h2 = Ph(c2, ol, t2) * c2 % t2, u2 = Ph(h2, cl, t2) * h2 % t2, l2 = Ph(u2, ol, t2) * c2 % t2, y2 = Ph(l2, nl, t2) * r2 % t2, f2 = Ph(y2, rl, t2) * e2 % t2;
  return Ph(f2, hl, t2) * y2 % t2;
}
function ll(e2) {
  return e2[0] &= 252, e2[55] |= 128, e2[56] = 0, e2;
}
function jl(e2, t2, r2, n2) {
  return 0 === e2 ? t2 ^ r2 ^ n2 : 1 === e2 ? t2 & r2 | ~t2 & n2 : 2 === e2 ? (t2 | ~r2) ^ n2 : 3 === e2 ? t2 & n2 | r2 & ~n2 : t2 ^ (r2 | ~n2);
}
function ry(e2, t2, r2, n2, i2, s2) {
  const a2 = [16843776, 0, 65536, 16843780, 16842756, 66564, 4, 65536, 1024, 16843776, 16843780, 1024, 16778244, 16842756, 16777216, 4, 1028, 16778240, 16778240, 66560, 66560, 16842752, 16842752, 16778244, 65540, 16777220, 16777220, 65540, 0, 1028, 66564, 16777216, 65536, 16843780, 4, 16842752, 16843776, 16777216, 16777216, 1024, 16842756, 65536, 66560, 16777220, 1024, 4, 16778244, 66564, 16843780, 65540, 16842752, 16778244, 16777220, 1028, 66564, 16843776, 1028, 16778240, 16778240, 0, 65540, 66560, 0, 16842756], o2 = [-2146402272, -2147450880, 32768, 1081376, 1048576, 32, -2146435040, -2147450848, -2147483616, -2146402272, -2146402304, -2147483648, -2147450880, 1048576, 32, -2146435040, 1081344, 1048608, -2147450848, 0, -2147483648, 32768, 1081376, -2146435072, 1048608, -2147483616, 0, 1081344, 32800, -2146402304, -2146435072, 32800, 0, 1081376, -2146435040, 1048576, -2147450848, -2146435072, -2146402304, 32768, -2146435072, -2147450880, 32, -2146402272, 1081376, 32, 32768, -2147483648, 32800, -2146402304, 1048576, -2147483616, 1048608, -2147450848, -2147483616, 1048608, 1081344, 0, -2147450880, 32800, -2147483648, -2146435040, -2146402272, 1081344], c2 = [520, 134349312, 0, 134348808, 134218240, 0, 131592, 134218240, 131080, 134217736, 134217736, 131072, 134349320, 131080, 134348800, 520, 134217728, 8, 134349312, 512, 131584, 134348800, 134348808, 131592, 134218248, 131584, 131072, 134218248, 8, 134349320, 512, 134217728, 134349312, 134217728, 131080, 520, 131072, 134349312, 134218240, 0, 512, 131080, 134349320, 134218240, 134217736, 512, 0, 134348808, 134218248, 131072, 134217728, 134349320, 8, 131592, 131584, 134217736, 134348800, 134218248, 520, 134348800, 131592, 8, 134348808, 131584], h2 = [8396801, 8321, 8321, 128, 8396928, 8388737, 8388609, 8193, 0, 8396800, 8396800, 8396929, 129, 0, 8388736, 8388609, 1, 8192, 8388608, 8396801, 128, 8388608, 8193, 8320, 8388737, 1, 8320, 8388736, 8192, 8396928, 8396929, 129, 8388736, 8388609, 8396800, 8396929, 129, 0, 0, 8396800, 8320, 8388736, 8388737, 1, 8396801, 8321, 8321, 128, 8396929, 129, 1, 8192, 8388609, 8193, 8396928, 8388737, 8193, 8320, 8388608, 8396801, 128, 8388608, 8192, 8396928], u2 = [256, 34078976, 34078720, 1107296512, 524288, 256, 1073741824, 34078720, 1074266368, 524288, 33554688, 1074266368, 1107296512, 1107820544, 524544, 1073741824, 33554432, 1074266112, 1074266112, 0, 1073742080, 1107820800, 1107820800, 33554688, 1107820544, 1073742080, 0, 1107296256, 34078976, 33554432, 1107296256, 524544, 524288, 1107296512, 256, 33554432, 1073741824, 34078720, 1107296512, 1074266368, 33554688, 1073741824, 1107820544, 34078976, 1074266368, 256, 33554432, 1107820544, 1107820800, 524544, 1107296256, 1107820800, 34078720, 0, 1074266112, 1107296256, 524544, 33554688, 1073742080, 524288, 0, 1074266112, 34078976, 1073742080], l2 = [536870928, 541065216, 16384, 541081616, 541065216, 16, 541081616, 4194304, 536887296, 4210704, 4194304, 536870928, 4194320, 536887296, 536870912, 16400, 0, 4194320, 536887312, 16384, 4210688, 536887312, 16, 541065232, 541065232, 0, 4210704, 541081600, 16400, 4210688, 541081600, 536870912, 536887296, 16, 541065232, 4210688, 541081616, 4194304, 16400, 536870928, 4194304, 536887296, 536870912, 16400, 536870928, 541081616, 4210688, 541065216, 4210704, 541081600, 0, 541065232, 16, 16384, 541065216, 4210704, 16384, 4194320, 536887312, 0, 541081600, 536870912, 4194320, 536887312], y2 = [2097152, 69206018, 67110914, 0, 2048, 67110914, 2099202, 69208064, 69208066, 2097152, 0, 67108866, 2, 67108864, 69206018, 2050, 67110912, 2099202, 2097154, 67110912, 67108866, 69206016, 69208064, 2097154, 69206016, 2048, 2050, 69208066, 2099200, 2, 67108864, 2099200, 67108864, 2099200, 2097152, 67110914, 67110914, 69206018, 69206018, 2, 2097154, 67108864, 67110912, 2097152, 69208064, 2050, 2099202, 69208064, 2050, 67108866, 69208066, 69206016, 2099200, 0, 2, 69208066, 0, 2099202, 69206016, 2048, 67108866, 67110912, 2048, 2097154], f2 = [268439616, 4096, 262144, 268701760, 268435456, 268439616, 64, 268435456, 262208, 268697600, 268701760, 266240, 268701696, 266304, 4096, 64, 268697600, 268435520, 268439552, 4160, 266240, 262208, 268697664, 268701696, 4160, 0, 0, 268697664, 268435520, 268439552, 266304, 262144, 266304, 262144, 268701696, 4096, 64, 268697664, 4096, 266304, 268439552, 64, 268435520, 268697600, 268697664, 268435456, 262144, 268439616, 0, 268701760, 262208, 268435520, 268697600, 268439552, 268439616, 0, 268701760, 266240, 266240, 4160, 4160, 262208, 268435456, 268701696];
  let g2, p2, d2, A2, w2, m2, b2, k2, E2, v2, I2 = 0, B2 = t2.length;
  const S2 = 32 === e2.length ? 3 : 9;
  k2 = 3 === S2 ? r2 ? [0, 32, 2] : [30, -2, -2] : r2 ? [0, 32, 2, 62, 30, -2, 64, 96, 2] : [94, 62, -2, 32, 64, 2, 30, -2, -2], r2 && (t2 = function(e3) {
    const t3 = 8 - e3.length % 8;
    let r3;
    if (!(t3 < 8)) {
      if (8 === t3) return e3;
      throw Error("des: invalid padding");
    }
    r3 = 0;
    const n3 = new Uint8Array(e3.length + t3);
    for (let t4 = 0; t4 < e3.length; t4++) n3[t4] = e3[t4];
    for (let i3 = 0; i3 < t3; i3++) n3[e3.length + i3] = r3;
    return n3;
  }(t2), B2 = t2.length);
  let K2 = new Uint8Array(B2), C2 = 0;
  for (; I2 < B2; ) {
    for (m2 = t2[I2++] << 24 | t2[I2++] << 16 | t2[I2++] << 8 | t2[I2++], b2 = t2[I2++] << 24 | t2[I2++] << 16 | t2[I2++] << 8 | t2[I2++], d2 = 252645135 & (m2 >>> 4 ^ b2), b2 ^= d2, m2 ^= d2 << 4, d2 = 65535 & (m2 >>> 16 ^ b2), b2 ^= d2, m2 ^= d2 << 16, d2 = 858993459 & (b2 >>> 2 ^ m2), m2 ^= d2, b2 ^= d2 << 2, d2 = 16711935 & (b2 >>> 8 ^ m2), m2 ^= d2, b2 ^= d2 << 8, d2 = 1431655765 & (m2 >>> 1 ^ b2), b2 ^= d2, m2 ^= d2 << 1, m2 = m2 << 1 | m2 >>> 31, b2 = b2 << 1 | b2 >>> 31, p2 = 0; p2 < S2; p2 += 3) {
      for (E2 = k2[p2 + 1], v2 = k2[p2 + 2], g2 = k2[p2]; g2 !== E2; g2 += v2) A2 = b2 ^ e2[g2], w2 = (b2 >>> 4 | b2 << 28) ^ e2[g2 + 1], d2 = m2, m2 = b2, b2 = d2 ^ (o2[A2 >>> 24 & 63] | h2[A2 >>> 16 & 63] | l2[A2 >>> 8 & 63] | f2[63 & A2] | a2[w2 >>> 24 & 63] | c2[w2 >>> 16 & 63] | u2[w2 >>> 8 & 63] | y2[63 & w2]);
      d2 = m2, m2 = b2, b2 = d2;
    }
    m2 = m2 >>> 1 | m2 << 31, b2 = b2 >>> 1 | b2 << 31, d2 = 1431655765 & (m2 >>> 1 ^ b2), b2 ^= d2, m2 ^= d2 << 1, d2 = 16711935 & (b2 >>> 8 ^ m2), m2 ^= d2, b2 ^= d2 << 8, d2 = 858993459 & (b2 >>> 2 ^ m2), m2 ^= d2, b2 ^= d2 << 2, d2 = 65535 & (m2 >>> 16 ^ b2), b2 ^= d2, m2 ^= d2 << 16, d2 = 252645135 & (m2 >>> 4 ^ b2), b2 ^= d2, m2 ^= d2 << 4, K2[C2++] = m2 >>> 24, K2[C2++] = m2 >>> 16 & 255, K2[C2++] = m2 >>> 8 & 255, K2[C2++] = 255 & m2, K2[C2++] = b2 >>> 24, K2[C2++] = b2 >>> 16 & 255, K2[C2++] = b2 >>> 8 & 255, K2[C2++] = 255 & b2;
  }
  return r2 || (K2 = function(e3) {
    let t3, r3 = null;
    if (t3 = 0, !r3) {
      for (r3 = 1; e3[e3.length - r3] === t3; ) r3++;
      r3--;
    }
    return e3.subarray(0, e3.length - r3);
  }(K2)), K2;
}
function ny(e2) {
  const t2 = [0, 4, 536870912, 536870916, 65536, 65540, 536936448, 536936452, 512, 516, 536871424, 536871428, 66048, 66052, 536936960, 536936964], r2 = [0, 1, 1048576, 1048577, 67108864, 67108865, 68157440, 68157441, 256, 257, 1048832, 1048833, 67109120, 67109121, 68157696, 68157697], n2 = [0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272, 0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272], i2 = [0, 2097152, 134217728, 136314880, 8192, 2105344, 134225920, 136323072, 131072, 2228224, 134348800, 136445952, 139264, 2236416, 134356992, 136454144], s2 = [0, 262144, 16, 262160, 0, 262144, 16, 262160, 4096, 266240, 4112, 266256, 4096, 266240, 4112, 266256], a2 = [0, 1024, 32, 1056, 0, 1024, 32, 1056, 33554432, 33555456, 33554464, 33555488, 33554432, 33555456, 33554464, 33555488], o2 = [0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746, 0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746], c2 = [0, 65536, 2048, 67584, 536870912, 536936448, 536872960, 536938496, 131072, 196608, 133120, 198656, 537001984, 537067520, 537004032, 537069568], h2 = [0, 262144, 0, 262144, 2, 262146, 2, 262146, 33554432, 33816576, 33554432, 33816576, 33554434, 33816578, 33554434, 33816578], u2 = [0, 268435456, 8, 268435464, 0, 268435456, 8, 268435464, 1024, 268436480, 1032, 268436488, 1024, 268436480, 1032, 268436488], l2 = [0, 32, 0, 32, 1048576, 1048608, 1048576, 1048608, 8192, 8224, 8192, 8224, 1056768, 1056800, 1056768, 1056800], y2 = [0, 16777216, 512, 16777728, 2097152, 18874368, 2097664, 18874880, 67108864, 83886080, 67109376, 83886592, 69206016, 85983232, 69206528, 85983744], f2 = [0, 4096, 134217728, 134221824, 524288, 528384, 134742016, 134746112, 16, 4112, 134217744, 134221840, 524304, 528400, 134742032, 134746128], g2 = [0, 4, 256, 260, 0, 4, 256, 260, 1, 5, 257, 261, 1, 5, 257, 261], p2 = e2.length > 8 ? 3 : 1, d2 = Array(32 * p2), A2 = [0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0];
  let w2, m2, b2, k2 = 0, E2 = 0;
  for (let v2 = 0; v2 < p2; v2++) {
    let p3 = e2[k2++] << 24 | e2[k2++] << 16 | e2[k2++] << 8 | e2[k2++], v3 = e2[k2++] << 24 | e2[k2++] << 16 | e2[k2++] << 8 | e2[k2++];
    b2 = 252645135 & (p3 >>> 4 ^ v3), v3 ^= b2, p3 ^= b2 << 4, b2 = 65535 & (v3 >>> -16 ^ p3), p3 ^= b2, v3 ^= b2 << -16, b2 = 858993459 & (p3 >>> 2 ^ v3), v3 ^= b2, p3 ^= b2 << 2, b2 = 65535 & (v3 >>> -16 ^ p3), p3 ^= b2, v3 ^= b2 << -16, b2 = 1431655765 & (p3 >>> 1 ^ v3), v3 ^= b2, p3 ^= b2 << 1, b2 = 16711935 & (v3 >>> 8 ^ p3), p3 ^= b2, v3 ^= b2 << 8, b2 = 1431655765 & (p3 >>> 1 ^ v3), v3 ^= b2, p3 ^= b2 << 1, b2 = p3 << 8 | v3 >>> 20 & 240, p3 = v3 << 24 | v3 << 8 & 16711680 | v3 >>> 8 & 65280 | v3 >>> 24 & 240, v3 = b2;
    for (let e3 = 0; e3 < 16; e3++) A2[e3] ? (p3 = p3 << 2 | p3 >>> 26, v3 = v3 << 2 | v3 >>> 26) : (p3 = p3 << 1 | p3 >>> 27, v3 = v3 << 1 | v3 >>> 27), p3 &= -15, v3 &= -15, w2 = t2[p3 >>> 28] | r2[p3 >>> 24 & 15] | n2[p3 >>> 20 & 15] | i2[p3 >>> 16 & 15] | s2[p3 >>> 12 & 15] | a2[p3 >>> 8 & 15] | o2[p3 >>> 4 & 15], m2 = c2[v3 >>> 28] | h2[v3 >>> 24 & 15] | u2[v3 >>> 20 & 15] | l2[v3 >>> 16 & 15] | y2[v3 >>> 12 & 15] | f2[v3 >>> 8 & 15] | g2[v3 >>> 4 & 15], b2 = 65535 & (m2 >>> 16 ^ w2), d2[E2++] = w2 ^ b2, d2[E2++] = m2 ^ b2 << 16;
  }
  return d2;
}
function iy(e2) {
  this.key = [];
  for (let t2 = 0; t2 < 3; t2++) this.key.push(new Uint8Array(e2.subarray(8 * t2, 8 * t2 + 8)));
  this.encrypt = function(e3) {
    return ry(ny(this.key[2]), ry(ny(this.key[1]), ry(ny(this.key[0]), e3, true), false), true);
  };
}
function sy() {
  this.BlockSize = 8, this.KeySize = 16, this.setKey = function(e3) {
    if (this.masking = Array(16), this.rotate = Array(16), this.reset(), e3.length !== this.KeySize) throw Error("CAST-128: keys must be 16 bytes");
    return this.keySchedule(e3), true;
  }, this.reset = function() {
    for (let e3 = 0; e3 < 16; e3++) this.masking[e3] = 0, this.rotate[e3] = 0;
  }, this.getBlockSize = function() {
    return this.BlockSize;
  }, this.encrypt = function(e3) {
    const t3 = Array(e3.length);
    for (let s3 = 0; s3 < e3.length; s3 += 8) {
      let a2, o2 = e3[s3] << 24 | e3[s3 + 1] << 16 | e3[s3 + 2] << 8 | e3[s3 + 3], c2 = e3[s3 + 4] << 24 | e3[s3 + 5] << 16 | e3[s3 + 6] << 8 | e3[s3 + 7];
      a2 = c2, c2 = o2 ^ r2(c2, this.masking[0], this.rotate[0]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[1], this.rotate[1]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[2], this.rotate[2]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[3], this.rotate[3]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[4], this.rotate[4]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[5], this.rotate[5]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[6], this.rotate[6]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[7], this.rotate[7]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[8], this.rotate[8]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[9], this.rotate[9]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[10], this.rotate[10]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[11], this.rotate[11]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[12], this.rotate[12]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[13], this.rotate[13]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[14], this.rotate[14]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[15], this.rotate[15]), o2 = a2, t3[s3] = c2 >>> 24 & 255, t3[s3 + 1] = c2 >>> 16 & 255, t3[s3 + 2] = c2 >>> 8 & 255, t3[s3 + 3] = 255 & c2, t3[s3 + 4] = o2 >>> 24 & 255, t3[s3 + 5] = o2 >>> 16 & 255, t3[s3 + 6] = o2 >>> 8 & 255, t3[s3 + 7] = 255 & o2;
    }
    return t3;
  }, this.decrypt = function(e3) {
    const t3 = Array(e3.length);
    for (let s3 = 0; s3 < e3.length; s3 += 8) {
      let a2, o2 = e3[s3] << 24 | e3[s3 + 1] << 16 | e3[s3 + 2] << 8 | e3[s3 + 3], c2 = e3[s3 + 4] << 24 | e3[s3 + 5] << 16 | e3[s3 + 6] << 8 | e3[s3 + 7];
      a2 = c2, c2 = o2 ^ r2(c2, this.masking[15], this.rotate[15]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[14], this.rotate[14]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[13], this.rotate[13]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[12], this.rotate[12]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[11], this.rotate[11]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[10], this.rotate[10]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[9], this.rotate[9]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[8], this.rotate[8]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[7], this.rotate[7]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[6], this.rotate[6]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[5], this.rotate[5]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[4], this.rotate[4]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[3], this.rotate[3]), o2 = a2, a2 = c2, c2 = o2 ^ i2(c2, this.masking[2], this.rotate[2]), o2 = a2, a2 = c2, c2 = o2 ^ n2(c2, this.masking[1], this.rotate[1]), o2 = a2, a2 = c2, c2 = o2 ^ r2(c2, this.masking[0], this.rotate[0]), o2 = a2, t3[s3] = c2 >>> 24 & 255, t3[s3 + 1] = c2 >>> 16 & 255, t3[s3 + 2] = c2 >>> 8 & 255, t3[s3 + 3] = 255 & c2, t3[s3 + 4] = o2 >>> 24 & 255, t3[s3 + 5] = o2 >> 16 & 255, t3[s3 + 6] = o2 >> 8 & 255, t3[s3 + 7] = 255 & o2;
    }
    return t3;
  };
  const e2 = [, , , ,];
  e2[0] = [, , , ,], e2[0][0] = [4, 0, 13, 15, 12, 14, 8], e2[0][1] = [5, 2, 16, 18, 17, 19, 10], e2[0][2] = [6, 3, 23, 22, 21, 20, 9], e2[0][3] = [7, 1, 26, 25, 27, 24, 11], e2[1] = [, , , ,], e2[1][0] = [0, 6, 21, 23, 20, 22, 16], e2[1][1] = [1, 4, 0, 2, 1, 3, 18], e2[1][2] = [2, 5, 7, 6, 5, 4, 17], e2[1][3] = [3, 7, 10, 9, 11, 8, 19], e2[2] = [, , , ,], e2[2][0] = [4, 0, 13, 15, 12, 14, 8], e2[2][1] = [5, 2, 16, 18, 17, 19, 10], e2[2][2] = [6, 3, 23, 22, 21, 20, 9], e2[2][3] = [7, 1, 26, 25, 27, 24, 11], e2[3] = [, , , ,], e2[3][0] = [0, 6, 21, 23, 20, 22, 16], e2[3][1] = [1, 4, 0, 2, 1, 3, 18], e2[3][2] = [2, 5, 7, 6, 5, 4, 17], e2[3][3] = [3, 7, 10, 9, 11, 8, 19];
  const t2 = [, , , ,];
  function r2(e3, t3, r3) {
    const n3 = t3 + e3, i3 = n3 << r3 | n3 >>> 32 - r3;
    return (s2[0][i3 >>> 24] ^ s2[1][i3 >>> 16 & 255]) - s2[2][i3 >>> 8 & 255] + s2[3][255 & i3];
  }
  function n2(e3, t3, r3) {
    const n3 = t3 ^ e3, i3 = n3 << r3 | n3 >>> 32 - r3;
    return s2[0][i3 >>> 24] - s2[1][i3 >>> 16 & 255] + s2[2][i3 >>> 8 & 255] ^ s2[3][255 & i3];
  }
  function i2(e3, t3, r3) {
    const n3 = t3 - e3, i3 = n3 << r3 | n3 >>> 32 - r3;
    return (s2[0][i3 >>> 24] + s2[1][i3 >>> 16 & 255] ^ s2[2][i3 >>> 8 & 255]) - s2[3][255 & i3];
  }
  t2[0] = [, , , ,], t2[0][0] = [24, 25, 23, 22, 18], t2[0][1] = [26, 27, 21, 20, 22], t2[0][2] = [28, 29, 19, 18, 25], t2[0][3] = [30, 31, 17, 16, 28], t2[1] = [, , , ,], t2[1][0] = [3, 2, 12, 13, 8], t2[1][1] = [1, 0, 14, 15, 13], t2[1][2] = [7, 6, 8, 9, 3], t2[1][3] = [5, 4, 10, 11, 7], t2[2] = [, , , ,], t2[2][0] = [19, 18, 28, 29, 25], t2[2][1] = [17, 16, 30, 31, 28], t2[2][2] = [23, 22, 24, 25, 18], t2[2][3] = [21, 20, 26, 27, 22], t2[3] = [, , , ,], t2[3][0] = [8, 9, 7, 6, 3], t2[3][1] = [10, 11, 5, 4, 7], t2[3][2] = [12, 13, 3, 2, 8], t2[3][3] = [14, 15, 1, 0, 13], this.keySchedule = function(r3) {
    const n3 = [, , , , , , , ,], i3 = Array(32);
    let a2;
    for (let e3 = 0; e3 < 4; e3++) a2 = 4 * e3, n3[e3] = r3[a2] << 24 | r3[a2 + 1] << 16 | r3[a2 + 2] << 8 | r3[a2 + 3];
    const o2 = [6, 7, 4, 5];
    let c2, h2 = 0;
    for (let r4 = 0; r4 < 2; r4++) for (let r5 = 0; r5 < 4; r5++) {
      for (a2 = 0; a2 < 4; a2++) {
        const t3 = e2[r5][a2];
        c2 = n3[t3[1]], c2 ^= s2[4][n3[t3[2] >>> 2] >>> 24 - 8 * (3 & t3[2]) & 255], c2 ^= s2[5][n3[t3[3] >>> 2] >>> 24 - 8 * (3 & t3[3]) & 255], c2 ^= s2[6][n3[t3[4] >>> 2] >>> 24 - 8 * (3 & t3[4]) & 255], c2 ^= s2[7][n3[t3[5] >>> 2] >>> 24 - 8 * (3 & t3[5]) & 255], c2 ^= s2[o2[a2]][n3[t3[6] >>> 2] >>> 24 - 8 * (3 & t3[6]) & 255], n3[t3[0]] = c2;
      }
      for (a2 = 0; a2 < 4; a2++) {
        const e3 = t2[r5][a2];
        c2 = s2[4][n3[e3[0] >>> 2] >>> 24 - 8 * (3 & e3[0]) & 255], c2 ^= s2[5][n3[e3[1] >>> 2] >>> 24 - 8 * (3 & e3[1]) & 255], c2 ^= s2[6][n3[e3[2] >>> 2] >>> 24 - 8 * (3 & e3[2]) & 255], c2 ^= s2[7][n3[e3[3] >>> 2] >>> 24 - 8 * (3 & e3[3]) & 255], c2 ^= s2[4 + a2][n3[e3[4] >>> 2] >>> 24 - 8 * (3 & e3[4]) & 255], i3[h2] = c2, h2++;
      }
    }
    for (let e3 = 0; e3 < 16; e3++) this.masking[e3] = i3[e3], this.rotate[e3] = 31 & i3[16 + e3];
  };
  const s2 = [, , , , , , , ,];
  s2[0] = [821772500, 2678128395, 1810681135, 1059425402, 505495343, 2617265619, 1610868032, 3483355465, 3218386727, 2294005173, 3791863952, 2563806837, 1852023008, 365126098, 3269944861, 584384398, 677919599, 3229601881, 4280515016, 2002735330, 1136869587, 3744433750, 2289869850, 2731719981, 2714362070, 879511577, 1639411079, 575934255, 717107937, 2857637483, 576097850, 2731753936, 1725645e3, 2810460463, 5111599, 767152862, 2543075244, 1251459544, 1383482551, 3052681127, 3089939183, 3612463449, 1878520045, 1510570527, 2189125840, 2431448366, 582008916, 3163445557, 1265446783, 1354458274, 3529918736, 3202711853, 3073581712, 3912963487, 3029263377, 1275016285, 4249207360, 2905708351, 3304509486, 1442611557, 3585198765, 2712415662, 2731849581, 3248163920, 2283946226, 208555832, 2766454743, 1331405426, 1447828783, 3315356441, 3108627284, 2957404670, 2981538698, 3339933917, 1669711173, 286233437, 1465092821, 1782121619, 3862771680, 710211251, 980974943, 1651941557, 430374111, 2051154026, 704238805, 4128970897, 3144820574, 2857402727, 948965521, 3333752299, 2227686284, 718756367, 2269778983, 2731643755, 718440111, 2857816721, 3616097120, 1113355533, 2478022182, 410092745, 1811985197, 1944238868, 2696854588, 1415722873, 1682284203, 1060277122, 1998114690, 1503841958, 82706478, 2315155686, 1068173648, 845149890, 2167947013, 1768146376, 1993038550, 3566826697, 3390574031, 940016341, 3355073782, 2328040721, 904371731, 1205506512, 4094660742, 2816623006, 825647681, 85914773, 2857843460, 1249926541, 1417871568, 3287612, 3211054559, 3126306446, 1975924523, 1353700161, 2814456437, 2438597621, 1800716203, 722146342, 2873936343, 1151126914, 4160483941, 2877670899, 458611604, 2866078500, 3483680063, 770352098, 2652916994, 3367839148, 3940505011, 3585973912, 3809620402, 718646636, 2504206814, 2914927912, 3631288169, 2857486607, 2860018678, 575749918, 2857478043, 718488780, 2069512688, 3548183469, 453416197, 1106044049, 3032691430, 52586708, 3378514636, 3459808877, 3211506028, 1785789304, 218356169, 3571399134, 3759170522, 1194783844, 1523787992, 3007827094, 1975193539, 2555452411, 1341901877, 3045838698, 3776907964, 3217423946, 2802510864, 2889438986, 1057244207, 1636348243, 3761863214, 1462225785, 2632663439, 481089165, 718503062, 24497053, 3332243209, 3344655856, 3655024856, 3960371065, 1195698900, 2971415156, 3710176158, 2115785917, 4027663609, 3525578417, 2524296189, 2745972565, 3564906415, 1372086093, 1452307862, 2780501478, 1476592880, 3389271281, 18495466, 2378148571, 901398090, 891748256, 3279637769, 3157290713, 2560960102, 1447622437, 4284372637, 216884176, 2086908623, 1879786977, 3588903153, 2242455666, 2938092967, 3559082096, 2810645491, 758861177, 1121993112, 215018983, 642190776, 4169236812, 1196255959, 2081185372, 3508738393, 941322904, 4124243163, 2877523539, 1848581667, 2205260958, 3180453958, 2589345134, 3694731276, 550028657, 2519456284, 3789985535, 2973870856, 2093648313, 443148163, 46942275, 2734146937, 1117713533, 1115362972, 1523183689, 3717140224, 1551984063], s2[1] = [522195092, 4010518363, 1776537470, 960447360, 4267822970, 4005896314, 1435016340, 1929119313, 2913464185, 1310552629, 3579470798, 3724818106, 2579771631, 1594623892, 417127293, 2715217907, 2696228731, 1508390405, 3994398868, 3925858569, 3695444102, 4019471449, 3129199795, 3770928635, 3520741761, 990456497, 4187484609, 2783367035, 21106139, 3840405339, 631373633, 3783325702, 532942976, 396095098, 3548038825, 4267192484, 2564721535, 2011709262, 2039648873, 620404603, 3776170075, 2898526339, 3612357925, 4159332703, 1645490516, 223693667, 1567101217, 3362177881, 1029951347, 3470931136, 3570957959, 1550265121, 119497089, 972513919, 907948164, 3840628539, 1613718692, 3594177948, 465323573, 2659255085, 654439692, 2575596212, 2699288441, 3127702412, 277098644, 624404830, 4100943870, 2717858591, 546110314, 2403699828, 3655377447, 1321679412, 4236791657, 1045293279, 4010672264, 895050893, 2319792268, 494945126, 1914543101, 2777056443, 3894764339, 2219737618, 311263384, 4275257268, 3458730721, 669096869, 3584475730, 3835122877, 3319158237, 3949359204, 2005142349, 2713102337, 2228954793, 3769984788, 569394103, 3855636576, 1425027204, 108000370, 2736431443, 3671869269, 3043122623, 1750473702, 2211081108, 762237499, 3972989403, 2798899386, 3061857628, 2943854345, 867476300, 964413654, 1591880597, 1594774276, 2179821409, 552026980, 3026064248, 3726140315, 2283577634, 3110545105, 2152310760, 582474363, 1582640421, 1383256631, 2043843868, 3322775884, 1217180674, 463797851, 2763038571, 480777679, 2718707717, 2289164131, 3118346187, 214354409, 200212307, 3810608407, 3025414197, 2674075964, 3997296425, 1847405948, 1342460550, 510035443, 4080271814, 815934613, 833030224, 1620250387, 1945732119, 2703661145, 3966000196, 1388869545, 3456054182, 2687178561, 2092620194, 562037615, 1356438536, 3409922145, 3261847397, 1688467115, 2150901366, 631725691, 3840332284, 549916902, 3455104640, 394546491, 837744717, 2114462948, 751520235, 2221554606, 2415360136, 3999097078, 2063029875, 803036379, 2702586305, 821456707, 3019566164, 360699898, 4018502092, 3511869016, 3677355358, 2402471449, 812317050, 49299192, 2570164949, 3259169295, 2816732080, 3331213574, 3101303564, 2156015656, 3705598920, 3546263921, 143268808, 3200304480, 1638124008, 3165189453, 3341807610, 578956953, 2193977524, 3638120073, 2333881532, 807278310, 658237817, 2969561766, 1641658566, 11683945, 3086995007, 148645947, 1138423386, 4158756760, 1981396783, 2401016740, 3699783584, 380097457, 2680394679, 2803068651, 3334260286, 441530178, 4016580796, 1375954390, 761952171, 891809099, 2183123478, 157052462, 3683840763, 1592404427, 341349109, 2438483839, 1417898363, 644327628, 2233032776, 2353769706, 2201510100, 220455161, 1815641738, 182899273, 2995019788, 3627381533, 3702638151, 2890684138, 1052606899, 588164016, 1681439879, 4038439418, 2405343923, 4229449282, 167996282, 1336969661, 1688053129, 2739224926, 1543734051, 1046297529, 1138201970, 2121126012, 115334942, 1819067631, 1902159161, 1941945968, 2206692869, 1159982321], s2[2] = [2381300288, 637164959, 3952098751, 3893414151, 1197506559, 916448331, 2350892612, 2932787856, 3199334847, 4009478890, 3905886544, 1373570990, 2450425862, 4037870920, 3778841987, 2456817877, 286293407, 124026297, 3001279700, 1028597854, 3115296800, 4208886496, 2691114635, 2188540206, 1430237888, 1218109995, 3572471700, 308166588, 570424558, 2187009021, 2455094765, 307733056, 1310360322, 3135275007, 1384269543, 2388071438, 863238079, 2359263624, 2801553128, 3380786597, 2831162807, 1470087780, 1728663345, 4072488799, 1090516929, 532123132, 2389430977, 1132193179, 2578464191, 3051079243, 1670234342, 1434557849, 2711078940, 1241591150, 3314043432, 3435360113, 3091448339, 1812415473, 2198440252, 267246943, 796911696, 3619716990, 38830015, 1526438404, 2806502096, 374413614, 2943401790, 1489179520, 1603809326, 1920779204, 168801282, 260042626, 2358705581, 1563175598, 2397674057, 1356499128, 2217211040, 514611088, 2037363785, 2186468373, 4022173083, 2792511869, 2913485016, 1173701892, 4200428547, 3896427269, 1334932762, 2455136706, 602925377, 2835607854, 1613172210, 41346230, 2499634548, 2457437618, 2188827595, 41386358, 4172255629, 1313404830, 2405527007, 3801973774, 2217704835, 873260488, 2528884354, 2478092616, 4012915883, 2555359016, 2006953883, 2463913485, 575479328, 2218240648, 2099895446, 660001756, 2341502190, 3038761536, 3888151779, 3848713377, 3286851934, 1022894237, 1620365795, 3449594689, 1551255054, 15374395, 3570825345, 4249311020, 4151111129, 3181912732, 310226346, 1133119310, 530038928, 136043402, 2476768958, 3107506709, 2544909567, 1036173560, 2367337196, 1681395281, 1758231547, 3641649032, 306774401, 1575354324, 3716085866, 1990386196, 3114533736, 2455606671, 1262092282, 3124342505, 2768229131, 4210529083, 1833535011, 423410938, 660763973, 2187129978, 1639812e3, 3508421329, 3467445492, 310289298, 272797111, 2188552562, 2456863912, 310240523, 677093832, 1013118031, 901835429, 3892695601, 1116285435, 3036471170, 1337354835, 243122523, 520626091, 277223598, 4244441197, 4194248841, 1766575121, 594173102, 316590669, 742362309, 3536858622, 4176435350, 3838792410, 2501204839, 1229605004, 3115755532, 1552908988, 2312334149, 979407927, 3959474601, 1148277331, 176638793, 3614686272, 2083809052, 40992502, 1340822838, 2731552767, 3535757508, 3560899520, 1354035053, 122129617, 7215240, 2732932949, 3118912700, 2718203926, 2539075635, 3609230695, 3725561661, 1928887091, 2882293555, 1988674909, 2063640240, 2491088897, 1459647954, 4189817080, 2302804382, 1113892351, 2237858528, 1927010603, 4002880361, 1856122846, 1594404395, 2944033133, 3855189863, 3474975698, 1643104450, 4054590833, 3431086530, 1730235576, 2984608721, 3084664418, 2131803598, 4178205752, 267404349, 1617849798, 1616132681, 1462223176, 736725533, 2327058232, 551665188, 2945899023, 1749386277, 2575514597, 1611482493, 674206544, 2201269090, 3642560800, 728599968, 1680547377, 2620414464, 1388111496, 453204106, 4156223445, 1094905244, 2754698257, 2201108165, 3757000246, 2704524545, 3922940700, 3996465027], s2[3] = [2645754912, 532081118, 2814278639, 3530793624, 1246723035, 1689095255, 2236679235, 4194438865, 2116582143, 3859789411, 157234593, 2045505824, 4245003587, 1687664561, 4083425123, 605965023, 672431967, 1336064205, 3376611392, 214114848, 4258466608, 3232053071, 489488601, 605322005, 3998028058, 264917351, 1912574028, 756637694, 436560991, 202637054, 135989450, 85393697, 2152923392, 3896401662, 2895836408, 2145855233, 3535335007, 115294817, 3147733898, 1922296357, 3464822751, 4117858305, 1037454084, 2725193275, 2127856640, 1417604070, 1148013728, 1827919605, 642362335, 2929772533, 909348033, 1346338451, 3547799649, 297154785, 1917849091, 4161712827, 2883604526, 3968694238, 1469521537, 3780077382, 3375584256, 1763717519, 136166297, 4290970789, 1295325189, 2134727907, 2798151366, 1566297257, 3672928234, 2677174161, 2672173615, 965822077, 2780786062, 289653839, 1133871874, 3491843819, 35685304, 1068898316, 418943774, 672553190, 642281022, 2346158704, 1954014401, 3037126780, 4079815205, 2030668546, 3840588673, 672283427, 1776201016, 359975446, 3750173538, 555499703, 2769985273, 1324923, 69110472, 152125443, 3176785106, 3822147285, 1340634837, 798073664, 1434183902, 15393959, 216384236, 1303690150, 3881221631, 3711134124, 3960975413, 106373927, 2578434224, 1455997841, 1801814300, 1578393881, 1854262133, 3188178946, 3258078583, 2302670060, 1539295533, 3505142565, 3078625975, 2372746020, 549938159, 3278284284, 2620926080, 181285381, 2865321098, 3970029511, 68876850, 488006234, 1728155692, 2608167508, 836007927, 2435231793, 919367643, 3339422534, 3655756360, 1457871481, 40520939, 1380155135, 797931188, 234455205, 2255801827, 3990488299, 397000196, 739833055, 3077865373, 2871719860, 4022553888, 772369276, 390177364, 3853951029, 557662966, 740064294, 1640166671, 1699928825, 3535942136, 622006121, 3625353122, 68743880, 1742502, 219489963, 1664179233, 1577743084, 1236991741, 410585305, 2366487942, 823226535, 1050371084, 3426619607, 3586839478, 212779912, 4147118561, 1819446015, 1911218849, 530248558, 3486241071, 3252585495, 2886188651, 3410272728, 2342195030, 20547779, 2982490058, 3032363469, 3631753222, 312714466, 1870521650, 1493008054, 3491686656, 615382978, 4103671749, 2534517445, 1932181, 2196105170, 278426614, 6369430, 3274544417, 2913018367, 697336853, 2143000447, 2946413531, 701099306, 1558357093, 2805003052, 3500818408, 2321334417, 3567135975, 216290473, 3591032198, 23009561, 1996984579, 3735042806, 2024298078, 3739440863, 569400510, 2339758983, 3016033873, 3097871343, 3639523026, 3844324983, 3256173865, 795471839, 2951117563, 4101031090, 4091603803, 3603732598, 971261452, 534414648, 428311343, 3389027175, 2844869880, 694888862, 1227866773, 2456207019, 3043454569, 2614353370, 3749578031, 3676663836, 459166190, 4132644070, 1794958188, 51825668, 2252611902, 3084671440, 2036672799, 3436641603, 1099053433, 2469121526, 3059204941, 1323291266, 2061838604, 1018778475, 2233344254, 2553501054, 334295216, 3556750194, 1065731521, 183467730], s2[4] = [2127105028, 745436345, 2601412319, 2788391185, 3093987327, 500390133, 1155374404, 389092991, 150729210, 3891597772, 3523549952, 1935325696, 716645080, 946045387, 2901812282, 1774124410, 3869435775, 4039581901, 3293136918, 3438657920, 948246080, 363898952, 3867875531, 1286266623, 1598556673, 68334250, 630723836, 1104211938, 1312863373, 613332731, 2377784574, 1101634306, 441780740, 3129959883, 1917973735, 2510624549, 3238456535, 2544211978, 3308894634, 1299840618, 4076074851, 1756332096, 3977027158, 297047435, 3790297736, 2265573040, 3621810518, 1311375015, 1667687725, 47300608, 3299642885, 2474112369, 201668394, 1468347890, 576830978, 3594690761, 3742605952, 1958042578, 1747032512, 3558991340, 1408974056, 3366841779, 682131401, 1033214337, 1545599232, 4265137049, 206503691, 103024618, 2855227313, 1337551222, 2428998917, 2963842932, 4015366655, 3852247746, 2796956967, 3865723491, 3747938335, 247794022, 3755824572, 702416469, 2434691994, 397379957, 851939612, 2314769512, 218229120, 1380406772, 62274761, 214451378, 3170103466, 2276210409, 3845813286, 28563499, 446592073, 1693330814, 3453727194, 29968656, 3093872512, 220656637, 2470637031, 77972100, 1667708854, 1358280214, 4064765667, 2395616961, 325977563, 4277240721, 4220025399, 3605526484, 3355147721, 811859167, 3069544926, 3962126810, 652502677, 3075892249, 4132761541, 3498924215, 1217549313, 3250244479, 3858715919, 3053989961, 1538642152, 2279026266, 2875879137, 574252750, 3324769229, 2651358713, 1758150215, 141295887, 2719868960, 3515574750, 4093007735, 4194485238, 1082055363, 3417560400, 395511885, 2966884026, 179534037, 3646028556, 3738688086, 1092926436, 2496269142, 257381841, 3772900718, 1636087230, 1477059743, 2499234752, 3811018894, 2675660129, 3285975680, 90732309, 1684827095, 1150307763, 1723134115, 3237045386, 1769919919, 1240018934, 815675215, 750138730, 2239792499, 1234303040, 1995484674, 138143821, 675421338, 1145607174, 1936608440, 3238603024, 2345230278, 2105974004, 323969391, 779555213, 3004902369, 2861610098, 1017501463, 2098600890, 2628620304, 2940611490, 2682542546, 1171473753, 3656571411, 3687208071, 4091869518, 393037935, 159126506, 1662887367, 1147106178, 391545844, 3452332695, 1891500680, 3016609650, 1851642611, 546529401, 1167818917, 3194020571, 2848076033, 3953471836, 575554290, 475796850, 4134673196, 450035699, 2351251534, 844027695, 1080539133, 86184846, 1554234488, 3692025454, 1972511363, 2018339607, 1491841390, 1141460869, 1061690759, 4244549243, 2008416118, 2351104703, 2868147542, 1598468138, 722020353, 1027143159, 212344630, 1387219594, 1725294528, 3745187956, 2500153616, 458938280, 4129215917, 1828119673, 544571780, 3503225445, 2297937496, 1241802790, 267843827, 2694610800, 1397140384, 1558801448, 3782667683, 1806446719, 929573330, 2234912681, 400817706, 616011623, 4121520928, 3603768725, 1761550015, 1968522284, 4053731006, 4192232858, 4005120285, 872482584, 3140537016, 3894607381, 2287405443, 1963876937, 3663887957, 1584857e3, 2975024454, 1833426440, 4025083860], s2[5] = [4143615901, 749497569, 1285769319, 3795025788, 2514159847, 23610292, 3974978748, 844452780, 3214870880, 3751928557, 2213566365, 1676510905, 448177848, 3730751033, 4086298418, 2307502392, 871450977, 3222878141, 4110862042, 3831651966, 2735270553, 1310974780, 2043402188, 1218528103, 2736035353, 4274605013, 2702448458, 3936360550, 2693061421, 162023535, 2827510090, 687910808, 23484817, 3784910947, 3371371616, 779677500, 3503626546, 3473927188, 4157212626, 3500679282, 4248902014, 2466621104, 3899384794, 1958663117, 925738300, 1283408968, 3669349440, 1840910019, 137959847, 2679828185, 1239142320, 1315376211, 1547541505, 1690155329, 739140458, 3128809933, 3933172616, 3876308834, 905091803, 1548541325, 4040461708, 3095483362, 144808038, 451078856, 676114313, 2861728291, 2469707347, 993665471, 373509091, 2599041286, 4025009006, 4170239449, 2149739950, 3275793571, 3749616649, 2794760199, 1534877388, 572371878, 2590613551, 1753320020, 3467782511, 1405125690, 4270405205, 633333386, 3026356924, 3475123903, 632057672, 2846462855, 1404951397, 3882875879, 3915906424, 195638627, 2385783745, 3902872553, 1233155085, 3355999740, 2380578713, 2702246304, 2144565621, 3663341248, 3894384975, 2502479241, 4248018925, 3094885567, 1594115437, 572884632, 3385116731, 767645374, 1331858858, 1475698373, 3793881790, 3532746431, 1321687957, 619889600, 1121017241, 3440213920, 2070816767, 2833025776, 1933951238, 4095615791, 890643334, 3874130214, 859025556, 360630002, 925594799, 1764062180, 3920222280, 4078305929, 979562269, 2810700344, 4087740022, 1949714515, 546639971, 1165388173, 3069891591, 1495988560, 922170659, 1291546247, 2107952832, 1813327274, 3406010024, 3306028637, 4241950635, 153207855, 2313154747, 1608695416, 1150242611, 1967526857, 721801357, 1220138373, 3691287617, 3356069787, 2112743302, 3281662835, 1111556101, 1778980689, 250857638, 2298507990, 673216130, 2846488510, 3207751581, 3562756981, 3008625920, 3417367384, 2198807050, 529510932, 3547516680, 3426503187, 2364944742, 102533054, 2294910856, 1617093527, 1204784762, 3066581635, 1019391227, 1069574518, 1317995090, 1691889997, 3661132003, 510022745, 3238594800, 1362108837, 1817929911, 2184153760, 805817662, 1953603311, 3699844737, 120799444, 2118332377, 207536705, 2282301548, 4120041617, 145305846, 2508124933, 3086745533, 3261524335, 1877257368, 2977164480, 3160454186, 2503252186, 4221677074, 759945014, 254147243, 2767453419, 3801518371, 629083197, 2471014217, 907280572, 3900796746, 940896768, 2751021123, 2625262786, 3161476951, 3661752313, 3260732218, 1425318020, 2977912069, 1496677566, 3988592072, 2140652971, 3126511541, 3069632175, 977771578, 1392695845, 1698528874, 1411812681, 1369733098, 1343739227, 3620887944, 1142123638, 67414216, 3102056737, 3088749194, 1626167401, 2546293654, 3941374235, 697522451, 33404913, 143560186, 2595682037, 994885535, 1247667115, 3859094837, 2699155541, 3547024625, 4114935275, 2968073508, 3199963069, 2732024527, 1237921620, 951448369, 1898488916, 1211705605, 2790989240, 2233243581, 3598044975], s2[6] = [2246066201, 858518887, 1714274303, 3485882003, 713916271, 2879113490, 3730835617, 539548191, 36158695, 1298409750, 419087104, 1358007170, 749914897, 2989680476, 1261868530, 2995193822, 2690628854, 3443622377, 3780124940, 3796824509, 2976433025, 4259637129, 1551479e3, 512490819, 1296650241, 951993153, 2436689437, 2460458047, 144139966, 3136204276, 310820559, 3068840729, 643875328, 1969602020, 1680088954, 2185813161, 3283332454, 672358534, 198762408, 896343282, 276269502, 3014846926, 84060815, 197145886, 376173866, 3943890818, 3813173521, 3545068822, 1316698879, 1598252827, 2633424951, 1233235075, 859989710, 2358460855, 3503838400, 3409603720, 1203513385, 1193654839, 2792018475, 2060853022, 207403770, 1144516871, 3068631394, 1121114134, 177607304, 3785736302, 326409831, 1929119770, 2983279095, 4183308101, 3474579288, 3200513878, 3228482096, 119610148, 1170376745, 3378393471, 3163473169, 951863017, 3337026068, 3135789130, 2907618374, 1183797387, 2015970143, 4045674555, 2182986399, 2952138740, 3928772205, 384012900, 2454997643, 10178499, 2879818989, 2596892536, 111523738, 2995089006, 451689641, 3196290696, 235406569, 1441906262, 3890558523, 3013735005, 4158569349, 1644036924, 376726067, 1006849064, 3664579700, 2041234796, 1021632941, 1374734338, 2566452058, 371631263, 4007144233, 490221539, 206551450, 3140638584, 1053219195, 1853335209, 3412429660, 3562156231, 735133835, 1623211703, 3104214392, 2738312436, 4096837757, 3366392578, 3110964274, 3956598718, 3196820781, 2038037254, 3877786376, 2339753847, 300912036, 3766732888, 2372630639, 1516443558, 4200396704, 1574567987, 4069441456, 4122592016, 2699739776, 146372218, 2748961456, 2043888151, 35287437, 2596680554, 655490400, 1132482787, 110692520, 1031794116, 2188192751, 1324057718, 1217253157, 919197030, 686247489, 3261139658, 1028237775, 3135486431, 3059715558, 2460921700, 986174950, 2661811465, 4062904701, 2752986992, 3709736643, 367056889, 1353824391, 731860949, 1650113154, 1778481506, 784341916, 357075625, 3608602432, 1074092588, 2480052770, 3811426202, 92751289, 877911070, 3600361838, 1231880047, 480201094, 3756190983, 3094495953, 434011822, 87971354, 363687820, 1717726236, 1901380172, 3926403882, 2481662265, 400339184, 1490350766, 2661455099, 1389319756, 2558787174, 784598401, 1983468483, 30828846, 3550527752, 2716276238, 3841122214, 1765724805, 1955612312, 1277890269, 1333098070, 1564029816, 2704417615, 1026694237, 3287671188, 1260819201, 3349086767, 1016692350, 1582273796, 1073413053, 1995943182, 694588404, 1025494639, 3323872702, 3551898420, 4146854327, 453260480, 1316140391, 1435673405, 3038941953, 3486689407, 1622062951, 403978347, 817677117, 950059133, 4246079218, 3278066075, 1486738320, 1417279718, 481875527, 2549965225, 3933690356, 760697757, 1452955855, 3897451437, 1177426808, 1702951038, 4085348628, 2447005172, 1084371187, 3516436277, 3068336338, 1073369276, 1027665953, 3284188590, 1230553676, 1368340146, 2226246512, 267243139, 2274220762, 4070734279, 2497715176, 2423353163, 2504755875], s2[7] = [3793104909, 3151888380, 2817252029, 895778965, 2005530807, 3871412763, 237245952, 86829237, 296341424, 3851759377, 3974600970, 2475086196, 709006108, 1994621201, 2972577594, 937287164, 3734691505, 168608556, 3189338153, 2225080640, 3139713551, 3033610191, 3025041904, 77524477, 185966941, 1208824168, 2344345178, 1721625922, 3354191921, 1066374631, 1927223579, 1971335949, 2483503697, 1551748602, 2881383779, 2856329572, 3003241482, 48746954, 1398218158, 2050065058, 313056748, 4255789917, 393167848, 1912293076, 940740642, 3465845460, 3091687853, 2522601570, 2197016661, 1727764327, 364383054, 492521376, 1291706479, 3264136376, 1474851438, 1685747964, 2575719748, 1619776915, 1814040067, 970743798, 1561002147, 2925768690, 2123093554, 1880132620, 3151188041, 697884420, 2550985770, 2607674513, 2659114323, 110200136, 1489731079, 997519150, 1378877361, 3527870668, 478029773, 2766872923, 1022481122, 431258168, 1112503832, 897933369, 2635587303, 669726182, 3383752315, 918222264, 163866573, 3246985393, 3776823163, 114105080, 1903216136, 761148244, 3571337562, 1690750982, 3166750252, 1037045171, 1888456500, 2010454850, 642736655, 616092351, 365016990, 1185228132, 4174898510, 1043824992, 2023083429, 2241598885, 3863320456, 3279669087, 3674716684, 108438443, 2132974366, 830746235, 606445527, 4173263986, 2204105912, 1844756978, 2532684181, 4245352700, 2969441100, 3796921661, 1335562986, 4061524517, 2720232303, 2679424040, 634407289, 885462008, 3294724487, 3933892248, 2094100220, 339117932, 4048830727, 3202280980, 1458155303, 2689246273, 1022871705, 2464987878, 3714515309, 353796843, 2822958815, 4256850100, 4052777845, 551748367, 618185374, 3778635579, 4020649912, 1904685140, 3069366075, 2670879810, 3407193292, 2954511620, 4058283405, 2219449317, 3135758300, 1120655984, 3447565834, 1474845562, 3577699062, 550456716, 3466908712, 2043752612, 881257467, 869518812, 2005220179, 938474677, 3305539448, 3850417126, 1315485940, 3318264702, 226533026, 965733244, 321539988, 1136104718, 804158748, 573969341, 3708209826, 937399083, 3290727049, 2901666755, 1461057207, 4013193437, 4066861423, 3242773476, 2421326174, 1581322155, 3028952165, 786071460, 3900391652, 3918438532, 1485433313, 4023619836, 3708277595, 3678951060, 953673138, 1467089153, 1930354364, 1533292819, 2492563023, 1346121658, 1685000834, 1965281866, 3765933717, 4190206607, 2052792609, 3515332758, 690371149, 3125873887, 2180283551, 2903598061, 3933952357, 436236910, 289419410, 14314871, 1242357089, 2904507907, 1616633776, 2666382180, 585885352, 3471299210, 2699507360, 1432659641, 277164553, 3354103607, 770115018, 2303809295, 3741942315, 3177781868, 2853364978, 2269453327, 3774259834, 987383833, 1290892879, 225909803, 1741533526, 890078084, 1496906255, 1111072499, 916028167, 243534141, 1252605537, 2204162171, 531204876, 290011180, 3916834213, 102027703, 237315147, 209093447, 1486785922, 220223953, 2758195998, 4175039106, 82940208, 3127791296, 2569425252, 518464269, 1353887104, 3941492737, 2377294467, 3935040926];
}
function ay(e2) {
  this.cast5 = new sy(), this.cast5.setKey(e2), this.encrypt = function(e3) {
    return this.cast5.encrypt(e3);
  };
}
function cy(e2, t2) {
  return (e2 << t2 | e2 >>> 32 - t2) & oy;
}
function hy(e2, t2) {
  return e2[t2] | e2[t2 + 1] << 8 | e2[t2 + 2] << 16 | e2[t2 + 3] << 24;
}
function uy(e2, t2, r2) {
  e2.splice(t2, 4, 255 & r2, r2 >>> 8 & 255, r2 >>> 16 & 255, r2 >>> 24 & 255);
}
function ly(e2, t2) {
  return e2 >>> 8 * t2 & 255;
}
function yy(e2) {
  this.tf = /* @__PURE__ */ function() {
    let e3 = null, t2 = null, r2 = -1, n2 = [], i2 = [[], [], [], []];
    function s2(e4) {
      return i2[0][ly(e4, 0)] ^ i2[1][ly(e4, 1)] ^ i2[2][ly(e4, 2)] ^ i2[3][ly(e4, 3)];
    }
    function a2(e4) {
      return i2[0][ly(e4, 3)] ^ i2[1][ly(e4, 0)] ^ i2[2][ly(e4, 1)] ^ i2[3][ly(e4, 2)];
    }
    function o2(e4, t3) {
      let r3 = s2(t3[0]), i3 = a2(t3[1]);
      t3[2] = cy(t3[2] ^ r3 + i3 + n2[4 * e4 + 8] & oy, 31), t3[3] = cy(t3[3], 1) ^ r3 + 2 * i3 + n2[4 * e4 + 9] & oy, r3 = s2(t3[2]), i3 = a2(t3[3]), t3[0] = cy(t3[0] ^ r3 + i3 + n2[4 * e4 + 10] & oy, 31), t3[1] = cy(t3[1], 1) ^ r3 + 2 * i3 + n2[4 * e4 + 11] & oy;
    }
    function c2(e4, t3) {
      let r3 = s2(t3[0]), i3 = a2(t3[1]);
      t3[2] = cy(t3[2], 1) ^ r3 + i3 + n2[4 * e4 + 10] & oy, t3[3] = cy(t3[3] ^ r3 + 2 * i3 + n2[4 * e4 + 11] & oy, 31), r3 = s2(t3[2]), i3 = a2(t3[3]), t3[0] = cy(t3[0], 1) ^ r3 + i3 + n2[4 * e4 + 8] & oy, t3[1] = cy(t3[1] ^ r3 + 2 * i3 + n2[4 * e4 + 9] & oy, 31);
    }
    return { name: "twofish", blocksize: 16, open: function(t3) {
      let r3, s3, a3, o3, c3;
      e3 = t3;
      const h2 = [], u2 = [], l2 = [];
      let y2;
      const f2 = [];
      let g2, p2, d2;
      const A2 = [[8, 1, 7, 13, 6, 15, 3, 2, 0, 11, 5, 9, 14, 12, 10, 4], [2, 8, 11, 13, 15, 7, 6, 14, 3, 1, 9, 4, 0, 10, 12, 5]], w2 = [[14, 12, 11, 8, 1, 2, 3, 5, 15, 4, 10, 6, 7, 0, 9, 13], [1, 14, 2, 11, 4, 12, 3, 7, 6, 13, 10, 5, 15, 9, 0, 8]], m2 = [[11, 10, 5, 14, 6, 13, 9, 0, 12, 8, 15, 3, 2, 4, 7, 1], [4, 12, 7, 5, 1, 6, 9, 10, 0, 14, 13, 8, 2, 11, 3, 15]], b2 = [[13, 7, 15, 4, 1, 2, 6, 14, 9, 11, 3, 0, 8, 5, 12, 10], [11, 9, 5, 1, 12, 3, 13, 14, 6, 4, 7, 15, 2, 0, 8, 10]], k2 = [0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15], E2 = [0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 5, 14, 7], v2 = [[], []], I2 = [[], [], [], []];
      function B2(e4) {
        return e4 ^ e4 >> 2 ^ [0, 90, 180, 238][3 & e4];
      }
      function S2(e4) {
        return e4 ^ e4 >> 1 ^ e4 >> 2 ^ [0, 238, 180, 90][3 & e4];
      }
      function K2(e4, t4) {
        let r4, n3, i3;
        for (r4 = 0; r4 < 8; r4++) n3 = t4 >>> 24, t4 = t4 << 8 & oy | e4 >>> 24, e4 = e4 << 8 & oy, i3 = n3 << 1, 128 & n3 && (i3 ^= 333), t4 ^= n3 ^ i3 << 16, i3 ^= n3 >>> 1, 1 & n3 && (i3 ^= 166), t4 ^= i3 << 24 | i3 << 8;
        return t4;
      }
      function C2(e4, t4) {
        const r4 = t4 >> 4, n3 = 15 & t4, i3 = A2[e4][r4 ^ n3], s4 = w2[e4][k2[n3] ^ E2[r4]];
        return b2[e4][k2[s4] ^ E2[i3]] << 4 | m2[e4][i3 ^ s4];
      }
      function D2(e4, t4) {
        let r4 = ly(e4, 0), n3 = ly(e4, 1), i3 = ly(e4, 2), s4 = ly(e4, 3);
        switch (y2) {
          case 4:
            r4 = v2[1][r4] ^ ly(t4[3], 0), n3 = v2[0][n3] ^ ly(t4[3], 1), i3 = v2[0][i3] ^ ly(t4[3], 2), s4 = v2[1][s4] ^ ly(t4[3], 3);
          case 3:
            r4 = v2[1][r4] ^ ly(t4[2], 0), n3 = v2[1][n3] ^ ly(t4[2], 1), i3 = v2[0][i3] ^ ly(t4[2], 2), s4 = v2[0][s4] ^ ly(t4[2], 3);
          case 2:
            r4 = v2[0][v2[0][r4] ^ ly(t4[1], 0)] ^ ly(t4[0], 0), n3 = v2[0][v2[1][n3] ^ ly(t4[1], 1)] ^ ly(t4[0], 1), i3 = v2[1][v2[0][i3] ^ ly(t4[1], 2)] ^ ly(t4[0], 2), s4 = v2[1][v2[1][s4] ^ ly(t4[1], 3)] ^ ly(t4[0], 3);
        }
        return I2[0][r4] ^ I2[1][n3] ^ I2[2][i3] ^ I2[3][s4];
      }
      for (e3 = e3.slice(0, 32), r3 = e3.length; 16 !== r3 && 24 !== r3 && 32 !== r3; ) e3[r3++] = 0;
      for (r3 = 0; r3 < e3.length; r3 += 4) l2[r3 >> 2] = hy(e3, r3);
      for (r3 = 0; r3 < 256; r3++) v2[0][r3] = C2(0, r3), v2[1][r3] = C2(1, r3);
      for (r3 = 0; r3 < 256; r3++) g2 = v2[1][r3], p2 = B2(g2), d2 = S2(g2), I2[0][r3] = g2 + (p2 << 8) + (d2 << 16) + (d2 << 24), I2[2][r3] = p2 + (d2 << 8) + (g2 << 16) + (d2 << 24), g2 = v2[0][r3], p2 = B2(g2), d2 = S2(g2), I2[1][r3] = d2 + (d2 << 8) + (p2 << 16) + (g2 << 24), I2[3][r3] = p2 + (g2 << 8) + (d2 << 16) + (p2 << 24);
      for (y2 = l2.length / 2, r3 = 0; r3 < y2; r3++) s3 = l2[r3 + r3], h2[r3] = s3, a3 = l2[r3 + r3 + 1], u2[r3] = a3, f2[y2 - r3 - 1] = K2(s3, a3);
      for (r3 = 0; r3 < 40; r3 += 2) s3 = 16843009 * r3, a3 = s3 + 16843009, s3 = D2(s3, h2), a3 = cy(D2(a3, u2), 8), n2[r3] = s3 + a3 & oy, n2[r3 + 1] = cy(s3 + 2 * a3, 9);
      for (r3 = 0; r3 < 256; r3++) switch (s3 = a3 = o3 = c3 = r3, y2) {
        case 4:
          s3 = v2[1][s3] ^ ly(f2[3], 0), a3 = v2[0][a3] ^ ly(f2[3], 1), o3 = v2[0][o3] ^ ly(f2[3], 2), c3 = v2[1][c3] ^ ly(f2[3], 3);
        case 3:
          s3 = v2[1][s3] ^ ly(f2[2], 0), a3 = v2[1][a3] ^ ly(f2[2], 1), o3 = v2[0][o3] ^ ly(f2[2], 2), c3 = v2[0][c3] ^ ly(f2[2], 3);
        case 2:
          i2[0][r3] = I2[0][v2[0][v2[0][s3] ^ ly(f2[1], 0)] ^ ly(f2[0], 0)], i2[1][r3] = I2[1][v2[0][v2[1][a3] ^ ly(f2[1], 1)] ^ ly(f2[0], 1)], i2[2][r3] = I2[2][v2[1][v2[0][o3] ^ ly(f2[1], 2)] ^ ly(f2[0], 2)], i2[3][r3] = I2[3][v2[1][v2[1][c3] ^ ly(f2[1], 3)] ^ ly(f2[0], 3)];
      }
    }, close: function() {
      n2 = [], i2 = [[], [], [], []];
    }, encrypt: function(e4, i3) {
      t2 = e4, r2 = i3;
      const s3 = [hy(t2, r2) ^ n2[0], hy(t2, r2 + 4) ^ n2[1], hy(t2, r2 + 8) ^ n2[2], hy(t2, r2 + 12) ^ n2[3]];
      for (let e5 = 0; e5 < 8; e5++) o2(e5, s3);
      return uy(t2, r2, s3[2] ^ n2[4]), uy(t2, r2 + 4, s3[3] ^ n2[5]), uy(t2, r2 + 8, s3[0] ^ n2[6]), uy(t2, r2 + 12, s3[1] ^ n2[7]), r2 += 16, t2;
    }, decrypt: function(e4, i3) {
      t2 = e4, r2 = i3;
      const s3 = [hy(t2, r2) ^ n2[4], hy(t2, r2 + 4) ^ n2[5], hy(t2, r2 + 8) ^ n2[6], hy(t2, r2 + 12) ^ n2[7]];
      for (let e5 = 7; e5 >= 0; e5--) c2(e5, s3);
      uy(t2, r2, s3[2] ^ n2[0]), uy(t2, r2 + 4, s3[3] ^ n2[1]), uy(t2, r2 + 8, s3[0] ^ n2[2]), uy(t2, r2 + 12, s3[1] ^ n2[3]), r2 += 16;
    }, finalize: function() {
      return t2;
    } };
  }(), this.tf.open(Array.from(e2), 0), this.encrypt = function(e3) {
    return this.tf.encrypt(Array.from(e3), 0);
  };
}
function fy() {
}
function gy(e2) {
  this.bf = new fy(), this.bf.init(e2), this.encrypt = function(e3) {
    return this.bf.encryptBlock(e3);
  };
}
function Ay(e2, t2, r2, n2) {
  e2[t2] += r2[n2], e2[t2 + 1] += r2[n2 + 1] + (e2[t2] < r2[n2]);
}
function wy(e2, t2) {
  e2[0] += t2, e2[1] += e2[0] < t2;
}
function my(e2, t2, r2, n2, i2, s2, a2, o2) {
  Ay(e2, r2, e2, n2), Ay(e2, r2, t2, a2);
  let c2 = e2[s2] ^ e2[r2], h2 = e2[s2 + 1] ^ e2[r2 + 1];
  e2[s2] = h2, e2[s2 + 1] = c2, Ay(e2, i2, e2, s2), c2 = e2[n2] ^ e2[i2], h2 = e2[n2 + 1] ^ e2[i2 + 1], e2[n2] = c2 >>> 24 ^ h2 << 8, e2[n2 + 1] = h2 >>> 24 ^ c2 << 8, Ay(e2, r2, e2, n2), Ay(e2, r2, t2, o2), c2 = e2[s2] ^ e2[r2], h2 = e2[s2 + 1] ^ e2[r2 + 1], e2[s2] = c2 >>> 16 ^ h2 << 16, e2[s2 + 1] = h2 >>> 16 ^ c2 << 16, Ay(e2, i2, e2, s2), c2 = e2[n2] ^ e2[i2], h2 = e2[n2 + 1] ^ e2[i2 + 1], e2[n2] = h2 >>> 31 ^ c2 << 1, e2[n2 + 1] = c2 >>> 31 ^ h2 << 1;
}
function Ey(e2, t2) {
  const r2 = new Uint32Array(32), n2 = new Uint32Array(e2.b.buffer, e2.b.byteOffset, 32);
  for (let t3 = 0; t3 < 16; t3++) r2[t3] = e2.h[t3], r2[t3 + 16] = by[t3];
  r2[24] ^= e2.t0[0], r2[25] ^= e2.t0[1];
  const i2 = t2 ? 4294967295 : 0;
  r2[28] ^= i2, r2[29] ^= i2;
  for (let e3 = 0; e3 < 12; e3++) {
    const t3 = e3 << 4;
    my(r2, n2, 0, 8, 16, 24, ky[t3 + 0], ky[t3 + 1]), my(r2, n2, 2, 10, 18, 26, ky[t3 + 2], ky[t3 + 3]), my(r2, n2, 4, 12, 20, 28, ky[t3 + 4], ky[t3 + 5]), my(r2, n2, 6, 14, 22, 30, ky[t3 + 6], ky[t3 + 7]), my(r2, n2, 0, 10, 20, 30, ky[t3 + 8], ky[t3 + 9]), my(r2, n2, 2, 12, 22, 24, ky[t3 + 10], ky[t3 + 11]), my(r2, n2, 4, 14, 16, 26, ky[t3 + 12], ky[t3 + 13]), my(r2, n2, 6, 8, 18, 28, ky[t3 + 14], ky[t3 + 15]);
  }
  for (let t3 = 0; t3 < 16; t3++) e2.h[t3] ^= r2[t3] ^ r2[t3 + 16];
}
function Iy(e2, t2, r2, n2) {
  if (e2 > By) throw Error(`outlen must be at most ${By} (given: ${e2})`);
  return new vy(e2, t2, r2, n2);
}
function Hy(e2, t2, r2) {
  return e2[r2 + 0] = t2, e2[r2 + 1] = t2 >> 8, e2[r2 + 2] = t2 >> 16, e2[r2 + 3] = t2 >> 24, e2;
}
function zy(e2, t2, r2) {
  if (t2 > Number.MAX_SAFE_INTEGER) throw Error("LE64: large numbers unsupported");
  let n2 = t2;
  for (let t3 = r2; t3 < r2 + 7; t3++) e2[t3] = n2, n2 = (n2 - e2[t3]) / 256;
  return e2;
}
function Gy(e2, t2, r2) {
  const n2 = new Uint8Array(64), i2 = new Uint8Array(4 + t2.length);
  if (Hy(i2, e2, 0), i2.set(t2, 4), e2 <= 64) return Iy(e2).update(i2).digest(r2), r2;
  const s2 = Math.ceil(e2 / 32) - 2;
  for (let e3 = 0; e3 < s2; e3++) Iy(64).update(0 === e3 ? i2 : n2).digest(n2), r2.set(n2.subarray(0, 32), 32 * e3);
  const a2 = new Uint8Array(Iy(e2 - 32 * s2).update(n2).digest());
  return r2.set(a2, 32 * s2), r2;
}
function _y(e2, t2, r2, n2) {
  return e2.fn.XOR(t2.byteOffset, r2.byteOffset, n2.byteOffset), t2;
}
function jy(e2, t2, r2, n2) {
  return e2.fn.G(t2.byteOffset, r2.byteOffset, n2.byteOffset, e2.refs.gZ.byteOffset), n2;
}
function qy(e2, t2, r2, n2) {
  return e2.fn.G2(t2.byteOffset, r2.byteOffset, n2.byteOffset, e2.refs.gZ.byteOffset), n2;
}
function* Vy(e2, t2, r2, n2, i2, s2, a2, o2) {
  e2.refs.prngTmp.fill(0);
  const c2 = e2.refs.prngTmp.subarray(0, 48);
  zy(c2, t2, 0), zy(c2, r2, 8), zy(c2, n2, 16), zy(c2, i2, 24), zy(c2, s2, 32), zy(c2, Ky, 40);
  for (let t3 = 1; t3 <= a2; t3++) {
    zy(e2.refs.prngTmp, t3, c2.length);
    const r3 = qy(e2, e2.refs.ZERO1024, e2.refs.prngTmp, e2.refs.prngR);
    for (let e3 = 1 === t3 ? 8 * o2 : 0; e3 < r3.length; e3 += 8) yield r3.subarray(e3, e3 + 8);
  }
  return [];
}
function Zy(e2, { memory: t2, instance: r2 }) {
  if (!Oy) throw Error("BigEndian system not supported");
  const n2 = function({ type: e3, version: t3, tagLength: r3, password: n3, salt: i3, ad: s3, secret: a3, parallelism: o3, memorySize: c3, passes: h3 }) {
    const u3 = (e4, t4, r4, n4) => {
      if (t4 < r4 || t4 > n4) throw Error(`${e4} size should be between ${r4} and ${n4} bytes`);
    };
    if (e3 !== Ky || t3 !== Cy) throw Error("Unsupported type or version");
    return u3("password", n3, Ry, Qy), u3("salt", i3, xy, Py), u3("tag", r3, Uy, Dy), u3("memory", c3, 8 * o3, Ty), s3 && u3("associated data", s3, 0, Ly), a3 && u3("secret", a3, 0, My), { type: e3, version: t3, tagLength: r3, password: n3, salt: i3, ad: s3, secret: a3, lanes: o3, memorySize: c3, passes: h3 };
  }({ type: Ky, version: Cy, ...e2 }), { G: i2, G2: s2, xor: a2, getLZ: o2 } = r2.exports, c2 = {}, h2 = {};
  h2.G = i2, h2.G2 = s2, h2.XOR = a2;
  const u2 = 4 * n2.lanes * Math.floor(n2.memorySize / (4 * n2.lanes)), l2 = u2 * Ny + 10 * Jy;
  if (t2.buffer.byteLength < l2) {
    const e3 = Math.ceil((l2 - t2.buffer.byteLength) / Yy);
    t2.grow(e3);
  }
  let y2 = 0;
  c2.gZ = new Uint8Array(t2.buffer, y2, Ny), y2 += c2.gZ.length, c2.prngR = new Uint8Array(t2.buffer, y2, Ny), y2 += c2.prngR.length, c2.prngTmp = new Uint8Array(t2.buffer, y2, Ny), y2 += c2.prngTmp.length, c2.ZERO1024 = new Uint8Array(t2.buffer, y2, 1024), y2 += c2.ZERO1024.length;
  const f2 = new Uint32Array(t2.buffer, y2, 2);
  y2 += f2.length * Uint32Array.BYTES_PER_ELEMENT;
  const g2 = { fn: h2, refs: c2 }, p2 = new Uint8Array(t2.buffer, y2, Ny);
  y2 += p2.length;
  const d2 = new Uint8Array(t2.buffer, y2, n2.memorySize * Ny), A2 = new Uint8Array(t2.buffer, 0, y2), w2 = function(e3) {
    const t3 = Iy(Fy), r3 = new Uint8Array(4), n3 = new Uint8Array(24);
    Hy(n3, e3.lanes, 0), Hy(n3, e3.tagLength, 4), Hy(n3, e3.memorySize, 8), Hy(n3, e3.passes, 12), Hy(n3, e3.version, 16), Hy(n3, e3.type, 20);
    const i3 = [n3];
    e3.password ? (i3.push(Hy(new Uint8Array(4), e3.password.length, 0)), i3.push(e3.password)) : i3.push(r3);
    e3.salt ? (i3.push(Hy(new Uint8Array(4), e3.salt.length, 0)), i3.push(e3.salt)) : i3.push(r3);
    e3.secret ? (i3.push(Hy(new Uint8Array(4), e3.secret.length, 0)), i3.push(e3.secret)) : i3.push(r3);
    e3.ad ? (i3.push(Hy(new Uint8Array(4), e3.ad.length, 0)), i3.push(e3.ad)) : i3.push(r3);
    t3.update(function(e4) {
      if (1 === e4.length) return e4[0];
      let t4 = 0;
      for (let r5 = 0; r5 < e4.length; r5++) {
        if (!(e4[r5] instanceof Uint8Array)) throw Error("concatArrays: Data must be in the form of a Uint8Array");
        t4 += e4[r5].length;
      }
      const r4 = new Uint8Array(t4);
      let n4 = 0;
      return e4.forEach((e5) => {
        r4.set(e5, n4), n4 += e5.length;
      }), r4;
    }(i3));
    const s3 = t3.digest();
    return new Uint8Array(s3);
  }(n2), m2 = u2 / n2.lanes, b2 = Array(n2.lanes).fill(null).map(() => Array(m2)), k2 = (e3, t3) => (b2[e3][t3] = d2.subarray(e3 * m2 * 1024 + 1024 * t3, e3 * m2 * 1024 + 1024 * t3 + Ny), b2[e3][t3]);
  for (let e3 = 0; e3 < n2.lanes; e3++) {
    const t3 = new Uint8Array(w2.length + 8);
    t3.set(w2), Hy(t3, 0, w2.length), Hy(t3, e3, w2.length + 4), Gy(Ny, t3, k2(e3, 0)), Hy(t3, 1, w2.length), Gy(Ny, t3, k2(e3, 1));
  }
  const E2 = m2 / 4;
  for (let e3 = 0; e3 < n2.passes; e3++) for (let t3 = 0; t3 < 4; t3++) {
    const r3 = 0 === e3 && t3 <= 1;
    for (let i3 = 0; i3 < n2.lanes; i3++) {
      let s3 = 0 === t3 && 0 === e3 ? 2 : 0;
      const a3 = r3 ? Vy(g2, e3, i3, t3, u2, n2.passes, E2, s3) : null;
      for (; s3 < E2; s3++) {
        const c3 = t3 * E2 + s3, h3 = c3 > 0 ? b2[i3][c3 - 1] : b2[i3][m2 - 1], u3 = r3 ? a3.next().value : h3;
        o2(f2.byteOffset, u3.byteOffset, i3, n2.lanes, e3, t3, s3, 4, E2);
        const l3 = f2[0], y3 = f2[1];
        0 === e3 && k2(i3, c3), jy(g2, h3, b2[l3][y3], e3 > 0 ? p2 : b2[i3][c3]), e3 > 0 && _y(g2, b2[i3][c3], p2, b2[i3][c3]);
      }
    }
  }
  const v2 = b2[0][m2 - 1];
  for (let e3 = 1; e3 < n2.lanes; e3++) _y(g2, v2, v2, b2[e3][m2 - 1]);
  const I2 = Gy(n2.tagLength, v2, new Uint8Array(n2.tagLength));
  return A2.fill(0), t2.grow(0), I2;
}
async function $y(e2, t2) {
  const r2 = new WebAssembly.Memory({ initial: 1040, maximum: 65536 }), n2 = await async function(e3, t3, r3) {
    const n3 = { env: { memory: e3 } };
    if (void 0 === Wy) try {
      const e4 = await t3(n3);
      return Wy = true, e4;
    } catch (e4) {
      Wy = false;
    }
    return (Wy ? t3 : r3)(n3);
  }(r2, e2, t2);
  return (e3) => Zy(e3, { instance: n2.instance, memory: r2 });
}
function Xy(t2, r2, n2, i2) {
  var s2 = null, a2 = e.atob(n2), o2 = a2.length;
  s2 = new Uint8Array(new ArrayBuffer(o2));
  for (var c2 = 0; c2 < o2; c2++) s2[c2] = a2.charCodeAt(c2);
  return function(e2, t3) {
    var r3 = WebAssembly.instantiate, n3 = WebAssembly.compile;
    return t3 ? r3(e2, t3) : n3(e2);
  }(s2, i2);
}
var e, r, n, i, s, a, y, f, Q, R, T, L, M, N, F, O, J, Y, ee, te, ye, pe, we, me, be, ke, Ie, Be, Se, Ke, Ce, De, Ue, Pe, xe, Te, Fe, Oe, He, Ye, Ze, We, $e, Xe, et, tt, rt, nt, it, st, at, ot, ct, Bt, Rt, Nt, Ot, Ht, Yt, Zt, Wt, ir, sr, ar, lr, yr, fr, dr, kr, Er, vr, Ir, Br, Sr, Cr, Dr, Ur, Pr, Rr, Tr, Lr, Mr, Nr, Or, Hr, zr, $r, Xr, en, tn, an, on, cn, hn, gn, An, mn, Cn, Dn, Un, Pn, xn, Qn, Rn, Tn, _n, jn, Yn, Xn, ti, ri, li, yi, fi, gi, pi, di, Si, Ki, Ci, Di, Qi, Ri, Li, Mi, Ni, Fi, Hi, _i, ji, qi, Vi, Ji, Yi, Zi, Wi, $i, rs, ns, is, cs, hs, ls, ys, fs, gs, ps, bs, ks, Es, vs, Is, Bs, Cs, Ds, Us, Ps, xs, Qs, Rs, Ts, Ls, Ms, Gs, Ns, Fs, Os, Hs, zs, _s, js, qs, Vs, Js, Ys, Zs, Ws, $s, Xs, ea, ta, ra, na, ia, sa, aa, oa, ca, ha, ua, la, ya, fa, ga, pa, da, Aa, wa, ma, ba, ka, Ea, va, Ia, Ba, Ca, Ua, Pa, xa, Ta, La, Ma, Na, Fa, Ha, za, Ga, ja, qa, Va, Ja, Ya, Za, Wa, $a, eo, to, ro, no, io, so, vo, Io, Bo, So, Ko, Co, Do, Uo, Po, No, Fo, Oo, Ho, qo, Vo, lc, kc, Ec, vc, Ic, Bc, Uc, Qc, Rc, Tc, Lc, Mc, Nc, Fc, Oc, Hc, zc, Gc, _c, jc, qc, Vc, Wc, th, uh, gh, ph, dh, wh, kh, Eh, vh, Ih, Bh, Sh, Kh, Ch, Rh, Fh, Oh, Hh, zh, Vh, Jh, Yh, Zh, Wh, $h, nu, iu, su, au, hu, uu, lu, yu, fu, gu, pu, du, Au, wu, mu, bu, ku, Eu, vu, Iu, Bu, Su, Ku, Cu, Du, Uu, Pu, xu, Qu, Ru, Tu, Lu, Mu, Nu, Fu, Ou, Hu, zu, Gu, _u, ju, qu, Vu, Ju, Yu, Wu, $u, el, tl, rl, nl, il, sl, al, ol, cl, hl, yl, fl, gl, pl, dl, Al, wl, ml, bl, kl, El, vl, Il, Bl, Sl, Kl, Cl, Dl, Ul, Pl, xl, Ql, Rl, Tl, Ll, Ml, Nl, Fl, Ol, Hl, zl, Gl, _l, ql, Vl, Jl, Yl, Zl, Wl, $l, Xl, ey, ty, oy, py, dy, by, ky, vy, By, Sy, Ky, Cy, Dy, Uy, Py, xy, Qy, Ry, Ty, Ly, My, Ny, Fy, Oy, Jy, Yy, Wy, ef, tf, rf, nf, sf, af, of, cf, hf, uf, lf, yf, ff, gf, pf, df, Af, wf, mf, bf;
var init_openpgp_min = __esm({
  "lib/openpgp.min.mjs"() {
    e = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
    r = Symbol("doneWritingPromise");
    n = Symbol("doneWritingResolve");
    i = Symbol("doneWritingReject");
    s = Symbol("readingIndex");
    a = class _a2 extends Array {
      constructor() {
        super(), Object.setPrototypeOf(this, _a2.prototype), this[r] = new Promise((e2, t2) => {
          this[n] = e2, this[i] = t2;
        }), this[r].catch(() => {
        });
      }
    };
    a.prototype.getReader = function() {
      return void 0 === this[s] && (this[s] = 0), { read: async () => (await this[r], this[s] === this.length ? { value: void 0, done: true } : { value: this[this[s]++], done: false }) };
    }, a.prototype.readToEnd = async function(e2) {
      await this[r];
      const t2 = e2(this.slice(this[s]));
      return this.length = 0, t2;
    }, a.prototype.clone = function() {
      const e2 = new a();
      return e2[r] = this[r].then(() => {
        e2.push(...this);
      }), e2;
    }, c.prototype.write = async function(e2) {
      this.stream.push(e2);
    }, c.prototype.close = async function() {
      this.stream[n]();
    }, c.prototype.abort = async function(e2) {
      return this.stream[i](e2), e2;
    }, c.prototype.releaseLock = function() {
    }, "object" == typeof e.process && e.process.versions;
    y = /* @__PURE__ */ new WeakSet();
    f = Symbol("externalBuffer");
    g.prototype.read = async function() {
      if (this[f] && this[f].length) {
        return { done: false, value: this[f].shift() };
      }
      return this._read();
    }, g.prototype.releaseLock = function() {
      this[f] && (this.stream[f] = this[f]), this._releaseLock();
    }, g.prototype.cancel = function(e2) {
      return this._cancel(e2);
    }, g.prototype.readLine = async function() {
      let e2, t2 = [];
      for (; !e2; ) {
        let { done: r2, value: n2 } = await this.read();
        if (n2 += "", r2) return t2.length ? A(t2) : void 0;
        const i2 = n2.indexOf("\n") + 1;
        i2 && (e2 = A(t2.concat(n2.substr(0, i2))), t2 = []), i2 !== n2.length && t2.push(n2.substr(i2));
      }
      return this.unshift(...t2), e2;
    }, g.prototype.readByte = async function() {
      const { done: e2, value: t2 } = await this.read();
      if (e2) return;
      const r2 = t2[0];
      return this.unshift(K(t2, 1)), r2;
    }, g.prototype.readBytes = async function(e2) {
      const t2 = [];
      let r2 = 0;
      for (; ; ) {
        const { done: n2, value: i2 } = await this.read();
        if (n2) return t2.length ? A(t2) : void 0;
        if (t2.push(i2), r2 += i2.length, r2 >= e2) {
          const r3 = A(t2);
          return this.unshift(K(r3, e2)), K(r3, 0, e2);
        }
      }
    }, g.prototype.peekBytes = async function(e2) {
      const t2 = await this.readBytes(e2);
      return this.unshift(t2), t2;
    }, g.prototype.unshift = function(...e2) {
      this[f] || (this[f] = []), 1 === e2.length && u(e2[0]) && this[f].length && e2[0].length && this[f][0].byteOffset >= e2[0].length ? this[f][0] = new Uint8Array(this[f][0].buffer, this[f][0].byteOffset - e2[0].length, this[f][0].byteLength + e2[0].length) : this[f].unshift(...e2.filter((e3) => e3 && e3.length));
    }, g.prototype.readToEnd = async function(e2 = A) {
      const t2 = [];
      for (; ; ) {
        const { done: e3, value: r2 } = await this.read();
        if (e3) break;
        t2.push(r2);
      }
      return e2(t2);
    };
    Q = Symbol("byValue");
    R = { curve: { nistP256: "nistP256", p256: "nistP256", nistP384: "nistP384", p384: "nistP384", nistP521: "nistP521", p521: "nistP521", secp256k1: "secp256k1", ed25519Legacy: "ed25519Legacy", ed25519: "ed25519Legacy", curve25519Legacy: "curve25519Legacy", curve25519: "curve25519Legacy", brainpoolP256r1: "brainpoolP256r1", brainpoolP384r1: "brainpoolP384r1", brainpoolP512r1: "brainpoolP512r1" }, s2k: { simple: 0, salted: 1, iterated: 3, argon2: 4, gnu: 101 }, publicKey: { rsaEncryptSign: 1, rsaEncrypt: 2, rsaSign: 3, elgamal: 16, dsa: 17, ecdh: 18, ecdsa: 19, eddsaLegacy: 22, aedh: 23, aedsa: 24, x25519: 25, x448: 26, ed25519: 27, ed448: 28 }, symmetric: { idea: 1, tripledes: 2, cast5: 3, blowfish: 4, aes128: 7, aes192: 8, aes256: 9, twofish: 10 }, compression: { uncompressed: 0, zip: 1, zlib: 2, bzip2: 3 }, hash: { md5: 1, sha1: 2, ripemd: 3, sha256: 8, sha384: 9, sha512: 10, sha224: 11, sha3_256: 12, sha3_512: 14 }, webHash: { "SHA-1": 2, "SHA-256": 8, "SHA-384": 9, "SHA-512": 10 }, aead: { eax: 1, ocb: 2, gcm: 3, experimentalGCM: 100 }, packet: { publicKeyEncryptedSessionKey: 1, signature: 2, symEncryptedSessionKey: 3, onePassSignature: 4, secretKey: 5, publicKey: 6, secretSubkey: 7, compressedData: 8, symmetricallyEncryptedData: 9, marker: 10, literalData: 11, trust: 12, userID: 13, publicSubkey: 14, userAttribute: 17, symEncryptedIntegrityProtectedData: 18, modificationDetectionCode: 19, aeadEncryptedData: 20, padding: 21 }, literal: { binary: 98, text: 116, utf8: 117, mime: 109 }, signature: { binary: 0, text: 1, standalone: 2, certGeneric: 16, certPersona: 17, certCasual: 18, certPositive: 19, certRevocation: 48, subkeyBinding: 24, keyBinding: 25, key: 31, keyRevocation: 32, subkeyRevocation: 40, timestamp: 64, thirdParty: 80 }, signatureSubpacket: { signatureCreationTime: 2, signatureExpirationTime: 3, exportableCertification: 4, trustSignature: 5, regularExpression: 6, revocable: 7, keyExpirationTime: 9, placeholderBackwardsCompatibility: 10, preferredSymmetricAlgorithms: 11, revocationKey: 12, issuerKeyID: 16, notationData: 20, preferredHashAlgorithms: 21, preferredCompressionAlgorithms: 22, keyServerPreferences: 23, preferredKeyServer: 24, primaryUserID: 25, policyURI: 26, keyFlags: 27, signersUserID: 28, reasonForRevocation: 29, features: 30, signatureTarget: 31, embeddedSignature: 32, issuerFingerprint: 33, preferredAEADAlgorithms: 34, preferredCipherSuites: 39 }, keyFlags: { certifyKeys: 1, signData: 2, encryptCommunication: 4, encryptStorage: 8, splitPrivateKey: 16, authentication: 32, sharedPrivateKey: 128 }, armor: { multipartSection: 0, multipartLast: 1, signed: 2, message: 3, publicKey: 4, privateKey: 5, signature: 6 }, reasonForRevocation: { noReason: 0, keySuperseded: 1, keyCompromised: 2, keyRetired: 3, userIDInvalid: 32 }, features: { modificationDetection: 1, aead: 2, v5Keys: 4, seipdv2: 8 }, write: function(e2, t2) {
      if ("number" == typeof t2 && (t2 = this.read(e2, t2)), void 0 !== e2[t2]) return e2[t2];
      throw Error("Invalid enum value.");
    }, read: function(e2, t2) {
      if (e2[Q] || (e2[Q] = [], Object.entries(e2).forEach(([t3, r2]) => {
        e2[Q][r2] = t3;
      })), void 0 !== e2[Q][t2]) return e2[Q][t2];
      throw Error("Invalid enum value.");
    } };
    T = { preferredHashAlgorithm: R.hash.sha512, preferredSymmetricAlgorithm: R.symmetric.aes256, preferredCompressionAlgorithm: R.compression.uncompressed, aeadProtect: false, parseAEADEncryptedV4KeysAsLegacy: false, preferredAEADAlgorithm: R.aead.gcm, aeadChunkSizeByte: 12, v6Keys: false, enableParsingV5Entities: false, s2kType: R.s2k.iterated, s2kIterationCountByte: 224, s2kArgon2Params: { passes: 3, parallelism: 4, memoryExponent: 16 }, allowUnauthenticatedMessages: false, allowUnauthenticatedStream: false, minRSABits: 2047, passwordCollisionCheck: false, allowInsecureDecryptionWithSigningKeys: false, allowInsecureVerificationWithReformattedKeys: false, allowMissingKeyFlags: false, constantTimePKCS1Decryption: false, constantTimePKCS1DecryptionSupportedSymmetricAlgorithms: /* @__PURE__ */ new Set([R.symmetric.aes128, R.symmetric.aes192, R.symmetric.aes256]), ignoreUnsupportedPackets: true, ignoreMalformedPackets: false, additionalAllowedPackets: [], showVersion: false, showComment: false, versionString: "OpenPGP.js 6.1.0", commentString: "https://openpgpjs.org", maxUserIDLength: 5120, knownNotations: [], nonDeterministicSignaturesViaNotation: true, useEllipticFallback: true, rejectHashAlgorithms: /* @__PURE__ */ new Set([R.hash.md5, R.hash.ripemd]), rejectMessageHashAlgorithms: /* @__PURE__ */ new Set([R.hash.md5, R.hash.ripemd, R.hash.sha1]), rejectPublicKeyAlgorithms: /* @__PURE__ */ new Set([R.publicKey.elgamal, R.publicKey.dsa]), rejectCurves: /* @__PURE__ */ new Set([R.curve.secp256k1]) };
    L = (() => {
      try {
        return "development" === process.env.NODE_ENV;
      } catch (e2) {
      }
      return false;
    })();
    M = { isString: function(e2) {
      return "string" == typeof e2 || e2 instanceof String;
    }, nodeRequire: () => {
    }, isArray: function(e2) {
      return e2 instanceof Array;
    }, isUint8Array: u, isStream: h, getNobleCurve: async (e2, t2) => {
      if (!T.useEllipticFallback) throw Error("This curve is only supported in the full build of OpenPGP.js");
      const { nobleCurves: r2 } = await Promise.resolve().then(function() {
        return Ul;
      });
      switch (e2) {
        case R.publicKey.ecdh:
        case R.publicKey.ecdsa: {
          const e3 = r2.get(t2);
          if (!e3) throw Error("Unsupported curve");
          return e3;
        }
        case R.publicKey.x448:
          return r2.get("x448");
        case R.publicKey.ed448:
          return r2.get("ed448");
        default:
          throw Error("Unsupported curve");
      }
    }, readNumber: function(e2) {
      let t2 = 0;
      for (let r2 = 0; r2 < e2.length; r2++) t2 += 256 ** r2 * e2[e2.length - 1 - r2];
      return t2;
    }, writeNumber: function(e2, t2) {
      const r2 = new Uint8Array(t2);
      for (let n2 = 0; n2 < t2; n2++) r2[n2] = e2 >> 8 * (t2 - n2 - 1) & 255;
      return r2;
    }, readDate: function(e2) {
      const t2 = M.readNumber(e2);
      return new Date(1e3 * t2);
    }, writeDate: function(e2) {
      const t2 = Math.floor(e2.getTime() / 1e3);
      return M.writeNumber(t2, 4);
    }, normalizeDate: function(e2 = Date.now()) {
      return null === e2 || e2 === 1 / 0 ? e2 : new Date(1e3 * Math.floor(+e2 / 1e3));
    }, readMPI: function(e2) {
      const t2 = (e2[0] << 8 | e2[1]) + 7 >>> 3;
      return M.readExactSubarray(e2, 2, 2 + t2);
    }, readExactSubarray: function(e2, t2, r2) {
      if (e2.length < r2 - t2) throw Error("Input array too short");
      return e2.subarray(t2, r2);
    }, leftPad(e2, t2) {
      if (e2.length > t2) throw Error("Input array too long");
      const r2 = new Uint8Array(t2), n2 = t2 - e2.length;
      return r2.set(e2, n2), r2;
    }, uint8ArrayToMPI: function(e2) {
      const t2 = M.uint8ArrayBitLength(e2);
      if (0 === t2) throw Error("Zero MPI");
      const r2 = e2.subarray(e2.length - Math.ceil(t2 / 8)), n2 = new Uint8Array([(65280 & t2) >> 8, 255 & t2]);
      return M.concatUint8Array([n2, r2]);
    }, uint8ArrayBitLength: function(e2) {
      let t2;
      for (t2 = 0; t2 < e2.length && 0 === e2[t2]; t2++) ;
      if (t2 === e2.length) return 0;
      const r2 = e2.subarray(t2);
      return 8 * (r2.length - 1) + M.nbits(r2[0]);
    }, hexToUint8Array: function(e2) {
      const t2 = new Uint8Array(e2.length >> 1);
      for (let r2 = 0; r2 < e2.length >> 1; r2++) t2[r2] = parseInt(e2.substr(r2 << 1, 2), 16);
      return t2;
    }, uint8ArrayToHex: function(e2) {
      const t2 = "0123456789abcdef";
      let r2 = "";
      return e2.forEach((e3) => {
        r2 += t2[e3 >> 4] + t2[15 & e3];
      }), r2;
    }, stringToUint8Array: function(e2) {
      return k(e2, (e3) => {
        if (!M.isString(e3)) throw Error("stringToUint8Array: Data must be in the form of a string");
        const t2 = new Uint8Array(e3.length);
        for (let r2 = 0; r2 < e3.length; r2++) t2[r2] = e3.charCodeAt(r2);
        return t2;
      });
    }, uint8ArrayToString: function(e2) {
      const t2 = [], r2 = 16384, n2 = (e2 = new Uint8Array(e2)).length;
      for (let i2 = 0; i2 < n2; i2 += r2) t2.push(String.fromCharCode.apply(String, e2.subarray(i2, i2 + r2 < n2 ? i2 + r2 : n2)));
      return t2.join("");
    }, encodeUTF8: function(e2) {
      const t2 = new TextEncoder("utf-8");
      function r2(e3, r3 = false) {
        return t2.encode(e3, { stream: !r3 });
      }
      return k(e2, r2, () => r2("", true));
    }, decodeUTF8: function(e2) {
      const t2 = new TextDecoder("utf-8");
      function r2(e3, r3 = false) {
        return t2.decode(e3, { stream: !r3 });
      }
      return k(e2, r2, () => r2(new Uint8Array(), true));
    }, concat: A, concatUint8Array: l, equalsUint8Array: function(e2, t2) {
      if (!M.isUint8Array(e2) || !M.isUint8Array(t2)) throw Error("Data must be in the form of a Uint8Array");
      if (e2.length !== t2.length) return false;
      for (let r2 = 0; r2 < e2.length; r2++) if (e2[r2] !== t2[r2]) return false;
      return true;
    }, writeChecksum: function(e2) {
      let t2 = 0;
      for (let r2 = 0; r2 < e2.length; r2++) t2 = t2 + e2[r2] & 65535;
      return M.writeNumber(t2, 2);
    }, printDebug: function(e2) {
      L && console.log("[OpenPGP.js debug]", e2);
    }, printDebugError: function(e2) {
      L && console.error("[OpenPGP.js debug]", e2);
    }, nbits: function(e2) {
      let t2 = 1, r2 = e2 >>> 16;
      return 0 !== r2 && (e2 = r2, t2 += 16), r2 = e2 >> 8, 0 !== r2 && (e2 = r2, t2 += 8), r2 = e2 >> 4, 0 !== r2 && (e2 = r2, t2 += 4), r2 = e2 >> 2, 0 !== r2 && (e2 = r2, t2 += 2), r2 = e2 >> 1, 0 !== r2 && (e2 = r2, t2 += 1), t2;
    }, double: function(e2) {
      const t2 = new Uint8Array(e2.length), r2 = e2.length - 1;
      for (let n2 = 0; n2 < r2; n2++) t2[n2] = e2[n2] << 1 ^ e2[n2 + 1] >> 7;
      return t2[r2] = e2[r2] << 1 ^ 135 * (e2[0] >> 7), t2;
    }, shiftRight: function(e2, t2) {
      if (t2) for (let r2 = e2.length - 1; r2 >= 0; r2--) e2[r2] >>= t2, r2 > 0 && (e2[r2] |= e2[r2 - 1] << 8 - t2);
      return e2;
    }, getWebCrypto: function() {
      const t2 = void 0 !== e && e.crypto && e.crypto.subtle || this.getNodeCrypto()?.webcrypto.subtle;
      if (!t2) throw Error("The WebCrypto API is not available");
      return t2;
    }, getNodeCrypto: function() {
      return this.nodeRequire("crypto");
    }, getNodeZlib: function() {
      return this.nodeRequire("zlib");
    }, getNodeBuffer: function() {
      return (this.nodeRequire("buffer") || {}).Buffer;
    }, getHardwareConcurrency: function() {
      if ("undefined" != typeof navigator) return navigator.hardwareConcurrency || 1;
      return this.nodeRequire("os").cpus().length;
    }, isEmailAddress: function(e2) {
      if (!M.isString(e2)) return false;
      return /^[^\p{C}\p{Z}@<>\\]+@[^\p{C}\p{Z}@<>\\]+[^\p{C}\p{Z}\p{P}]$/u.test(e2);
    }, canonicalizeEOL: function(e2) {
      let t2 = false;
      return k(e2, (e3) => {
        let r2;
        t2 && (e3 = M.concatUint8Array([new Uint8Array([13]), e3])), 13 === e3[e3.length - 1] ? (t2 = true, e3 = e3.subarray(0, -1)) : t2 = false;
        const n2 = [];
        for (let t3 = 0; r2 = e3.indexOf(10, t3) + 1, r2; t3 = r2) 13 !== e3[r2 - 2] && n2.push(r2);
        if (!n2.length) return e3;
        const i2 = new Uint8Array(e3.length + n2.length);
        let s2 = 0;
        for (let t3 = 0; t3 < n2.length; t3++) {
          const r3 = e3.subarray(n2[t3 - 1] || 0, n2[t3]);
          i2.set(r3, s2), s2 += r3.length, i2[s2 - 1] = 13, i2[s2] = 10, s2++;
        }
        return i2.set(e3.subarray(n2[n2.length - 1] || 0), s2), i2;
      }, () => t2 ? new Uint8Array([13]) : void 0);
    }, nativeEOL: function(e2) {
      let t2 = false;
      return k(e2, (e3) => {
        let r2;
        13 === (e3 = t2 && 10 !== e3[0] ? M.concatUint8Array([new Uint8Array([13]), e3]) : new Uint8Array(e3))[e3.length - 1] ? (t2 = true, e3 = e3.subarray(0, -1)) : t2 = false;
        let n2 = 0;
        for (let t3 = 0; t3 !== e3.length; t3 = r2) {
          r2 = e3.indexOf(13, t3) + 1, r2 || (r2 = e3.length);
          const i2 = r2 - (10 === e3[r2] ? 1 : 0);
          t3 && e3.copyWithin(n2, t3, i2), n2 += i2 - t3;
        }
        return e3.subarray(0, n2);
      }, () => t2 ? new Uint8Array([13]) : void 0);
    }, removeTrailingSpaces: function(e2) {
      return e2.split("\n").map((e3) => {
        let t2 = e3.length - 1;
        for (; t2 >= 0 && (" " === e3[t2] || "	" === e3[t2] || "\r" === e3[t2]); t2--) ;
        return e3.substr(0, t2 + 1);
      }).join("\n");
    }, wrapError: function(e2, t2) {
      if (!t2) return Error(e2);
      try {
        t2.message = e2 + ": " + t2.message;
      } catch (e3) {
      }
      return t2;
    }, constructAllowedPackets: function(e2) {
      const t2 = {};
      return e2.forEach((e3) => {
        if (!e3.tag) throw Error("Invalid input: expected a packet class");
        t2[e3.tag] = e3;
      }), t2;
    }, anyPromise: function(e2) {
      return new Promise(async (t2, r2) => {
        let n2;
        await Promise.all(e2.map(async (e3) => {
          try {
            t2(await e3);
          } catch (e4) {
            n2 = e4;
          }
        })), r2(n2);
      });
    }, selectUint8Array: function(e2, t2, r2) {
      const n2 = Math.max(t2.length, r2.length), i2 = new Uint8Array(n2);
      let s2 = 0;
      for (let n3 = 0; n3 < i2.length; n3++) i2[n3] = t2[n3] & 256 - e2 | r2[n3] & 255 + e2, s2 += e2 & n3 < t2.length | 1 - e2 & n3 < r2.length;
      return i2.subarray(0, s2);
    }, selectUint8: function(e2, t2, r2) {
      return t2 & 256 - e2 | r2 & 255 + e2;
    }, isAES: function(e2) {
      return e2 === R.symmetric.aes128 || e2 === R.symmetric.aes192 || e2 === R.symmetric.aes256;
    } };
    N = M.getNodeBuffer();
    N ? (F = (e2) => N.from(e2).toString("base64"), O = (e2) => {
      const t2 = N.from(e2, "base64");
      return new Uint8Array(t2.buffer, t2.byteOffset, t2.byteLength);
    }) : (F = (e2) => btoa(M.uint8ArrayToString(e2)), O = (e2) => M.stringToUint8Array(atob(e2)));
    J = [Array(255), Array(255), Array(255), Array(255)];
    for (let e2 = 0; e2 <= 255; e2++) {
      let t2 = e2 << 16;
      for (let e3 = 0; e3 < 8; e3++) t2 = t2 << 1 ^ (8388608 & t2 ? 8801531 : 0);
      J[0][e2] = (16711680 & t2) >> 16 | 65280 & t2 | (255 & t2) << 16;
    }
    for (let e2 = 0; e2 <= 255; e2++) J[1][e2] = J[0][e2] >> 8 ^ J[0][255 & J[0][e2]];
    for (let e2 = 0; e2 <= 255; e2++) J[2][e2] = J[1][e2] >> 8 ^ J[0][255 & J[1][e2]];
    for (let e2 = 0; e2 <= 255; e2++) J[3][e2] = J[2][e2] >> 8 ^ J[0][255 & J[2][e2]];
    Y = function() {
      const e2 = new ArrayBuffer(2);
      return new DataView(e2).setInt16(0, 255, true), 255 === new Int16Array(e2)[0];
    }();
    ee = BigInt(0);
    te = BigInt(1);
    ye = M.getNodeCrypto();
    pe = BigInt(1);
    we = [7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997, 1009, 1013, 1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087, 1091, 1093, 1097, 1103, 1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213, 1217, 1223, 1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289, 1291, 1297, 1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373, 1381, 1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451, 1453, 1459, 1471, 1481, 1483, 1487, 1489, 1493, 1499, 1511, 1523, 1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583, 1597, 1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637, 1657, 1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721, 1723, 1733, 1741, 1747, 1753, 1759, 1777, 1783, 1787, 1789, 1801, 1811, 1823, 1831, 1847, 1861, 1867, 1871, 1873, 1877, 1879, 1889, 1901, 1907, 1913, 1931, 1933, 1949, 1951, 1973, 1979, 1987, 1993, 1997, 1999, 2003, 2011, 2017, 2027, 2029, 2039, 2053, 2063, 2069, 2081, 2083, 2087, 2089, 2099, 2111, 2113, 2129, 2131, 2137, 2141, 2143, 2153, 2161, 2179, 2203, 2207, 2213, 2221, 2237, 2239, 2243, 2251, 2267, 2269, 2273, 2281, 2287, 2293, 2297, 2309, 2311, 2333, 2339, 2341, 2347, 2351, 2357, 2371, 2377, 2381, 2383, 2389, 2393, 2399, 2411, 2417, 2423, 2437, 2441, 2447, 2459, 2467, 2473, 2477, 2503, 2521, 2531, 2539, 2543, 2549, 2551, 2557, 2579, 2591, 2593, 2609, 2617, 2621, 2633, 2647, 2657, 2659, 2663, 2671, 2677, 2683, 2687, 2689, 2693, 2699, 2707, 2711, 2713, 2719, 2729, 2731, 2741, 2749, 2753, 2767, 2777, 2789, 2791, 2797, 2801, 2803, 2819, 2833, 2837, 2843, 2851, 2857, 2861, 2879, 2887, 2897, 2903, 2909, 2917, 2927, 2939, 2953, 2957, 2963, 2969, 2971, 2999, 3001, 3011, 3019, 3023, 3037, 3041, 3049, 3061, 3067, 3079, 3083, 3089, 3109, 3119, 3121, 3137, 3163, 3167, 3169, 3181, 3187, 3191, 3203, 3209, 3217, 3221, 3229, 3251, 3253, 3257, 3259, 3271, 3299, 3301, 3307, 3313, 3319, 3323, 3329, 3331, 3343, 3347, 3359, 3361, 3371, 3373, 3389, 3391, 3407, 3413, 3433, 3449, 3457, 3461, 3463, 3467, 3469, 3491, 3499, 3511, 3517, 3527, 3529, 3533, 3539, 3541, 3547, 3557, 3559, 3571, 3581, 3583, 3593, 3607, 3613, 3617, 3623, 3631, 3637, 3643, 3659, 3671, 3673, 3677, 3691, 3697, 3701, 3709, 3719, 3727, 3733, 3739, 3761, 3767, 3769, 3779, 3793, 3797, 3803, 3821, 3823, 3833, 3847, 3851, 3853, 3863, 3877, 3881, 3889, 3907, 3911, 3917, 3919, 3923, 3929, 3931, 3943, 3947, 3967, 3989, 4001, 4003, 4007, 4013, 4019, 4021, 4027, 4049, 4051, 4057, 4073, 4079, 4091, 4093, 4099, 4111, 4127, 4129, 4133, 4139, 4153, 4157, 4159, 4177, 4201, 4211, 4217, 4219, 4229, 4231, 4241, 4243, 4253, 4259, 4261, 4271, 4273, 4283, 4289, 4297, 4327, 4337, 4339, 4349, 4357, 4363, 4373, 4391, 4397, 4409, 4421, 4423, 4441, 4447, 4451, 4457, 4463, 4481, 4483, 4493, 4507, 4513, 4517, 4519, 4523, 4547, 4549, 4561, 4567, 4583, 4591, 4597, 4603, 4621, 4637, 4639, 4643, 4649, 4651, 4657, 4663, 4673, 4679, 4691, 4703, 4721, 4723, 4729, 4733, 4751, 4759, 4783, 4787, 4789, 4793, 4799, 4801, 4813, 4817, 4831, 4861, 4871, 4877, 4889, 4903, 4909, 4919, 4931, 4933, 4937, 4943, 4951, 4957, 4967, 4969, 4973, 4987, 4993, 4999].map((e2) => BigInt(e2));
    me = M.getWebCrypto();
    be = M.getNodeCrypto();
    ke = be && be.getHashes();
    Ie = Ee("md5") || ve("md5");
    Be = Ee("sha1") || ve("sha1", "SHA-1");
    Se = Ee("sha224") || ve("sha224");
    Ke = Ee("sha256") || ve("sha256", "SHA-256");
    Ce = Ee("sha384") || ve("sha384", "SHA-384");
    De = Ee("sha512") || ve("sha512", "SHA-512");
    Ue = Ee("ripemd160") || ve("ripemd160");
    Pe = Ee("sha3-256") || ve("sha3_256");
    xe = Ee("sha3-512") || ve("sha3_512");
    Te = [];
    Te[1] = [48, 32, 48, 12, 6, 8, 42, 134, 72, 134, 247, 13, 2, 5, 5, 0, 4, 16], Te[2] = [48, 33, 48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0, 4, 20], Te[3] = [48, 33, 48, 9, 6, 5, 43, 36, 3, 2, 1, 5, 0, 4, 20], Te[8] = [48, 49, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 1, 5, 0, 4, 32], Te[9] = [48, 65, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 2, 5, 0, 4, 48], Te[10] = [48, 81, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 3, 5, 0, 4, 64], Te[11] = [48, 45, 48, 13, 6, 9, 96, 134, 72, 1, 101, 3, 4, 2, 4, 5, 0, 4, 28];
    Fe = M.getWebCrypto();
    Oe = M.getNodeCrypto();
    He = BigInt(1);
    Ye = BigInt(1);
    Ze = "object" == typeof e && "crypto" in e ? e.crypto : void 0;
    We = {};
    $e = function(e2) {
      var t2, r2 = new Float64Array(16);
      if (e2) for (t2 = 0; t2 < e2.length; t2++) r2[t2] = e2[t2];
      return r2;
    };
    Xe = function() {
      throw Error("no PRNG");
    };
    et = new Uint8Array(32);
    et[0] = 9;
    tt = $e();
    rt = $e([1]);
    nt = $e([56129, 1]);
    it = $e([30883, 4953, 19914, 30187, 55467, 16705, 2637, 112, 59544, 30585, 16505, 36039, 65139, 11119, 27886, 20995]);
    st = $e([61785, 9906, 39828, 60374, 45398, 33411, 5274, 224, 53552, 61171, 33010, 6542, 64743, 22239, 55772, 9222]);
    at = $e([54554, 36645, 11616, 51542, 42930, 38181, 51040, 26924, 56412, 64982, 57905, 49316, 21502, 52590, 14035, 8553]);
    ot = $e([26200, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214, 26214]);
    ct = $e([41136, 18958, 6951, 50414, 58488, 44335, 6150, 12099, 55207, 15867, 153, 11085, 57099, 20417, 9344, 11139]);
    Bt = [1116352408, 3609767458, 1899447441, 602891725, 3049323471, 3964484399, 3921009573, 2173295548, 961987163, 4081628472, 1508970993, 3053834265, 2453635748, 2937671579, 2870763221, 3664609560, 3624381080, 2734883394, 310598401, 1164996542, 607225278, 1323610764, 1426881987, 3590304994, 1925078388, 4068182383, 2162078206, 991336113, 2614888103, 633803317, 3248222580, 3479774868, 3835390401, 2666613458, 4022224774, 944711139, 264347078, 2341262773, 604807628, 2007800933, 770255983, 1495990901, 1249150122, 1856431235, 1555081692, 3175218132, 1996064986, 2198950837, 2554220882, 3999719339, 2821834349, 766784016, 2952996808, 2566594879, 3210313671, 3203337956, 3336571891, 1034457026, 3584528711, 2466948901, 113926993, 3758326383, 338241895, 168717936, 666307205, 1188179964, 773529912, 1546045734, 1294757372, 1522805485, 1396182291, 2643833823, 1695183700, 2343527390, 1986661051, 1014477480, 2177026350, 1206759142, 2456956037, 344077627, 2730485921, 1290863460, 2820302411, 3158454273, 3259730800, 3505952657, 3345764771, 106217008, 3516065817, 3606008344, 3600352804, 1432725776, 4094571909, 1467031594, 275423344, 851169720, 430227734, 3100823752, 506948616, 1363258195, 659060556, 3750685593, 883997877, 3785050280, 958139571, 3318307427, 1322822218, 3812723403, 1537002063, 2003034995, 1747873779, 3602036899, 1955562222, 1575990012, 2024104815, 1125592928, 2227730452, 2716904306, 2361852424, 442776044, 2428436474, 593698344, 2756734187, 3733110249, 3204031479, 2999351573, 3329325298, 3815920427, 3391569614, 3928383900, 3515267271, 566280711, 3940187606, 3454069534, 4118630271, 4000239992, 116418474, 1914138554, 174292421, 2731055270, 289380356, 3203993006, 460393269, 320620315, 685471733, 587496836, 852142971, 1086792851, 1017036298, 365543100, 1126000580, 2618297676, 1288033470, 3409855158, 1501505948, 4234509866, 1607167915, 987167468, 1816402316, 1246189591];
    Rt = new Float64Array([237, 211, 245, 92, 26, 99, 18, 88, 214, 156, 247, 162, 222, 249, 222, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16]);
    Nt = 64;
    We.scalarMult = function(e2, t2) {
      if (Ft(e2, t2), 32 !== e2.length) throw Error("bad n size");
      if (32 !== t2.length) throw Error("bad p size");
      var r2 = new Uint8Array(32);
      return vt(r2, e2, t2), r2;
    }, We.box = {}, We.box.keyPair = function() {
      var e2 = new Uint8Array(32), t2 = new Uint8Array(32);
      return function(e3, t3) {
        Xe(t3, 32), It(e3, t3);
      }(e2, t2), { publicKey: e2, secretKey: t2 };
    }, We.box.keyPair.fromSecretKey = function(e2) {
      if (Ft(e2), 32 !== e2.length) throw Error("bad secret key size");
      var t2 = new Uint8Array(32);
      return It(t2, e2), { publicKey: t2, secretKey: new Uint8Array(e2) };
    }, We.sign = function(e2, t2) {
      if (Ft(e2, t2), 64 !== t2.length) throw Error("bad secret key size");
      var r2 = new Uint8Array(Nt + e2.length);
      return function(e3, t3, r3, n2) {
        var i2, s2, a2 = new Uint8Array(64), o2 = new Uint8Array(64), c2 = new Uint8Array(64), h2 = new Float64Array(64), u2 = [$e(), $e(), $e(), $e()];
        Kt(a2, n2, 32), a2[0] &= 248, a2[31] &= 127, a2[31] |= 64;
        var l2 = r3 + 64;
        for (i2 = 0; i2 < r3; i2++) e3[64 + i2] = t3[i2];
        for (i2 = 0; i2 < 32; i2++) e3[32 + i2] = a2[32 + i2];
        for (Kt(c2, e3.subarray(32), r3 + 32), Lt(c2), xt(u2, c2), Ut(e3, u2), i2 = 32; i2 < 64; i2++) e3[i2] = n2[i2];
        for (Kt(o2, e3, r3 + 64), Lt(o2), i2 = 0; i2 < 64; i2++) h2[i2] = 0;
        for (i2 = 0; i2 < 32; i2++) h2[i2] = c2[i2];
        for (i2 = 0; i2 < 32; i2++) for (s2 = 0; s2 < 32; s2++) h2[i2 + s2] += o2[i2] * a2[s2];
        Tt(e3.subarray(32), h2);
      }(r2, e2, e2.length, t2), r2;
    }, We.sign.detached = function(e2, t2) {
      for (var r2 = We.sign(e2, t2), n2 = new Uint8Array(Nt), i2 = 0; i2 < n2.length; i2++) n2[i2] = r2[i2];
      return n2;
    }, We.sign.detached.verify = function(e2, t2, r2) {
      if (Ft(e2, t2, r2), t2.length !== Nt) throw Error("bad signature size");
      if (32 !== r2.length) throw Error("bad public key size");
      var n2, i2 = new Uint8Array(Nt + e2.length), s2 = new Uint8Array(Nt + e2.length);
      for (n2 = 0; n2 < Nt; n2++) i2[n2] = t2[n2];
      for (n2 = 0; n2 < e2.length; n2++) i2[n2 + Nt] = e2[n2];
      return function(e3, t3, r3, n3) {
        var i3, s3 = new Uint8Array(32), a2 = new Uint8Array(64), o2 = [$e(), $e(), $e(), $e()], c2 = [$e(), $e(), $e(), $e()];
        if (r3 < 64) return -1;
        if (Mt(c2, n3)) return -1;
        for (i3 = 0; i3 < r3; i3++) e3[i3] = t3[i3];
        for (i3 = 0; i3 < 32; i3++) e3[i3 + 32] = n3[i3];
        if (Kt(a2, e3, r3), Lt(a2), Pt(o2, c2, a2), xt(c2, t3.subarray(32)), Ct(o2, c2), Ut(s3, o2), r3 -= 64, ut(t3, 0, s3, 0)) {
          for (i3 = 0; i3 < r3; i3++) e3[i3] = 0;
          return -1;
        }
        for (i3 = 0; i3 < r3; i3++) e3[i3] = t3[i3 + 64];
        return r3;
      }(s2, i2, i2.length, r2) >= 0;
    }, We.sign.keyPair = function() {
      var e2 = new Uint8Array(32), t2 = new Uint8Array(64);
      return Qt(e2, t2), { publicKey: e2, secretKey: t2 };
    }, We.sign.keyPair.fromSecretKey = function(e2) {
      if (Ft(e2), 64 !== e2.length) throw Error("bad secret key size");
      for (var t2 = new Uint8Array(32), r2 = 0; r2 < t2.length; r2++) t2[r2] = e2[32 + r2];
      return { publicKey: t2, secretKey: new Uint8Array(e2) };
    }, We.sign.keyPair.fromSeed = function(e2) {
      if (Ft(e2), 32 !== e2.length) throw Error("bad seed size");
      for (var t2 = new Uint8Array(32), r2 = new Uint8Array(64), n2 = 0; n2 < 32; n2++) r2[n2] = e2[n2];
      return Qt(t2, r2, true), { publicKey: t2, secretKey: r2 };
    }, We.setPRNG = function(e2) {
      Xe = e2;
    }, function() {
      if (Ze && Ze.getRandomValues) {
        We.setPRNG(function(e2, t2) {
          var r2, n2 = new Uint8Array(t2);
          for (r2 = 0; r2 < t2; r2 += 65536) Ze.getRandomValues(n2.subarray(r2, r2 + Math.min(t2 - r2, 65536)));
          for (r2 = 0; r2 < t2; r2++) e2[r2] = n2[r2];
          !function(e3) {
            for (var t3 = 0; t3 < e3.length; t3++) e3[t3] = 0;
          }(n2);
        });
      }
    }();
    Ot = { "2a8648ce3d030107": R.curve.nistP256, "2b81040022": R.curve.nistP384, "2b81040023": R.curve.nistP521, "2b8104000a": R.curve.secp256k1, "2b06010401da470f01": R.curve.ed25519Legacy, "2b060104019755010501": R.curve.curve25519Legacy, "2b2403030208010107": R.curve.brainpoolP256r1, "2b240303020801010b": R.curve.brainpoolP384r1, "2b240303020801010d": R.curve.brainpoolP512r1 };
    Ht = class _Ht {
      constructor(e2) {
        if (e2 instanceof _Ht) this.oid = e2.oid;
        else if (M.isArray(e2) || M.isUint8Array(e2)) {
          if (6 === (e2 = new Uint8Array(e2))[0]) {
            if (e2[1] !== e2.length - 2) throw Error("Length mismatch in DER encoded oid");
            e2 = e2.subarray(2);
          }
          this.oid = e2;
        } else this.oid = "";
      }
      read(e2) {
        if (e2.length >= 1) {
          const t2 = e2[0];
          if (e2.length >= 1 + t2) return this.oid = e2.subarray(1, 1 + t2), 1 + this.oid.length;
        }
        throw Error("Invalid oid");
      }
      write() {
        return M.concatUint8Array([new Uint8Array([this.oid.length]), this.oid]);
      }
      toHex() {
        return M.uint8ArrayToHex(this.oid);
      }
      getName() {
        const e2 = Ot[this.toHex()];
        if (!e2) throw Error("Unknown curve object identifier.");
        return e2;
      }
    };
    Yt = class _Yt extends Error {
      constructor(...e2) {
        super(...e2), Error.captureStackTrace && Error.captureStackTrace(this, _Yt), this.name = "UnsupportedError";
      }
    };
    Zt = class extends Yt {
      constructor(...e2) {
        super(...e2), Error.captureStackTrace && Error.captureStackTrace(this, Yt), this.name = "UnknownPacketError";
      }
    };
    Wt = class {
      constructor(e2, t2) {
        this.tag = e2, this.rawContent = t2;
      }
      write() {
        return this.rawContent;
      }
    };
    ir = (e2, t2) => {
      if (e2 === R.publicKey.ed25519) {
        return { kty: "OKP", crv: "Ed25519", x: _(t2), ext: true };
      }
      throw Error("Unsupported EdDSA algorithm");
    };
    sr = (e2, t2, r2) => {
      if (e2 === R.publicKey.ed25519) {
        const n2 = ir(e2, t2);
        return n2.d = _(r2), n2;
      }
      throw Error("Unsupported EdDSA algorithm");
    };
    ar = /* @__PURE__ */ Object.freeze({ __proto__: null, generate: $t, getPayloadSize: rr, getPreferredHashAlgo: nr, sign: Xt, validateParams: tr, verify: er });
    lr = (e2) => new Uint8Array(e2.buffer, e2.byteOffset, e2.byteLength);
    yr = (e2) => new Uint32Array(e2.buffer, e2.byteOffset, Math.floor(e2.byteLength / 4));
    fr = (e2) => new DataView(e2.buffer, e2.byteOffset, e2.byteLength);
    if (!(68 === new Uint8Array(new Uint32Array([287454020]).buffer)[0])) throw Error("Non little-endian hardware is not supported");
    dr = (e2, t2) => (Object.assign(t2, e2), t2);
    kr = 16;
    Er = /* @__PURE__ */ new Uint8Array(16);
    vr = yr(Er);
    Ir = (e2) => (e2 >>> 0 & 255) << 24 | (e2 >>> 8 & 255) << 16 | (e2 >>> 16 & 255) << 8 | e2 >>> 24 & 255;
    Br = class {
      constructor(e2, t2) {
        this.blockLen = kr, this.outputLen = kr, this.s0 = 0, this.s1 = 0, this.s2 = 0, this.s3 = 0, this.finished = false, cr(e2 = gr(e2), 16);
        const r2 = fr(e2);
        let n2 = r2.getUint32(0, false), i2 = r2.getUint32(4, false), s2 = r2.getUint32(8, false), a2 = r2.getUint32(12, false);
        const o2 = [];
        for (let e3 = 0; e3 < 128; e3++) o2.push({ s0: Ir(n2), s1: Ir(i2), s2: Ir(s2), s3: Ir(a2) }), { s0: n2, s1: i2, s2, s3: a2 } = { s3: (u2 = s2) << 31 | (l2 = a2) >>> 1, s2: (h2 = i2) << 31 | u2 >>> 1, s1: (c2 = n2) << 31 | h2 >>> 1, s0: c2 >>> 1 ^ 225 << 24 & -(1 & l2) };
        var c2, h2, u2, l2;
        const y2 = ((e3) => e3 > 65536 ? 8 : e3 > 1024 ? 4 : 2)(t2 || 1024);
        if (![1, 2, 4, 8].includes(y2)) throw Error(`ghash: wrong window size=${y2}, should be 2, 4 or 8`);
        this.W = y2;
        const f2 = 128 / y2, g2 = this.windowSize = 2 ** y2, p2 = [];
        for (let e3 = 0; e3 < f2; e3++) for (let t3 = 0; t3 < g2; t3++) {
          let r3 = 0, n3 = 0, i3 = 0, s3 = 0;
          for (let a3 = 0; a3 < y2; a3++) {
            if (!(t3 >>> y2 - a3 - 1 & 1)) continue;
            const { s0: c3, s1: h3, s2: u3, s3: l3 } = o2[y2 * e3 + a3];
            r3 ^= c3, n3 ^= h3, i3 ^= u3, s3 ^= l3;
          }
          p2.push({ s0: r3, s1: n3, s2: i3, s3 });
        }
        this.t = p2;
      }
      _updateBlock(e2, t2, r2, n2) {
        e2 ^= this.s0, t2 ^= this.s1, r2 ^= this.s2, n2 ^= this.s3;
        const { W: i2, t: s2, windowSize: a2 } = this;
        let o2 = 0, c2 = 0, h2 = 0, u2 = 0;
        const l2 = (1 << i2) - 1;
        let y2 = 0;
        for (const f2 of [e2, t2, r2, n2]) for (let e3 = 0; e3 < 4; e3++) {
          const t3 = f2 >>> 8 * e3 & 255;
          for (let e4 = 8 / i2 - 1; e4 >= 0; e4--) {
            const r3 = t3 >>> i2 * e4 & l2, { s0: n3, s1: f3, s2: g2, s3: p2 } = s2[y2 * a2 + r3];
            o2 ^= n3, c2 ^= f3, h2 ^= g2, u2 ^= p2, y2 += 1;
          }
        }
        this.s0 = o2, this.s1 = c2, this.s2 = h2, this.s3 = u2;
      }
      update(e2) {
        e2 = gr(e2), hr(this);
        const t2 = yr(e2), r2 = Math.floor(e2.length / kr), n2 = e2.length % kr;
        for (let e3 = 0; e3 < r2; e3++) this._updateBlock(t2[4 * e3 + 0], t2[4 * e3 + 1], t2[4 * e3 + 2], t2[4 * e3 + 3]);
        return n2 && (Er.set(e2.subarray(r2 * kr)), this._updateBlock(vr[0], vr[1], vr[2], vr[3]), br(vr)), this;
      }
      destroy() {
        const { t: e2 } = this;
        for (const t2 of e2) t2.s0 = 0, t2.s1 = 0, t2.s2 = 0, t2.s3 = 0;
      }
      digestInto(e2) {
        hr(this), ur(e2, this), this.finished = true;
        const { s0: t2, s1: r2, s2: n2, s3: i2 } = this, s2 = yr(e2);
        return s2[0] = t2, s2[1] = r2, s2[2] = n2, s2[3] = i2, e2;
      }
      digest() {
        const e2 = new Uint8Array(kr);
        return this.digestInto(e2), this.destroy(), e2;
      }
    };
    Sr = class extends Br {
      constructor(e2, t2) {
        const r2 = function(e3) {
          e3.reverse();
          const t3 = 1 & e3[15];
          let r3 = 0;
          for (let t4 = 0; t4 < e3.length; t4++) {
            const n2 = e3[t4];
            e3[t4] = n2 >>> 1 | r3, r3 = (1 & n2) << 7;
          }
          return e3[0] ^= 225 & -t3, e3;
        }(mr(e2 = gr(e2)));
        super(r2, t2), br(r2);
      }
      update(e2) {
        e2 = gr(e2), hr(this);
        const t2 = yr(e2), r2 = e2.length % kr, n2 = Math.floor(e2.length / kr);
        for (let e3 = 0; e3 < n2; e3++) this._updateBlock(Ir(t2[4 * e3 + 3]), Ir(t2[4 * e3 + 2]), Ir(t2[4 * e3 + 1]), Ir(t2[4 * e3 + 0]));
        return r2 && (Er.set(e2.subarray(n2 * kr)), this._updateBlock(Ir(vr[3]), Ir(vr[2]), Ir(vr[1]), Ir(vr[0])), br(vr)), this;
      }
      digestInto(e2) {
        hr(this), ur(e2, this), this.finished = true;
        const { s0: t2, s1: r2, s2: n2, s3: i2 } = this, s2 = yr(e2);
        return s2[0] = t2, s2[1] = r2, s2[2] = n2, s2[3] = i2, e2.reverse();
      }
    };
    Cr = Kr((e2, t2) => new Br(e2, t2));
    Kr((e2, t2) => new Sr(e2, t2));
    Dr = 16;
    Ur = new Uint8Array(Dr);
    Pr = 283;
    Rr = /* @__PURE__ */ (() => {
      const e2 = new Uint8Array(256);
      for (let t3 = 0, r2 = 1; t3 < 256; t3++, r2 ^= xr(r2)) e2[t3] = r2;
      const t2 = new Uint8Array(256);
      t2[0] = 99;
      for (let r2 = 0; r2 < 255; r2++) {
        let n2 = e2[255 - r2];
        n2 |= n2 << 8, t2[e2[r2]] = 255 & (n2 ^ n2 >> 4 ^ n2 >> 5 ^ n2 >> 6 ^ n2 >> 7 ^ 99);
      }
      return br(e2), t2;
    })();
    Tr = /* @__PURE__ */ Rr.map((e2, t2) => Rr.indexOf(t2));
    Lr = (e2) => e2 << 24 | e2 >>> 8;
    Mr = (e2) => e2 << 8 | e2 >>> 24;
    Nr = (e2) => e2 << 24 & 4278190080 | e2 << 8 & 16711680 | e2 >>> 8 & 65280 | e2 >>> 24 & 255;
    Or = /* @__PURE__ */ Fr(Rr, (e2) => Qr(e2, 3) << 24 | e2 << 16 | e2 << 8 | Qr(e2, 2));
    Hr = /* @__PURE__ */ Fr(Tr, (e2) => Qr(e2, 11) << 24 | Qr(e2, 13) << 16 | Qr(e2, 9) << 8 | Qr(e2, 14));
    zr = /* @__PURE__ */ (() => {
      const e2 = new Uint8Array(16);
      for (let t2 = 0, r2 = 1; t2 < 16; t2++, r2 = xr(r2)) e2[t2] = r2;
      return e2;
    })();
    $r = dr({ blockSize: 16, nonceLength: 16 }, function(e2, t2) {
      function r2(r3, n2) {
        if (cr(r3), void 0 !== n2 && (cr(n2), !wr(n2))) throw Error("unaligned destination");
        const i2 = Gr(e2), s2 = mr(t2), a2 = [i2, s2];
        wr(r3) || a2.push(r3 = mr(r3));
        const o2 = Zr(i2, s2, r3, n2);
        return br(...a2), o2;
      }
      return cr(e2), cr(t2, Dr), { encrypt: (e3, t3) => r2(e3, t3), decrypt: (e3, t3) => r2(e3, t3) };
    });
    Xr = dr({ blockSize: 16, nonceLength: 16 }, function(e2, t2, r2 = {}) {
      cr(e2), cr(t2, 16);
      const n2 = !r2.disablePadding;
      return { encrypt(r3, i2) {
        const s2 = Gr(e2), { b: a2, o: o2, out: c2 } = function(e3, t3, r4) {
          cr(e3);
          let n3 = e3.length;
          const i3 = n3 % Dr;
          if (!t3 && 0 !== i3) throw Error("aec/(cbc-ecb): unpadded plaintext with disabled padding");
          wr(e3) || (e3 = mr(e3));
          const s3 = yr(e3);
          if (t3) {
            let e4 = Dr - i3;
            e4 || (e4 = Dr), n3 += e4;
          }
          const a3 = Yr(n3, r4);
          return { b: s3, o: yr(a3), out: a3 };
        }(r3, n2, i2);
        let h2 = t2;
        const u2 = [s2];
        wr(h2) || u2.push(h2 = mr(h2));
        const l2 = yr(h2);
        let y2 = l2[0], f2 = l2[1], g2 = l2[2], p2 = l2[3], d2 = 0;
        for (; d2 + 4 <= a2.length; ) y2 ^= a2[d2 + 0], f2 ^= a2[d2 + 1], g2 ^= a2[d2 + 2], p2 ^= a2[d2 + 3], { s0: y2, s1: f2, s2: g2, s3: p2 } = Vr(s2, y2, f2, g2, p2), o2[d2++] = y2, o2[d2++] = f2, o2[d2++] = g2, o2[d2++] = p2;
        if (n2) {
          const e3 = function(e4) {
            const t3 = new Uint8Array(16), r4 = yr(t3);
            t3.set(e4);
            const n3 = Dr - e4.length;
            for (let e5 = Dr - n3; e5 < Dr; e5++) t3[e5] = n3;
            return r4;
          }(r3.subarray(4 * d2));
          y2 ^= e3[0], f2 ^= e3[1], g2 ^= e3[2], p2 ^= e3[3], { s0: y2, s1: f2, s2: g2, s3: p2 } = Vr(s2, y2, f2, g2, p2), o2[d2++] = y2, o2[d2++] = f2, o2[d2++] = g2, o2[d2++] = p2;
        }
        return br(...u2), c2;
      }, decrypt(r3, i2) {
        !function(e3) {
          if (cr(e3), e3.length % Dr != 0) throw Error("aes/(cbc-ecb).decrypt ciphertext should consist of blocks with size 16");
        }(r3);
        const s2 = _r(e2);
        let a2 = t2;
        const o2 = [s2];
        wr(a2) || o2.push(a2 = mr(a2));
        const c2 = yr(a2), h2 = Yr(r3.length, i2);
        wr(r3) || o2.push(r3 = mr(r3));
        const u2 = yr(r3), l2 = yr(h2);
        let y2 = c2[0], f2 = c2[1], g2 = c2[2], p2 = c2[3];
        for (let e3 = 0; e3 + 4 <= u2.length; ) {
          const t3 = y2, r4 = f2, n3 = g2, i3 = p2;
          y2 = u2[e3 + 0], f2 = u2[e3 + 1], g2 = u2[e3 + 2], p2 = u2[e3 + 3];
          const { s0: a3, s1: o3, s2: c3, s3: h3 } = Jr(s2, y2, f2, g2, p2);
          l2[e3++] = a3 ^ t3, l2[e3++] = o3 ^ r4, l2[e3++] = c3 ^ n3, l2[e3++] = h3 ^ i3;
        }
        return br(...o2), function(e3, t3) {
          if (!t3) return e3;
          const r4 = e3.length;
          if (!r4) throw Error("aes/pcks5: empty ciphertext not allowed");
          const n3 = e3[r4 - 1];
          if (n3 <= 0 || n3 > 16) throw Error("aes/pcks5: wrong padding");
          const i3 = e3.subarray(0, -n3);
          for (let t4 = 0; t4 < n3; t4++) if (e3[r4 - t4 - 1] !== n3) throw Error("aes/pcks5: wrong padding");
          return i3;
        }(h2, n2);
      } };
    });
    en = dr({ blockSize: 16, nonceLength: 16 }, function(e2, t2) {
      function r2(r3, n2, i2) {
        cr(r3);
        const s2 = r3.length;
        i2 = Yr(s2, i2);
        const a2 = Gr(e2);
        let o2 = t2;
        const c2 = [a2];
        wr(o2) || c2.push(o2 = mr(o2)), wr(r3) || c2.push(r3 = mr(r3));
        const h2 = yr(r3), u2 = yr(i2), l2 = n2 ? u2 : h2, y2 = yr(o2);
        let f2 = y2[0], g2 = y2[1], p2 = y2[2], d2 = y2[3];
        for (let e3 = 0; e3 + 4 <= h2.length; ) {
          const { s0: t3, s1: r4, s2: n3, s3: i3 } = Vr(a2, f2, g2, p2, d2);
          u2[e3 + 0] = h2[e3 + 0] ^ t3, u2[e3 + 1] = h2[e3 + 1] ^ r4, u2[e3 + 2] = h2[e3 + 2] ^ n3, u2[e3 + 3] = h2[e3 + 3] ^ i3, f2 = l2[e3++], g2 = l2[e3++], p2 = l2[e3++], d2 = l2[e3++];
        }
        const A2 = Dr * Math.floor(h2.length / 4);
        if (A2 < s2) {
          ({ s0: f2, s1: g2, s2: p2, s3: d2 } = Vr(a2, f2, g2, p2, d2));
          const e3 = lr(new Uint32Array([f2, g2, p2, d2]));
          for (let t3 = A2, n3 = 0; t3 < s2; t3++, n3++) i2[t3] = r3[t3] ^ e3[n3];
          br(e3);
        }
        return br(...c2), i2;
      }
      return cr(e2), cr(t2, 16), { encrypt: (e3, t3) => r2(e3, true, t3), decrypt: (e3, t3) => r2(e3, false, t3) };
    });
    tn = dr({ blockSize: 16, nonceLength: 12, tagLength: 16 }, function(e2, t2, r2) {
      if (cr(e2), cr(t2), void 0 !== r2 && cr(r2), t2.length < 8) throw Error("aes/gcm: invalid nonce length");
      const n2 = 16;
      function i2(e3, t3, n3) {
        const i3 = function(e4, t4, r3, n4, i4) {
          const s3 = null == i4 ? 0 : i4.length, a2 = e4.create(r3, n4.length + s3);
          i4 && a2.update(i4), a2.update(n4);
          const o2 = new Uint8Array(16), c2 = fr(o2);
          i4 && Ar(c2, 0, BigInt(8 * s3), t4), Ar(c2, 8, BigInt(8 * n4.length), t4), a2.update(o2);
          const h2 = a2.digest();
          return br(o2), h2;
        }(Cr, false, e3, n3, r2);
        for (let e4 = 0; e4 < t3.length; e4++) i3[e4] ^= t3[e4];
        return i3;
      }
      function s2() {
        const r3 = Gr(e2), n3 = Ur.slice(), i3 = Ur.slice();
        if (Wr(r3, false, i3, i3, n3), 12 === t2.length) i3.set(t2);
        else {
          const e3 = Ur.slice();
          Ar(fr(e3), 8, BigInt(8 * t2.length), false);
          const r4 = Cr.create(n3).update(t2).update(e3);
          r4.digestInto(i3), r4.destroy();
        }
        return { xk: r3, authKey: n3, counter: i3, tagMask: Wr(r3, false, i3, Ur) };
      }
      return { encrypt(e3) {
        cr(e3);
        const { xk: t3, authKey: r3, counter: a2, tagMask: o2 } = s2(), c2 = new Uint8Array(e3.length + n2), h2 = [t3, r3, a2, o2];
        wr(e3) || h2.push(e3 = mr(e3)), Wr(t3, false, a2, e3, c2);
        const u2 = i2(r3, o2, c2.subarray(0, c2.length - n2));
        return h2.push(u2), c2.set(u2, e3.length), br(...h2), c2;
      }, decrypt(e3) {
        if (cr(e3), e3.length < n2) throw Error("aes/gcm: ciphertext less than tagLen (16)");
        const { xk: t3, authKey: r3, counter: a2, tagMask: o2 } = s2(), c2 = [t3, r3, o2, a2];
        wr(e3) || c2.push(e3 = mr(e3));
        const h2 = e3.subarray(0, -16), u2 = e3.subarray(-16), l2 = i2(r3, o2, h2);
        if (c2.push(l2), !pr(l2, u2)) throw Error("aes/gcm: invalid ghash tag");
        const y2 = Wr(t3, false, a2, h2);
        return br(...c2), y2;
      } };
    });
    an = { encrypt(e2, t2) {
      if (t2.length >= 2 ** 32) throw Error("plaintext should be less than 4gb");
      const r2 = Gr(e2);
      if (16 === t2.length) nn(r2, t2);
      else {
        const e3 = yr(t2);
        let n2 = e3[0], i2 = e3[1];
        for (let t3 = 0, s2 = 1; t3 < 6; t3++) for (let t4 = 2; t4 < e3.length; t4 += 2, s2++) {
          const { s0: a2, s1: o2, s2: c2, s3: h2 } = Vr(r2, n2, i2, e3[t4], e3[t4 + 1]);
          n2 = a2, i2 = o2 ^ Nr(s2), e3[t4] = c2, e3[t4 + 1] = h2;
        }
        e3[0] = n2, e3[1] = i2;
      }
      r2.fill(0);
    }, decrypt(e2, t2) {
      if (t2.length - 8 >= 2 ** 32) throw Error("ciphertext should be less than 4gb");
      const r2 = _r(e2), n2 = t2.length / 8 - 1;
      if (1 === n2) sn(r2, t2);
      else {
        const e3 = yr(t2);
        let i2 = e3[0], s2 = e3[1];
        for (let t3 = 0, a2 = 6 * n2; t3 < 6; t3++) for (let t4 = 2 * n2; t4 >= 1; t4 -= 2, a2--) {
          s2 ^= Nr(a2);
          const { s0: n3, s1: o2, s2: c2, s3: h2 } = Jr(r2, i2, s2, e3[t4], e3[t4 + 1]);
          i2 = n3, s2 = o2, e3[t4] = c2, e3[t4 + 1] = h2;
        }
        e3[0] = i2, e3[1] = s2;
      }
      r2.fill(0);
    } };
    on = new Uint8Array(8).fill(166);
    cn = dr({ blockSize: 8 }, (e2) => ({ encrypt(t2) {
      if (cr(t2), !t2.length || t2.length % 8 != 0) throw Error("invalid plaintext length");
      if (8 === t2.length) throw Error("8-byte keys not allowed in AESKW, use AESKWP instead");
      const r2 = function(...e3) {
        let t3 = 0;
        for (let r4 = 0; r4 < e3.length; r4++) {
          const n2 = e3[r4];
          cr(n2), t3 += n2.length;
        }
        const r3 = new Uint8Array(t3);
        for (let t4 = 0, n2 = 0; t4 < e3.length; t4++) {
          const i2 = e3[t4];
          r3.set(i2, n2), n2 += i2.length;
        }
        return r3;
      }(on, t2);
      return an.encrypt(e2, r2), r2;
    }, decrypt(t2) {
      if (cr(t2), t2.length % 8 != 0 || t2.length < 24) throw Error("invalid ciphertext length");
      const r2 = mr(t2);
      if (an.decrypt(e2, r2), !pr(r2.subarray(0, 8), on)) throw Error("integrity check failed");
      return r2.subarray(0, 8).fill(0), r2.subarray(8);
    } }));
    hn = { expandKeyLE: Gr, expandKeyDecLE: _r, encrypt: Vr, decrypt: Jr, encryptBlock: nn, decryptBlock: sn, ctrCounter: Zr, ctr32: Wr };
    gn = M.getWebCrypto();
    An = M.getWebCrypto();
    mn = { x25519: M.encodeUTF8("OpenPGP X25519"), x448: M.encodeUTF8("OpenPGP X448") };
    Cn = /* @__PURE__ */ Object.freeze({ __proto__: null, decrypt: vn, encrypt: En, generate: bn, generateEphemeralEncryptionMaterial: Bn, getPayloadSize: In, recomputeSharedSecret: Sn, validateParams: kn });
    Dn = M.getWebCrypto();
    Un = M.getNodeCrypto();
    Pn = { [R.curve.nistP256]: "P-256", [R.curve.nistP384]: "P-384", [R.curve.nistP521]: "P-521" };
    xn = Un ? Un.getCurves() : [];
    Qn = Un ? { [R.curve.secp256k1]: xn.includes("secp256k1") ? "secp256k1" : void 0, [R.curve.nistP256]: xn.includes("prime256v1") ? "prime256v1" : void 0, [R.curve.nistP384]: xn.includes("secp384r1") ? "secp384r1" : void 0, [R.curve.nistP521]: xn.includes("secp521r1") ? "secp521r1" : void 0, [R.curve.ed25519Legacy]: xn.includes("ED25519") ? "ED25519" : void 0, [R.curve.curve25519Legacy]: xn.includes("X25519") ? "X25519" : void 0, [R.curve.brainpoolP256r1]: xn.includes("brainpoolP256r1") ? "brainpoolP256r1" : void 0, [R.curve.brainpoolP384r1]: xn.includes("brainpoolP384r1") ? "brainpoolP384r1" : void 0, [R.curve.brainpoolP512r1]: xn.includes("brainpoolP512r1") ? "brainpoolP512r1" : void 0 } : {};
    Rn = { [R.curve.nistP256]: { oid: [6, 8, 42, 134, 72, 206, 61, 3, 1, 7], keyType: R.publicKey.ecdsa, hash: R.hash.sha256, cipher: R.symmetric.aes128, node: Qn[R.curve.nistP256], web: Pn[R.curve.nistP256], payloadSize: 32, sharedSize: 256, wireFormatLeadingByte: 4 }, [R.curve.nistP384]: { oid: [6, 5, 43, 129, 4, 0, 34], keyType: R.publicKey.ecdsa, hash: R.hash.sha384, cipher: R.symmetric.aes192, node: Qn[R.curve.nistP384], web: Pn[R.curve.nistP384], payloadSize: 48, sharedSize: 384, wireFormatLeadingByte: 4 }, [R.curve.nistP521]: { oid: [6, 5, 43, 129, 4, 0, 35], keyType: R.publicKey.ecdsa, hash: R.hash.sha512, cipher: R.symmetric.aes256, node: Qn[R.curve.nistP521], web: Pn[R.curve.nistP521], payloadSize: 66, sharedSize: 528, wireFormatLeadingByte: 4 }, [R.curve.secp256k1]: { oid: [6, 5, 43, 129, 4, 0, 10], keyType: R.publicKey.ecdsa, hash: R.hash.sha256, cipher: R.symmetric.aes128, node: Qn[R.curve.secp256k1], payloadSize: 32, wireFormatLeadingByte: 4 }, [R.curve.ed25519Legacy]: { oid: [6, 9, 43, 6, 1, 4, 1, 218, 71, 15, 1], keyType: R.publicKey.eddsaLegacy, hash: R.hash.sha512, node: false, payloadSize: 32, wireFormatLeadingByte: 64 }, [R.curve.curve25519Legacy]: { oid: [6, 10, 43, 6, 1, 4, 1, 151, 85, 1, 5, 1], keyType: R.publicKey.ecdh, hash: R.hash.sha256, cipher: R.symmetric.aes128, node: false, payloadSize: 32, wireFormatLeadingByte: 64 }, [R.curve.brainpoolP256r1]: { oid: [6, 9, 43, 36, 3, 3, 2, 8, 1, 1, 7], keyType: R.publicKey.ecdsa, hash: R.hash.sha256, cipher: R.symmetric.aes128, node: Qn[R.curve.brainpoolP256r1], payloadSize: 32, wireFormatLeadingByte: 4 }, [R.curve.brainpoolP384r1]: { oid: [6, 9, 43, 36, 3, 3, 2, 8, 1, 1, 11], keyType: R.publicKey.ecdsa, hash: R.hash.sha384, cipher: R.symmetric.aes192, node: Qn[R.curve.brainpoolP384r1], payloadSize: 48, wireFormatLeadingByte: 4 }, [R.curve.brainpoolP512r1]: { oid: [6, 9, 43, 36, 3, 3, 2, 8, 1, 1, 13], keyType: R.publicKey.ecdsa, hash: R.hash.sha512, cipher: R.symmetric.aes256, node: Qn[R.curve.brainpoolP512r1], payloadSize: 64, wireFormatLeadingByte: 4 } };
    Tn = class {
      constructor(e2) {
        try {
          this.name = e2 instanceof Ht ? e2.getName() : R.write(R.curve, e2);
        } catch (e3) {
          throw new Yt("Unknown curve");
        }
        const t2 = Rn[this.name];
        this.keyType = t2.keyType, this.oid = t2.oid, this.hash = t2.hash, this.cipher = t2.cipher, this.node = t2.node, this.web = t2.web, this.payloadSize = t2.payloadSize, this.sharedSize = t2.sharedSize, this.wireFormatLeadingByte = t2.wireFormatLeadingByte, this.web && M.getWebCrypto() ? this.type = "web" : this.node && M.getNodeCrypto() ? this.type = "node" : this.name === R.curve.curve25519Legacy ? this.type = "curve25519Legacy" : this.name === R.curve.ed25519Legacy && (this.type = "ed25519Legacy");
      }
      async genKeyPair() {
        switch (this.type) {
          case "web":
            try {
              return await async function(e2, t2) {
                const r2 = await Dn.generateKey({ name: "ECDSA", namedCurve: Pn[e2] }, true, ["sign", "verify"]), n2 = await Dn.exportKey("jwk", r2.privateKey);
                return { publicKey: Hn(await Dn.exportKey("jwk", r2.publicKey), t2), privateKey: G(n2.d) };
              }(this.name, this.wireFormatLeadingByte);
            } catch (e2) {
              return M.printDebugError("Browser did not support generating ec key " + e2.message), On(this.name);
            }
          case "node":
            return async function(e2) {
              const t2 = Un.createECDH(Qn[e2]);
              return await t2.generateKeys(), { publicKey: new Uint8Array(t2.getPublicKey()), privateKey: new Uint8Array(t2.getPrivateKey()) };
            }(this.name);
          case "curve25519Legacy": {
            const { k: e2, A: t2 } = await bn(R.publicKey.x25519), r2 = e2.slice().reverse();
            r2[0] = 127 & r2[0] | 64, r2[31] &= 248;
            return { publicKey: M.concatUint8Array([new Uint8Array([this.wireFormatLeadingByte]), t2]), privateKey: r2 };
          }
          case "ed25519Legacy": {
            const { seed: e2, A: t2 } = await $t(R.publicKey.ed25519);
            return { publicKey: M.concatUint8Array([new Uint8Array([this.wireFormatLeadingByte]), t2]), privateKey: e2 };
          }
          default:
            return On(this.name);
        }
      }
    };
    _n = M.getWebCrypto();
    jn = M.getNodeCrypto();
    Yn = /* @__PURE__ */ Object.freeze({ __proto__: null, sign: qn, validateParams: async function(e2, t2, r2) {
      const n2 = new Tn(e2);
      if (n2.keyType !== R.publicKey.ecdsa) return false;
      switch (n2.type) {
        case "web":
        case "node": {
          const n3 = fe(8), i2 = R.hash.sha256, s2 = await Qe(i2, n3);
          try {
            const a2 = await qn(e2, i2, n3, t2, r2, s2);
            return await Vn(e2, i2, a2, n3, t2, s2);
          } catch (e3) {
            return false;
          }
        }
        default:
          return Nn(R.publicKey.ecdsa, e2, t2, r2);
      }
    }, verify: Vn });
    Xn = /* @__PURE__ */ Object.freeze({ __proto__: null, sign: Zn, validateParams: $n, verify: Wn });
    ti = M.getWebCrypto();
    ri = M.getNodeCrypto();
    li = /* @__PURE__ */ Object.freeze({ __proto__: null, CurveWithOID: Tn, ecdh: /* @__PURE__ */ Object.freeze({ __proto__: null, decrypt: ci, encrypt: ai, validateParams: async function(e2, t2, r2) {
      return Nn(R.publicKey.ecdh, e2, t2, r2);
    } }), ecdhX: Cn, ecdsa: Yn, eddsa: ar, eddsaLegacy: Xn, generate: Ln, getPreferredHashAlgo: Mn });
    yi = BigInt(0);
    fi = BigInt(1);
    gi = class {
      constructor(e2) {
        e2 && (this.data = e2);
      }
      read(e2) {
        if (e2.length >= 1) {
          const t2 = e2[0];
          if (e2.length >= 1 + t2) return this.data = e2.subarray(1, 1 + t2), 1 + this.data.length;
        }
        throw Error("Invalid symmetric key");
      }
      write() {
        return M.concatUint8Array([new Uint8Array([this.data.length]), this.data]);
      }
    };
    pi = class {
      constructor(e2) {
        if (e2) {
          const { hash: t2, cipher: r2 } = e2;
          this.hash = t2, this.cipher = r2;
        } else this.hash = null, this.cipher = null;
      }
      read(e2) {
        if (e2.length < 4 || 3 !== e2[0] || 1 !== e2[1]) throw new Yt("Cannot read KDFParams");
        return this.hash = e2[2], this.cipher = e2[3], 4;
      }
      write() {
        return new Uint8Array([3, 1, this.hash, this.cipher]);
      }
    };
    di = class _di {
      static fromObject({ wrappedKey: e2, algorithm: t2 }) {
        const r2 = new _di();
        return r2.wrappedKey = e2, r2.algorithm = t2, r2;
      }
      read(e2) {
        let t2 = 0, r2 = e2[t2++];
        this.algorithm = r2 % 2 ? e2[t2++] : null, r2 -= r2 % 2, this.wrappedKey = M.readExactSubarray(e2, t2, t2 + r2), t2 += r2;
      }
      write() {
        return M.concatUint8Array([this.algorithm ? new Uint8Array([this.wrappedKey.length + 1, this.algorithm]) : new Uint8Array([this.wrappedKey.length]), this.wrappedKey]);
      }
    };
    Si = M.getWebCrypto();
    Ki = M.getNodeCrypto();
    Ci = Ki ? Ki.getCiphers() : [];
    Di = { idea: Ci.includes("idea-cfb") ? "idea-cfb" : void 0, tripledes: Ci.includes("des-ede3-cfb") ? "des-ede3-cfb" : void 0, cast5: Ci.includes("cast5-cfb") ? "cast5-cfb" : void 0, blowfish: Ci.includes("bf-cfb") ? "bf-cfb" : void 0, aes128: Ci.includes("aes-128-cfb") ? "aes-128-cfb" : void 0, aes192: Ci.includes("aes-192-cfb") ? "aes-192-cfb" : void 0, aes256: Ci.includes("aes-256-cfb") ? "aes-256-cfb" : void 0 };
    Qi = class {
      constructor(e2, t2, r2) {
        const { blockSize: n2 } = fn(e2);
        this.key = t2, this.prevBlock = r2, this.nextBlock = new Uint8Array(n2), this.i = 0, this.blockSize = n2, this.zeroBlock = new Uint8Array(this.blockSize);
      }
      static async isSupported(e2) {
        const { keySize: t2 } = fn(e2);
        return Si.importKey("raw", new Uint8Array(t2), "aes-cbc", false, ["encrypt"]).then(() => true, () => false);
      }
      async _runCBC(e2, t2) {
        const r2 = "AES-CBC";
        this.keyRef = this.keyRef || await Si.importKey("raw", this.key, r2, false, ["encrypt"]);
        const n2 = await Si.encrypt({ name: r2, iv: t2 || this.zeroBlock }, this.keyRef, e2);
        return new Uint8Array(n2).subarray(0, e2.length);
      }
      async encryptChunk(e2) {
        const t2 = this.nextBlock.length - this.i, r2 = e2.subarray(0, t2);
        if (this.nextBlock.set(r2, this.i), this.i + e2.length >= 2 * this.blockSize) {
          const r3 = (e2.length - t2) % this.blockSize, n3 = M.concatUint8Array([this.nextBlock, e2.subarray(t2, e2.length - r3)]), i2 = M.concatUint8Array([this.prevBlock, n3.subarray(0, n3.length - this.blockSize)]), s2 = await this._runCBC(i2);
          return Ti(s2, n3), this.prevBlock = s2.slice(-this.blockSize), r3 > 0 && this.nextBlock.set(e2.subarray(-r3)), this.i = r3, s2;
        }
        let n2;
        if (this.i += r2.length, this.i === this.nextBlock.length) {
          const t3 = this.nextBlock;
          n2 = await this._runCBC(this.prevBlock), Ti(n2, t3), this.prevBlock = n2.slice(), this.i = 0;
          const i2 = e2.subarray(r2.length);
          this.nextBlock.set(i2, this.i), this.i += i2.length;
        } else n2 = new Uint8Array();
        return n2;
      }
      async finish() {
        let e2;
        if (0 === this.i) e2 = new Uint8Array();
        else {
          this.nextBlock = this.nextBlock.subarray(0, this.i);
          const t2 = this.nextBlock, r2 = await this._runCBC(this.prevBlock);
          Ti(r2, t2), e2 = r2.subarray(0, t2.length);
        }
        return this.clearSensitiveData(), e2;
      }
      clearSensitiveData() {
        this.nextBlock.fill(0), this.prevBlock.fill(0), this.keyRef = null, this.key = null;
      }
      async encrypt(e2) {
        const t2 = (await this._runCBC(M.concatUint8Array([new Uint8Array(this.blockSize), e2]), this.iv)).subarray(0, e2.length);
        return Ti(t2, e2), this.clearSensitiveData(), t2;
      }
    };
    Ri = class {
      constructor(e2, t2, r2, n2) {
        this.forEncryption = e2;
        const { blockSize: i2 } = fn(t2);
        this.key = hn.expandKeyLE(r2), n2.byteOffset % 4 != 0 && (n2 = n2.slice()), this.prevBlock = Li(n2), this.nextBlock = new Uint8Array(i2), this.i = 0, this.blockSize = i2;
      }
      _runCFB(e2) {
        const t2 = Li(e2), r2 = new Uint8Array(e2.length), n2 = Li(r2);
        for (let e3 = 0; e3 + 4 <= n2.length; e3 += 4) {
          const { s0: r3, s1: i2, s2, s3: a2 } = hn.encrypt(this.key, this.prevBlock[0], this.prevBlock[1], this.prevBlock[2], this.prevBlock[3]);
          n2[e3 + 0] = t2[e3 + 0] ^ r3, n2[e3 + 1] = t2[e3 + 1] ^ i2, n2[e3 + 2] = t2[e3 + 2] ^ s2, n2[e3 + 3] = t2[e3 + 3] ^ a2, this.prevBlock = (this.forEncryption ? n2 : t2).slice(e3, e3 + 4);
        }
        return r2;
      }
      async processChunk(e2) {
        const t2 = this.nextBlock.length - this.i, r2 = e2.subarray(0, t2);
        if (this.nextBlock.set(r2, this.i), this.i + e2.length >= 2 * this.blockSize) {
          const r3 = (e2.length - t2) % this.blockSize, n3 = M.concatUint8Array([this.nextBlock, e2.subarray(t2, e2.length - r3)]), i2 = this._runCFB(n3);
          return r3 > 0 && this.nextBlock.set(e2.subarray(-r3)), this.i = r3, i2;
        }
        let n2;
        if (this.i += r2.length, this.i === this.nextBlock.length) {
          n2 = this._runCFB(this.nextBlock), this.i = 0;
          const t3 = e2.subarray(r2.length);
          this.nextBlock.set(t3, this.i), this.i += t3.length;
        } else n2 = new Uint8Array();
        return n2;
      }
      async finish() {
        let e2;
        if (0 === this.i) e2 = new Uint8Array();
        else {
          e2 = this._runCFB(this.nextBlock).subarray(0, this.i);
        }
        return this.clearSensitiveData(), e2;
      }
      clearSensitiveData() {
        this.nextBlock.fill(0), this.prevBlock.fill(0), this.key.fill(0);
      }
    };
    Li = (e2) => new Uint32Array(e2.buffer, e2.byteOffset, Math.floor(e2.byteLength / 4));
    Mi = M.getWebCrypto();
    Ni = M.getNodeCrypto();
    Fi = 16;
    Hi = new Uint8Array(Fi);
    _i = M.getWebCrypto();
    ji = M.getNodeCrypto();
    qi = M.getNodeBuffer();
    Vi = 16;
    Ji = Vi;
    Yi = Vi;
    Zi = new Uint8Array(Vi);
    Wi = new Uint8Array(Vi);
    Wi[Vi - 1] = 1;
    $i = new Uint8Array(Vi);
    $i[Vi - 1] = 2, ts.getNonce = function(e2, t2) {
      const r2 = e2.slice();
      for (let e3 = 0; e3 < t2.length; e3++) r2[8 + e3] ^= t2[e3];
      return r2;
    }, ts.blockLength = Vi, ts.ivLength = Ji, ts.tagLength = Yi;
    rs = 16;
    ns = 15;
    is = 16;
    cs = new Uint8Array(rs);
    hs = new Uint8Array([1]);
    us.getNonce = function(e2, t2) {
      const r2 = e2.slice();
      for (let e3 = 0; e3 < t2.length; e3++) r2[7 + e3] ^= t2[e3];
      return r2;
    }, us.blockLength = rs, us.ivLength = ns, us.tagLength = is;
    ls = M.getWebCrypto();
    ys = M.getNodeCrypto();
    fs = M.getNodeBuffer();
    gs = 16;
    ps = "AES-GCM";
    ds.getNonce = function(e2, t2) {
      const r2 = e2.slice();
      for (let e3 = 0; e3 < t2.length; e3++) r2[4 + e3] ^= t2[e3];
      return r2;
    }, ds.blockLength = 16, ds.ivLength = 12, ds.tagLength = gs;
    bs = class _bs extends Error {
      constructor(...e2) {
        super(...e2), Error.captureStackTrace && Error.captureStackTrace(this, _bs), this.name = "Argon2OutOfMemoryError";
      }
    };
    vs = class {
      constructor(e2 = T) {
        const { passes: t2, parallelism: r2, memoryExponent: n2 } = e2.s2kArgon2Params;
        this.type = "argon2", this.salt = null, this.t = t2, this.p = r2, this.encodedM = n2;
      }
      generateSalt() {
        this.salt = fe(16);
      }
      read(e2) {
        let t2 = 0;
        return this.salt = e2.subarray(t2, t2 + 16), t2 += 16, this.t = e2[t2++], this.p = e2[t2++], this.encodedM = e2[t2++], t2;
      }
      write() {
        const e2 = [new Uint8Array([R.write(R.s2k, this.type)]), this.salt, new Uint8Array([this.t, this.p, this.encodedM])];
        return M.concatUint8Array(e2);
      }
      async produceKey(e2, t2) {
        const r2 = 2 << this.encodedM - 1;
        try {
          ks = ks || (await Promise.resolve().then(function() {
            return ef;
          })).default, Es = Es || ks();
          const n2 = await Es, i2 = n2({ version: 19, type: 2, password: M.encodeUTF8(e2), salt: this.salt, tagLength: t2, memorySize: r2, parallelism: this.p, passes: this.t });
          return r2 > 1048576 && (Es = ks(), Es.catch(() => {
          })), i2;
        } catch (e3) {
          throw e3.message && (e3.message.includes("Unable to grow instance memory") || e3.message.includes("failed to grow memory") || e3.message.includes("WebAssembly.Memory.grow") || e3.message.includes("Out of memory")) ? new bs("Could not allocate required memory for Argon2") : e3;
        }
      }
    };
    Is = class {
      constructor(e2, t2 = T) {
        this.algorithm = R.hash.sha256, this.type = R.read(R.s2k, e2), this.c = t2.s2kIterationCountByte, this.salt = null;
      }
      generateSalt() {
        switch (this.type) {
          case "salted":
          case "iterated":
            this.salt = fe(8);
        }
      }
      getCount() {
        return 16 + (15 & this.c) << 6 + (this.c >> 4);
      }
      read(e2) {
        let t2 = 0;
        switch (this.algorithm = e2[t2++], this.type) {
          case "simple":
            break;
          case "salted":
            this.salt = e2.subarray(t2, t2 + 8), t2 += 8;
            break;
          case "iterated":
            this.salt = e2.subarray(t2, t2 + 8), t2 += 8, this.c = e2[t2++];
            break;
          case "gnu":
            if ("GNU" !== M.uint8ArrayToString(e2.subarray(t2, t2 + 3))) throw new Yt("Unknown s2k type.");
            t2 += 3;
            if (1001 !== 1e3 + e2[t2++]) throw new Yt("Unknown s2k gnu protection mode.");
            this.type = "gnu-dummy";
            break;
          default:
            throw new Yt("Unknown s2k type.");
        }
        return t2;
      }
      write() {
        if ("gnu-dummy" === this.type) return new Uint8Array([101, 0, ...M.stringToUint8Array("GNU"), 1]);
        const e2 = [new Uint8Array([R.write(R.s2k, this.type), this.algorithm])];
        switch (this.type) {
          case "simple":
            break;
          case "salted":
            e2.push(this.salt);
            break;
          case "iterated":
            e2.push(this.salt), e2.push(new Uint8Array([this.c]));
            break;
          case "gnu":
            throw Error("GNU s2k type not supported.");
          default:
            throw Error("Unknown s2k type.");
        }
        return M.concatUint8Array(e2);
      }
      async produceKey(e2, t2) {
        e2 = M.encodeUTF8(e2);
        const r2 = [];
        let n2 = 0, i2 = 0;
        for (; n2 < t2; ) {
          let t3;
          switch (this.type) {
            case "simple":
              t3 = M.concatUint8Array([new Uint8Array(i2), e2]);
              break;
            case "salted":
              t3 = M.concatUint8Array([new Uint8Array(i2), this.salt, e2]);
              break;
            case "iterated": {
              const r3 = M.concatUint8Array([this.salt, e2]);
              let n3 = r3.length;
              const s3 = Math.max(this.getCount(), n3);
              t3 = new Uint8Array(i2 + s3), t3.set(r3, i2);
              for (let e3 = i2 + n3; e3 < s3; e3 += n3, n3 *= 2) t3.copyWithin(e3, i2, e3);
              break;
            }
            case "gnu":
              throw Error("GNU s2k type not supported.");
            default:
              throw Error("Unknown s2k type.");
          }
          const s2 = await Qe(this.algorithm, t3);
          r2.push(s2), n2 += s2.length, i2++;
        }
        return M.concatUint8Array(r2).subarray(0, t2);
      }
    };
    Bs = /* @__PURE__ */ new Set([R.s2k.argon2, R.s2k.iterated]);
    Cs = Uint8Array;
    Ds = Uint16Array;
    Us = Uint32Array;
    Ps = new Cs([0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0, 0, 0, 0]);
    xs = new Cs([0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 0, 0]);
    Qs = new Cs([16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15]);
    Rs = function(e2, t2) {
      for (var r2 = new Ds(31), n2 = 0; n2 < 31; ++n2) r2[n2] = t2 += 1 << e2[n2 - 1];
      var i2 = new Us(r2[30]);
      for (n2 = 1; n2 < 30; ++n2) for (var s2 = r2[n2]; s2 < r2[n2 + 1]; ++s2) i2[s2] = s2 - r2[n2] << 5 | n2;
      return [r2, i2];
    };
    Ts = Rs(Ps, 2);
    Ls = Ts[0];
    Ms = Ts[1];
    Ls[28] = 258, Ms[258] = 28;
    for (Ns = Rs(xs, 0), Fs = Ns[0], Os = Ns[1], Hs = new Ds(32768), zs = 0; zs < 32768; ++zs) {
      Gs = (43690 & zs) >>> 1 | (21845 & zs) << 1;
      Gs = (61680 & (Gs = (52428 & Gs) >>> 2 | (13107 & Gs) << 2)) >>> 4 | (3855 & Gs) << 4, Hs[zs] = ((65280 & Gs) >>> 8 | (255 & Gs) << 8) >>> 1;
    }
    _s = function(e2, t2, r2) {
      for (var n2 = e2.length, i2 = 0, s2 = new Ds(t2); i2 < n2; ++i2) e2[i2] && ++s2[e2[i2] - 1];
      var a2, o2 = new Ds(t2);
      for (i2 = 0; i2 < t2; ++i2) o2[i2] = o2[i2 - 1] + s2[i2 - 1] << 1;
      if (r2) {
        a2 = new Ds(1 << t2);
        var c2 = 15 - t2;
        for (i2 = 0; i2 < n2; ++i2) if (e2[i2]) for (var h2 = i2 << 4 | e2[i2], u2 = t2 - e2[i2], l2 = o2[e2[i2] - 1]++ << u2, y2 = l2 | (1 << u2) - 1; l2 <= y2; ++l2) a2[Hs[l2] >>> c2] = h2;
      } else for (a2 = new Ds(n2), i2 = 0; i2 < n2; ++i2) e2[i2] && (a2[i2] = Hs[o2[e2[i2] - 1]++] >>> 15 - e2[i2]);
      return a2;
    };
    js = new Cs(288);
    for (zs = 0; zs < 144; ++zs) js[zs] = 8;
    for (zs = 144; zs < 256; ++zs) js[zs] = 9;
    for (zs = 256; zs < 280; ++zs) js[zs] = 7;
    for (zs = 280; zs < 288; ++zs) js[zs] = 8;
    qs = new Cs(32);
    for (zs = 0; zs < 32; ++zs) qs[zs] = 5;
    Vs = /* @__PURE__ */ _s(js, 9, 0);
    Js = /* @__PURE__ */ _s(js, 9, 1);
    Ys = /* @__PURE__ */ _s(qs, 5, 0);
    Zs = /* @__PURE__ */ _s(qs, 5, 1);
    Ws = function(e2) {
      for (var t2 = e2[0], r2 = 1; r2 < e2.length; ++r2) e2[r2] > t2 && (t2 = e2[r2]);
      return t2;
    };
    $s = function(e2, t2, r2) {
      var n2 = t2 / 8 | 0;
      return (e2[n2] | e2[n2 + 1] << 8) >> (7 & t2) & r2;
    };
    Xs = function(e2, t2) {
      var r2 = t2 / 8 | 0;
      return (e2[r2] | e2[r2 + 1] << 8 | e2[r2 + 2] << 16) >> (7 & t2);
    };
    ea = function(e2) {
      return (e2 + 7) / 8 | 0;
    };
    ta = function(e2, t2, r2) {
      (null == t2 || t2 < 0) && (t2 = 0), (null == r2 || r2 > e2.length) && (r2 = e2.length);
      var n2 = new (2 == e2.BYTES_PER_ELEMENT ? Ds : 4 == e2.BYTES_PER_ELEMENT ? Us : Cs)(r2 - t2);
      return n2.set(e2.subarray(t2, r2)), n2;
    };
    ra = ["unexpected EOF", "invalid block type", "invalid length/literal", "invalid distance", "stream finished", "no stream handler", , "no callback", "invalid UTF-8 data", "extra field too long", "date not in range 1980-2099", "filename too long", "stream finishing", "invalid zip data"];
    na = function(e2, t2, r2) {
      var n2 = Error(t2 || ra[e2]);
      if (n2.code = e2, Error.captureStackTrace && Error.captureStackTrace(n2, na), !r2) throw n2;
      return n2;
    };
    ia = function(e2, t2, r2) {
      r2 <<= 7 & t2;
      var n2 = t2 / 8 | 0;
      e2[n2] |= r2, e2[n2 + 1] |= r2 >>> 8;
    };
    sa = function(e2, t2, r2) {
      r2 <<= 7 & t2;
      var n2 = t2 / 8 | 0;
      e2[n2] |= r2, e2[n2 + 1] |= r2 >>> 8, e2[n2 + 2] |= r2 >>> 16;
    };
    aa = function(e2, t2) {
      for (var r2 = [], n2 = 0; n2 < e2.length; ++n2) e2[n2] && r2.push({ s: n2, f: e2[n2] });
      var i2 = r2.length, s2 = r2.slice();
      if (!i2) return [fa, 0];
      if (1 == i2) {
        var a2 = new Cs(r2[0].s + 1);
        return a2[r2[0].s] = 1, [a2, 1];
      }
      r2.sort(function(e3, t3) {
        return e3.f - t3.f;
      }), r2.push({ s: -1, f: 25001 });
      var o2 = r2[0], c2 = r2[1], h2 = 0, u2 = 1, l2 = 2;
      for (r2[0] = { s: -1, f: o2.f + c2.f, l: o2, r: c2 }; u2 != i2 - 1; ) o2 = r2[r2[h2].f < r2[l2].f ? h2++ : l2++], c2 = r2[h2 != u2 && r2[h2].f < r2[l2].f ? h2++ : l2++], r2[u2++] = { s: -1, f: o2.f + c2.f, l: o2, r: c2 };
      var y2 = s2[0].s;
      for (n2 = 1; n2 < i2; ++n2) s2[n2].s > y2 && (y2 = s2[n2].s);
      var f2 = new Ds(y2 + 1), g2 = oa(r2[u2 - 1], f2, 0);
      if (g2 > t2) {
        n2 = 0;
        var p2 = 0, d2 = g2 - t2, A2 = 1 << d2;
        for (s2.sort(function(e3, t3) {
          return f2[t3.s] - f2[e3.s] || e3.f - t3.f;
        }); n2 < i2; ++n2) {
          var w2 = s2[n2].s;
          if (!(f2[w2] > t2)) break;
          p2 += A2 - (1 << g2 - f2[w2]), f2[w2] = t2;
        }
        for (p2 >>>= d2; p2 > 0; ) {
          var m2 = s2[n2].s;
          f2[m2] < t2 ? p2 -= 1 << t2 - f2[m2]++ - 1 : ++n2;
        }
        for (; n2 >= 0 && p2; --n2) {
          var b2 = s2[n2].s;
          f2[b2] == t2 && (--f2[b2], ++p2);
        }
        g2 = t2;
      }
      return [new Cs(f2), g2];
    };
    oa = function(e2, t2, r2) {
      return -1 == e2.s ? Math.max(oa(e2.l, t2, r2 + 1), oa(e2.r, t2, r2 + 1)) : t2[e2.s] = r2;
    };
    ca = function(e2) {
      for (var t2 = e2.length; t2 && !e2[--t2]; ) ;
      for (var r2 = new Ds(++t2), n2 = 0, i2 = e2[0], s2 = 1, a2 = function(e3) {
        r2[n2++] = e3;
      }, o2 = 1; o2 <= t2; ++o2) if (e2[o2] == i2 && o2 != t2) ++s2;
      else {
        if (!i2 && s2 > 2) {
          for (; s2 > 138; s2 -= 138) a2(32754);
          s2 > 2 && (a2(s2 > 10 ? s2 - 11 << 5 | 28690 : s2 - 3 << 5 | 12305), s2 = 0);
        } else if (s2 > 3) {
          for (a2(i2), --s2; s2 > 6; s2 -= 6) a2(8304);
          s2 > 2 && (a2(s2 - 3 << 5 | 8208), s2 = 0);
        }
        for (; s2--; ) a2(i2);
        s2 = 1, i2 = e2[o2];
      }
      return [r2.subarray(0, n2), t2];
    };
    ha = function(e2, t2) {
      for (var r2 = 0, n2 = 0; n2 < t2.length; ++n2) r2 += e2[n2] * t2[n2];
      return r2;
    };
    ua = function(e2, t2, r2) {
      var n2 = r2.length, i2 = ea(t2 + 2);
      e2[i2] = 255 & n2, e2[i2 + 1] = n2 >>> 8, e2[i2 + 2] = 255 ^ e2[i2], e2[i2 + 3] = 255 ^ e2[i2 + 1];
      for (var s2 = 0; s2 < n2; ++s2) e2[i2 + s2 + 4] = r2[s2];
      return 8 * (i2 + 4 + n2);
    };
    la = function(e2, t2, r2, n2, i2, s2, a2, o2, c2, h2, u2) {
      ia(t2, u2++, r2), ++i2[256];
      for (var l2 = aa(i2, 15), y2 = l2[0], f2 = l2[1], g2 = aa(s2, 15), p2 = g2[0], d2 = g2[1], A2 = ca(y2), w2 = A2[0], m2 = A2[1], b2 = ca(p2), k2 = b2[0], E2 = b2[1], v2 = new Ds(19), I2 = 0; I2 < w2.length; ++I2) v2[31 & w2[I2]]++;
      for (I2 = 0; I2 < k2.length; ++I2) v2[31 & k2[I2]]++;
      for (var B2 = aa(v2, 7), S2 = B2[0], K2 = B2[1], C2 = 19; C2 > 4 && !S2[Qs[C2 - 1]]; --C2) ;
      var D2, U2, P2, x2, Q2 = h2 + 5 << 3, R2 = ha(i2, js) + ha(s2, qs) + a2, T2 = ha(i2, y2) + ha(s2, p2) + a2 + 14 + 3 * C2 + ha(v2, S2) + (2 * v2[16] + 3 * v2[17] + 7 * v2[18]);
      if (Q2 <= R2 && Q2 <= T2) return ua(t2, u2, e2.subarray(c2, c2 + h2));
      if (ia(t2, u2, 1 + (T2 < R2)), u2 += 2, T2 < R2) {
        D2 = _s(y2, f2, 0), U2 = y2, P2 = _s(p2, d2, 0), x2 = p2;
        var L2 = _s(S2, K2, 0);
        ia(t2, u2, m2 - 257), ia(t2, u2 + 5, E2 - 1), ia(t2, u2 + 10, C2 - 4), u2 += 14;
        for (I2 = 0; I2 < C2; ++I2) ia(t2, u2 + 3 * I2, S2[Qs[I2]]);
        u2 += 3 * C2;
        for (var M2 = [w2, k2], N2 = 0; N2 < 2; ++N2) {
          var F2 = M2[N2];
          for (I2 = 0; I2 < F2.length; ++I2) {
            var O2 = 31 & F2[I2];
            ia(t2, u2, L2[O2]), u2 += S2[O2], O2 > 15 && (ia(t2, u2, F2[I2] >>> 5 & 127), u2 += F2[I2] >>> 12);
          }
        }
      } else D2 = Vs, U2 = js, P2 = Ys, x2 = qs;
      for (I2 = 0; I2 < o2; ++I2) if (n2[I2] > 255) {
        O2 = n2[I2] >>> 18 & 31;
        sa(t2, u2, D2[O2 + 257]), u2 += U2[O2 + 257], O2 > 7 && (ia(t2, u2, n2[I2] >>> 23 & 31), u2 += Ps[O2]);
        var H2 = 31 & n2[I2];
        sa(t2, u2, P2[H2]), u2 += x2[H2], H2 > 3 && (sa(t2, u2, n2[I2] >>> 5 & 8191), u2 += xs[H2]);
      } else sa(t2, u2, D2[n2[I2]]), u2 += U2[n2[I2]];
      return sa(t2, u2, D2[256]), u2 + U2[256];
    };
    ya = /* @__PURE__ */ new Us([65540, 131080, 131088, 131104, 262176, 1048704, 1048832, 2114560, 2117632]);
    fa = /* @__PURE__ */ new Cs(0);
    ga = function(e2, t2, r2, n2, i2) {
      return function(e3, t3, r3, n3, i3, s2) {
        var a2 = e3.length, o2 = new Cs(n3 + a2 + 5 * (1 + Math.ceil(a2 / 7e3)) + i3), c2 = o2.subarray(n3, o2.length - i3), h2 = 0;
        if (!t3 || a2 < 8) for (var u2 = 0; u2 <= a2; u2 += 65535) {
          var l2 = u2 + 65535;
          l2 >= a2 && (c2[h2 >> 3] = s2), h2 = ua(c2, h2 + 1, e3.subarray(u2, l2));
        }
        else {
          for (var y2 = ya[t3 - 1], f2 = y2 >>> 13, g2 = 8191 & y2, p2 = (1 << r3) - 1, d2 = new Ds(32768), A2 = new Ds(p2 + 1), w2 = Math.ceil(r3 / 3), m2 = 2 * w2, b2 = function(t4) {
            return (e3[t4] ^ e3[t4 + 1] << w2 ^ e3[t4 + 2] << m2) & p2;
          }, k2 = new Us(25e3), E2 = new Ds(288), v2 = new Ds(32), I2 = 0, B2 = 0, S2 = (u2 = 0, 0), K2 = 0, C2 = 0; u2 < a2; ++u2) {
            var D2 = b2(u2), U2 = 32767 & u2, P2 = A2[D2];
            if (d2[U2] = P2, A2[D2] = U2, K2 <= u2) {
              var x2 = a2 - u2;
              if ((I2 > 7e3 || S2 > 24576) && x2 > 423) {
                h2 = la(e3, c2, 0, k2, E2, v2, B2, S2, C2, u2 - C2, h2), S2 = I2 = B2 = 0, C2 = u2;
                for (var Q2 = 0; Q2 < 286; ++Q2) E2[Q2] = 0;
                for (Q2 = 0; Q2 < 30; ++Q2) v2[Q2] = 0;
              }
              var R2 = 2, T2 = 0, L2 = g2, M2 = U2 - P2 & 32767;
              if (x2 > 2 && D2 == b2(u2 - M2)) for (var N2 = Math.min(f2, x2) - 1, F2 = Math.min(32767, u2), O2 = Math.min(258, x2); M2 <= F2 && --L2 && U2 != P2; ) {
                if (e3[u2 + R2] == e3[u2 + R2 - M2]) {
                  for (var H2 = 0; H2 < O2 && e3[u2 + H2] == e3[u2 + H2 - M2]; ++H2) ;
                  if (H2 > R2) {
                    if (R2 = H2, T2 = M2, H2 > N2) break;
                    var z2 = Math.min(M2, H2 - 2), G2 = 0;
                    for (Q2 = 0; Q2 < z2; ++Q2) {
                      var _2 = u2 - M2 + Q2 + 32768 & 32767, j2 = _2 - d2[_2] + 32768 & 32767;
                      j2 > G2 && (G2 = j2, P2 = _2);
                    }
                  }
                }
                M2 += (U2 = P2) - (P2 = d2[U2]) + 32768 & 32767;
              }
              if (T2) {
                k2[S2++] = 268435456 | Ms[R2] << 18 | Os[T2];
                var q2 = 31 & Ms[R2], V2 = 31 & Os[T2];
                B2 += Ps[q2] + xs[V2], ++E2[257 + q2], ++v2[V2], K2 = u2 + R2, ++I2;
              } else k2[S2++] = e3[u2], ++E2[e3[u2]];
            }
          }
          h2 = la(e3, c2, s2, k2, E2, v2, B2, S2, C2, u2 - C2, h2), !s2 && 7 & h2 && (h2 = ua(c2, h2 + 1, fa));
        }
        return ta(o2, 0, n3 + ea(h2) + i3);
      }(e2, null == t2.level ? 6 : t2.level, null == t2.mem ? Math.ceil(1.5 * Math.max(8, Math.min(13, Math.log(e2.length)))) : 12 + t2.mem, r2, n2, !i2);
    };
    pa = /* @__PURE__ */ function() {
      function e2(e3, t2) {
        t2 || "function" != typeof e3 || (t2 = e3, e3 = {}), this.ondata = t2, this.o = e3 || {};
      }
      return e2.prototype.p = function(e3, t2) {
        this.ondata(ga(e3, this.o, 0, 0, !t2), t2);
      }, e2.prototype.push = function(e3, t2) {
        this.ondata || na(5), this.d && na(4), this.d = t2, this.p(e3, t2 || false);
      }, e2;
    }();
    da = /* @__PURE__ */ function() {
      function e2(e3) {
        this.s = {}, this.p = new Cs(0), this.ondata = e3;
      }
      return e2.prototype.e = function(e3) {
        this.ondata || na(5), this.d && na(4);
        var t2 = this.p.length, r2 = new Cs(t2 + e3.length);
        r2.set(this.p), r2.set(e3, t2), this.p = r2;
      }, e2.prototype.c = function(e3) {
        this.d = this.s.i = e3 || false;
        var t2 = this.s.b, r2 = function(e4, t3, r3) {
          var n2 = e4.length;
          if (!n2 || r3 && r3.f && !r3.l) return t3 || new Cs(0);
          var i2 = !t3 || r3, s2 = !r3 || r3.i;
          r3 || (r3 = {}), t3 || (t3 = new Cs(3 * n2));
          var a2 = function(e5) {
            var r4 = t3.length;
            if (e5 > r4) {
              var n3 = new Cs(Math.max(2 * r4, e5));
              n3.set(t3), t3 = n3;
            }
          }, o2 = r3.f || 0, c2 = r3.p || 0, h2 = r3.b || 0, u2 = r3.l, l2 = r3.d, y2 = r3.m, f2 = r3.n, g2 = 8 * n2;
          do {
            if (!u2) {
              o2 = $s(e4, c2, 1);
              var p2 = $s(e4, c2 + 1, 3);
              if (c2 += 3, !p2) {
                var d2 = e4[(K2 = ea(c2) + 4) - 4] | e4[K2 - 3] << 8, A2 = K2 + d2;
                if (A2 > n2) {
                  s2 && na(0);
                  break;
                }
                i2 && a2(h2 + d2), t3.set(e4.subarray(K2, A2), h2), r3.b = h2 += d2, r3.p = c2 = 8 * A2, r3.f = o2;
                continue;
              }
              if (1 == p2) u2 = Js, l2 = Zs, y2 = 9, f2 = 5;
              else if (2 == p2) {
                var w2 = $s(e4, c2, 31) + 257, m2 = $s(e4, c2 + 10, 15) + 4, b2 = w2 + $s(e4, c2 + 5, 31) + 1;
                c2 += 14;
                for (var k2 = new Cs(b2), E2 = new Cs(19), v2 = 0; v2 < m2; ++v2) E2[Qs[v2]] = $s(e4, c2 + 3 * v2, 7);
                c2 += 3 * m2;
                var I2 = Ws(E2), B2 = (1 << I2) - 1, S2 = _s(E2, I2, 1);
                for (v2 = 0; v2 < b2; ) {
                  var K2, C2 = S2[$s(e4, c2, B2)];
                  if (c2 += 15 & C2, (K2 = C2 >>> 4) < 16) k2[v2++] = K2;
                  else {
                    var D2 = 0, U2 = 0;
                    for (16 == K2 ? (U2 = 3 + $s(e4, c2, 3), c2 += 2, D2 = k2[v2 - 1]) : 17 == K2 ? (U2 = 3 + $s(e4, c2, 7), c2 += 3) : 18 == K2 && (U2 = 11 + $s(e4, c2, 127), c2 += 7); U2--; ) k2[v2++] = D2;
                  }
                }
                var P2 = k2.subarray(0, w2), x2 = k2.subarray(w2);
                y2 = Ws(P2), f2 = Ws(x2), u2 = _s(P2, y2, 1), l2 = _s(x2, f2, 1);
              } else na(1);
              if (c2 > g2) {
                s2 && na(0);
                break;
              }
            }
            i2 && a2(h2 + 131072);
            for (var Q2 = (1 << y2) - 1, R2 = (1 << f2) - 1, T2 = c2; ; T2 = c2) {
              var L2 = (D2 = u2[Xs(e4, c2) & Q2]) >>> 4;
              if ((c2 += 15 & D2) > g2) {
                s2 && na(0);
                break;
              }
              if (D2 || na(2), L2 < 256) t3[h2++] = L2;
              else {
                if (256 == L2) {
                  T2 = c2, u2 = null;
                  break;
                }
                var M2 = L2 - 254;
                if (L2 > 264) {
                  var N2 = Ps[v2 = L2 - 257];
                  M2 = $s(e4, c2, (1 << N2) - 1) + Ls[v2], c2 += N2;
                }
                var F2 = l2[Xs(e4, c2) & R2], O2 = F2 >>> 4;
                if (F2 || na(3), c2 += 15 & F2, x2 = Fs[O2], O2 > 3 && (N2 = xs[O2], x2 += Xs(e4, c2) & (1 << N2) - 1, c2 += N2), c2 > g2) {
                  s2 && na(0);
                  break;
                }
                i2 && a2(h2 + 131072);
                for (var H2 = h2 + M2; h2 < H2; h2 += 4) t3[h2] = t3[h2 - x2], t3[h2 + 1] = t3[h2 + 1 - x2], t3[h2 + 2] = t3[h2 + 2 - x2], t3[h2 + 3] = t3[h2 + 3 - x2];
                h2 = H2;
              }
            }
            r3.l = u2, r3.p = T2, r3.b = h2, r3.f = o2, u2 && (o2 = 1, r3.m = y2, r3.d = l2, r3.n = f2);
          } while (!o2);
          return h2 == t3.length ? t3 : ta(t3, 0, h2);
        }(this.p, this.o, this.s);
        this.ondata(ta(r2, t2, this.s.b), this.d), this.o = ta(r2, this.s.b - 32768), this.s.b = this.o.length, this.p = ta(this.p, this.s.p / 8 | 0), this.s.p &= 7;
      }, e2.prototype.push = function(e3, t2) {
        this.e(e3), this.c(t2);
      }, e2;
    }();
    Aa = /* @__PURE__ */ function() {
      function e2(e3, t2) {
        var r2, n2;
        this.c = (r2 = 1, n2 = 0, { p: function(e4) {
          for (var t3 = r2, i2 = n2, s2 = 0 | e4.length, a2 = 0; a2 != s2; ) {
            for (var o2 = Math.min(a2 + 2655, s2); a2 < o2; ++a2) i2 += t3 += e4[a2];
            t3 = (65535 & t3) + 15 * (t3 >> 16), i2 = (65535 & i2) + 15 * (i2 >> 16);
          }
          r2 = t3, n2 = i2;
        }, d: function() {
          return (255 & (r2 %= 65521)) << 24 | r2 >>> 8 << 16 | (255 & (n2 %= 65521)) << 8 | n2 >>> 8;
        } }), this.v = 1, pa.call(this, e3, t2);
      }
      return e2.prototype.push = function(e3, t2) {
        pa.prototype.push.call(this, e3, t2);
      }, e2.prototype.p = function(e3, t2) {
        this.c.p(e3);
        var r2 = ga(e3, this.o, this.v && 2, t2 && 4, !t2);
        this.v && (function(e4, t3) {
          var r3 = t3.level, n2 = 0 == r3 ? 0 : r3 < 6 ? 1 : 9 == r3 ? 3 : 2;
          e4[0] = 120, e4[1] = n2 << 6 | (n2 ? 32 - 2 * n2 : 1);
        }(r2, this.o), this.v = 0), t2 && function(e4, t3, r3) {
          for (; r3; ++t3) e4[t3] = r3, r3 >>>= 8;
        }(r2, r2.length - 4, this.c.d()), this.ondata(r2, t2);
      }, e2;
    }();
    wa = /* @__PURE__ */ function() {
      function e2(e3) {
        this.v = 1, da.call(this, e3);
      }
      return e2.prototype.push = function(e3, t2) {
        if (da.prototype.e.call(this, e3), this.v) {
          if (this.p.length < 2 && !t2) return;
          this.p = this.p.subarray(2), this.v = 0;
        }
        t2 && (this.p.length < 4 && na(6, "invalid zlib data"), this.p = this.p.subarray(0, -4)), da.prototype.c.call(this, t2);
      }, e2;
    }();
    ma = "undefined" != typeof TextDecoder && /* @__PURE__ */ new TextDecoder();
    try {
      ma.decode(fa, { stream: true }), 1;
    } catch (e2) {
    }
    ba = class {
      static get tag() {
        return R.packet.literalData;
      }
      constructor(e2 = /* @__PURE__ */ new Date()) {
        this.format = R.literal.utf8, this.date = M.normalizeDate(e2), this.text = null, this.data = null, this.filename = "";
      }
      setText(e2, t2 = R.literal.utf8) {
        this.format = t2, this.text = e2, this.data = null;
      }
      getText(e2 = false) {
        return (null === this.text || M.isStream(this.text)) && (this.text = M.decodeUTF8(M.nativeEOL(this.getBytes(e2)))), this.text;
      }
      setBytes(e2, t2) {
        this.format = t2, this.data = e2, this.text = null;
      }
      getBytes(e2 = false) {
        return null === this.data && (this.data = M.canonicalizeEOL(M.encodeUTF8(this.text))), e2 ? B(this.data) : this.data;
      }
      setFilename(e2) {
        this.filename = e2;
      }
      getFilename() {
        return this.filename;
      }
      async read(e2) {
        await v(e2, async (e3) => {
          const t2 = await e3.readByte(), r2 = await e3.readByte();
          this.filename = M.decodeUTF8(await e3.readBytes(r2)), this.date = M.readDate(await e3.readBytes(4));
          let n2 = e3.remainder();
          o(n2) && (n2 = await C(n2)), this.setBytes(n2, t2);
        });
      }
      writeHeader() {
        const e2 = M.encodeUTF8(this.filename), t2 = new Uint8Array([e2.length]), r2 = new Uint8Array([this.format]), n2 = M.writeDate(this.date);
        return M.concatUint8Array([r2, t2, e2, n2]);
      }
      write() {
        const e2 = this.writeHeader(), t2 = this.getBytes();
        return M.concat([e2, t2]);
      }
    };
    ka = class _ka {
      constructor() {
        this.bytes = "";
      }
      read(e2) {
        return this.bytes = M.uint8ArrayToString(e2.subarray(0, 8)), this.bytes.length;
      }
      write() {
        return M.stringToUint8Array(this.bytes);
      }
      toHex() {
        return M.uint8ArrayToHex(M.stringToUint8Array(this.bytes));
      }
      equals(e2, t2 = false) {
        return t2 && (e2.isWildcard() || this.isWildcard()) || this.bytes === e2.bytes;
      }
      isNull() {
        return "" === this.bytes;
      }
      isWildcard() {
        return /^0+$/.test(this.toHex());
      }
      static mapToHex(e2) {
        return e2.toHex();
      }
      static fromID(e2) {
        const t2 = new _ka();
        return t2.read(M.hexToUint8Array(e2)), t2;
      }
      static wildcard() {
        const e2 = new _ka();
        return e2.read(new Uint8Array(8)), e2;
      }
    };
    Ea = Symbol("verified");
    va = "salt@notations.openpgpjs.org";
    Ia = /* @__PURE__ */ new Set([R.signatureSubpacket.issuerKeyID, R.signatureSubpacket.issuerFingerprint, R.signatureSubpacket.embeddedSignature]);
    Ba = class _Ba {
      static get tag() {
        return R.packet.signature;
      }
      constructor() {
        this.version = null, this.signatureType = null, this.hashAlgorithm = null, this.publicKeyAlgorithm = null, this.signatureData = null, this.unhashedSubpackets = [], this.unknownSubpackets = [], this.signedHashValue = null, this.salt = null, this.created = null, this.signatureExpirationTime = null, this.signatureNeverExpires = true, this.exportable = null, this.trustLevel = null, this.trustAmount = null, this.regularExpression = null, this.revocable = null, this.keyExpirationTime = null, this.keyNeverExpires = null, this.preferredSymmetricAlgorithms = null, this.revocationKeyClass = null, this.revocationKeyAlgorithm = null, this.revocationKeyFingerprint = null, this.issuerKeyID = new ka(), this.rawNotations = [], this.notations = {}, this.preferredHashAlgorithms = null, this.preferredCompressionAlgorithms = null, this.keyServerPreferences = null, this.preferredKeyServer = null, this.isPrimaryUserID = null, this.policyURI = null, this.keyFlags = null, this.signersUserID = null, this.reasonForRevocationFlag = null, this.reasonForRevocationString = null, this.features = null, this.signatureTargetPublicKeyAlgorithm = null, this.signatureTargetHashAlgorithm = null, this.signatureTargetHash = null, this.embeddedSignature = null, this.issuerKeyVersion = null, this.issuerFingerprint = null, this.preferredAEADAlgorithms = null, this.preferredCipherSuites = null, this.revoked = null, this[Ea] = null;
      }
      read(e2, t2 = T) {
        let r2 = 0;
        if (this.version = e2[r2++], 5 === this.version && !t2.enableParsingV5Entities) throw new Yt("Support for v5 entities is disabled; turn on `config.enableParsingV5Entities` if needed");
        if (4 !== this.version && 5 !== this.version && 6 !== this.version) throw new Yt(`Version ${this.version} of the signature packet is unsupported.`);
        if (this.signatureType = e2[r2++], this.publicKeyAlgorithm = e2[r2++], this.hashAlgorithm = e2[r2++], r2 += this.readSubPackets(e2.subarray(r2, e2.length), true), !this.created) throw Error("Missing signature creation time subpacket.");
        if (this.signatureData = e2.subarray(0, r2), r2 += this.readSubPackets(e2.subarray(r2, e2.length), false), this.signedHashValue = e2.subarray(r2, r2 + 2), r2 += 2, 6 === this.version) {
          const t3 = e2[r2++];
          this.salt = e2.subarray(r2, r2 + t3), r2 += t3;
        }
        const n2 = e2.subarray(r2, e2.length), { read: i2, signatureParams: s2 } = function(e3, t3) {
          let r3 = 0;
          switch (e3) {
            case R.publicKey.rsaEncryptSign:
            case R.publicKey.rsaEncrypt:
            case R.publicKey.rsaSign: {
              const e4 = M.readMPI(t3.subarray(r3));
              return r3 += e4.length + 2, { read: r3, signatureParams: { s: e4 } };
            }
            case R.publicKey.dsa:
            case R.publicKey.ecdsa: {
              const e4 = M.readMPI(t3.subarray(r3));
              r3 += e4.length + 2;
              const n3 = M.readMPI(t3.subarray(r3));
              return r3 += n3.length + 2, { read: r3, signatureParams: { r: e4, s: n3 } };
            }
            case R.publicKey.eddsaLegacy: {
              const e4 = M.readMPI(t3.subarray(r3));
              r3 += e4.length + 2;
              const n3 = M.readMPI(t3.subarray(r3));
              return r3 += n3.length + 2, { read: r3, signatureParams: { r: e4, s: n3 } };
            }
            case R.publicKey.ed25519:
            case R.publicKey.ed448: {
              const n3 = 2 * rr(e3), i3 = M.readExactSubarray(t3, r3, r3 + n3);
              return r3 += i3.length, { read: r3, signatureParams: { RS: i3 } };
            }
            default:
              throw new Yt("Unknown signature algorithm.");
          }
        }(this.publicKeyAlgorithm, n2);
        if (i2 < n2.length) throw Error("Error reading MPIs");
        this.params = s2;
      }
      writeParams() {
        return this.params instanceof Promise ? U(async () => bi(this.publicKeyAlgorithm, await this.params)) : bi(this.publicKeyAlgorithm, this.params);
      }
      write() {
        const e2 = [];
        return e2.push(this.signatureData), e2.push(this.writeUnhashedSubPackets()), e2.push(this.signedHashValue), 6 === this.version && (e2.push(new Uint8Array([this.salt.length])), e2.push(this.salt)), e2.push(this.writeParams()), M.concat(e2);
      }
      async sign(e2, t2, r2 = /* @__PURE__ */ new Date(), n2 = false, i2) {
        this.version = e2.version, this.created = M.normalizeDate(r2), this.issuerKeyVersion = e2.version, this.issuerFingerprint = e2.getFingerprintBytes(), this.issuerKeyID = e2.getKeyID();
        const s2 = [new Uint8Array([this.version, this.signatureType, this.publicKeyAlgorithm, this.hashAlgorithm])];
        if (6 === this.version) {
          const e3 = Ka(this.hashAlgorithm);
          if (null === this.salt) this.salt = fe(e3);
          else if (e3 !== this.salt.length) throw Error("Provided salt does not have the required length");
        } else if (i2.nonDeterministicSignaturesViaNotation) {
          if (0 !== this.rawNotations.filter(({ name: e3 }) => e3 === va).length) throw Error("Unexpected existing salt notation");
          {
            const e3 = fe(Ka(this.hashAlgorithm));
            this.rawNotations.push({ name: va, value: e3, humanReadable: false, critical: false });
          }
        }
        s2.push(this.writeHashedSubPackets()), this.unhashedSubpackets = [], this.signatureData = M.concat(s2);
        const a2 = this.toHash(this.signatureType, t2, n2), o2 = await this.hash(this.signatureType, t2, a2, n2);
        this.signedHashValue = K(I(o2), 0, 2);
        const c2 = async () => ms(this.publicKeyAlgorithm, this.hashAlgorithm, e2.publicParams, e2.privateParams, a2, await C(o2));
        M.isStream(o2) ? this.params = c2() : (this.params = await c2(), this[Ea] = true);
      }
      writeHashedSubPackets() {
        const e2 = R.signatureSubpacket, t2 = [];
        let r2;
        if (null === this.created) throw Error("Missing signature creation time");
        t2.push(Sa(e2.signatureCreationTime, true, M.writeDate(this.created))), null !== this.signatureExpirationTime && t2.push(Sa(e2.signatureExpirationTime, true, M.writeNumber(this.signatureExpirationTime, 4))), null !== this.exportable && t2.push(Sa(e2.exportableCertification, true, new Uint8Array([this.exportable ? 1 : 0]))), null !== this.trustLevel && (r2 = new Uint8Array([this.trustLevel, this.trustAmount]), t2.push(Sa(e2.trustSignature, true, r2))), null !== this.regularExpression && t2.push(Sa(e2.regularExpression, true, this.regularExpression)), null !== this.revocable && t2.push(Sa(e2.revocable, true, new Uint8Array([this.revocable ? 1 : 0]))), null !== this.keyExpirationTime && t2.push(Sa(e2.keyExpirationTime, true, M.writeNumber(this.keyExpirationTime, 4))), null !== this.preferredSymmetricAlgorithms && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.preferredSymmetricAlgorithms)), t2.push(Sa(e2.preferredSymmetricAlgorithms, false, r2))), null !== this.revocationKeyClass && (r2 = new Uint8Array([this.revocationKeyClass, this.revocationKeyAlgorithm]), r2 = M.concat([r2, this.revocationKeyFingerprint]), t2.push(Sa(e2.revocationKey, false, r2))), !this.issuerKeyID.isNull() && this.issuerKeyVersion < 5 && t2.push(Sa(e2.issuerKeyID, true, this.issuerKeyID.write())), this.rawNotations.forEach(({ name: n3, value: i3, humanReadable: s2, critical: a2 }) => {
          r2 = [new Uint8Array([s2 ? 128 : 0, 0, 0, 0])];
          const o2 = M.encodeUTF8(n3);
          r2.push(M.writeNumber(o2.length, 2)), r2.push(M.writeNumber(i3.length, 2)), r2.push(o2), r2.push(i3), r2 = M.concat(r2), t2.push(Sa(e2.notationData, a2, r2));
        }), null !== this.preferredHashAlgorithms && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.preferredHashAlgorithms)), t2.push(Sa(e2.preferredHashAlgorithms, false, r2))), null !== this.preferredCompressionAlgorithms && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.preferredCompressionAlgorithms)), t2.push(Sa(e2.preferredCompressionAlgorithms, false, r2))), null !== this.keyServerPreferences && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.keyServerPreferences)), t2.push(Sa(e2.keyServerPreferences, false, r2))), null !== this.preferredKeyServer && t2.push(Sa(e2.preferredKeyServer, false, M.encodeUTF8(this.preferredKeyServer))), null !== this.isPrimaryUserID && t2.push(Sa(e2.primaryUserID, false, new Uint8Array([this.isPrimaryUserID ? 1 : 0]))), null !== this.policyURI && t2.push(Sa(e2.policyURI, false, M.encodeUTF8(this.policyURI))), null !== this.keyFlags && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.keyFlags)), t2.push(Sa(e2.keyFlags, true, r2))), null !== this.signersUserID && t2.push(Sa(e2.signersUserID, false, M.encodeUTF8(this.signersUserID))), null !== this.reasonForRevocationFlag && (r2 = M.stringToUint8Array(String.fromCharCode(this.reasonForRevocationFlag) + this.reasonForRevocationString), t2.push(Sa(e2.reasonForRevocation, true, r2))), null !== this.features && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.features)), t2.push(Sa(e2.features, false, r2))), null !== this.signatureTargetPublicKeyAlgorithm && (r2 = [new Uint8Array([this.signatureTargetPublicKeyAlgorithm, this.signatureTargetHashAlgorithm])], r2.push(M.stringToUint8Array(this.signatureTargetHash)), r2 = M.concat(r2), t2.push(Sa(e2.signatureTarget, true, r2))), null !== this.embeddedSignature && t2.push(Sa(e2.embeddedSignature, true, this.embeddedSignature.write())), null !== this.issuerFingerprint && (r2 = [new Uint8Array([this.issuerKeyVersion]), this.issuerFingerprint], r2 = M.concat(r2), t2.push(Sa(e2.issuerFingerprint, this.version >= 5, r2))), null !== this.preferredAEADAlgorithms && (r2 = M.stringToUint8Array(M.uint8ArrayToString(this.preferredAEADAlgorithms)), t2.push(Sa(e2.preferredAEADAlgorithms, false, r2))), null !== this.preferredCipherSuites && (r2 = new Uint8Array([].concat(...this.preferredCipherSuites)), t2.push(Sa(e2.preferredCipherSuites, false, r2)));
        const n2 = M.concat(t2), i2 = M.writeNumber(n2.length, 6 === this.version ? 4 : 2);
        return M.concat([i2, n2]);
      }
      writeUnhashedSubPackets() {
        const e2 = this.unhashedSubpackets.map(({ type: e3, critical: t3, body: r3 }) => Sa(e3, t3, r3)), t2 = M.concat(e2), r2 = M.writeNumber(t2.length, 6 === this.version ? 4 : 2);
        return M.concat([r2, t2]);
      }
      readSubPacket(e2, t2 = true) {
        let r2 = 0;
        const n2 = !!(128 & e2[r2]), i2 = 127 & e2[r2];
        if (r2++, t2 || (this.unhashedSubpackets.push({ type: i2, critical: n2, body: e2.subarray(r2, e2.length) }), Ia.has(i2))) switch (i2) {
          case R.signatureSubpacket.signatureCreationTime:
            this.created = M.readDate(e2.subarray(r2, e2.length));
            break;
          case R.signatureSubpacket.signatureExpirationTime: {
            const t3 = M.readNumber(e2.subarray(r2, e2.length));
            this.signatureNeverExpires = 0 === t3, this.signatureExpirationTime = t3;
            break;
          }
          case R.signatureSubpacket.exportableCertification:
            this.exportable = 1 === e2[r2++];
            break;
          case R.signatureSubpacket.trustSignature:
            this.trustLevel = e2[r2++], this.trustAmount = e2[r2++];
            break;
          case R.signatureSubpacket.regularExpression:
            this.regularExpression = e2[r2];
            break;
          case R.signatureSubpacket.revocable:
            this.revocable = 1 === e2[r2++];
            break;
          case R.signatureSubpacket.keyExpirationTime: {
            const t3 = M.readNumber(e2.subarray(r2, e2.length));
            this.keyExpirationTime = t3, this.keyNeverExpires = 0 === t3;
            break;
          }
          case R.signatureSubpacket.preferredSymmetricAlgorithms:
            this.preferredSymmetricAlgorithms = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.revocationKey:
            this.revocationKeyClass = e2[r2++], this.revocationKeyAlgorithm = e2[r2++], this.revocationKeyFingerprint = e2.subarray(r2, r2 + 20);
            break;
          case R.signatureSubpacket.issuerKeyID:
            if (4 === this.version) this.issuerKeyID.read(e2.subarray(r2, e2.length));
            else if (t2) throw Error("Unexpected Issuer Key ID subpacket");
            break;
          case R.signatureSubpacket.notationData: {
            const t3 = !!(128 & e2[r2]);
            r2 += 4;
            const i3 = M.readNumber(e2.subarray(r2, r2 + 2));
            r2 += 2;
            const s2 = M.readNumber(e2.subarray(r2, r2 + 2));
            r2 += 2;
            const a2 = M.decodeUTF8(e2.subarray(r2, r2 + i3)), o2 = e2.subarray(r2 + i3, r2 + i3 + s2);
            this.rawNotations.push({ name: a2, humanReadable: t3, value: o2, critical: n2 }), t3 && (this.notations[a2] = M.decodeUTF8(o2));
            break;
          }
          case R.signatureSubpacket.preferredHashAlgorithms:
            this.preferredHashAlgorithms = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.preferredCompressionAlgorithms:
            this.preferredCompressionAlgorithms = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.keyServerPreferences:
            this.keyServerPreferences = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.preferredKeyServer:
            this.preferredKeyServer = M.decodeUTF8(e2.subarray(r2, e2.length));
            break;
          case R.signatureSubpacket.primaryUserID:
            this.isPrimaryUserID = 0 !== e2[r2++];
            break;
          case R.signatureSubpacket.policyURI:
            this.policyURI = M.decodeUTF8(e2.subarray(r2, e2.length));
            break;
          case R.signatureSubpacket.keyFlags:
            this.keyFlags = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.signersUserID:
            this.signersUserID = M.decodeUTF8(e2.subarray(r2, e2.length));
            break;
          case R.signatureSubpacket.reasonForRevocation:
            this.reasonForRevocationFlag = e2[r2++], this.reasonForRevocationString = M.decodeUTF8(e2.subarray(r2, e2.length));
            break;
          case R.signatureSubpacket.features:
            this.features = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.signatureTarget: {
            this.signatureTargetPublicKeyAlgorithm = e2[r2++], this.signatureTargetHashAlgorithm = e2[r2++];
            const t3 = Re(this.signatureTargetHashAlgorithm);
            this.signatureTargetHash = M.uint8ArrayToString(e2.subarray(r2, r2 + t3));
            break;
          }
          case R.signatureSubpacket.embeddedSignature:
            this.embeddedSignature = new _Ba(), this.embeddedSignature.read(e2.subarray(r2, e2.length));
            break;
          case R.signatureSubpacket.issuerFingerprint:
            this.issuerKeyVersion = e2[r2++], this.issuerFingerprint = e2.subarray(r2, e2.length), this.issuerKeyVersion >= 5 ? this.issuerKeyID.read(this.issuerFingerprint) : this.issuerKeyID.read(this.issuerFingerprint.subarray(-8));
            break;
          case R.signatureSubpacket.preferredAEADAlgorithms:
            this.preferredAEADAlgorithms = [...e2.subarray(r2, e2.length)];
            break;
          case R.signatureSubpacket.preferredCipherSuites:
            this.preferredCipherSuites = [];
            for (let t3 = r2; t3 < e2.length; t3 += 2) this.preferredCipherSuites.push([e2[t3], e2[t3 + 1]]);
            break;
          default:
            this.unknownSubpackets.push({ type: i2, critical: n2, body: e2.subarray(r2, e2.length) });
        }
      }
      readSubPackets(e2, t2 = true, r2) {
        const n2 = 6 === this.version ? 4 : 2, i2 = M.readNumber(e2.subarray(0, n2));
        let s2 = n2;
        for (; s2 < 2 + i2; ) {
          const n3 = zt(e2.subarray(s2, e2.length));
          s2 += n3.offset, this.readSubPacket(e2.subarray(s2, s2 + n3.len), t2, r2), s2 += n3.len;
        }
        return s2;
      }
      toSign(e2, t2) {
        const r2 = R.signature;
        switch (e2) {
          case r2.binary:
            return null !== t2.text ? M.encodeUTF8(t2.getText(true)) : t2.getBytes(true);
          case r2.text: {
            const e3 = t2.getBytes(true);
            return M.canonicalizeEOL(e3);
          }
          case r2.standalone:
            return new Uint8Array(0);
          case r2.certGeneric:
          case r2.certPersona:
          case r2.certCasual:
          case r2.certPositive:
          case r2.certRevocation: {
            let e3, n2;
            if (t2.userID) n2 = 180, e3 = t2.userID;
            else {
              if (!t2.userAttribute) throw Error("Either a userID or userAttribute packet needs to be supplied for certification.");
              n2 = 209, e3 = t2.userAttribute;
            }
            const i2 = e3.write();
            return M.concat([this.toSign(r2.key, t2), new Uint8Array([n2]), M.writeNumber(i2.length, 4), i2]);
          }
          case r2.subkeyBinding:
          case r2.subkeyRevocation:
          case r2.keyBinding:
            return M.concat([this.toSign(r2.key, t2), this.toSign(r2.key, { key: t2.bind })]);
          case r2.key:
            if (void 0 === t2.key) throw Error("Key packet is required for this signature.");
            return t2.key.writeForHash(this.version);
          case r2.keyRevocation:
            return this.toSign(r2.key, t2);
          case r2.timestamp:
            return new Uint8Array(0);
          case r2.thirdParty:
            throw Error("Not implemented");
          default:
            throw Error("Unknown signature type.");
        }
      }
      calculateTrailer(e2, t2) {
        let r2 = 0;
        return k(I(this.signatureData), (e3) => {
          r2 += e3.length;
        }, () => {
          const n2 = [];
          return 5 !== this.version || this.signatureType !== R.signature.binary && this.signatureType !== R.signature.text || (t2 ? n2.push(new Uint8Array(6)) : n2.push(e2.writeHeader())), n2.push(new Uint8Array([this.version, 255])), 5 === this.version && n2.push(new Uint8Array(4)), n2.push(M.writeNumber(r2, 4)), M.concat(n2);
        });
      }
      toHash(e2, t2, r2 = false) {
        const n2 = this.toSign(e2, t2);
        return M.concat([this.salt || new Uint8Array(), n2, this.signatureData, this.calculateTrailer(t2, r2)]);
      }
      async hash(e2, t2, r2, n2 = false) {
        if (6 === this.version && this.salt.length !== Ka(this.hashAlgorithm)) throw Error("Signature salt does not have the expected length");
        return r2 || (r2 = this.toHash(e2, t2, n2)), Qe(this.hashAlgorithm, r2);
      }
      async verify(e2, t2, r2, n2 = /* @__PURE__ */ new Date(), i2 = false, s2 = T) {
        if (!this.issuerKeyID.equals(e2.getKeyID())) throw Error("Signature was not issued by the given public key");
        if (this.publicKeyAlgorithm !== e2.algorithm) throw Error("Public key algorithm used to sign signature does not match issuer key algorithm.");
        const a2 = t2 === R.signature.binary || t2 === R.signature.text;
        if (!(this[Ea] && !a2)) {
          let n3, s3;
          if (this.hashed ? s3 = await this.hashed : (n3 = this.toHash(t2, r2, i2), s3 = await this.hash(t2, r2, n3)), s3 = await C(s3), this.signedHashValue[0] !== s3[0] || this.signedHashValue[1] !== s3[1]) throw Error("Signed digest did not match");
          if (this.params = await this.params, this[Ea] = await ws(this.publicKeyAlgorithm, this.hashAlgorithm, this.params, e2.publicParams, n3, s3), !this[Ea]) throw Error("Signature verification failed");
        }
        const o2 = M.normalizeDate(n2);
        if (o2 && this.created > o2) throw Error("Signature creation time is in the future");
        if (o2 && o2 >= this.getExpirationTime()) throw Error("Signature is expired");
        if (s2.rejectHashAlgorithms.has(this.hashAlgorithm)) throw Error("Insecure hash algorithm: " + R.read(R.hash, this.hashAlgorithm).toUpperCase());
        if (s2.rejectMessageHashAlgorithms.has(this.hashAlgorithm) && [R.signature.binary, R.signature.text].includes(this.signatureType)) throw Error("Insecure message hash algorithm: " + R.read(R.hash, this.hashAlgorithm).toUpperCase());
        if (this.unknownSubpackets.forEach(({ type: e3, critical: t3 }) => {
          if (t3) throw Error("Unknown critical signature subpacket type " + e3);
        }), this.rawNotations.forEach(({ name: e3, critical: t3 }) => {
          if (t3 && s2.knownNotations.indexOf(e3) < 0) throw Error("Unknown critical notation: " + e3);
        }), null !== this.revocationKeyClass) throw Error("This key is intended to be revoked with an authorized key, which OpenPGP.js does not support.");
      }
      isExpired(e2 = /* @__PURE__ */ new Date()) {
        const t2 = M.normalizeDate(e2);
        return null !== t2 && !(this.created <= t2 && t2 < this.getExpirationTime());
      }
      getExpirationTime() {
        return this.signatureNeverExpires ? 1 / 0 : new Date(this.created.getTime() + 1e3 * this.signatureExpirationTime);
      }
    };
    Ca = class _Ca {
      static get tag() {
        return R.packet.onePassSignature;
      }
      static fromSignaturePacket(e2, t2) {
        const r2 = new _Ca();
        return r2.version = 6 === e2.version ? 6 : 3, r2.signatureType = e2.signatureType, r2.hashAlgorithm = e2.hashAlgorithm, r2.publicKeyAlgorithm = e2.publicKeyAlgorithm, r2.issuerKeyID = e2.issuerKeyID, r2.salt = e2.salt, r2.issuerFingerprint = e2.issuerFingerprint, r2.flags = t2 ? 1 : 0, r2;
      }
      constructor() {
        this.version = null, this.signatureType = null, this.hashAlgorithm = null, this.publicKeyAlgorithm = null, this.salt = null, this.issuerKeyID = null, this.issuerFingerprint = null, this.flags = null;
      }
      read(e2) {
        let t2 = 0;
        if (this.version = e2[t2++], 3 !== this.version && 6 !== this.version) throw new Yt(`Version ${this.version} of the one-pass signature packet is unsupported.`);
        if (this.signatureType = e2[t2++], this.hashAlgorithm = e2[t2++], this.publicKeyAlgorithm = e2[t2++], 6 === this.version) {
          const r2 = e2[t2++];
          this.salt = e2.subarray(t2, t2 + r2), t2 += r2, this.issuerFingerprint = e2.subarray(t2, t2 + 32), t2 += 32, this.issuerKeyID = new ka(), this.issuerKeyID.read(this.issuerFingerprint);
        } else this.issuerKeyID = new ka(), this.issuerKeyID.read(e2.subarray(t2, t2 + 8)), t2 += 8;
        return this.flags = e2[t2++], this;
      }
      write() {
        const e2 = [new Uint8Array([this.version, this.signatureType, this.hashAlgorithm, this.publicKeyAlgorithm])];
        return 6 === this.version ? e2.push(new Uint8Array([this.salt.length]), this.salt, this.issuerFingerprint) : e2.push(this.issuerKeyID.write()), e2.push(new Uint8Array([this.flags])), M.concatUint8Array(e2);
      }
      calculateTrailer(...e2) {
        return U(async () => Ba.prototype.calculateTrailer.apply(await this.correspondingSig, e2));
      }
      async verify() {
        const e2 = await this.correspondingSig;
        if (!e2 || e2.constructor.tag !== R.packet.signature) throw Error("Corresponding signature packet missing");
        if (e2.signatureType !== this.signatureType || e2.hashAlgorithm !== this.hashAlgorithm || e2.publicKeyAlgorithm !== this.publicKeyAlgorithm || !e2.issuerKeyID.equals(this.issuerKeyID) || 3 === this.version && 6 === e2.version || 6 === this.version && 6 !== e2.version || 6 === this.version && !M.equalsUint8Array(e2.issuerFingerprint, this.issuerFingerprint) || 6 === this.version && !M.equalsUint8Array(e2.salt, this.salt)) throw Error("Corresponding signature packet does not match one-pass signature packet");
        return e2.hashed = this.hashed, e2.verify.apply(e2, arguments);
      }
    };
    Ca.prototype.hash = Ba.prototype.hash, Ca.prototype.toHash = Ba.prototype.toHash, Ca.prototype.toSign = Ba.prototype.toSign;
    Ua = class _Ua extends Array {
      static async fromBinary(e2, t2, r2 = T) {
        const n2 = new _Ua();
        return await n2.read(e2, t2, r2), n2;
      }
      async read(e2, t2, r2 = T) {
        r2.additionalAllowedPackets.length && (t2 = { ...t2, ...M.constructAllowedPackets(r2.additionalAllowedPackets) }), this.stream = E(e2, async (e3, n3) => {
          const i2 = x(n3);
          try {
            for (; ; ) {
              await i2.ready;
              if (await Jt(e3, async (e4) => {
                try {
                  if (e4.tag === R.packet.marker || e4.tag === R.packet.trust || e4.tag === R.packet.padding) return;
                  const n4 = Da(e4.tag, t2);
                  n4.packets = new _Ua(), n4.fromStream = M.isStream(e4.packet), await n4.read(e4.packet, r2), await i2.write(n4);
                } catch (t3) {
                  if (t3 instanceof Zt) {
                    if (!(e4.tag <= 39)) return;
                    await i2.abort(t3);
                  }
                  const n4 = !r2.ignoreUnsupportedPackets && t3 instanceof Yt, s2 = !(r2.ignoreMalformedPackets || t3 instanceof Yt);
                  if (n4 || s2 || Vt(e4.tag)) await i2.abort(t3);
                  else {
                    const t4 = new Wt(e4.tag, e4.packet);
                    await i2.write(t4);
                  }
                  M.printDebugError(t3);
                }
              })) return await i2.ready, void await i2.close();
            }
          } catch (e4) {
            await i2.abort(e4);
          }
        });
        const n2 = P(this.stream);
        for (; ; ) {
          const { done: e3, value: t3 } = await n2.read();
          if (e3 ? this.stream = null : this.push(t3), e3 || Vt(t3.constructor.tag)) break;
        }
        n2.releaseLock();
      }
      write() {
        const e2 = [];
        for (let t2 = 0; t2 < this.length; t2++) {
          const r2 = this[t2] instanceof Wt ? this[t2].tag : this[t2].constructor.tag, n2 = this[t2].write();
          if (M.isStream(n2) && Vt(this[t2].constructor.tag)) {
            let t3 = [], i2 = 0;
            const s2 = 512;
            e2.push(jt(r2)), e2.push(k(n2, (e3) => {
              if (t3.push(e3), i2 += e3.length, i2 >= s2) {
                const e4 = Math.min(Math.log(i2) / Math.LN2 | 0, 30), r3 = 2 ** e4, n3 = M.concat([_t(e4)].concat(t3));
                return t3 = [n3.subarray(1 + r3)], i2 = t3[0].length, n3.subarray(0, 1 + r3);
              }
            }, () => M.concat([Gt(i2)].concat(t3))));
          } else {
            if (M.isStream(n2)) {
              let t3 = 0;
              e2.push(k(I(n2), (e3) => {
                t3 += e3.length;
              }, () => qt(r2, t3)));
            } else e2.push(qt(r2, n2.length));
            e2.push(n2);
          }
        }
        return M.concat(e2);
      }
      filterByTag(...e2) {
        const t2 = new _Ua(), r2 = (e3) => (t3) => e3 === t3;
        for (let n2 = 0; n2 < this.length; n2++) e2.some(r2(this[n2].constructor.tag)) && t2.push(this[n2]);
        return t2;
      }
      findPacket(e2) {
        return this.find((t2) => t2.constructor.tag === e2);
      }
      indexOfTag(...e2) {
        const t2 = [], r2 = this, n2 = (e3) => (t3) => e3 === t3;
        for (let i2 = 0; i2 < this.length; i2++) e2.some(n2(r2[i2].constructor.tag)) && t2.push(i2);
        return t2;
      }
    };
    Pa = /* @__PURE__ */ M.constructAllowedPackets([ba, Ca, Ba]);
    xa = class {
      static get tag() {
        return R.packet.compressedData;
      }
      constructor(e2 = T) {
        this.packets = null, this.algorithm = e2.preferredCompressionAlgorithm, this.compressed = null;
      }
      async read(e2, t2 = T) {
        await v(e2, async (e3) => {
          this.algorithm = await e3.readByte(), this.compressed = e3.remainder(), await this.decompress(t2);
        });
      }
      write() {
        return null === this.compressed && this.compress(), M.concat([new Uint8Array([this.algorithm]), this.compressed]);
      }
      async decompress(e2 = T) {
        const t2 = R.read(R.compression, this.algorithm), r2 = Ma[t2];
        if (!r2) throw Error(t2 + " decompression not supported");
        this.packets = await Ua.fromBinary(await r2(this.compressed), Pa, e2);
      }
      compress() {
        const e2 = R.read(R.compression, this.algorithm), t2 = La[e2];
        if (!t2) throw Error(e2 + " compression not supported");
        this.compressed = t2(this.packets.write());
      }
    };
    Ta = (e2) => ({ compressor: "undefined" != typeof CompressionStream && (() => new CompressionStream(e2)), decompressor: "undefined" != typeof DecompressionStream && (() => new DecompressionStream(e2)) });
    La = { zip: /* @__PURE__ */ Qa(Ta("deflate-raw").compressor, pa), zlib: /* @__PURE__ */ Qa(Ta("deflate").compressor, Aa) };
    Ma = { uncompressed: (e2) => e2, zip: /* @__PURE__ */ Qa(Ta("deflate-raw").decompressor, da), zlib: /* @__PURE__ */ Qa(Ta("deflate").decompressor, wa), bzip2: /* @__PURE__ */ Ra() };
    Na = /* @__PURE__ */ M.constructAllowedPackets([ba, xa, Ca, Ba]);
    Fa = class _Fa {
      static get tag() {
        return R.packet.symEncryptedIntegrityProtectedData;
      }
      static fromObject({ version: e2, aeadAlgorithm: t2 }) {
        if (1 !== e2 && 2 !== e2) throw Error("Unsupported SEIPD version");
        const r2 = new _Fa();
        return r2.version = e2, 2 === e2 && (r2.aeadAlgorithm = t2), r2;
      }
      constructor() {
        this.version = null, this.cipherAlgorithm = null, this.aeadAlgorithm = null, this.chunkSizeByte = null, this.salt = null, this.encrypted = null, this.packets = null;
      }
      async read(e2) {
        await v(e2, async (e3) => {
          if (this.version = await e3.readByte(), 1 !== this.version && 2 !== this.version) throw new Yt(`Version ${this.version} of the SEIP packet is unsupported.`);
          2 === this.version && (this.cipherAlgorithm = await e3.readByte(), this.aeadAlgorithm = await e3.readByte(), this.chunkSizeByte = await e3.readByte(), this.salt = await e3.readBytes(32)), this.encrypted = e3.remainder();
        });
      }
      write() {
        return 2 === this.version ? M.concat([new Uint8Array([this.version, this.cipherAlgorithm, this.aeadAlgorithm, this.chunkSizeByte]), this.salt, this.encrypted]) : M.concat([new Uint8Array([this.version]), this.encrypted]);
      }
      async encrypt(e2, t2, r2 = T) {
        const { blockSize: n2, keySize: i2 } = fn(e2);
        if (t2.length !== i2) throw Error("Unexpected session key size");
        let s2 = this.packets.write();
        if (o(s2) && (s2 = await C(s2)), 2 === this.version) this.cipherAlgorithm = e2, this.salt = fe(32), this.chunkSizeByte = r2.aeadChunkSizeByte, this.encrypted = await Oa(this, "encrypt", t2, s2);
        else {
          const r3 = await Ui(e2), i3 = new Uint8Array([211, 20]), a2 = M.concat([r3, s2, i3]), o2 = await Qe(R.hash.sha1, B(a2)), c2 = M.concat([a2, o2]);
          this.encrypted = await Pi(e2, t2, c2, new Uint8Array(n2));
        }
        return true;
      }
      async decrypt(e2, t2, r2 = T) {
        if (t2.length !== fn(e2).keySize) throw Error("Unexpected session key size");
        let n2, i2 = I(this.encrypted);
        if (o(i2) && (i2 = await C(i2)), 2 === this.version) {
          if (this.cipherAlgorithm !== e2) throw Error("Unexpected session key algorithm");
          n2 = await Oa(this, "decrypt", t2, i2);
        } else {
          const { blockSize: s2 } = fn(e2), a2 = await xi(e2, t2, i2, new Uint8Array(s2)), o2 = K(B(a2), -20), c2 = K(a2, 0, -20), h2 = Promise.all([C(await Qe(R.hash.sha1, B(c2))), C(o2)]).then(([e3, t3]) => {
            if (!M.equalsUint8Array(e3, t3)) throw Error("Modification detected.");
            return new Uint8Array();
          }), u2 = K(c2, s2 + 2);
          n2 = K(u2, 0, -2), n2 = A([n2, U(() => h2)]), M.isStream(i2) && r2.allowUnauthenticatedStream || (n2 = await C(n2));
        }
        return this.packets = await Ua.fromBinary(n2, Na, r2), true;
      }
    };
    Ha = /* @__PURE__ */ M.constructAllowedPackets([ba, xa, Ca, Ba]);
    za = class {
      static get tag() {
        return R.packet.aeadEncryptedData;
      }
      constructor() {
        this.version = 1, this.cipherAlgorithm = null, this.aeadAlgorithm = R.aead.eax, this.chunkSizeByte = null, this.iv = null, this.encrypted = null, this.packets = null;
      }
      async read(e2) {
        await v(e2, async (e3) => {
          const t2 = await e3.readByte();
          if (1 !== t2) throw new Yt(`Version ${t2} of the AEAD-encrypted data packet is not supported.`);
          this.cipherAlgorithm = await e3.readByte(), this.aeadAlgorithm = await e3.readByte(), this.chunkSizeByte = await e3.readByte();
          const r2 = As(this.aeadAlgorithm, true);
          this.iv = await e3.readBytes(r2.ivLength), this.encrypted = e3.remainder();
        });
      }
      write() {
        return M.concat([new Uint8Array([this.version, this.cipherAlgorithm, this.aeadAlgorithm, this.chunkSizeByte]), this.iv, this.encrypted]);
      }
      async decrypt(e2, t2, r2 = T) {
        this.packets = await Ua.fromBinary(await Oa(this, "decrypt", t2, I(this.encrypted)), Ha, r2);
      }
      async encrypt(e2, t2, r2 = T) {
        this.cipherAlgorithm = e2;
        const { ivLength: n2 } = As(this.aeadAlgorithm, true);
        this.iv = fe(n2), this.chunkSizeByte = r2.aeadChunkSizeByte;
        const i2 = this.packets.write();
        this.encrypted = await Oa(this, "encrypt", t2, i2);
      }
    };
    Ga = class _Ga {
      static get tag() {
        return R.packet.publicKeyEncryptedSessionKey;
      }
      constructor() {
        this.version = null, this.publicKeyID = new ka(), this.publicKeyVersion = null, this.publicKeyFingerprint = null, this.publicKeyAlgorithm = null, this.sessionKey = null, this.sessionKeyAlgorithm = null, this.encrypted = {};
      }
      static fromObject({ version: e2, encryptionKeyPacket: t2, anonymousRecipient: r2, sessionKey: n2, sessionKeyAlgorithm: i2 }) {
        const s2 = new _Ga();
        if (3 !== e2 && 6 !== e2) throw Error("Unsupported PKESK version");
        return s2.version = e2, 6 === e2 && (s2.publicKeyVersion = r2 ? null : t2.version, s2.publicKeyFingerprint = r2 ? null : t2.getFingerprintBytes()), s2.publicKeyID = r2 ? ka.wildcard() : t2.getKeyID(), s2.publicKeyAlgorithm = t2.algorithm, s2.sessionKey = n2, s2.sessionKeyAlgorithm = i2, s2;
      }
      read(e2) {
        let t2 = 0;
        if (this.version = e2[t2++], 3 !== this.version && 6 !== this.version) throw new Yt(`Version ${this.version} of the PKESK packet is unsupported.`);
        if (6 === this.version) {
          const r2 = e2[t2++];
          if (r2) {
            this.publicKeyVersion = e2[t2++];
            const n2 = r2 - 1;
            this.publicKeyFingerprint = e2.subarray(t2, t2 + n2), t2 += n2, this.publicKeyVersion >= 5 ? this.publicKeyID.read(this.publicKeyFingerprint) : this.publicKeyID.read(this.publicKeyFingerprint.subarray(-8));
          } else this.publicKeyID = ka.wildcard();
        } else t2 += this.publicKeyID.read(e2.subarray(t2, t2 + 8));
        if (this.publicKeyAlgorithm = e2[t2++], this.encrypted = function(e3, t3) {
          let r2 = 0;
          switch (e3) {
            case R.publicKey.rsaEncrypt:
            case R.publicKey.rsaEncryptSign:
              return { c: M.readMPI(t3.subarray(r2)) };
            case R.publicKey.elgamal: {
              const e4 = M.readMPI(t3.subarray(r2));
              return r2 += e4.length + 2, { c1: e4, c2: M.readMPI(t3.subarray(r2)) };
            }
            case R.publicKey.ecdh: {
              const e4 = M.readMPI(t3.subarray(r2));
              r2 += e4.length + 2;
              const n2 = new gi();
              return n2.read(t3.subarray(r2)), { V: e4, C: n2 };
            }
            case R.publicKey.x25519:
            case R.publicKey.x448: {
              const n2 = Bi(e3), i2 = M.readExactSubarray(t3, r2, r2 + n2);
              r2 += i2.length;
              const s2 = new di();
              return s2.read(t3.subarray(r2)), { ephemeralPublicKey: i2, C: s2 };
            }
            default:
              throw new Yt("Unknown public key encryption algorithm.");
          }
        }(this.publicKeyAlgorithm, e2.subarray(t2)), this.publicKeyAlgorithm === R.publicKey.x25519 || this.publicKeyAlgorithm === R.publicKey.x448) {
          if (3 === this.version) this.sessionKeyAlgorithm = R.write(R.symmetric, this.encrypted.C.algorithm);
          else if (null !== this.encrypted.C.algorithm) throw Error("Unexpected cleartext symmetric algorithm");
        }
      }
      write() {
        const e2 = [new Uint8Array([this.version])];
        return 6 === this.version ? null !== this.publicKeyFingerprint ? (e2.push(new Uint8Array([this.publicKeyFingerprint.length + 1, this.publicKeyVersion])), e2.push(this.publicKeyFingerprint)) : e2.push(new Uint8Array([0])) : e2.push(this.publicKeyID.write()), e2.push(new Uint8Array([this.publicKeyAlgorithm]), bi(this.publicKeyAlgorithm, this.encrypted)), M.concatUint8Array(e2);
      }
      async encrypt(e2) {
        const t2 = R.write(R.publicKey, this.publicKeyAlgorithm), r2 = 3 === this.version ? this.sessionKeyAlgorithm : null, n2 = 5 === e2.version ? e2.getFingerprintBytes().subarray(0, 20) : e2.getFingerprintBytes(), i2 = _a(this.version, t2, r2, this.sessionKey);
        this.encrypted = await Ai(t2, r2, e2.publicParams, i2, n2);
      }
      async decrypt(e2, t2) {
        if (this.publicKeyAlgorithm !== e2.algorithm) throw Error("Decryption error");
        const r2 = t2 ? _a(this.version, this.publicKeyAlgorithm, t2.sessionKeyAlgorithm, t2.sessionKey) : null, n2 = 5 === e2.version ? e2.getFingerprintBytes().subarray(0, 20) : e2.getFingerprintBytes(), i2 = await wi(this.publicKeyAlgorithm, e2.publicParams, e2.privateParams, this.encrypted, n2, r2), { sessionKey: s2, sessionKeyAlgorithm: a2 } = function(e3, t3, r3, n3) {
          switch (t3) {
            case R.publicKey.rsaEncrypt:
            case R.publicKey.rsaEncryptSign:
            case R.publicKey.elgamal:
            case R.publicKey.ecdh: {
              const t4 = r3.subarray(0, r3.length - 2), i3 = r3.subarray(r3.length - 2), s3 = M.writeChecksum(t4.subarray(t4.length % 8)), a3 = s3[0] === i3[0] & s3[1] === i3[1], o2 = 6 === e3 ? { sessionKeyAlgorithm: null, sessionKey: t4 } : { sessionKeyAlgorithm: t4[0], sessionKey: t4.subarray(1) };
              if (n3) {
                const t5 = a3 & o2.sessionKeyAlgorithm === n3.sessionKeyAlgorithm & o2.sessionKey.length === n3.sessionKey.length;
                return { sessionKey: M.selectUint8Array(t5, o2.sessionKey, n3.sessionKey), sessionKeyAlgorithm: 6 === e3 ? null : M.selectUint8(t5, o2.sessionKeyAlgorithm, n3.sessionKeyAlgorithm) };
              }
              if (a3 && (6 === e3 || R.read(R.symmetric, o2.sessionKeyAlgorithm))) return o2;
              throw Error("Decryption error");
            }
            case R.publicKey.x25519:
            case R.publicKey.x448:
              return { sessionKeyAlgorithm: null, sessionKey: r3 };
            default:
              throw Error("Unsupported public key algorithm");
          }
        }(this.version, this.publicKeyAlgorithm, i2, t2);
        if (3 === this.version) {
          const e3 = this.publicKeyAlgorithm !== R.publicKey.x25519 && this.publicKeyAlgorithm !== R.publicKey.x448;
          if (this.sessionKeyAlgorithm = e3 ? a2 : this.sessionKeyAlgorithm, s2.length !== fn(this.sessionKeyAlgorithm).keySize) throw Error("Unexpected session key size");
        }
        this.sessionKey = s2;
      }
    };
    ja = class _ja {
      static get tag() {
        return R.packet.symEncryptedSessionKey;
      }
      constructor(e2 = T) {
        this.version = e2.aeadProtect ? 6 : 4, this.sessionKey = null, this.sessionKeyEncryptionAlgorithm = null, this.sessionKeyAlgorithm = null, this.aeadAlgorithm = R.write(R.aead, e2.preferredAEADAlgorithm), this.encrypted = null, this.s2k = null, this.iv = null;
      }
      read(e2) {
        let t2 = 0;
        if (this.version = e2[t2++], 4 !== this.version && 5 !== this.version && 6 !== this.version) throw new Yt(`Version ${this.version} of the SKESK packet is unsupported.`);
        6 === this.version && t2++;
        const r2 = e2[t2++];
        this.version >= 5 && (this.aeadAlgorithm = e2[t2++], 6 === this.version && t2++);
        const n2 = e2[t2++];
        if (this.s2k = Ss(n2), t2 += this.s2k.read(e2.subarray(t2, e2.length)), this.version >= 5) {
          const r3 = As(this.aeadAlgorithm, true);
          this.iv = e2.subarray(t2, t2 += r3.ivLength);
        }
        this.version >= 5 || t2 < e2.length ? (this.encrypted = e2.subarray(t2, e2.length), this.sessionKeyEncryptionAlgorithm = r2) : this.sessionKeyAlgorithm = r2;
      }
      write() {
        const e2 = null === this.encrypted ? this.sessionKeyAlgorithm : this.sessionKeyEncryptionAlgorithm;
        let t2;
        const r2 = this.s2k.write();
        if (6 === this.version) {
          const n2 = r2.length, i2 = 3 + n2 + this.iv.length;
          t2 = M.concatUint8Array([new Uint8Array([this.version, i2, e2, this.aeadAlgorithm, n2]), r2, this.iv, this.encrypted]);
        } else 5 === this.version ? t2 = M.concatUint8Array([new Uint8Array([this.version, e2, this.aeadAlgorithm]), r2, this.iv, this.encrypted]) : (t2 = M.concatUint8Array([new Uint8Array([this.version, e2]), r2]), null !== this.encrypted && (t2 = M.concatUint8Array([t2, this.encrypted])));
        return t2;
      }
      async decrypt(e2) {
        const t2 = null !== this.sessionKeyEncryptionAlgorithm ? this.sessionKeyEncryptionAlgorithm : this.sessionKeyAlgorithm, { blockSize: r2, keySize: n2 } = fn(t2), i2 = await this.s2k.produceKey(e2, n2);
        if (this.version >= 5) {
          const e3 = As(this.aeadAlgorithm, true), r3 = new Uint8Array([192 | _ja.tag, this.version, this.sessionKeyEncryptionAlgorithm, this.aeadAlgorithm]), s2 = 6 === this.version ? await wn(R.hash.sha256, i2, new Uint8Array(), r3, n2) : i2, a2 = await e3(t2, s2);
          this.sessionKey = await a2.decrypt(this.encrypted, this.iv, r3);
        } else if (null !== this.encrypted) {
          const e3 = await xi(t2, i2, this.encrypted, new Uint8Array(r2));
          if (this.sessionKeyAlgorithm = R.write(R.symmetric, e3[0]), this.sessionKey = e3.subarray(1, e3.length), this.sessionKey.length !== fn(this.sessionKeyAlgorithm).keySize) throw Error("Unexpected session key size");
        } else this.sessionKey = i2;
      }
      async encrypt(e2, t2 = T) {
        const r2 = null !== this.sessionKeyEncryptionAlgorithm ? this.sessionKeyEncryptionAlgorithm : this.sessionKeyAlgorithm;
        this.sessionKeyEncryptionAlgorithm = r2, this.s2k = Ks(t2), this.s2k.generateSalt();
        const { blockSize: n2, keySize: i2 } = fn(r2), s2 = await this.s2k.produceKey(e2, i2);
        if (null === this.sessionKey && (this.sessionKey = vi(this.sessionKeyAlgorithm)), this.version >= 5) {
          const e3 = As(this.aeadAlgorithm);
          this.iv = fe(e3.ivLength);
          const t3 = new Uint8Array([192 | _ja.tag, this.version, this.sessionKeyEncryptionAlgorithm, this.aeadAlgorithm]), n3 = 6 === this.version ? await wn(R.hash.sha256, s2, new Uint8Array(), t3, i2) : s2, a2 = await e3(r2, n3);
          this.encrypted = await a2.encrypt(this.sessionKey, this.iv, t3);
        } else {
          const e3 = M.concatUint8Array([new Uint8Array([this.sessionKeyAlgorithm]), this.sessionKey]);
          this.encrypted = await Pi(r2, s2, e3, new Uint8Array(n2));
        }
      }
    };
    qa = class _qa {
      static get tag() {
        return R.packet.publicKey;
      }
      constructor(e2 = /* @__PURE__ */ new Date(), t2 = T) {
        this.version = t2.v6Keys ? 6 : 4, this.created = M.normalizeDate(e2), this.algorithm = null, this.publicParams = null, this.expirationTimeV3 = 0, this.fingerprint = null, this.keyID = null;
      }
      static fromSecretKeyPacket(e2) {
        const t2 = new _qa(), { version: r2, created: n2, algorithm: i2, publicParams: s2, keyID: a2, fingerprint: o2 } = e2;
        return t2.version = r2, t2.created = n2, t2.algorithm = i2, t2.publicParams = s2, t2.keyID = a2, t2.fingerprint = o2, t2;
      }
      async read(e2, t2 = T) {
        let r2 = 0;
        if (this.version = e2[r2++], 5 === this.version && !t2.enableParsingV5Entities) throw new Yt("Support for parsing v5 entities is disabled; turn on `config.enableParsingV5Entities` if needed");
        if (4 === this.version || 5 === this.version || 6 === this.version) {
          this.created = M.readDate(e2.subarray(r2, r2 + 4)), r2 += 4, this.algorithm = e2[r2++], this.version >= 5 && (r2 += 4);
          const { read: t3, publicParams: n2 } = function(e3, t4) {
            let r3 = 0;
            switch (e3) {
              case R.publicKey.rsaEncrypt:
              case R.publicKey.rsaEncryptSign:
              case R.publicKey.rsaSign: {
                const e4 = M.readMPI(t4.subarray(r3));
                r3 += e4.length + 2;
                const n3 = M.readMPI(t4.subarray(r3));
                return r3 += n3.length + 2, { read: r3, publicParams: { n: e4, e: n3 } };
              }
              case R.publicKey.dsa: {
                const e4 = M.readMPI(t4.subarray(r3));
                r3 += e4.length + 2;
                const n3 = M.readMPI(t4.subarray(r3));
                r3 += n3.length + 2;
                const i2 = M.readMPI(t4.subarray(r3));
                r3 += i2.length + 2;
                const s2 = M.readMPI(t4.subarray(r3));
                return r3 += s2.length + 2, { read: r3, publicParams: { p: e4, q: n3, g: i2, y: s2 } };
              }
              case R.publicKey.elgamal: {
                const e4 = M.readMPI(t4.subarray(r3));
                r3 += e4.length + 2;
                const n3 = M.readMPI(t4.subarray(r3));
                r3 += n3.length + 2;
                const i2 = M.readMPI(t4.subarray(r3));
                return r3 += i2.length + 2, { read: r3, publicParams: { p: e4, g: n3, y: i2 } };
              }
              case R.publicKey.ecdsa: {
                const e4 = new Ht();
                r3 += e4.read(t4), Ii(e4);
                const n3 = M.readMPI(t4.subarray(r3));
                return r3 += n3.length + 2, { read: r3, publicParams: { oid: e4, Q: n3 } };
              }
              case R.publicKey.eddsaLegacy: {
                const e4 = new Ht();
                if (r3 += e4.read(t4), Ii(e4), e4.getName() !== R.curve.ed25519Legacy) throw Error("Unexpected OID for eddsaLegacy");
                let n3 = M.readMPI(t4.subarray(r3));
                return r3 += n3.length + 2, n3 = M.leftPad(n3, 33), { read: r3, publicParams: { oid: e4, Q: n3 } };
              }
              case R.publicKey.ecdh: {
                const e4 = new Ht();
                r3 += e4.read(t4), Ii(e4);
                const n3 = M.readMPI(t4.subarray(r3));
                r3 += n3.length + 2;
                const i2 = new pi();
                return r3 += i2.read(t4.subarray(r3)), { read: r3, publicParams: { oid: e4, Q: n3, kdfParams: i2 } };
              }
              case R.publicKey.ed25519:
              case R.publicKey.ed448:
              case R.publicKey.x25519:
              case R.publicKey.x448: {
                const n3 = M.readExactSubarray(t4, r3, r3 + Bi(e3));
                return r3 += n3.length, { read: r3, publicParams: { A: n3 } };
              }
              default:
                throw new Yt("Unknown public key encryption algorithm.");
            }
          }(this.algorithm, e2.subarray(r2));
          if (6 === this.version && n2.oid && (n2.oid.getName() === R.curve.curve25519Legacy || n2.oid.getName() === R.curve.ed25519Legacy)) throw Error("Legacy curve25519 cannot be used with v6 keys");
          return this.publicParams = n2, r2 += t3, await this.computeFingerprintAndKeyID(), r2;
        }
        throw new Yt(`Version ${this.version} of the key packet is unsupported.`);
      }
      write() {
        const e2 = [];
        e2.push(new Uint8Array([this.version])), e2.push(M.writeDate(this.created)), e2.push(new Uint8Array([this.algorithm]));
        const t2 = bi(this.algorithm, this.publicParams);
        return this.version >= 5 && e2.push(M.writeNumber(t2.length, 4)), e2.push(t2), M.concatUint8Array(e2);
      }
      writeForHash(e2) {
        const t2 = this.writePublicKey(), r2 = 149 + e2, n2 = e2 >= 5 ? 4 : 2;
        return M.concatUint8Array([new Uint8Array([r2]), M.writeNumber(t2.length, n2), t2]);
      }
      isDecrypted() {
        return null;
      }
      getCreationTime() {
        return this.created;
      }
      getKeyID() {
        return this.keyID;
      }
      async computeFingerprintAndKeyID() {
        if (await this.computeFingerprint(), this.keyID = new ka(), this.version >= 5) this.keyID.read(this.fingerprint.subarray(0, 8));
        else {
          if (4 !== this.version) throw Error("Unsupported key version");
          this.keyID.read(this.fingerprint.subarray(12, 20));
        }
      }
      async computeFingerprint() {
        const e2 = this.writeForHash(this.version);
        if (this.version >= 5) this.fingerprint = await Qe(R.hash.sha256, e2);
        else {
          if (4 !== this.version) throw Error("Unsupported key version");
          this.fingerprint = await Qe(R.hash.sha1, e2);
        }
      }
      getFingerprintBytes() {
        return this.fingerprint;
      }
      getFingerprint() {
        return M.uint8ArrayToHex(this.getFingerprintBytes());
      }
      hasSameFingerprintAs(e2) {
        return this.version === e2.version && M.equalsUint8Array(this.writePublicKey(), e2.writePublicKey());
      }
      getAlgorithmInfo() {
        const e2 = {};
        e2.algorithm = R.read(R.publicKey, this.algorithm);
        const t2 = this.publicParams.n || this.publicParams.p;
        return t2 ? e2.bits = M.uint8ArrayBitLength(t2) : this.publicParams.oid && (e2.curve = this.publicParams.oid.getName()), e2;
      }
    };
    qa.prototype.readPublicKey = qa.prototype.read, qa.prototype.writePublicKey = qa.prototype.write;
    Va = /* @__PURE__ */ M.constructAllowedPackets([ba, xa, Ca, Ba]);
    Ja = class {
      static get tag() {
        return R.packet.symmetricallyEncryptedData;
      }
      constructor() {
        this.encrypted = null, this.packets = null;
      }
      read(e2) {
        this.encrypted = e2;
      }
      write() {
        return this.encrypted;
      }
      async decrypt(e2, t2, r2 = T) {
        if (!r2.allowUnauthenticatedMessages) throw Error("Message is not authenticated.");
        const { blockSize: n2 } = fn(e2), i2 = await C(I(this.encrypted)), s2 = await xi(e2, t2, i2.subarray(n2 + 2), i2.subarray(2, n2 + 2));
        this.packets = await Ua.fromBinary(s2, Va, r2);
      }
      async encrypt(e2, t2, r2 = T) {
        const n2 = this.packets.write(), { blockSize: i2 } = fn(e2), s2 = await Ui(e2), a2 = await Pi(e2, t2, s2, new Uint8Array(i2)), o2 = await Pi(e2, t2, n2, a2.subarray(2));
        this.encrypted = M.concat([a2, o2]);
      }
    };
    Ya = class {
      static get tag() {
        return R.packet.marker;
      }
      read(e2) {
        return 80 === e2[0] && 71 === e2[1] && 80 === e2[2];
      }
      write() {
        return new Uint8Array([80, 71, 80]);
      }
    };
    Za = class _Za extends qa {
      static get tag() {
        return R.packet.publicSubkey;
      }
      constructor(e2, t2) {
        super(e2, t2);
      }
      static fromSecretSubkeyPacket(e2) {
        const t2 = new _Za(), { version: r2, created: n2, algorithm: i2, publicParams: s2, keyID: a2, fingerprint: o2 } = e2;
        return t2.version = r2, t2.created = n2, t2.algorithm = i2, t2.publicParams = s2, t2.keyID = a2, t2.fingerprint = o2, t2;
      }
    };
    Wa = class _Wa {
      static get tag() {
        return R.packet.userAttribute;
      }
      constructor() {
        this.attributes = [];
      }
      read(e2) {
        let t2 = 0;
        for (; t2 < e2.length; ) {
          const r2 = zt(e2.subarray(t2, e2.length));
          t2 += r2.offset, this.attributes.push(M.uint8ArrayToString(e2.subarray(t2, t2 + r2.len))), t2 += r2.len;
        }
      }
      write() {
        const e2 = [];
        for (let t2 = 0; t2 < this.attributes.length; t2++) e2.push(Gt(this.attributes[t2].length)), e2.push(M.stringToUint8Array(this.attributes[t2]));
        return M.concatUint8Array(e2);
      }
      equals(e2) {
        return !!(e2 && e2 instanceof _Wa) && this.attributes.every(function(t2, r2) {
          return t2 === e2.attributes[r2];
        });
      }
    };
    $a = class extends qa {
      static get tag() {
        return R.packet.secretKey;
      }
      constructor(e2 = /* @__PURE__ */ new Date(), t2 = T) {
        super(e2, t2), this.keyMaterial = null, this.isEncrypted = null, this.s2kUsage = 0, this.s2k = null, this.symmetric = null, this.aead = null, this.isLegacyAEAD = null, this.privateParams = null, this.usedModernAEAD = null;
      }
      async read(e2, t2 = T) {
        let r2 = await this.readPublicKey(e2, t2);
        const n2 = r2;
        this.s2kUsage = e2[r2++], 5 === this.version && r2++, 6 === this.version && this.s2kUsage && r2++;
        try {
          if (255 === this.s2kUsage || 254 === this.s2kUsage || 253 === this.s2kUsage) {
            this.symmetric = e2[r2++], 253 === this.s2kUsage && (this.aead = e2[r2++]), 6 === this.version && r2++;
            const t3 = e2[r2++];
            if (this.s2k = Ss(t3), r2 += this.s2k.read(e2.subarray(r2, e2.length)), "gnu-dummy" === this.s2k.type) return;
          } else this.s2kUsage && (this.symmetric = this.s2kUsage);
          this.s2kUsage && (this.isLegacyAEAD = 253 === this.s2kUsage && (5 === this.version || 4 === this.version && t2.parseAEADEncryptedV4KeysAsLegacy), 253 !== this.s2kUsage || this.isLegacyAEAD ? (this.iv = e2.subarray(r2, r2 + fn(this.symmetric).blockSize), this.usedModernAEAD = false) : (this.iv = e2.subarray(r2, r2 + As(this.aead).ivLength), this.usedModernAEAD = true), r2 += this.iv.length);
        } catch (t3) {
          if (!this.s2kUsage) throw t3;
          this.unparseableKeyMaterial = e2.subarray(n2), this.isEncrypted = true;
        }
        if (5 === this.version && (r2 += 4), this.keyMaterial = e2.subarray(r2), this.isEncrypted = !!this.s2kUsage, !this.isEncrypted) {
          let e3;
          if (6 === this.version) e3 = this.keyMaterial;
          else if (e3 = this.keyMaterial.subarray(0, -2), !M.equalsUint8Array(M.writeChecksum(e3), this.keyMaterial.subarray(-2))) throw Error("Key checksum mismatch");
          try {
            const { read: t3, privateParams: r3 } = mi(this.algorithm, e3, this.publicParams);
            if (t3 < e3.length) throw Error("Error reading MPIs");
            this.privateParams = r3;
          } catch (e4) {
            if (e4 instanceof Yt) throw e4;
            throw Error("Error reading MPIs");
          }
        }
      }
      write() {
        const e2 = this.writePublicKey();
        if (this.unparseableKeyMaterial) return M.concatUint8Array([e2, this.unparseableKeyMaterial]);
        const t2 = [e2];
        t2.push(new Uint8Array([this.s2kUsage]));
        const r2 = [];
        if (255 === this.s2kUsage || 254 === this.s2kUsage || 253 === this.s2kUsage) {
          r2.push(this.symmetric), 253 === this.s2kUsage && r2.push(this.aead);
          const e3 = this.s2k.write();
          6 === this.version && r2.push(e3.length), r2.push(...e3);
        }
        return this.s2kUsage && "gnu-dummy" !== this.s2k.type && r2.push(...this.iv), (5 === this.version || 6 === this.version && this.s2kUsage) && t2.push(new Uint8Array([r2.length])), t2.push(new Uint8Array(r2)), this.isDummy() || (this.s2kUsage || (this.keyMaterial = bi(this.algorithm, this.privateParams)), 5 === this.version && t2.push(M.writeNumber(this.keyMaterial.length, 4)), t2.push(this.keyMaterial), this.s2kUsage || 6 === this.version || t2.push(M.writeChecksum(this.keyMaterial))), M.concatUint8Array(t2);
      }
      isDecrypted() {
        return false === this.isEncrypted;
      }
      isMissingSecretKeyMaterial() {
        return void 0 !== this.unparseableKeyMaterial || this.isDummy();
      }
      isDummy() {
        return !(!this.s2k || "gnu-dummy" !== this.s2k.type);
      }
      makeDummy(e2 = T) {
        this.isDummy() || (this.isDecrypted() && this.clearPrivateParams(), delete this.unparseableKeyMaterial, this.isEncrypted = null, this.keyMaterial = null, this.s2k = Ss(R.s2k.gnu, e2), this.s2k.algorithm = 0, this.s2k.c = 0, this.s2k.type = "gnu-dummy", this.s2kUsage = 254, this.symmetric = R.symmetric.aes256, this.isLegacyAEAD = null, this.usedModernAEAD = null);
      }
      async encrypt(e2, t2 = T) {
        if (this.isDummy()) return;
        if (!this.isDecrypted()) throw Error("Key packet is already encrypted");
        if (!e2) throw Error("A non-empty passphrase is required for key encryption.");
        this.s2k = Ks(t2), this.s2k.generateSalt();
        const r2 = bi(this.algorithm, this.privateParams);
        this.symmetric = R.symmetric.aes256;
        const { blockSize: n2 } = fn(this.symmetric);
        if (t2.aeadProtect) {
          this.s2kUsage = 253, this.aead = t2.preferredAEADAlgorithm;
          const i2 = As(this.aead);
          this.isLegacyAEAD = 5 === this.version, this.usedModernAEAD = !this.isLegacyAEAD;
          const s2 = jt(this.constructor.tag), a2 = await Xa(this.version, this.s2k, e2, this.symmetric, this.aead, s2, this.isLegacyAEAD), o2 = await i2(this.symmetric, a2);
          this.iv = this.isLegacyAEAD ? fe(n2) : fe(i2.ivLength);
          const c2 = this.isLegacyAEAD ? new Uint8Array() : M.concatUint8Array([s2, this.writePublicKey()]);
          this.keyMaterial = await o2.encrypt(r2, this.iv.subarray(0, i2.ivLength), c2);
        } else {
          this.s2kUsage = 254, this.usedModernAEAD = false;
          const t3 = await Xa(this.version, this.s2k, e2, this.symmetric);
          this.iv = fe(n2), this.keyMaterial = await Pi(this.symmetric, t3, M.concatUint8Array([r2, await Qe(R.hash.sha1, r2)]), this.iv);
        }
      }
      async decrypt(e2) {
        if (this.isDummy()) return false;
        if (this.unparseableKeyMaterial) throw Error("Key packet cannot be decrypted: unsupported S2K or cipher algo");
        if (this.isDecrypted()) throw Error("Key packet is already decrypted.");
        let t2;
        const r2 = jt(this.constructor.tag);
        if (254 !== this.s2kUsage && 253 !== this.s2kUsage) throw 255 === this.s2kUsage ? Error("Encrypted private key is authenticated using an insecure two-byte hash") : Error("Private key is encrypted using an insecure S2K function: unsalted MD5");
        let n2;
        if (t2 = await Xa(this.version, this.s2k, e2, this.symmetric, this.aead, r2, this.isLegacyAEAD), 253 === this.s2kUsage) {
          const e3 = As(this.aead, true), i2 = await e3(this.symmetric, t2);
          try {
            const t3 = this.isLegacyAEAD ? new Uint8Array() : M.concatUint8Array([r2, this.writePublicKey()]);
            n2 = await i2.decrypt(this.keyMaterial, this.iv.subarray(0, e3.ivLength), t3);
          } catch (e4) {
            if ("Authentication tag mismatch" === e4.message) throw Error("Incorrect key passphrase: " + e4.message);
            throw e4;
          }
        } else {
          const e3 = await xi(this.symmetric, t2, this.keyMaterial, this.iv);
          n2 = e3.subarray(0, -20);
          const r3 = await Qe(R.hash.sha1, n2);
          if (!M.equalsUint8Array(r3, e3.subarray(-20))) throw Error("Incorrect key passphrase");
        }
        try {
          const { privateParams: e3 } = mi(this.algorithm, n2, this.publicParams);
          this.privateParams = e3;
        } catch (e3) {
          throw Error("Error reading MPIs");
        }
        this.isEncrypted = false, this.keyMaterial = null, this.s2kUsage = 0, this.aead = null, this.symmetric = null, this.isLegacyAEAD = null;
      }
      async validate() {
        if (this.isDummy()) return;
        if (!this.isDecrypted()) throw Error("Key is not decrypted");
        if (this.usedModernAEAD) return;
        let e2;
        try {
          e2 = await Ei(this.algorithm, this.publicParams, this.privateParams);
        } catch (t2) {
          e2 = false;
        }
        if (!e2) throw Error("Key is invalid");
      }
      async generate(e2, t2) {
        if (6 === this.version && (this.algorithm === R.publicKey.ecdh && t2 === R.curve.curve25519Legacy || this.algorithm === R.publicKey.eddsaLegacy)) throw Error(`Cannot generate v6 keys of type 'ecc' with curve ${t2}. Generate a key of type 'curve25519' instead`);
        const { privateParams: r2, publicParams: n2 } = await ki(this.algorithm, e2, t2);
        this.privateParams = r2, this.publicParams = n2, this.isEncrypted = false;
      }
      clearPrivateParams() {
        this.isMissingSecretKeyMaterial() || (Object.keys(this.privateParams).forEach((e2) => {
          this.privateParams[e2].fill(0), delete this.privateParams[e2];
        }), this.privateParams = null, this.isEncrypted = true);
      }
    };
    eo = class _eo {
      static get tag() {
        return R.packet.userID;
      }
      constructor() {
        this.userID = "", this.name = "", this.email = "", this.comment = "";
      }
      static fromObject(e2) {
        if (M.isString(e2) || e2.name && !M.isString(e2.name) || e2.email && !M.isEmailAddress(e2.email) || e2.comment && !M.isString(e2.comment)) throw Error("Invalid user ID format");
        const t2 = new _eo();
        Object.assign(t2, e2);
        const r2 = [];
        return t2.name && r2.push(t2.name), t2.comment && r2.push(`(${t2.comment})`), t2.email && r2.push(`<${t2.email}>`), t2.userID = r2.join(" "), t2;
      }
      read(e2, t2 = T) {
        const r2 = M.decodeUTF8(e2);
        if (r2.length > t2.maxUserIDLength) throw Error("User ID string is too long");
        const n2 = /^(?<name>[^()]+\s+)?(?<comment>\([^()]+\)\s+)?(?<email><\S+@\S+>)$/.exec(r2);
        if (null !== n2) {
          const { name: e3, comment: t3, email: r3 } = n2.groups;
          this.comment = t3?.replace(/^\(|\)|\s$/g, "").trim() || "", this.name = e3?.trim() || "", this.email = r3.substring(1, r3.length - 1);
        } else /^[^\s@]+@[^\s@]+$/.test(r2) && (this.email = r2);
        this.userID = r2;
      }
      write() {
        return M.encodeUTF8(this.userID);
      }
      equals(e2) {
        return e2 && e2.userID === this.userID;
      }
    };
    to = class extends $a {
      static get tag() {
        return R.packet.secretSubkey;
      }
      constructor(e2 = /* @__PURE__ */ new Date(), t2 = T) {
        super(e2, t2);
      }
    };
    ro = class {
      static get tag() {
        return R.packet.trust;
      }
      read() {
        throw new Yt("Trust packets are not supported");
      }
      write() {
        throw new Yt("Trust packets are not supported");
      }
    };
    no = class {
      static get tag() {
        return R.packet.padding;
      }
      constructor() {
        this.padding = null;
      }
      read(e2) {
      }
      write() {
        return this.padding;
      }
      async createPadding(e2) {
        this.padding = await fe(e2);
      }
    };
    io = /* @__PURE__ */ M.constructAllowedPackets([Ba]);
    so = class {
      constructor(e2) {
        this.packets = e2 || new Ua();
      }
      write() {
        return this.packets.write();
      }
      armor(e2 = T) {
        const t2 = this.packets.some((e3) => e3.constructor.tag === Ba.tag && 6 !== e3.version);
        return X(R.armor.signature, this.write(), void 0, void 0, void 0, t2, e2);
      }
      getSigningKeyIDs() {
        return this.packets.map((e2) => e2.issuerKeyID);
      }
    };
    vo = class _vo {
      constructor(e2, t2) {
        this.userID = e2.constructor.tag === R.packet.userID ? e2 : null, this.userAttribute = e2.constructor.tag === R.packet.userAttribute ? e2 : null, this.selfCertifications = [], this.otherCertifications = [], this.revocationSignatures = [], this.mainKey = t2;
      }
      toPacketList() {
        const e2 = new Ua();
        return e2.push(this.userID || this.userAttribute), e2.push(...this.revocationSignatures), e2.push(...this.selfCertifications), e2.push(...this.otherCertifications), e2;
      }
      clone() {
        const e2 = new _vo(this.userID || this.userAttribute, this.mainKey);
        return e2.selfCertifications = [...this.selfCertifications], e2.otherCertifications = [...this.otherCertifications], e2.revocationSignatures = [...this.revocationSignatures], e2;
      }
      async certify(e2, t2, r2) {
        const n2 = this.mainKey.keyPacket, i2 = { userID: this.userID, userAttribute: this.userAttribute, key: n2 }, s2 = new _vo(i2.userID || i2.userAttribute, this.mainKey);
        return s2.otherCertifications = await Promise.all(e2.map(async function(e3) {
          if (!e3.isPrivate()) throw Error("Need private key for signing");
          if (e3.hasSameFingerprintAs(n2)) throw Error("The user's own key can only be used for self-certifications");
          const s3 = await e3.getSigningKey(void 0, t2, void 0, r2);
          return fo(i2, [e3], s3.keyPacket, { signatureType: R.signature.certGeneric, keyFlags: [R.keyFlags.certifyKeys | R.keyFlags.signData] }, t2, void 0, void 0, void 0, r2);
        })), await s2.update(this, t2, r2), s2;
      }
      async isRevoked(e2, t2, r2 = /* @__PURE__ */ new Date(), n2 = T) {
        const i2 = this.mainKey.keyPacket;
        return po(i2, R.signature.certRevocation, { key: i2, userID: this.userID, userAttribute: this.userAttribute }, this.revocationSignatures, e2, t2, r2, n2);
      }
      async verifyCertificate(e2, t2, r2 = /* @__PURE__ */ new Date(), n2) {
        const i2 = this, s2 = this.mainKey.keyPacket, a2 = { userID: this.userID, userAttribute: this.userAttribute, key: s2 }, { issuerKeyID: o2 } = e2, c2 = t2.filter((e3) => e3.getKeys(o2).length > 0);
        return 0 === c2.length ? null : (await Promise.all(c2.map(async (t3) => {
          const s3 = await t3.getSigningKey(o2, e2.created, void 0, n2);
          if (e2.revoked || await i2.isRevoked(e2, s3.keyPacket, r2, n2)) throw Error("User certificate is revoked");
          try {
            await e2.verify(s3.keyPacket, R.signature.certGeneric, a2, r2, void 0, n2);
          } catch (e3) {
            throw M.wrapError("User certificate is invalid", e3);
          }
        })), true);
      }
      async verifyAllCertifications(e2, t2 = /* @__PURE__ */ new Date(), r2) {
        const n2 = this, i2 = this.selfCertifications.concat(this.otherCertifications);
        return Promise.all(i2.map(async (i3) => ({ keyID: i3.issuerKeyID, valid: await n2.verifyCertificate(i3, e2, t2, r2).catch(() => false) })));
      }
      async verify(e2 = /* @__PURE__ */ new Date(), t2) {
        if (!this.selfCertifications.length) throw Error("No self-certifications found");
        const r2 = this, n2 = this.mainKey.keyPacket, i2 = { userID: this.userID, userAttribute: this.userAttribute, key: n2 };
        let s2;
        for (let a2 = this.selfCertifications.length - 1; a2 >= 0; a2--) try {
          const s3 = this.selfCertifications[a2];
          if (s3.revoked || await r2.isRevoked(s3, void 0, e2, t2)) throw Error("Self-certification is revoked");
          try {
            await s3.verify(n2, R.signature.certGeneric, i2, e2, void 0, t2);
          } catch (e3) {
            throw M.wrapError("Self-certification is invalid", e3);
          }
          return true;
        } catch (e3) {
          s2 = e3;
        }
        throw s2;
      }
      async update(e2, t2, r2) {
        const n2 = this.mainKey.keyPacket, i2 = { userID: this.userID, userAttribute: this.userAttribute, key: n2 };
        await go(e2, this, "selfCertifications", t2, async function(e3) {
          try {
            return await e3.verify(n2, R.signature.certGeneric, i2, t2, false, r2), true;
          } catch (e4) {
            return false;
          }
        }), await go(e2, this, "otherCertifications", t2), await go(e2, this, "revocationSignatures", t2, function(e3) {
          return po(n2, R.signature.certRevocation, i2, [e3], void 0, void 0, t2, r2);
        });
      }
      async revoke(e2, { flag: t2 = R.reasonForRevocation.noReason, string: r2 = "" } = {}, n2 = /* @__PURE__ */ new Date(), i2 = T) {
        const s2 = { userID: this.userID, userAttribute: this.userAttribute, key: e2 }, a2 = new _vo(s2.userID || s2.userAttribute, this.mainKey);
        return a2.revocationSignatures.push(await fo(s2, [], e2, { signatureType: R.signature.certRevocation, reasonForRevocationFlag: R.write(R.reasonForRevocation, t2), reasonForRevocationString: r2 }, n2, void 0, void 0, false, i2)), await a2.update(this), a2;
      }
    };
    Io = class _Io {
      constructor(e2, t2) {
        this.keyPacket = e2, this.bindingSignatures = [], this.revocationSignatures = [], this.mainKey = t2;
      }
      toPacketList() {
        const e2 = new Ua();
        return e2.push(this.keyPacket), e2.push(...this.revocationSignatures), e2.push(...this.bindingSignatures), e2;
      }
      clone() {
        const e2 = new _Io(this.keyPacket, this.mainKey);
        return e2.bindingSignatures = [...this.bindingSignatures], e2.revocationSignatures = [...this.revocationSignatures], e2;
      }
      async isRevoked(e2, t2, r2 = /* @__PURE__ */ new Date(), n2 = T) {
        const i2 = this.mainKey.keyPacket;
        return po(i2, R.signature.subkeyRevocation, { key: i2, bind: this.keyPacket }, this.revocationSignatures, e2, t2, r2, n2);
      }
      async verify(e2 = /* @__PURE__ */ new Date(), t2 = T) {
        const r2 = this.mainKey.keyPacket, n2 = { key: r2, bind: this.keyPacket }, i2 = await ho(this.bindingSignatures, r2, R.signature.subkeyBinding, n2, e2, t2);
        if (i2.revoked || await this.isRevoked(i2, null, e2, t2)) throw Error("Subkey is revoked");
        if (uo(this.keyPacket, i2, e2)) throw Error("Subkey is expired");
        return i2;
      }
      async getExpirationTime(e2 = /* @__PURE__ */ new Date(), t2 = T) {
        const r2 = this.mainKey.keyPacket, n2 = { key: r2, bind: this.keyPacket };
        let i2;
        try {
          i2 = await ho(this.bindingSignatures, r2, R.signature.subkeyBinding, n2, e2, t2);
        } catch (e3) {
          return null;
        }
        const s2 = Ao(this.keyPacket, i2), a2 = i2.getExpirationTime();
        return s2 < a2 ? s2 : a2;
      }
      async update(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        const n2 = this.mainKey.keyPacket;
        if (!this.hasSameFingerprintAs(e2)) throw Error("Subkey update method: fingerprints of subkeys not equal");
        this.keyPacket.constructor.tag === R.packet.publicSubkey && e2.keyPacket.constructor.tag === R.packet.secretSubkey && (this.keyPacket = e2.keyPacket);
        const i2 = this, s2 = { key: n2, bind: i2.keyPacket };
        await go(e2, this, "bindingSignatures", t2, async function(e3) {
          for (let t3 = 0; t3 < i2.bindingSignatures.length; t3++) if (i2.bindingSignatures[t3].issuerKeyID.equals(e3.issuerKeyID)) return e3.created > i2.bindingSignatures[t3].created && (i2.bindingSignatures[t3] = e3), false;
          try {
            return await e3.verify(n2, R.signature.subkeyBinding, s2, t2, void 0, r2), true;
          } catch (e4) {
            return false;
          }
        }), await go(e2, this, "revocationSignatures", t2, function(e3) {
          return po(n2, R.signature.subkeyRevocation, s2, [e3], void 0, void 0, t2, r2);
        });
      }
      async revoke(e2, { flag: t2 = R.reasonForRevocation.noReason, string: r2 = "" } = {}, n2 = /* @__PURE__ */ new Date(), i2 = T) {
        const s2 = { key: e2, bind: this.keyPacket }, a2 = new _Io(this.keyPacket, this.mainKey);
        return a2.revocationSignatures.push(await fo(s2, [], e2, { signatureType: R.signature.subkeyRevocation, reasonForRevocationFlag: R.write(R.reasonForRevocation, t2), reasonForRevocationString: r2 }, n2, void 0, void 0, false, i2)), await a2.update(this), a2;
      }
      hasSameFingerprintAs(e2) {
        return this.keyPacket.hasSameFingerprintAs(e2.keyPacket || e2);
      }
    };
    ["getKeyID", "getFingerprint", "getAlgorithmInfo", "getCreationTime", "isDecrypted"].forEach((e2) => {
      Io.prototype[e2] = function() {
        return this.keyPacket[e2]();
      };
    });
    Bo = /* @__PURE__ */ M.constructAllowedPackets([Ba]);
    So = /* @__PURE__ */ new Set([R.packet.publicKey, R.packet.privateKey]);
    Ko = /* @__PURE__ */ new Set([R.packet.publicKey, R.packet.privateKey, R.packet.publicSubkey, R.packet.privateSubkey]);
    Co = class {
      packetListToStructure(e2, t2 = /* @__PURE__ */ new Set()) {
        let r2, n2, i2, s2;
        for (const a2 of e2) {
          if (a2 instanceof Wt) {
            Ko.has(a2.tag) && !s2 && (s2 = So.has(a2.tag) ? So : Ko);
            continue;
          }
          const e3 = a2.constructor.tag;
          if (s2) {
            if (!s2.has(e3)) continue;
            s2 = null;
          }
          if (t2.has(e3)) throw Error("Unexpected packet type: " + e3);
          switch (e3) {
            case R.packet.publicKey:
            case R.packet.secretKey:
              if (this.keyPacket) throw Error("Key block contains multiple keys");
              if (this.keyPacket = a2, n2 = this.getKeyID(), !n2) throw Error("Missing Key ID");
              break;
            case R.packet.userID:
            case R.packet.userAttribute:
              r2 = new vo(a2, this), this.users.push(r2);
              break;
            case R.packet.publicSubkey:
            case R.packet.secretSubkey:
              r2 = null, i2 = new Io(a2, this), this.subkeys.push(i2);
              break;
            case R.packet.signature:
              switch (a2.signatureType) {
                case R.signature.certGeneric:
                case R.signature.certPersona:
                case R.signature.certCasual:
                case R.signature.certPositive:
                  if (!r2) {
                    M.printDebug("Dropping certification signatures without preceding user packet");
                    continue;
                  }
                  a2.issuerKeyID.equals(n2) ? r2.selfCertifications.push(a2) : r2.otherCertifications.push(a2);
                  break;
                case R.signature.certRevocation:
                  r2 ? r2.revocationSignatures.push(a2) : this.directSignatures.push(a2);
                  break;
                case R.signature.key:
                  this.directSignatures.push(a2);
                  break;
                case R.signature.subkeyBinding:
                  if (!i2) {
                    M.printDebug("Dropping subkey binding signature without preceding subkey packet");
                    continue;
                  }
                  i2.bindingSignatures.push(a2);
                  break;
                case R.signature.keyRevocation:
                  this.revocationSignatures.push(a2);
                  break;
                case R.signature.subkeyRevocation:
                  if (!i2) {
                    M.printDebug("Dropping subkey revocation signature without preceding subkey packet");
                    continue;
                  }
                  i2.revocationSignatures.push(a2);
              }
          }
        }
      }
      toPacketList() {
        const e2 = new Ua();
        return e2.push(this.keyPacket), e2.push(...this.revocationSignatures), e2.push(...this.directSignatures), this.users.map((t2) => e2.push(...t2.toPacketList())), this.subkeys.map((t2) => e2.push(...t2.toPacketList())), e2;
      }
      clone(e2 = false) {
        const t2 = new this.constructor(this.toPacketList());
        return e2 && t2.getKeys().forEach((e3) => {
          if (e3.keyPacket = Object.create(Object.getPrototypeOf(e3.keyPacket), Object.getOwnPropertyDescriptors(e3.keyPacket)), !e3.keyPacket.isDecrypted()) return;
          const t3 = {};
          Object.keys(e3.keyPacket.privateParams).forEach((r2) => {
            t3[r2] = new Uint8Array(e3.keyPacket.privateParams[r2]);
          }), e3.keyPacket.privateParams = t3;
        }), t2;
      }
      getSubkeys(e2 = null) {
        return this.subkeys.filter((t2) => !e2 || t2.getKeyID().equals(e2, true));
      }
      getKeys(e2 = null) {
        const t2 = [];
        return e2 && !this.getKeyID().equals(e2, true) || t2.push(this), t2.concat(this.getSubkeys(e2));
      }
      getKeyIDs() {
        return this.getKeys().map((e2) => e2.getKeyID());
      }
      getUserIDs() {
        return this.users.map((e2) => e2.userID ? e2.userID.userID : null).filter((e2) => null !== e2);
      }
      write() {
        return this.toPacketList().write();
      }
      async getSigningKey(e2 = null, t2 = /* @__PURE__ */ new Date(), r2 = {}, n2 = T) {
        await this.verifyPrimaryKey(t2, r2, n2);
        const i2 = this.keyPacket;
        try {
          Eo(i2, n2);
        } catch (e3) {
          throw M.wrapError("Could not verify primary key", e3);
        }
        const s2 = this.subkeys.slice().sort((e3, t3) => t3.keyPacket.created - e3.keyPacket.created);
        let a2;
        for (const r3 of s2) if (!e2 || r3.getKeyID().equals(e2)) try {
          await r3.verify(t2, n2);
          const e3 = { key: i2, bind: r3.keyPacket }, s3 = await ho(r3.bindingSignatures, i2, R.signature.subkeyBinding, e3, t2, n2);
          if (!mo(r3.keyPacket, s3, n2)) continue;
          if (!s3.embeddedSignature) throw Error("Missing embedded signature");
          return await ho([s3.embeddedSignature], r3.keyPacket, R.signature.keyBinding, e3, t2, n2), Eo(r3.keyPacket, n2), r3;
        } catch (e3) {
          a2 = e3;
        }
        try {
          const s3 = await this.getPrimarySelfSignature(t2, r2, n2);
          if ((!e2 || i2.getKeyID().equals(e2)) && mo(i2, s3, n2)) return Eo(i2, n2), this;
        } catch (e3) {
          a2 = e3;
        }
        throw M.wrapError("Could not find valid signing key packet in key " + this.getKeyID().toHex(), a2);
      }
      async getEncryptionKey(e2, t2 = /* @__PURE__ */ new Date(), r2 = {}, n2 = T) {
        await this.verifyPrimaryKey(t2, r2, n2);
        const i2 = this.keyPacket;
        try {
          Eo(i2, n2);
        } catch (e3) {
          throw M.wrapError("Could not verify primary key", e3);
        }
        const s2 = this.subkeys.slice().sort((e3, t3) => t3.keyPacket.created - e3.keyPacket.created);
        let a2;
        for (const r3 of s2) if (!e2 || r3.getKeyID().equals(e2)) try {
          await r3.verify(t2, n2);
          const e3 = { key: i2, bind: r3.keyPacket }, s3 = await ho(r3.bindingSignatures, i2, R.signature.subkeyBinding, e3, t2, n2);
          if (bo(r3.keyPacket, s3, n2)) return Eo(r3.keyPacket, n2), r3;
        } catch (e3) {
          a2 = e3;
        }
        try {
          const s3 = await this.getPrimarySelfSignature(t2, r2, n2);
          if ((!e2 || i2.getKeyID().equals(e2)) && bo(i2, s3, n2)) return Eo(i2, n2), this;
        } catch (e3) {
          a2 = e3;
        }
        throw M.wrapError("Could not find valid encryption key packet in key " + this.getKeyID().toHex(), a2);
      }
      async isRevoked(e2, t2, r2 = /* @__PURE__ */ new Date(), n2 = T) {
        return po(this.keyPacket, R.signature.keyRevocation, { key: this.keyPacket }, this.revocationSignatures, e2, t2, r2, n2);
      }
      async verifyPrimaryKey(e2 = /* @__PURE__ */ new Date(), t2 = {}, r2 = T) {
        const n2 = this.keyPacket;
        if (await this.isRevoked(null, null, e2, r2)) throw Error("Primary key is revoked");
        if (uo(n2, await this.getPrimarySelfSignature(e2, t2, r2), e2)) throw Error("Primary key is expired");
        if (6 !== n2.version) {
          const t3 = await ho(this.directSignatures, n2, R.signature.key, { key: n2 }, e2, r2).catch(() => {
          });
          if (t3 && uo(n2, t3, e2)) throw Error("Primary key is expired");
        }
      }
      async getExpirationTime(e2, t2 = T) {
        let r2;
        try {
          const n2 = await this.getPrimarySelfSignature(null, e2, t2), i2 = Ao(this.keyPacket, n2), s2 = n2.getExpirationTime(), a2 = 6 !== this.keyPacket.version && await ho(this.directSignatures, this.keyPacket, R.signature.key, { key: this.keyPacket }, null, t2).catch(() => {
          });
          if (a2) {
            const e3 = Ao(this.keyPacket, a2);
            r2 = Math.min(i2, s2, e3);
          } else r2 = i2 < s2 ? i2 : s2;
        } catch (e3) {
          r2 = null;
        }
        return M.normalizeDate(r2);
      }
      async getPrimarySelfSignature(e2 = /* @__PURE__ */ new Date(), t2 = {}, r2 = T) {
        const n2 = this.keyPacket;
        if (6 === n2.version) return ho(this.directSignatures, n2, R.signature.key, { key: n2 }, e2, r2);
        const { selfCertification: i2 } = await this.getPrimaryUser(e2, t2, r2);
        return i2;
      }
      async getPrimaryUser(e2 = /* @__PURE__ */ new Date(), t2 = {}, r2 = T) {
        const n2 = this.keyPacket, i2 = [];
        let s2;
        for (let a3 = 0; a3 < this.users.length; a3++) try {
          const s3 = this.users[a3];
          if (!s3.userID) continue;
          if (void 0 !== t2.name && s3.userID.name !== t2.name || void 0 !== t2.email && s3.userID.email !== t2.email || void 0 !== t2.comment && s3.userID.comment !== t2.comment) throw Error("Could not find user that matches that user ID");
          const o3 = { userID: s3.userID, key: n2 }, c3 = await ho(s3.selfCertifications, n2, R.signature.certGeneric, o3, e2, r2);
          i2.push({ index: a3, user: s3, selfCertification: c3 });
        } catch (e3) {
          s2 = e3;
        }
        if (!i2.length) throw s2 || Error("Could not find primary user");
        await Promise.all(i2.map(async function(t3) {
          return t3.selfCertification.revoked || t3.user.isRevoked(t3.selfCertification, null, e2, r2);
        }));
        const a2 = i2.sort(function(e3, t3) {
          const r3 = e3.selfCertification, n3 = t3.selfCertification;
          return n3.revoked - r3.revoked || r3.isPrimaryUserID - n3.isPrimaryUserID || r3.created - n3.created;
        }).pop(), { user: o2, selfCertification: c2 } = a2;
        if (c2.revoked || await o2.isRevoked(c2, null, e2, r2)) throw Error("Primary user is revoked");
        return a2;
      }
      async update(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        if (!this.hasSameFingerprintAs(e2)) throw Error("Primary key fingerprints must be equal to update the key");
        if (!this.isPrivate() && e2.isPrivate()) {
          if (!(this.subkeys.length === e2.subkeys.length && this.subkeys.every((t3) => e2.subkeys.some((e3) => t3.hasSameFingerprintAs(e3))))) throw Error("Cannot update public key with private key if subkeys mismatch");
          return e2.update(this, r2);
        }
        const n2 = this.clone();
        return await go(e2, n2, "revocationSignatures", t2, (i2) => po(n2.keyPacket, R.signature.keyRevocation, n2, [i2], null, e2.keyPacket, t2, r2)), await go(e2, n2, "directSignatures", t2), await Promise.all(e2.users.map(async (e3) => {
          const i2 = n2.users.filter((t3) => e3.userID && e3.userID.equals(t3.userID) || e3.userAttribute && e3.userAttribute.equals(t3.userAttribute));
          if (i2.length > 0) await Promise.all(i2.map((n3) => n3.update(e3, t2, r2)));
          else {
            const t3 = e3.clone();
            t3.mainKey = n2, n2.users.push(t3);
          }
        })), await Promise.all(e2.subkeys.map(async (e3) => {
          const i2 = n2.subkeys.filter((t3) => t3.hasSameFingerprintAs(e3));
          if (i2.length > 0) await Promise.all(i2.map((n3) => n3.update(e3, t2, r2)));
          else {
            const t3 = e3.clone();
            t3.mainKey = n2, n2.subkeys.push(t3);
          }
        })), n2;
      }
      async getRevocationCertificate(e2 = /* @__PURE__ */ new Date(), t2 = T) {
        const r2 = { key: this.keyPacket }, n2 = await ho(this.revocationSignatures, this.keyPacket, R.signature.keyRevocation, r2, e2, t2), i2 = new Ua();
        i2.push(n2);
        const s2 = 6 !== this.keyPacket.version;
        return X(R.armor.publicKey, i2.write(), null, null, "This is a revocation certificate", s2, t2);
      }
      async applyRevocationCertificate(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        const n2 = await $(e2), i2 = (await Ua.fromBinary(n2.data, Bo, r2)).findPacket(R.packet.signature);
        if (!i2 || i2.signatureType !== R.signature.keyRevocation) throw Error("Could not find revocation signature packet");
        if (!i2.issuerKeyID.equals(this.getKeyID())) throw Error("Revocation signature does not match key");
        try {
          await i2.verify(this.keyPacket, R.signature.keyRevocation, { key: this.keyPacket }, t2, void 0, r2);
        } catch (e3) {
          throw M.wrapError("Could not verify revocation signature", e3);
        }
        const s2 = this.clone();
        return s2.revocationSignatures.push(i2), s2;
      }
      async signPrimaryUser(e2, t2, r2, n2 = T) {
        const { index: i2, user: s2 } = await this.getPrimaryUser(t2, r2, n2), a2 = await s2.certify(e2, t2, n2), o2 = this.clone();
        return o2.users[i2] = a2, o2;
      }
      async signAllUsers(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        const n2 = this.clone();
        return n2.users = await Promise.all(this.users.map(function(n3) {
          return n3.certify(e2, t2, r2);
        })), n2;
      }
      async verifyPrimaryUser(e2, t2 = /* @__PURE__ */ new Date(), r2, n2 = T) {
        const i2 = this.keyPacket, { user: s2 } = await this.getPrimaryUser(t2, r2, n2);
        return e2 ? await s2.verifyAllCertifications(e2, t2, n2) : [{ keyID: i2.getKeyID(), valid: await s2.verify(t2, n2).catch(() => false) }];
      }
      async verifyAllUsers(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        const n2 = this.keyPacket, i2 = [];
        return await Promise.all(this.users.map(async (s2) => {
          const a2 = e2 ? await s2.verifyAllCertifications(e2, t2, r2) : [{ keyID: n2.getKeyID(), valid: await s2.verify(t2, r2).catch(() => false) }];
          i2.push(...a2.map((e3) => ({ userID: s2.userID ? s2.userID.userID : null, userAttribute: s2.userAttribute, keyID: e3.keyID, valid: e3.valid })));
        })), i2;
      }
    };
    ["getKeyID", "getFingerprint", "getAlgorithmInfo", "getCreationTime", "hasSameFingerprintAs"].forEach((e2) => {
      Co.prototype[e2] = Io.prototype[e2];
    });
    Do = class extends Co {
      constructor(e2) {
        if (super(), this.keyPacket = null, this.revocationSignatures = [], this.directSignatures = [], this.users = [], this.subkeys = [], e2 && (this.packetListToStructure(e2, /* @__PURE__ */ new Set([R.packet.secretKey, R.packet.secretSubkey])), !this.keyPacket)) throw Error("Invalid key: missing public-key packet");
      }
      isPrivate() {
        return false;
      }
      toPublic() {
        return this;
      }
      armor(e2 = T) {
        const t2 = 6 !== this.keyPacket.version;
        return X(R.armor.publicKey, this.toPacketList().write(), void 0, void 0, void 0, t2, e2);
      }
    };
    Uo = class _Uo extends Do {
      constructor(e2) {
        if (super(), this.packetListToStructure(e2, /* @__PURE__ */ new Set([R.packet.publicKey, R.packet.publicSubkey])), !this.keyPacket) throw Error("Invalid key: missing private-key packet");
      }
      isPrivate() {
        return true;
      }
      toPublic() {
        const e2 = new Ua(), t2 = this.toPacketList();
        for (const r2 of t2) switch (r2.constructor.tag) {
          case R.packet.secretKey: {
            const t3 = qa.fromSecretKeyPacket(r2);
            e2.push(t3);
            break;
          }
          case R.packet.secretSubkey: {
            const t3 = Za.fromSecretSubkeyPacket(r2);
            e2.push(t3);
            break;
          }
          default:
            e2.push(r2);
        }
        return new Do(e2);
      }
      armor(e2 = T) {
        const t2 = 6 !== this.keyPacket.version;
        return X(R.armor.privateKey, this.toPacketList().write(), void 0, void 0, void 0, t2, e2);
      }
      async getDecryptionKeys(e2, t2 = /* @__PURE__ */ new Date(), r2 = {}, n2 = T) {
        const i2 = this.keyPacket, s2 = [];
        let a2 = null;
        for (let r3 = 0; r3 < this.subkeys.length; r3++) if (!e2 || this.subkeys[r3].getKeyID().equals(e2, true)) {
          if (this.subkeys[r3].keyPacket.isDummy()) {
            a2 = a2 || Error("Gnu-dummy key packets cannot be used for decryption");
            continue;
          }
          try {
            const e3 = { key: i2, bind: this.subkeys[r3].keyPacket }, a3 = await ho(this.subkeys[r3].bindingSignatures, i2, R.signature.subkeyBinding, e3, t2, n2);
            ko(this.subkeys[r3].keyPacket, a3, n2) && s2.push(this.subkeys[r3]);
          } catch (e3) {
            a2 = e3;
          }
        }
        const o2 = await this.getPrimarySelfSignature(t2, r2, n2);
        if (e2 && !i2.getKeyID().equals(e2, true) || !ko(i2, o2, n2) || (i2.isDummy() ? a2 = a2 || Error("Gnu-dummy key packets cannot be used for decryption") : s2.push(this)), 0 === s2.length) throw a2 || Error("No decryption key packets found");
        return s2;
      }
      isDecrypted() {
        return this.getKeys().some(({ keyPacket: e2 }) => e2.isDecrypted());
      }
      async validate(e2 = T) {
        if (!this.isPrivate()) throw Error("Cannot validate a public key");
        let t2;
        if (this.keyPacket.isDummy()) {
          const r2 = await this.getSigningKey(null, null, void 0, { ...e2, rejectPublicKeyAlgorithms: /* @__PURE__ */ new Set(), minRSABits: 0 });
          r2 && !r2.keyPacket.isDummy() && (t2 = r2.keyPacket);
        } else t2 = this.keyPacket;
        if (t2) return t2.validate();
        {
          const e3 = this.getKeys();
          if (e3.map((e4) => e4.keyPacket.isDummy()).every(Boolean)) throw Error("Cannot validate an all-gnu-dummy key");
          return Promise.all(e3.map(async (e4) => e4.keyPacket.validate()));
        }
      }
      clearPrivateParams() {
        this.getKeys().forEach(({ keyPacket: e2 }) => {
          e2.isDecrypted() && e2.clearPrivateParams();
        });
      }
      async revoke({ flag: e2 = R.reasonForRevocation.noReason, string: t2 = "" } = {}, r2 = /* @__PURE__ */ new Date(), n2 = T) {
        if (!this.isPrivate()) throw Error("Need private key for revoking");
        const i2 = { key: this.keyPacket }, s2 = this.clone();
        return s2.revocationSignatures.push(await fo(i2, [], this.keyPacket, { signatureType: R.signature.keyRevocation, reasonForRevocationFlag: R.write(R.reasonForRevocation, e2), reasonForRevocationString: t2 }, r2, void 0, void 0, void 0, n2)), s2;
      }
      async addSubkey(e2 = {}) {
        const t2 = { ...T, ...e2.config };
        if (e2.passphrase) throw Error("Subkey could not be encrypted here, please encrypt whole key");
        if (e2.rsaBits < t2.minRSABits) throw Error(`rsaBits should be at least ${t2.minRSABits}, got: ${e2.rsaBits}`);
        const r2 = this.keyPacket;
        if (r2.isDummy()) throw Error("Cannot add subkey to gnu-dummy primary key");
        if (!r2.isDecrypted()) throw Error("Key is not decrypted");
        const n2 = r2.getAlgorithmInfo();
        n2.type = function(e3) {
          switch (R.write(R.publicKey, e3)) {
            case R.publicKey.rsaEncrypt:
            case R.publicKey.rsaEncryptSign:
            case R.publicKey.rsaSign:
            case R.publicKey.dsa:
              return "rsa";
            case R.publicKey.ecdsa:
            case R.publicKey.eddsaLegacy:
              return "ecc";
            case R.publicKey.ed25519:
              return "curve25519";
            case R.publicKey.ed448:
              return "curve448";
            default:
              throw Error("Unsupported algorithm");
          }
        }(n2.algorithm), n2.rsaBits = n2.bits || 4096, n2.curve = n2.curve || "curve25519Legacy", e2 = wo(e2, n2);
        const i2 = await oo(e2, { ...t2, v6Keys: 6 === this.keyPacket.version });
        Eo(i2, t2);
        const s2 = await lo(i2, r2, e2, t2), a2 = this.toPacketList();
        return a2.push(i2, s2), new _Uo(a2);
      }
    };
    Po = /* @__PURE__ */ M.constructAllowedPackets([qa, Za, $a, to, eo, Wa, Ba]);
    No = /* @__PURE__ */ M.constructAllowedPackets([ba, xa, za, Fa, Ja, Ga, ja, Ca, Ba]);
    Fo = /* @__PURE__ */ M.constructAllowedPackets([ja]);
    Oo = /* @__PURE__ */ M.constructAllowedPackets([Ba]);
    Ho = class _Ho {
      constructor(e2) {
        this.packets = e2 || new Ua();
      }
      getEncryptionKeyIDs() {
        const e2 = [];
        return this.packets.filterByTag(R.packet.publicKeyEncryptedSessionKey).forEach(function(t2) {
          e2.push(t2.publicKeyID);
        }), e2;
      }
      getSigningKeyIDs() {
        const e2 = this.unwrapCompressed(), t2 = e2.packets.filterByTag(R.packet.onePassSignature);
        if (t2.length > 0) return t2.map((e3) => e3.issuerKeyID);
        return e2.packets.filterByTag(R.packet.signature).map((e3) => e3.issuerKeyID);
      }
      async decrypt(e2, t2, r2, n2 = /* @__PURE__ */ new Date(), i2 = T) {
        const s2 = this.packets.filterByTag(R.packet.symmetricallyEncryptedData, R.packet.symEncryptedIntegrityProtectedData, R.packet.aeadEncryptedData);
        if (0 === s2.length) throw Error("No encrypted data found");
        const a2 = s2[0], o2 = a2.cipherAlgorithm, c2 = r2 || await this.decryptSessionKeys(e2, t2, o2, n2, i2);
        let h2 = null;
        const u2 = Promise.all(c2.map(async ({ algorithm: e3, data: t3 }) => {
          if (!M.isUint8Array(t3) || !a2.cipherAlgorithm && !M.isString(e3)) throw Error("Invalid session key for decryption.");
          try {
            const r3 = a2.cipherAlgorithm || R.write(R.symmetric, e3);
            await a2.decrypt(r3, t3, i2);
          } catch (e4) {
            M.printDebugError(e4), h2 = e4;
          }
        }));
        if (D(a2.encrypted), a2.encrypted = null, await u2, !a2.packets || !a2.packets.length) throw h2 || Error("Decryption failed.");
        const l2 = new _Ho(a2.packets);
        return a2.packets = new Ua(), l2;
      }
      async decryptSessionKeys(e2, t2, r2, n2 = /* @__PURE__ */ new Date(), i2 = T) {
        let s2, a2 = [];
        if (t2) {
          const e3 = this.packets.filterByTag(R.packet.symEncryptedSessionKey);
          if (0 === e3.length) throw Error("No symmetrically encrypted session key packet found.");
          await Promise.all(t2.map(async function(t3, r3) {
            let n3;
            n3 = r3 ? await Ua.fromBinary(e3.write(), Fo, i2) : e3, await Promise.all(n3.map(async function(e4) {
              try {
                await e4.decrypt(t3), a2.push(e4);
              } catch (e5) {
                M.printDebugError(e5), e5 instanceof bs && (s2 = e5);
              }
            }));
          }));
        } else {
          if (!e2) throw Error("No key or password specified.");
          {
            const t3 = this.packets.filterByTag(R.packet.publicKeyEncryptedSessionKey);
            if (0 === t3.length) throw Error("No public key encrypted session key packet found.");
            await Promise.all(t3.map(async function(t4) {
              await Promise.all(e2.map(async function(e3) {
                let o2;
                try {
                  o2 = (await e3.getDecryptionKeys(t4.publicKeyID, null, void 0, i2)).map((e4) => e4.keyPacket);
                } catch (e4) {
                  return void (s2 = e4);
                }
                let c2 = [R.symmetric.aes256, R.symmetric.aes128, R.symmetric.tripledes, R.symmetric.cast5];
                try {
                  const t5 = await e3.getPrimarySelfSignature(n2, void 0, i2);
                  t5.preferredSymmetricAlgorithms && (c2 = c2.concat(t5.preferredSymmetricAlgorithms));
                } catch (e4) {
                }
                await Promise.all(o2.map(async function(e4) {
                  if (!e4.isDecrypted()) throw Error("Decryption key is not decrypted.");
                  if (i2.constantTimePKCS1Decryption && (t4.publicKeyAlgorithm === R.publicKey.rsaEncrypt || t4.publicKeyAlgorithm === R.publicKey.rsaEncryptSign || t4.publicKeyAlgorithm === R.publicKey.rsaSign || t4.publicKeyAlgorithm === R.publicKey.elgamal)) {
                    const n3 = t4.write();
                    await Promise.all((r2 ? [r2] : Array.from(i2.constantTimePKCS1DecryptionSupportedSymmetricAlgorithms)).map(async (t5) => {
                      const r3 = new Ga();
                      r3.read(n3);
                      const i3 = { sessionKeyAlgorithm: t5, sessionKey: vi(t5) };
                      try {
                        await r3.decrypt(e4, i3), a2.push(r3);
                      } catch (e5) {
                        M.printDebugError(e5), s2 = e5;
                      }
                    }));
                  } else try {
                    await t4.decrypt(e4);
                    const n3 = r2 || t4.sessionKeyAlgorithm;
                    if (n3 && !c2.includes(R.write(R.symmetric, n3))) throw Error("A non-preferred symmetric algorithm was used.");
                    a2.push(t4);
                  } catch (e5) {
                    M.printDebugError(e5), s2 = e5;
                  }
                }));
              })), D(t4.encrypted), t4.encrypted = null;
            }));
          }
        }
        if (a2.length > 0) {
          if (a2.length > 1) {
            const e3 = /* @__PURE__ */ new Set();
            a2 = a2.filter((t3) => {
              const r3 = t3.sessionKeyAlgorithm + M.uint8ArrayToString(t3.sessionKey);
              return !e3.has(r3) && (e3.add(r3), true);
            });
          }
          return a2.map((e3) => ({ data: e3.sessionKey, algorithm: e3.sessionKeyAlgorithm && R.read(R.symmetric, e3.sessionKeyAlgorithm) }));
        }
        throw s2 || Error("Session key decryption failed.");
      }
      getLiteralData() {
        const e2 = this.unwrapCompressed().packets.findPacket(R.packet.literalData);
        return e2 && e2.getBytes() || null;
      }
      getFilename() {
        const e2 = this.unwrapCompressed().packets.findPacket(R.packet.literalData);
        return e2 && e2.getFilename() || null;
      }
      getText() {
        const e2 = this.unwrapCompressed().packets.findPacket(R.packet.literalData);
        return e2 ? e2.getText() : null;
      }
      static async generateSessionKey(e2 = [], t2 = /* @__PURE__ */ new Date(), r2 = [], n2 = T) {
        const { symmetricAlgo: i2, aeadAlgo: s2 } = await async function(e3 = [], t3 = /* @__PURE__ */ new Date(), r3 = [], n3 = T) {
          const i3 = await Promise.all(e3.map((e4, i4) => e4.getPrimarySelfSignature(t3, r3[i4], n3)));
          if (e3.length ? i3.every((e4) => e4.features && e4.features[0] & R.features.seipdv2) : n3.aeadProtect) {
            const e4 = { symmetricAlgo: R.symmetric.aes128, aeadAlgo: R.aead.ocb }, t4 = [{ symmetricAlgo: n3.preferredSymmetricAlgorithm, aeadAlgo: n3.preferredAEADAlgorithm }, { symmetricAlgo: n3.preferredSymmetricAlgorithm, aeadAlgo: R.aead.ocb }, { symmetricAlgo: R.symmetric.aes128, aeadAlgo: n3.preferredAEADAlgorithm }];
            for (const e5 of t4) if (i3.every((t5) => t5.preferredCipherSuites && t5.preferredCipherSuites.some((t6) => t6[0] === e5.symmetricAlgo && t6[1] === e5.aeadAlgo))) return e5;
            return e4;
          }
          const s3 = R.symmetric.aes128, a3 = n3.preferredSymmetricAlgorithm;
          return { symmetricAlgo: i3.every((e4) => e4.preferredSymmetricAlgorithms && e4.preferredSymmetricAlgorithms.includes(a3)) ? a3 : s3, aeadAlgo: void 0 };
        }(e2, t2, r2, n2), a2 = R.read(R.symmetric, i2), o2 = s2 ? R.read(R.aead, s2) : void 0;
        await Promise.all(e2.map((e3) => e3.getEncryptionKey().catch(() => null).then((e4) => {
          if (e4 && (e4.keyPacket.algorithm === R.publicKey.x25519 || e4.keyPacket.algorithm === R.publicKey.x448) && !o2 && !M.isAES(i2)) throw Error("Could not generate a session key compatible with the given `encryptionKeys`: X22519 and X448 keys can only be used to encrypt AES session keys; change `config.preferredSymmetricAlgorithm` accordingly.");
        })));
        return { data: vi(i2), algorithm: a2, aeadAlgorithm: o2 };
      }
      async encrypt(e2, t2, r2, n2 = false, i2 = [], s2 = /* @__PURE__ */ new Date(), a2 = [], o2 = T) {
        if (r2) {
          if (!M.isUint8Array(r2.data) || !M.isString(r2.algorithm)) throw Error("Invalid session key for encryption.");
        } else if (e2 && e2.length) r2 = await _Ho.generateSessionKey(e2, s2, a2, o2);
        else {
          if (!t2 || !t2.length) throw Error("No keys, passwords, or session key provided.");
          r2 = await _Ho.generateSessionKey(void 0, void 0, void 0, o2);
        }
        const { data: c2, algorithm: h2, aeadAlgorithm: u2 } = r2, l2 = await _Ho.encryptSessionKey(c2, h2, u2, e2, t2, n2, i2, s2, a2, o2), y2 = Fa.fromObject({ version: u2 ? 2 : 1, aeadAlgorithm: u2 ? R.write(R.aead, u2) : null });
        y2.packets = this.packets;
        const f2 = R.write(R.symmetric, h2);
        return await y2.encrypt(f2, c2, o2), l2.packets.push(y2), y2.packets = new Ua(), l2;
      }
      static async encryptSessionKey(e2, t2, r2, n2, i2, s2 = false, a2 = [], o2 = /* @__PURE__ */ new Date(), c2 = [], h2 = T) {
        const u2 = new Ua(), l2 = R.write(R.symmetric, t2), y2 = r2 && R.write(R.aead, r2);
        if (n2) {
          const t3 = await Promise.all(n2.map(async function(t4, r3) {
            const n3 = await t4.getEncryptionKey(a2[r3], o2, c2, h2), i3 = Ga.fromObject({ version: y2 ? 6 : 3, encryptionKeyPacket: n3.keyPacket, anonymousRecipient: s2, sessionKey: e2, sessionKeyAlgorithm: l2 });
            return await i3.encrypt(n3.keyPacket), delete i3.sessionKey, i3;
          }));
          u2.push(...t3);
        }
        if (i2) {
          const t3 = async function(e3, t4) {
            try {
              return await e3.decrypt(t4), 1;
            } catch (e4) {
              return 0;
            }
          }, r3 = (e3, t4) => e3 + t4, n3 = async function(e3, s4, a3, o3) {
            const c3 = new ja(h2);
            if (c3.sessionKey = e3, c3.sessionKeyAlgorithm = s4, a3 && (c3.aeadAlgorithm = a3), await c3.encrypt(o3, h2), h2.passwordCollisionCheck) {
              if (1 !== (await Promise.all(i2.map((e4) => t3(c3, e4)))).reduce(r3)) return n3(e3, s4, o3);
            }
            return delete c3.sessionKey, c3;
          }, s3 = await Promise.all(i2.map((t4) => n3(e2, l2, y2, t4)));
          u2.push(...s3);
        }
        return new _Ho(u2);
      }
      async sign(e2 = [], t2 = [], r2 = null, n2 = [], i2 = /* @__PURE__ */ new Date(), s2 = [], a2 = [], o2 = [], c2 = T) {
        const h2 = new Ua(), u2 = this.packets.findPacket(R.packet.literalData);
        if (!u2) throw Error("No literal data packet to sign.");
        const l2 = await zo(u2, e2, t2, r2, n2, i2, s2, a2, o2, false, c2), y2 = l2.map((e3, t3) => Ca.fromSignaturePacket(e3, 0 === t3)).reverse();
        return h2.push(...y2), h2.push(u2), h2.push(...l2), new _Ho(h2);
      }
      compress(e2, t2 = T) {
        if (e2 === R.compression.uncompressed) return this;
        const r2 = new xa(t2);
        r2.algorithm = e2, r2.packets = this.packets;
        const n2 = new Ua();
        return n2.push(r2), new _Ho(n2);
      }
      async signDetached(e2 = [], t2 = [], r2 = null, n2 = [], i2 = [], s2 = /* @__PURE__ */ new Date(), a2 = [], o2 = [], c2 = T) {
        const h2 = this.packets.findPacket(R.packet.literalData);
        if (!h2) throw Error("No literal data packet to sign.");
        return new so(await zo(h2, e2, t2, r2, n2, i2, s2, a2, o2, true, c2));
      }
      async verify(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        const n2 = this.unwrapCompressed(), i2 = n2.packets.filterByTag(R.packet.literalData);
        if (1 !== i2.length) throw Error("Can only verify message with one literal data packet.");
        o(n2.packets.stream) && n2.packets.push(...await C(n2.packets.stream, (e3) => e3 || []));
        const s2 = n2.packets.filterByTag(R.packet.onePassSignature).reverse(), a2 = n2.packets.filterByTag(R.packet.signature);
        return s2.length && !a2.length && M.isStream(n2.packets.stream) && !o(n2.packets.stream) ? (await Promise.all(s2.map(async (e3) => {
          e3.correspondingSig = new Promise((t3, r3) => {
            e3.correspondingSigResolve = t3, e3.correspondingSigReject = r3;
          }), e3.signatureData = U(async () => (await e3.correspondingSig).signatureData), e3.hashed = C(await e3.hash(e3.signatureType, i2[0], void 0, false)), e3.hashed.catch(() => {
          });
        })), n2.packets.stream = E(n2.packets.stream, async (e3, t3) => {
          const r3 = P(e3), n3 = x(t3);
          try {
            for (let e4 = 0; e4 < s2.length; e4++) {
              const { value: t4 } = await r3.read();
              s2[e4].correspondingSigResolve(t4);
            }
            await r3.readToEnd(), await n3.ready, await n3.close();
          } catch (e4) {
            s2.forEach((t4) => {
              t4.correspondingSigReject(e4);
            }), await n3.abort(e4);
          }
        }), Go(s2, i2, e2, t2, false, r2)) : Go(a2, i2, e2, t2, false, r2);
      }
      verifyDetached(e2, t2, r2 = /* @__PURE__ */ new Date(), n2 = T) {
        const i2 = this.unwrapCompressed().packets.filterByTag(R.packet.literalData);
        if (1 !== i2.length) throw Error("Can only verify message with one literal data packet.");
        return Go(e2.packets.filterByTag(R.packet.signature), i2, t2, r2, true, n2);
      }
      unwrapCompressed() {
        const e2 = this.packets.filterByTag(R.packet.compressedData);
        return e2.length ? new _Ho(e2[0].packets) : this;
      }
      async appendSignature(e2, t2 = T) {
        await this.packets.read(M.isUint8Array(e2) ? e2 : (await $(e2)).data, Oo, t2);
      }
      write() {
        return this.packets.write();
      }
      armor(e2 = T) {
        const t2 = this.packets[this.packets.length - 1], r2 = t2.constructor.tag === Fa.tag ? 2 !== t2.version : this.packets.some((e3) => e3.constructor.tag === Ba.tag && 6 !== e3.version);
        return X(R.armor.message, this.write(), null, null, null, r2, e2);
      }
    };
    qo = /* @__PURE__ */ M.constructAllowedPackets([Ba]);
    Vo = class _Vo {
      constructor(e2, t2) {
        if (this.text = M.removeTrailingSpaces(e2).replace(/\r?\n/g, "\r\n"), t2 && !(t2 instanceof so)) throw Error("Invalid signature input");
        this.signature = t2 || new so(new Ua());
      }
      getSigningKeyIDs() {
        const e2 = [];
        return this.signature.packets.forEach(function(t2) {
          e2.push(t2.issuerKeyID);
        }), e2;
      }
      async sign(e2, t2 = [], r2 = null, n2 = [], i2 = /* @__PURE__ */ new Date(), s2 = [], a2 = [], o2 = [], c2 = T) {
        const h2 = new ba();
        h2.setText(this.text);
        const u2 = new so(await zo(h2, e2, t2, r2, n2, i2, s2, a2, o2, true, c2));
        return new _Vo(this.text, u2);
      }
      verify(e2, t2 = /* @__PURE__ */ new Date(), r2 = T) {
        const n2 = this.signature.packets.filterByTag(R.packet.signature), i2 = new ba();
        return i2.setText(this.text), Go(n2, [i2], e2, t2, true, r2);
      }
      getText() {
        return this.text.replace(/\r\n/g, "\n");
      }
      armor(e2 = T) {
        const t2 = this.signature.packets.some((e3) => 6 !== e3.version), r2 = { hash: t2 ? Array.from(new Set(this.signature.packets.map((e3) => R.read(R.hash, e3.hashAlgorithm).toUpperCase()))).join() : null, text: this.text, data: this.signature.packets.write() };
        return X(R.armor.signed, r2, void 0, void 0, void 0, t2, e2);
      }
    };
    lc = Object.keys(T).length;
    kc = "object" == typeof e && "crypto" in e ? e.crypto : void 0;
    Ec = (e2) => new DataView(e2.buffer, e2.byteOffset, e2.byteLength);
    vc = (e2, t2) => e2 << 32 - t2 | e2 >>> t2;
    Ic = (e2, t2) => e2 << t2 | e2 >>> 32 - t2 >>> 0;
    Bc = 68 === new Uint8Array(new Uint32Array([287454020]).buffer)[0];
    Uc = class {
      clone() {
        return this._cloneInto();
      }
    };
    Qc = (e2, t2, r2) => e2 & t2 ^ ~e2 & r2;
    Rc = (e2, t2, r2) => e2 & t2 ^ e2 & r2 ^ t2 & r2;
    Tc = class extends Uc {
      constructor(e2, t2, r2, n2) {
        super(), this.blockLen = e2, this.outputLen = t2, this.padOffset = r2, this.isLE = n2, this.finished = false, this.length = 0, this.pos = 0, this.destroyed = false, this.buffer = new Uint8Array(e2), this.view = Ec(this.buffer);
      }
      update(e2) {
        mc(this);
        const { view: t2, buffer: r2, blockLen: n2 } = this, i2 = (e2 = Cc(e2)).length;
        for (let s2 = 0; s2 < i2; ) {
          const a2 = Math.min(n2 - this.pos, i2 - s2);
          if (a2 !== n2) r2.set(e2.subarray(s2, s2 + a2), this.pos), this.pos += a2, s2 += a2, this.pos === n2 && (this.process(t2, 0), this.pos = 0);
          else {
            const t3 = Ec(e2);
            for (; n2 <= i2 - s2; s2 += n2) this.process(t3, s2);
          }
        }
        return this.length += e2.length, this.roundClean(), this;
      }
      digestInto(e2) {
        mc(this), bc(e2, this), this.finished = true;
        const { buffer: t2, view: r2, blockLen: n2, isLE: i2 } = this;
        let { pos: s2 } = this;
        t2[s2++] = 128, this.buffer.subarray(s2).fill(0), this.padOffset > n2 - s2 && (this.process(r2, 0), s2 = 0);
        for (let e3 = s2; e3 < n2; e3++) t2[e3] = 0;
        !function(e3, t3, r3, n3) {
          if ("function" == typeof e3.setBigUint64) return e3.setBigUint64(t3, r3, n3);
          const i3 = BigInt(32), s3 = BigInt(4294967295), a3 = Number(r3 >> i3 & s3), o3 = Number(r3 & s3), c3 = n3 ? 4 : 0, h3 = n3 ? 0 : 4;
          e3.setUint32(t3 + c3, a3, n3), e3.setUint32(t3 + h3, o3, n3);
        }(r2, n2 - 8, BigInt(8 * this.length), i2), this.process(r2, 0);
        const a2 = Ec(e2), o2 = this.outputLen;
        if (o2 % 4) throw Error("_sha2: outputLen should be aligned to 32bit");
        const c2 = o2 / 4, h2 = this.get();
        if (c2 > h2.length) throw Error("_sha2: outputLen bigger than state");
        for (let e3 = 0; e3 < c2; e3++) a2.setUint32(4 * e3, h2[e3], i2);
      }
      digest() {
        const { buffer: e2, outputLen: t2 } = this;
        this.digestInto(e2);
        const r2 = e2.slice(0, t2);
        return this.destroy(), r2;
      }
      _cloneInto(e2) {
        e2 || (e2 = new this.constructor()), e2.set(...this.get());
        const { blockLen: t2, buffer: r2, length: n2, finished: i2, destroyed: s2, pos: a2 } = this;
        return e2.length = n2, e2.pos = a2, e2.finished = i2, e2.destroyed = s2, n2 % t2 && e2.buffer.set(r2), e2;
      }
    };
    Lc = /* @__PURE__ */ new Uint32Array([1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298]);
    Mc = /* @__PURE__ */ new Uint32Array([1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225]);
    Nc = /* @__PURE__ */ new Uint32Array(64);
    Fc = class extends Tc {
      constructor() {
        super(64, 32, 8, false), this.A = 0 | Mc[0], this.B = 0 | Mc[1], this.C = 0 | Mc[2], this.D = 0 | Mc[3], this.E = 0 | Mc[4], this.F = 0 | Mc[5], this.G = 0 | Mc[6], this.H = 0 | Mc[7];
      }
      get() {
        const { A: e2, B: t2, C: r2, D: n2, E: i2, F: s2, G: a2, H: o2 } = this;
        return [e2, t2, r2, n2, i2, s2, a2, o2];
      }
      set(e2, t2, r2, n2, i2, s2, a2, o2) {
        this.A = 0 | e2, this.B = 0 | t2, this.C = 0 | r2, this.D = 0 | n2, this.E = 0 | i2, this.F = 0 | s2, this.G = 0 | a2, this.H = 0 | o2;
      }
      process(e2, t2) {
        for (let r3 = 0; r3 < 16; r3++, t2 += 4) Nc[r3] = e2.getUint32(t2, false);
        for (let e3 = 16; e3 < 64; e3++) {
          const t3 = Nc[e3 - 15], r3 = Nc[e3 - 2], n3 = vc(t3, 7) ^ vc(t3, 18) ^ t3 >>> 3, i3 = vc(r3, 17) ^ vc(r3, 19) ^ r3 >>> 10;
          Nc[e3] = i3 + Nc[e3 - 7] + n3 + Nc[e3 - 16] | 0;
        }
        let { A: r2, B: n2, C: i2, D: s2, E: a2, F: o2, G: c2, H: h2 } = this;
        for (let e3 = 0; e3 < 64; e3++) {
          const t3 = h2 + (vc(a2, 6) ^ vc(a2, 11) ^ vc(a2, 25)) + Qc(a2, o2, c2) + Lc[e3] + Nc[e3] | 0, u2 = (vc(r2, 2) ^ vc(r2, 13) ^ vc(r2, 22)) + Rc(r2, n2, i2) | 0;
          h2 = c2, c2 = o2, o2 = a2, a2 = s2 + t3 | 0, s2 = i2, i2 = n2, n2 = r2, r2 = t3 + u2 | 0;
        }
        r2 = r2 + this.A | 0, n2 = n2 + this.B | 0, i2 = i2 + this.C | 0, s2 = s2 + this.D | 0, a2 = a2 + this.E | 0, o2 = o2 + this.F | 0, c2 = c2 + this.G | 0, h2 = h2 + this.H | 0, this.set(r2, n2, i2, s2, a2, o2, c2, h2);
      }
      roundClean() {
        Nc.fill(0);
      }
      destroy() {
        this.set(0, 0, 0, 0, 0, 0, 0, 0), this.buffer.fill(0);
      }
    };
    Oc = class extends Fc {
      constructor() {
        super(), this.A = -1056596264, this.B = 914150663, this.C = 812702999, this.D = -150054599, this.E = -4191439, this.F = 1750603025, this.G = 1694076839, this.H = -1090891868, this.outputLen = 28;
      }
    };
    Hc = /* @__PURE__ */ Pc(() => new Fc());
    zc = /* @__PURE__ */ Pc(() => new Oc());
    Gc = class extends Uc {
      constructor(e2, t2) {
        super(), this.finished = false, this.destroyed = false, function(e3) {
          if ("function" != typeof e3 || "function" != typeof e3.create) throw Error("Hash should be wrapped by utils.wrapConstructor");
          Ac(e3.outputLen), Ac(e3.blockLen);
        }(e2);
        const r2 = Cc(t2);
        if (this.iHash = e2.create(), "function" != typeof this.iHash.update) throw Error("Expected instance of class which extends utils.Hash");
        this.blockLen = this.iHash.blockLen, this.outputLen = this.iHash.outputLen;
        const n2 = this.blockLen, i2 = new Uint8Array(n2);
        i2.set(r2.length > n2 ? e2.create().update(r2).digest() : r2);
        for (let e3 = 0; e3 < i2.length; e3++) i2[e3] ^= 54;
        this.iHash.update(i2), this.oHash = e2.create();
        for (let e3 = 0; e3 < i2.length; e3++) i2[e3] ^= 106;
        this.oHash.update(i2), i2.fill(0);
      }
      update(e2) {
        return mc(this), this.iHash.update(e2), this;
      }
      digestInto(e2) {
        mc(this), wc(e2, this.outputLen), this.finished = true, this.iHash.digestInto(e2), this.oHash.update(e2), this.oHash.digestInto(e2), this.destroy();
      }
      digest() {
        const e2 = new Uint8Array(this.oHash.outputLen);
        return this.digestInto(e2), e2;
      }
      _cloneInto(e2) {
        e2 || (e2 = Object.create(Object.getPrototypeOf(this), {}));
        const { oHash: t2, iHash: r2, finished: n2, destroyed: i2, blockLen: s2, outputLen: a2 } = this;
        return e2.finished = n2, e2.destroyed = i2, e2.blockLen = s2, e2.outputLen = a2, e2.oHash = t2._cloneInto(e2.oHash), e2.iHash = r2._cloneInto(e2.iHash), e2;
      }
      destroy() {
        this.destroyed = true, this.oHash.destroy(), this.iHash.destroy();
      }
    };
    _c = (e2, t2, r2) => new Gc(e2, t2).update(r2).digest();
    _c.create = (e2, t2) => new Gc(e2, t2);
    jc = /* @__PURE__ */ BigInt(0);
    qc = /* @__PURE__ */ BigInt(1);
    Vc = /* @__PURE__ */ BigInt(2);
    Wc = /* @__PURE__ */ Array.from({ length: 256 }, (e2, t2) => t2.toString(16).padStart(2, "0"));
    th = { _0: 48, _9: 57, _A: 65, _F: 70, _a: 97, _f: 102 };
    uh = (e2) => "bigint" == typeof e2 && jc <= e2;
    gh = (e2) => (Vc << BigInt(e2 - 1)) - qc;
    ph = (e2) => new Uint8Array(e2);
    dh = (e2) => Uint8Array.from(e2);
    wh = { bigint: (e2) => "bigint" == typeof e2, function: (e2) => "function" == typeof e2, boolean: (e2) => "boolean" == typeof e2, string: (e2) => "string" == typeof e2, stringOrUint8Array: (e2) => "string" == typeof e2 || Jc(e2), isSafeInteger: (e2) => Number.isSafeInteger(e2), array: (e2) => Array.isArray(e2), field: (e2, t2) => t2.Fp.isValid(e2), hash: (e2) => "function" == typeof e2 && Number.isSafeInteger(e2.outputLen) };
    kh = /* @__PURE__ */ Object.freeze({ __proto__: null, aInRange: yh, abool: Zc, abytes: Yc, bitGet: function(e2, t2) {
      return e2 >> BigInt(t2) & qc;
    }, bitLen: fh, bitMask: gh, bitSet: function(e2, t2, r2) {
      return e2 | (r2 ? qc : jc) << BigInt(t2);
    }, bytesToHex: $c, bytesToNumberBE: ih, bytesToNumberLE: sh, concatBytes: hh, createHmacDrbg: Ah, ensureBytes: ch, equalBytes: function(e2, t2) {
      if (e2.length !== t2.length) return false;
      let r2 = 0;
      for (let n2 = 0; n2 < e2.length; n2++) r2 |= e2[n2] ^ t2[n2];
      return 0 === r2;
    }, hexToBytes: nh, hexToNumber: eh, inRange: lh, isBytes: Jc, memoized: bh, notImplemented: () => {
      throw Error("not implemented");
    }, numberToBytesBE: ah, numberToBytesLE: oh, numberToHexUnpadded: Xc, numberToVarBytesBE: function(e2) {
      return nh(Xc(e2));
    }, utf8ToBytes: function(e2) {
      if ("string" != typeof e2) throw Error("utf8ToBytes expected string, got " + typeof e2);
      return new Uint8Array(new TextEncoder().encode(e2));
    }, validateObject: mh });
    Eh = BigInt(0);
    vh = BigInt(1);
    Ih = BigInt(2);
    Bh = BigInt(3);
    Sh = BigInt(4);
    Kh = BigInt(5);
    Ch = BigInt(8);
    BigInt(9), BigInt(16);
    Rh = ["create", "isValid", "is0", "neg", "inv", "sqrt", "sqr", "eql", "add", "sub", "mul", "pow", "div", "addN", "subN", "mulN", "sqrN"];
    Fh = BigInt(0);
    Oh = BigInt(1);
    Hh = /* @__PURE__ */ new WeakMap();
    zh = /* @__PURE__ */ new WeakMap();
    ({ bytesToNumberBE: Vh, hexToBytes: Jh } = kh);
    Yh = { Err: class extends Error {
      constructor(e2 = "") {
        super(e2);
      }
    }, _tlv: { encode: (e2, t2) => {
      const { Err: r2 } = Yh;
      if (e2 < 0 || e2 > 256) throw new r2("tlv.encode: wrong tag");
      if (1 & t2.length) throw new r2("tlv.encode: unpadded data");
      const n2 = t2.length / 2, i2 = Xc(n2);
      if (i2.length / 2 & 128) throw new r2("tlv.encode: long form length too big");
      const s2 = n2 > 127 ? Xc(i2.length / 2 | 128) : "";
      return `${Xc(e2)}${s2}${i2}${t2}`;
    }, decode(e2, t2) {
      const { Err: r2 } = Yh;
      let n2 = 0;
      if (e2 < 0 || e2 > 256) throw new r2("tlv.encode: wrong tag");
      if (t2.length < 2 || t2[n2++] !== e2) throw new r2("tlv.decode: wrong tlv");
      const i2 = t2[n2++];
      let s2 = 0;
      if (!!(128 & i2)) {
        const e3 = 127 & i2;
        if (!e3) throw new r2("tlv.decode(long): indefinite length not supported");
        if (e3 > 4) throw new r2("tlv.decode(long): byte length is too big");
        const a3 = t2.subarray(n2, n2 + e3);
        if (a3.length !== e3) throw new r2("tlv.decode: length bytes not complete");
        if (0 === a3[0]) throw new r2("tlv.decode(long): zero leftmost byte");
        for (const e4 of a3) s2 = s2 << 8 | e4;
        if (n2 += e3, s2 < 128) throw new r2("tlv.decode(long): not minimal encoding");
      } else s2 = i2;
      const a2 = t2.subarray(n2, n2 + s2);
      if (a2.length !== s2) throw new r2("tlv.decode: wrong value length");
      return { v: a2, l: t2.subarray(n2 + s2) };
    } }, _int: { encode(e2) {
      const { Err: t2 } = Yh;
      if (e2 < Zh) throw new t2("integer: negative integers are not allowed");
      let r2 = Xc(e2);
      if (8 & Number.parseInt(r2[0], 16) && (r2 = "00" + r2), 1 & r2.length) throw new t2("unexpected assertion");
      return r2;
    }, decode(e2) {
      const { Err: t2 } = Yh;
      if (128 & e2[0]) throw new t2("Invalid signature integer: negative");
      if (0 === e2[0] && !(128 & e2[1])) throw new t2("Invalid signature integer: unnecessary leading zero");
      return Vh(e2);
    } }, toSig(e2) {
      const { Err: t2, _int: r2, _tlv: n2 } = Yh, i2 = "string" == typeof e2 ? Jh(e2) : e2;
      Yc(i2);
      const { v: s2, l: a2 } = n2.decode(48, i2);
      if (a2.length) throw new t2("Invalid signature: left bytes after parsing");
      const { v: o2, l: c2 } = n2.decode(2, s2), { v: h2, l: u2 } = n2.decode(2, c2);
      if (u2.length) throw new t2("Invalid signature: left bytes after parsing");
      return { r: r2.decode(o2), s: r2.decode(h2) };
    }, hexFromSig(e2) {
      const { _tlv: t2, _int: r2 } = Yh, n2 = `${t2.encode(2, r2.encode(e2.r))}${t2.encode(2, r2.encode(e2.s))}`;
      return t2.encode(48, n2);
    } };
    Zh = BigInt(0);
    Wh = BigInt(1);
    BigInt(2);
    $h = BigInt(3);
    BigInt(4);
    nu = Lh(BigInt("0xffffffff00000001000000000000000000000000ffffffffffffffffffffffff"));
    iu = ru({ a: nu.create(BigInt("-3")), b: BigInt("0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b"), Fp: nu, n: BigInt("0xffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551"), Gx: BigInt("0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296"), Gy: BigInt("0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5"), h: BigInt(1), lowS: false }, Hc);
    su = /* @__PURE__ */ BigInt(2 ** 32 - 1);
    au = /* @__PURE__ */ BigInt(32);
    hu = (e2, t2, r2) => e2 << r2 | t2 >>> 32 - r2;
    uu = (e2, t2, r2) => t2 << r2 | e2 >>> 32 - r2;
    lu = (e2, t2, r2) => t2 << r2 - 32 | e2 >>> 64 - r2;
    yu = (e2, t2, r2) => e2 << r2 - 32 | t2 >>> 64 - r2;
    fu = { fromBig: ou, split: cu, toBig: (e2, t2) => BigInt(e2 >>> 0) << au | BigInt(t2 >>> 0), shrSH: (e2, t2, r2) => e2 >>> r2, shrSL: (e2, t2, r2) => e2 << 32 - r2 | t2 >>> r2, rotrSH: (e2, t2, r2) => e2 >>> r2 | t2 << 32 - r2, rotrSL: (e2, t2, r2) => e2 << 32 - r2 | t2 >>> r2, rotrBH: (e2, t2, r2) => e2 << 64 - r2 | t2 >>> r2 - 32, rotrBL: (e2, t2, r2) => e2 >>> r2 - 32 | t2 << 64 - r2, rotr32H: (e2, t2) => t2, rotr32L: (e2, t2) => e2, rotlSH: hu, rotlSL: uu, rotlBH: lu, rotlBL: yu, add: function(e2, t2, r2, n2) {
      const i2 = (t2 >>> 0) + (n2 >>> 0);
      return { h: e2 + r2 + (i2 / 2 ** 32 | 0) | 0, l: 0 | i2 };
    }, add3L: (e2, t2, r2) => (e2 >>> 0) + (t2 >>> 0) + (r2 >>> 0), add3H: (e2, t2, r2, n2) => t2 + r2 + n2 + (e2 / 2 ** 32 | 0) | 0, add4L: (e2, t2, r2, n2) => (e2 >>> 0) + (t2 >>> 0) + (r2 >>> 0) + (n2 >>> 0), add4H: (e2, t2, r2, n2, i2) => t2 + r2 + n2 + i2 + (e2 / 2 ** 32 | 0) | 0, add5H: (e2, t2, r2, n2, i2, s2) => t2 + r2 + n2 + i2 + s2 + (e2 / 2 ** 32 | 0) | 0, add5L: (e2, t2, r2, n2, i2) => (e2 >>> 0) + (t2 >>> 0) + (r2 >>> 0) + (n2 >>> 0) + (i2 >>> 0) };
    [gu, pu] = /* @__PURE__ */ (() => fu.split(["0x428a2f98d728ae22", "0x7137449123ef65cd", "0xb5c0fbcfec4d3b2f", "0xe9b5dba58189dbbc", "0x3956c25bf348b538", "0x59f111f1b605d019", "0x923f82a4af194f9b", "0xab1c5ed5da6d8118", "0xd807aa98a3030242", "0x12835b0145706fbe", "0x243185be4ee4b28c", "0x550c7dc3d5ffb4e2", "0x72be5d74f27b896f", "0x80deb1fe3b1696b1", "0x9bdc06a725c71235", "0xc19bf174cf692694", "0xe49b69c19ef14ad2", "0xefbe4786384f25e3", "0x0fc19dc68b8cd5b5", "0x240ca1cc77ac9c65", "0x2de92c6f592b0275", "0x4a7484aa6ea6e483", "0x5cb0a9dcbd41fbd4", "0x76f988da831153b5", "0x983e5152ee66dfab", "0xa831c66d2db43210", "0xb00327c898fb213f", "0xbf597fc7beef0ee4", "0xc6e00bf33da88fc2", "0xd5a79147930aa725", "0x06ca6351e003826f", "0x142929670a0e6e70", "0x27b70a8546d22ffc", "0x2e1b21385c26c926", "0x4d2c6dfc5ac42aed", "0x53380d139d95b3df", "0x650a73548baf63de", "0x766a0abb3c77b2a8", "0x81c2c92e47edaee6", "0x92722c851482353b", "0xa2bfe8a14cf10364", "0xa81a664bbc423001", "0xc24b8b70d0f89791", "0xc76c51a30654be30", "0xd192e819d6ef5218", "0xd69906245565a910", "0xf40e35855771202a", "0x106aa07032bbd1b8", "0x19a4c116b8d2d0c8", "0x1e376c085141ab53", "0x2748774cdf8eeb99", "0x34b0bcb5e19b48a8", "0x391c0cb3c5c95a63", "0x4ed8aa4ae3418acb", "0x5b9cca4f7763e373", "0x682e6ff3d6b2b8a3", "0x748f82ee5defb2fc", "0x78a5636f43172f60", "0x84c87814a1f0ab72", "0x8cc702081a6439ec", "0x90befffa23631e28", "0xa4506cebde82bde9", "0xbef9a3f7b2c67915", "0xc67178f2e372532b", "0xca273eceea26619c", "0xd186b8c721c0c207", "0xeada7dd6cde0eb1e", "0xf57d4f7fee6ed178", "0x06f067aa72176fba", "0x0a637dc5a2c898a6", "0x113f9804bef90dae", "0x1b710b35131c471b", "0x28db77f523047d84", "0x32caab7b40c72493", "0x3c9ebe0a15c9bebc", "0x431d67c49c100d4c", "0x4cc5d4becb3e42b6", "0x597f299cfc657e2a", "0x5fcb6fab3ad6faec", "0x6c44198c4a475817"].map((e2) => BigInt(e2))))();
    du = /* @__PURE__ */ new Uint32Array(80);
    Au = /* @__PURE__ */ new Uint32Array(80);
    wu = class extends Tc {
      constructor() {
        super(128, 64, 16, false), this.Ah = 1779033703, this.Al = -205731576, this.Bh = -1150833019, this.Bl = -2067093701, this.Ch = 1013904242, this.Cl = -23791573, this.Dh = -1521486534, this.Dl = 1595750129, this.Eh = 1359893119, this.El = -1377402159, this.Fh = -1694144372, this.Fl = 725511199, this.Gh = 528734635, this.Gl = -79577749, this.Hh = 1541459225, this.Hl = 327033209;
      }
      get() {
        const { Ah: e2, Al: t2, Bh: r2, Bl: n2, Ch: i2, Cl: s2, Dh: a2, Dl: o2, Eh: c2, El: h2, Fh: u2, Fl: l2, Gh: y2, Gl: f2, Hh: g2, Hl: p2 } = this;
        return [e2, t2, r2, n2, i2, s2, a2, o2, c2, h2, u2, l2, y2, f2, g2, p2];
      }
      set(e2, t2, r2, n2, i2, s2, a2, o2, c2, h2, u2, l2, y2, f2, g2, p2) {
        this.Ah = 0 | e2, this.Al = 0 | t2, this.Bh = 0 | r2, this.Bl = 0 | n2, this.Ch = 0 | i2, this.Cl = 0 | s2, this.Dh = 0 | a2, this.Dl = 0 | o2, this.Eh = 0 | c2, this.El = 0 | h2, this.Fh = 0 | u2, this.Fl = 0 | l2, this.Gh = 0 | y2, this.Gl = 0 | f2, this.Hh = 0 | g2, this.Hl = 0 | p2;
      }
      process(e2, t2) {
        for (let r3 = 0; r3 < 16; r3++, t2 += 4) du[r3] = e2.getUint32(t2), Au[r3] = e2.getUint32(t2 += 4);
        for (let e3 = 16; e3 < 80; e3++) {
          const t3 = 0 | du[e3 - 15], r3 = 0 | Au[e3 - 15], n3 = fu.rotrSH(t3, r3, 1) ^ fu.rotrSH(t3, r3, 8) ^ fu.shrSH(t3, r3, 7), i3 = fu.rotrSL(t3, r3, 1) ^ fu.rotrSL(t3, r3, 8) ^ fu.shrSL(t3, r3, 7), s3 = 0 | du[e3 - 2], a3 = 0 | Au[e3 - 2], o3 = fu.rotrSH(s3, a3, 19) ^ fu.rotrBH(s3, a3, 61) ^ fu.shrSH(s3, a3, 6), c3 = fu.rotrSL(s3, a3, 19) ^ fu.rotrBL(s3, a3, 61) ^ fu.shrSL(s3, a3, 6), h3 = fu.add4L(i3, c3, Au[e3 - 7], Au[e3 - 16]), u3 = fu.add4H(h3, n3, o3, du[e3 - 7], du[e3 - 16]);
          du[e3] = 0 | u3, Au[e3] = 0 | h3;
        }
        let { Ah: r2, Al: n2, Bh: i2, Bl: s2, Ch: a2, Cl: o2, Dh: c2, Dl: h2, Eh: u2, El: l2, Fh: y2, Fl: f2, Gh: g2, Gl: p2, Hh: d2, Hl: A2 } = this;
        for (let e3 = 0; e3 < 80; e3++) {
          const t3 = fu.rotrSH(u2, l2, 14) ^ fu.rotrSH(u2, l2, 18) ^ fu.rotrBH(u2, l2, 41), w2 = fu.rotrSL(u2, l2, 14) ^ fu.rotrSL(u2, l2, 18) ^ fu.rotrBL(u2, l2, 41), m2 = u2 & y2 ^ ~u2 & g2, b2 = l2 & f2 ^ ~l2 & p2, k2 = fu.add5L(A2, w2, b2, pu[e3], Au[e3]), E2 = fu.add5H(k2, d2, t3, m2, gu[e3], du[e3]), v2 = 0 | k2, I2 = fu.rotrSH(r2, n2, 28) ^ fu.rotrBH(r2, n2, 34) ^ fu.rotrBH(r2, n2, 39), B2 = fu.rotrSL(r2, n2, 28) ^ fu.rotrBL(r2, n2, 34) ^ fu.rotrBL(r2, n2, 39), S2 = r2 & i2 ^ r2 & a2 ^ i2 & a2, K2 = n2 & s2 ^ n2 & o2 ^ s2 & o2;
          d2 = 0 | g2, A2 = 0 | p2, g2 = 0 | y2, p2 = 0 | f2, y2 = 0 | u2, f2 = 0 | l2, { h: u2, l: l2 } = fu.add(0 | c2, 0 | h2, 0 | E2, 0 | v2), c2 = 0 | a2, h2 = 0 | o2, a2 = 0 | i2, o2 = 0 | s2, i2 = 0 | r2, s2 = 0 | n2;
          const C2 = fu.add3L(v2, B2, K2);
          r2 = fu.add3H(C2, E2, I2, S2), n2 = 0 | C2;
        }
        ({ h: r2, l: n2 } = fu.add(0 | this.Ah, 0 | this.Al, 0 | r2, 0 | n2)), { h: i2, l: s2 } = fu.add(0 | this.Bh, 0 | this.Bl, 0 | i2, 0 | s2), { h: a2, l: o2 } = fu.add(0 | this.Ch, 0 | this.Cl, 0 | a2, 0 | o2), { h: c2, l: h2 } = fu.add(0 | this.Dh, 0 | this.Dl, 0 | c2, 0 | h2), { h: u2, l: l2 } = fu.add(0 | this.Eh, 0 | this.El, 0 | u2, 0 | l2), { h: y2, l: f2 } = fu.add(0 | this.Fh, 0 | this.Fl, 0 | y2, 0 | f2), { h: g2, l: p2 } = fu.add(0 | this.Gh, 0 | this.Gl, 0 | g2, 0 | p2), { h: d2, l: A2 } = fu.add(0 | this.Hh, 0 | this.Hl, 0 | d2, 0 | A2), this.set(r2, n2, i2, s2, a2, o2, c2, h2, u2, l2, y2, f2, g2, p2, d2, A2);
      }
      roundClean() {
        du.fill(0), Au.fill(0);
      }
      destroy() {
        this.buffer.fill(0), this.set(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      }
    };
    mu = class extends wu {
      constructor() {
        super(), this.Ah = -876896931, this.Al = -1056596264, this.Bh = 1654270250, this.Bl = 914150663, this.Ch = -1856437926, this.Cl = 812702999, this.Dh = 355462360, this.Dl = -150054599, this.Eh = 1731405415, this.El = -4191439, this.Fh = -1900787065, this.Fl = 1750603025, this.Gh = -619958771, this.Gl = 1694076839, this.Hh = 1203062813, this.Hl = -1090891868, this.outputLen = 48;
      }
    };
    bu = /* @__PURE__ */ Pc(() => new wu());
    ku = /* @__PURE__ */ Pc(() => new mu());
    Eu = Lh(BigInt("0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000ffffffff"));
    vu = ru({ a: Eu.create(BigInt("-3")), b: BigInt("0xb3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef"), Fp: Eu, n: BigInt("0xffffffffffffffffffffffffffffffffffffffffffffffffc7634d81f4372ddf581a0db248b0a77aecec196accc52973"), Gx: BigInt("0xaa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7"), Gy: BigInt("0x3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f"), h: BigInt(1), lowS: false }, ku);
    Iu = Lh(BigInt("0x1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"));
    Bu = { a: Iu.create(BigInt("-3")), b: BigInt("0x0051953eb9618e1c9a1f929a21a0b68540eea2da725b99b315f3b8b489918ef109e156193951ec7e937b1652c0bd3bb1bf073573df883d2c34f1ef451fd46b503f00"), Fp: Iu, n: BigInt("0x01fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa51868783bf2f966b7fcc0148f709a5d03bb5c9b8899c47aebb6fb71e91386409"), Gx: BigInt("0x00c6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af606b4d3dbaa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf97e7e31c2e5bd66"), Gy: BigInt("0x011839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e662c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650"), h: BigInt(1) };
    Su = ru({ a: Bu.a, b: Bu.b, Fp: Iu, n: Bu.n, Gx: Bu.Gx, Gy: Bu.Gy, h: Bu.h, lowS: false, allowedPrivateKeyLengths: [130, 131, 132] }, bu);
    Ku = [];
    Cu = [];
    Du = [];
    Uu = /* @__PURE__ */ BigInt(0);
    Pu = /* @__PURE__ */ BigInt(1);
    xu = /* @__PURE__ */ BigInt(2);
    Qu = /* @__PURE__ */ BigInt(7);
    Ru = /* @__PURE__ */ BigInt(256);
    Tu = /* @__PURE__ */ BigInt(113);
    for (let e2 = 0, t2 = Pu, r2 = 1, n2 = 0; e2 < 24; e2++) {
      [r2, n2] = [n2, (2 * r2 + 3 * n2) % 5], Ku.push(2 * (5 * n2 + r2)), Cu.push((e2 + 1) * (e2 + 2) / 2 % 64);
      let i2 = Uu;
      for (let e3 = 0; e3 < 7; e3++) t2 = (t2 << Pu ^ (t2 >> Qu) * Tu) % Ru, t2 & xu && (i2 ^= Pu << (Pu << /* @__PURE__ */ BigInt(e3)) - Pu);
      Du.push(i2);
    }
    [Lu, Mu] = /* @__PURE__ */ cu(Du, true);
    Nu = (e2, t2, r2) => r2 > 32 ? lu(e2, t2, r2) : hu(e2, t2, r2);
    Fu = (e2, t2, r2) => r2 > 32 ? yu(e2, t2, r2) : uu(e2, t2, r2);
    Ou = class _Ou extends Uc {
      constructor(e2, t2, r2, n2 = false, i2 = 24) {
        if (super(), this.blockLen = e2, this.suffix = t2, this.outputLen = r2, this.enableXOF = n2, this.rounds = i2, this.pos = 0, this.posOut = 0, this.finished = false, this.destroyed = false, Ac(r2), 0 >= this.blockLen || this.blockLen >= 200) throw Error("Sha3 supports only keccak-f1600 function");
        var s2;
        this.state = new Uint8Array(200), this.state32 = (s2 = this.state, new Uint32Array(s2.buffer, s2.byteOffset, Math.floor(s2.byteLength / 4)));
      }
      keccak() {
        Bc || Sc(this.state32), function(e2, t2 = 24) {
          const r2 = new Uint32Array(10);
          for (let n2 = 24 - t2; n2 < 24; n2++) {
            for (let t4 = 0; t4 < 10; t4++) r2[t4] = e2[t4] ^ e2[t4 + 10] ^ e2[t4 + 20] ^ e2[t4 + 30] ^ e2[t4 + 40];
            for (let t4 = 0; t4 < 10; t4 += 2) {
              const n3 = (t4 + 8) % 10, i3 = (t4 + 2) % 10, s2 = r2[i3], a2 = r2[i3 + 1], o2 = Nu(s2, a2, 1) ^ r2[n3], c2 = Fu(s2, a2, 1) ^ r2[n3 + 1];
              for (let r3 = 0; r3 < 50; r3 += 10) e2[t4 + r3] ^= o2, e2[t4 + r3 + 1] ^= c2;
            }
            let t3 = e2[2], i2 = e2[3];
            for (let r3 = 0; r3 < 24; r3++) {
              const n3 = Cu[r3], s2 = Nu(t3, i2, n3), a2 = Fu(t3, i2, n3), o2 = Ku[r3];
              t3 = e2[o2], i2 = e2[o2 + 1], e2[o2] = s2, e2[o2 + 1] = a2;
            }
            for (let t4 = 0; t4 < 50; t4 += 10) {
              for (let n3 = 0; n3 < 10; n3++) r2[n3] = e2[t4 + n3];
              for (let n3 = 0; n3 < 10; n3++) e2[t4 + n3] ^= ~r2[(n3 + 2) % 10] & r2[(n3 + 4) % 10];
            }
            e2[0] ^= Lu[n2], e2[1] ^= Mu[n2];
          }
          r2.fill(0);
        }(this.state32, this.rounds), Bc || Sc(this.state32), this.posOut = 0, this.pos = 0;
      }
      update(e2) {
        mc(this);
        const { blockLen: t2, state: r2 } = this, n2 = (e2 = Cc(e2)).length;
        for (let i2 = 0; i2 < n2; ) {
          const s2 = Math.min(t2 - this.pos, n2 - i2);
          for (let t3 = 0; t3 < s2; t3++) r2[this.pos++] ^= e2[i2++];
          this.pos === t2 && this.keccak();
        }
        return this;
      }
      finish() {
        if (this.finished) return;
        this.finished = true;
        const { state: e2, suffix: t2, pos: r2, blockLen: n2 } = this;
        e2[r2] ^= t2, 128 & t2 && r2 === n2 - 1 && this.keccak(), e2[n2 - 1] ^= 128, this.keccak();
      }
      writeInto(e2) {
        mc(this, false), wc(e2), this.finish();
        const t2 = this.state, { blockLen: r2 } = this;
        for (let n2 = 0, i2 = e2.length; n2 < i2; ) {
          this.posOut >= r2 && this.keccak();
          const s2 = Math.min(r2 - this.posOut, i2 - n2);
          e2.set(t2.subarray(this.posOut, this.posOut + s2), n2), this.posOut += s2, n2 += s2;
        }
        return e2;
      }
      xofInto(e2) {
        if (!this.enableXOF) throw Error("XOF is not possible for this instance");
        return this.writeInto(e2);
      }
      xof(e2) {
        return Ac(e2), this.xofInto(new Uint8Array(e2));
      }
      digestInto(e2) {
        if (bc(e2, this), this.finished) throw Error("digest() was already called");
        return this.writeInto(e2), this.destroy(), e2;
      }
      digest() {
        return this.digestInto(new Uint8Array(this.outputLen));
      }
      destroy() {
        this.destroyed = true, this.state.fill(0);
      }
      _cloneInto(e2) {
        const { blockLen: t2, suffix: r2, outputLen: n2, rounds: i2, enableXOF: s2 } = this;
        return e2 || (e2 = new _Ou(t2, r2, n2, s2, i2)), e2.state32.set(this.state32), e2.pos = this.pos, e2.posOut = this.posOut, e2.finished = this.finished, e2.rounds = i2, e2.suffix = r2, e2.outputLen = n2, e2.enableXOF = s2, e2.destroyed = this.destroyed, e2;
      }
    };
    Hu = (e2, t2, r2) => Pc(() => new Ou(t2, e2, r2));
    zu = /* @__PURE__ */ Hu(6, 136, 32);
    Gu = /* @__PURE__ */ Hu(6, 72, 64);
    _u = /* @__PURE__ */ ((e2, t2, r2) => function(e3) {
      const t3 = (t4, r4) => e3(r4).update(Cc(t4)).digest(), r3 = e3({});
      return t3.outputLen = r3.outputLen, t3.blockLen = r3.blockLen, t3.create = (t4) => e3(t4), t3;
    }((n2 = {}) => new Ou(t2, e2, void 0 === n2.dkLen ? r2 : n2.dkLen, true)))(31, 136, 32);
    ju = BigInt(0);
    qu = BigInt(1);
    Vu = BigInt(2);
    Ju = BigInt(8);
    Yu = { zip215: true };
    Wu = BigInt(0);
    $u = BigInt(1);
    el = Pc(() => _u.create({ dkLen: 114 }));
    tl = (Pc(() => _u.create({ dkLen: 64 })), BigInt("726838724295606890549323807888004534353641360687318060281490199180612328166730772686396383698676545930088884461843637361053498018365439"));
    rl = BigInt(1);
    nl = BigInt(2);
    il = BigInt(3);
    BigInt(4);
    sl = BigInt(11);
    al = BigInt(22);
    ol = BigInt(44);
    cl = BigInt(88);
    hl = BigInt(223);
    yl = Lh(tl, 456, true);
    fl = { a: BigInt(1), d: BigInt("726838724295606890549323807888004534353641360687318060281490199180612328166730772686396383698676545930088884461843637361053498018326358"), Fp: yl, n: BigInt("181709681073901722637330951972001133588410340171829515070372549795146003961539585716195755291692375963310293709091662304773755859649779"), nBitLength: 456, h: BigInt(4), Gx: BigInt("224580040295924300187604334099896036246789641632564134246125461686950415467406032909029192869357953282578032075146446173674602635247710"), Gy: BigInt("298819210078481492676017930443930673437544040154080242095928241372331506189835876003536878655418784733982303233503462500531545062832660"), hash: el, randomBytes: xc, adjustScalarBytes: ll, domain: (e2, t2, r2) => {
      if (t2.length > 255) throw Error("Context is too big: " + t2.length);
      return Dc(Kc("SigEd448"), new Uint8Array([r2 ? 1 : 0, t2.length]), t2, e2);
    }, uvRatio: function(e2, t2) {
      const r2 = tl, n2 = Dh(e2 * e2 * t2, r2), i2 = Dh(n2 * e2, r2), s2 = Dh(i2 * n2 * t2, r2), a2 = Dh(i2 * ul(s2), r2), o2 = Dh(a2 * a2, r2);
      return { isValid: Dh(o2 * t2, r2) === e2, value: a2 };
    } };
    gl = /* @__PURE__ */ Zu(fl);
    pl = /* @__PURE__ */ (() => Xu({ a: BigInt(156326), montgomeryBits: 448, nByteLength: 56, P: tl, Gu: BigInt(5), powPminus2: (e2) => {
      const t2 = tl;
      return Dh(Ph(ul(e2), BigInt(2), t2) * e2, t2);
    }, adjustScalarBytes: ll, randomBytes: xc }))();
    yl.ORDER, BigInt(3), BigInt(4), BigInt(156326), BigInt("39082"), BigInt("78163"), BigInt("98944233647732219769177004876929019128417576295529901074099889598043702116001257856802131563896515373927712232092845883226922417596214"), BigInt("315019913931389607337177038330951043522456072897266928557328499619017160722351061360252776265186336876723201881398623946864393857820716"), BigInt("0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
    dl = BigInt("0xfffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f");
    Al = BigInt("0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141");
    wl = BigInt(1);
    ml = BigInt(2);
    bl = (e2, t2) => (e2 + t2 / ml) / t2;
    kl = Lh(dl, void 0, void 0, { sqrt: function(e2) {
      const t2 = dl, r2 = BigInt(3), n2 = BigInt(6), i2 = BigInt(11), s2 = BigInt(22), a2 = BigInt(23), o2 = BigInt(44), c2 = BigInt(88), h2 = e2 * e2 * e2 % t2, u2 = h2 * h2 * e2 % t2, l2 = Ph(u2, r2, t2) * u2 % t2, y2 = Ph(l2, r2, t2) * u2 % t2, f2 = Ph(y2, ml, t2) * h2 % t2, g2 = Ph(f2, i2, t2) * f2 % t2, p2 = Ph(g2, s2, t2) * g2 % t2, d2 = Ph(p2, o2, t2) * p2 % t2, A2 = Ph(d2, c2, t2) * d2 % t2, w2 = Ph(A2, o2, t2) * p2 % t2, m2 = Ph(w2, r2, t2) * u2 % t2, b2 = Ph(m2, a2, t2) * g2 % t2, k2 = Ph(b2, n2, t2) * h2 % t2, E2 = Ph(k2, ml, t2);
      if (!kl.eql(kl.sqr(E2), e2)) throw Error("Cannot find square root");
      return E2;
    } });
    El = ru({ a: BigInt(0), b: BigInt(7), Fp: kl, n: Al, Gx: BigInt("55066263022277343669578718895168534326250603453777594175500187360389116729240"), Gy: BigInt("32670510020758816978083085130507043184471273380659243275938904335757337482424"), h: BigInt(1), lowS: true, endo: { beta: BigInt("0x7ae96a2b657c07106e64479eac3434e99cf0497512f58995c1396c28719501ee"), splitScalar: (e2) => {
      const t2 = Al, r2 = BigInt("0x3086d221a7d46bcde86c90e49284eb15"), n2 = -wl * BigInt("0xe4437ed6010e88286f547fa90abfe4c3"), i2 = BigInt("0x114ca50f7a8e2f3f657c1108d9d44cfd8"), s2 = r2, a2 = BigInt("0x100000000000000000000000000000000"), o2 = bl(s2 * e2, t2), c2 = bl(-n2 * e2, t2);
      let h2 = Dh(e2 - o2 * r2 - c2 * i2, t2), u2 = Dh(-o2 * n2 - c2 * s2, t2);
      const l2 = h2 > a2, y2 = u2 > a2;
      if (l2 && (h2 = t2 - h2), y2 && (u2 = t2 - u2), h2 > a2 || u2 > a2) throw Error("splitScalar: Endomorphism failed, k=" + e2);
      return { k1neg: l2, k1: h2, k2neg: y2, k2: u2 };
    } } }, Hc);
    BigInt(0), El.ProjectivePoint;
    vl = Lh(BigInt("0xa9fb57dba1eea9bc3e660a909d838d726e3bf623d52620282013481d1f6e5377"));
    Il = ru({ a: vl.create(BigInt("0x7d5a0975fc2c3057eef67530417affe7fb8055c126dc5c6ce94a4b44f330b5d9")), b: BigInt("0x26dc5c6ce94a4b44f330b5d9bbd77cbf958416295cf7e1ce6bccdc18ff8c07b6"), Fp: vl, n: BigInt("0xa9fb57dba1eea9bc3e660a909d838d718c397aa3b561a6f7901e0e82974856a7"), Gx: BigInt("0x8bd2aeb9cb7e57cb2c4b482ffc81b7afb9de27e1e3bd23c23a4453bd9ace3262"), Gy: BigInt("0x547ef835c3dac4fd97f8461a14611dc9c27745132ded8e545c1d54c72f046997"), h: BigInt(1), lowS: false }, Hc);
    Bl = Lh(BigInt("0x8cb91e82a3386d280f5d6f7e50e641df152f7109ed5456b412b1da197fb71123acd3a729901d1a71874700133107ec53"));
    Sl = ru({ a: Bl.create(BigInt("0x7bc382c63d8c150c3c72080ace05afa0c2bea28e4fb22787139165efba91f90f8aa5814a503ad4eb04a8c7dd22ce2826")), b: BigInt("0x04a8c7dd22ce28268b39b55416f0447c2fb77de107dcd2a62e880ea53eeb62d57cb4390295dbc9943ab78696fa504c11"), Fp: Bl, n: BigInt("0x8cb91e82a3386d280f5d6f7e50e641df152f7109ed5456b31f166e6cac0425a7cf3ab6af6b7fc3103b883202e9046565"), Gx: BigInt("0x1d1c64f068cf45ffa2a63a81b7c13f6b8847a3e77ef14fe3db7fcafe0cbd10e8e826e03436d646aaef87b2e247d4af1e"), Gy: BigInt("0x8abe1d7520f9c2a45cb1eb8e95cfd55262b70b29feec5864e19c054ff99129280e4646217791811142820341263c5315"), h: BigInt(1), lowS: false }, ku);
    Kl = Lh(BigInt("0xaadd9db8dbe9c48b3fd4e6ae33c9fc07cb308db3b3c9d20ed6639cca703308717d4d9b009bc66842aecda12ae6a380e62881ff2f2d82c68528aa6056583a48f3"));
    Cl = ru({ a: Kl.create(BigInt("0x7830a3318b603b89e2327145ac234cc594cbdd8d3df91610a83441caea9863bc2ded5d5aa8253aa10a2ef1c98b9ac8b57f1117a72bf2c7b9e7c1ac4d77fc94ca")), b: BigInt("0x3df91610a83441caea9863bc2ded5d5aa8253aa10a2ef1c98b9ac8b57f1117a72bf2c7b9e7c1ac4d77fc94cadc083e67984050b75ebae5dd2809bd638016f723"), Fp: Kl, n: BigInt("0xaadd9db8dbe9c48b3fd4e6ae33c9fc07cb308db3b3c9d20ed6639cca70330870553e5c414ca92619418661197fac10471db1d381085ddaddb58796829ca90069"), Gx: BigInt("0x81aee4bdd82ed9645a21322e9c4c6a9385ed9f70b5d916c1b43b62eef4d0098eff3b1f78e2d0d48d50d1687b93b97d5f7c6d5047406a5e688b352209bcb9f822"), Gy: BigInt("0x7dde385d566332ecc0eabfa9cf7822fdf209f70024a57b1aa000c55b881f8111b2dcde494a5f485e5bca4bd88a2763aed1ca2b2fa8f0540678cd1e0f3ad80892"), h: BigInt(1), lowS: false }, bu);
    Dl = new Map(Object.entries({ nistP256: iu, nistP384: vu, nistP521: Su, brainpoolP256r1: Il, brainpoolP384r1: Sl, brainpoolP512r1: Cl, secp256k1: El, x448: pl, ed448: gl }));
    Ul = /* @__PURE__ */ Object.freeze({ __proto__: null, nobleCurves: Dl });
    Pl = /* @__PURE__ */ new Uint32Array([1732584193, 4023233417, 2562383102, 271733878, 3285377520]);
    xl = /* @__PURE__ */ new Uint32Array(80);
    Ql = class extends Tc {
      constructor() {
        super(64, 20, 8, false), this.A = 0 | Pl[0], this.B = 0 | Pl[1], this.C = 0 | Pl[2], this.D = 0 | Pl[3], this.E = 0 | Pl[4];
      }
      get() {
        const { A: e2, B: t2, C: r2, D: n2, E: i2 } = this;
        return [e2, t2, r2, n2, i2];
      }
      set(e2, t2, r2, n2, i2) {
        this.A = 0 | e2, this.B = 0 | t2, this.C = 0 | r2, this.D = 0 | n2, this.E = 0 | i2;
      }
      process(e2, t2) {
        for (let r3 = 0; r3 < 16; r3++, t2 += 4) xl[r3] = e2.getUint32(t2, false);
        for (let e3 = 16; e3 < 80; e3++) xl[e3] = Ic(xl[e3 - 3] ^ xl[e3 - 8] ^ xl[e3 - 14] ^ xl[e3 - 16], 1);
        let { A: r2, B: n2, C: i2, D: s2, E: a2 } = this;
        for (let e3 = 0; e3 < 80; e3++) {
          let t3, o2;
          e3 < 20 ? (t3 = Qc(n2, i2, s2), o2 = 1518500249) : e3 < 40 ? (t3 = n2 ^ i2 ^ s2, o2 = 1859775393) : e3 < 60 ? (t3 = Rc(n2, i2, s2), o2 = 2400959708) : (t3 = n2 ^ i2 ^ s2, o2 = 3395469782);
          const c2 = Ic(r2, 5) + t3 + a2 + o2 + xl[e3] | 0;
          a2 = s2, s2 = i2, i2 = Ic(n2, 30), n2 = r2, r2 = c2;
        }
        r2 = r2 + this.A | 0, n2 = n2 + this.B | 0, i2 = i2 + this.C | 0, s2 = s2 + this.D | 0, a2 = a2 + this.E | 0, this.set(r2, n2, i2, s2, a2);
      }
      roundClean() {
        xl.fill(0);
      }
      destroy() {
        this.set(0, 0, 0, 0, 0), this.buffer.fill(0);
      }
    };
    Rl = /* @__PURE__ */ Pc(() => new Ql());
    Tl = /* @__PURE__ */ new Uint8Array([7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8]);
    Ll = /* @__PURE__ */ new Uint8Array(Array(16).fill(0).map((e2, t2) => t2));
    Ml = /* @__PURE__ */ Ll.map((e2) => (9 * e2 + 5) % 16);
    Nl = [Ll];
    Fl = [Ml];
    for (let e2 = 0; e2 < 4; e2++) for (let t2 of [Nl, Fl]) t2.push(t2[e2].map((e3) => Tl[e3]));
    Ol = /* @__PURE__ */ [[11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8], [12, 13, 11, 15, 6, 9, 9, 7, 12, 15, 11, 13, 7, 8, 7, 7], [13, 15, 14, 11, 7, 7, 6, 8, 13, 14, 13, 12, 5, 5, 6, 9], [14, 11, 12, 14, 8, 6, 5, 5, 15, 12, 15, 14, 9, 9, 8, 6], [15, 12, 13, 13, 9, 5, 8, 6, 14, 11, 12, 11, 8, 6, 5, 5]].map((e2) => new Uint8Array(e2));
    Hl = /* @__PURE__ */ Nl.map((e2, t2) => e2.map((e3) => Ol[t2][e3]));
    zl = /* @__PURE__ */ Fl.map((e2, t2) => e2.map((e3) => Ol[t2][e3]));
    Gl = /* @__PURE__ */ new Uint32Array([0, 1518500249, 1859775393, 2400959708, 2840853838]);
    _l = /* @__PURE__ */ new Uint32Array([1352829926, 1548603684, 1836072691, 2053994217, 0]);
    ql = /* @__PURE__ */ new Uint32Array(16);
    Vl = class extends Tc {
      constructor() {
        super(64, 20, 8, true), this.h0 = 1732584193, this.h1 = -271733879, this.h2 = -1732584194, this.h3 = 271733878, this.h4 = -1009589776;
      }
      get() {
        const { h0: e2, h1: t2, h2: r2, h3: n2, h4: i2 } = this;
        return [e2, t2, r2, n2, i2];
      }
      set(e2, t2, r2, n2, i2) {
        this.h0 = 0 | e2, this.h1 = 0 | t2, this.h2 = 0 | r2, this.h3 = 0 | n2, this.h4 = 0 | i2;
      }
      process(e2, t2) {
        for (let r3 = 0; r3 < 16; r3++, t2 += 4) ql[r3] = e2.getUint32(t2, true);
        let r2 = 0 | this.h0, n2 = r2, i2 = 0 | this.h1, s2 = i2, a2 = 0 | this.h2, o2 = a2, c2 = 0 | this.h3, h2 = c2, u2 = 0 | this.h4, l2 = u2;
        for (let e3 = 0; e3 < 5; e3++) {
          const t3 = 4 - e3, y2 = Gl[e3], f2 = _l[e3], g2 = Nl[e3], p2 = Fl[e3], d2 = Hl[e3], A2 = zl[e3];
          for (let t4 = 0; t4 < 16; t4++) {
            const n3 = Ic(r2 + jl(e3, i2, a2, c2) + ql[g2[t4]] + y2, d2[t4]) + u2 | 0;
            r2 = u2, u2 = c2, c2 = 0 | Ic(a2, 10), a2 = i2, i2 = n3;
          }
          for (let e4 = 0; e4 < 16; e4++) {
            const r3 = Ic(n2 + jl(t3, s2, o2, h2) + ql[p2[e4]] + f2, A2[e4]) + l2 | 0;
            n2 = l2, l2 = h2, h2 = 0 | Ic(o2, 10), o2 = s2, s2 = r3;
          }
        }
        this.set(this.h1 + a2 + h2 | 0, this.h2 + c2 + l2 | 0, this.h3 + u2 + n2 | 0, this.h4 + r2 + s2 | 0, this.h0 + i2 + o2 | 0);
      }
      roundClean() {
        ql.fill(0);
      }
      destroy() {
        this.destroyed = true, this.buffer.fill(0), this.set(0, 0, 0, 0, 0);
      }
    };
    Jl = /* @__PURE__ */ Pc(() => new Vl());
    Yl = Array.from({ length: 64 }, (e2, t2) => Math.floor(2 ** 32 * Math.abs(Math.sin(t2 + 1))));
    Zl = (e2, t2, r2) => e2 & t2 ^ ~e2 & r2;
    Wl = /* @__PURE__ */ new Uint32Array([1732584193, 4023233417, 2562383102, 271733878]);
    $l = /* @__PURE__ */ new Uint32Array(16);
    Xl = class extends Tc {
      constructor() {
        super(64, 16, 8, true), this.A = 0 | Wl[0], this.B = 0 | Wl[1], this.C = 0 | Wl[2], this.D = 0 | Wl[3];
      }
      get() {
        const { A: e2, B: t2, C: r2, D: n2 } = this;
        return [e2, t2, r2, n2];
      }
      set(e2, t2, r2, n2) {
        this.A = 0 | e2, this.B = 0 | t2, this.C = 0 | r2, this.D = 0 | n2;
      }
      process(e2, t2) {
        for (let r3 = 0; r3 < 16; r3++, t2 += 4) $l[r3] = e2.getUint32(t2, true);
        let { A: r2, B: n2, C: i2, D: s2 } = this;
        for (let e3 = 0; e3 < 64; e3++) {
          let t3, a2, o2;
          e3 < 16 ? (t3 = Zl(n2, i2, s2), a2 = e3, o2 = [7, 12, 17, 22]) : e3 < 32 ? (t3 = Zl(s2, n2, i2), a2 = (5 * e3 + 1) % 16, o2 = [5, 9, 14, 20]) : e3 < 48 ? (t3 = n2 ^ i2 ^ s2, a2 = (3 * e3 + 5) % 16, o2 = [4, 11, 16, 23]) : (t3 = i2 ^ (n2 | ~s2), a2 = 7 * e3 % 16, o2 = [6, 10, 15, 21]), t3 = t3 + r2 + Yl[e3] + $l[a2], r2 = s2, s2 = i2, i2 = n2, n2 += Ic(t3, o2[e3 % 4]);
        }
        r2 = r2 + this.A | 0, n2 = n2 + this.B | 0, i2 = i2 + this.C | 0, s2 = s2 + this.D | 0, this.set(r2, n2, i2, s2);
      }
      roundClean() {
        $l.fill(0);
      }
      destroy() {
        this.set(0, 0, 0, 0), this.buffer.fill(0);
      }
    };
    ey = new Map(Object.entries({ md5: /* @__PURE__ */ Pc(() => new Xl()), sha1: Rl, sha224: zc, sha256: Hc, sha384: ku, sha512: bu, sha3_256: zu, sha3_512: Gu, ripemd160: Jl }));
    ty = /* @__PURE__ */ Object.freeze({ __proto__: null, nobleHashes: ey });
    iy.keySize = iy.prototype.keySize = 24, iy.blockSize = iy.prototype.blockSize = 8, ay.blockSize = ay.prototype.blockSize = 8, ay.keySize = ay.prototype.keySize = 16;
    oy = 4294967295;
    yy.keySize = yy.prototype.keySize = 32, yy.blockSize = yy.prototype.blockSize = 16, fy.prototype.BLOCKSIZE = 8, fy.prototype.SBOXES = [[3509652390, 2564797868, 805139163, 3491422135, 3101798381, 1780907670, 3128725573, 4046225305, 614570311, 3012652279, 134345442, 2240740374, 1667834072, 1901547113, 2757295779, 4103290238, 227898511, 1921955416, 1904987480, 2182433518, 2069144605, 3260701109, 2620446009, 720527379, 3318853667, 677414384, 3393288472, 3101374703, 2390351024, 1614419982, 1822297739, 2954791486, 3608508353, 3174124327, 2024746970, 1432378464, 3864339955, 2857741204, 1464375394, 1676153920, 1439316330, 715854006, 3033291828, 289532110, 2706671279, 2087905683, 3018724369, 1668267050, 732546397, 1947742710, 3462151702, 2609353502, 2950085171, 1814351708, 2050118529, 680887927, 999245976, 1800124847, 3300911131, 1713906067, 1641548236, 4213287313, 1216130144, 1575780402, 4018429277, 3917837745, 3693486850, 3949271944, 596196993, 3549867205, 258830323, 2213823033, 772490370, 2760122372, 1774776394, 2652871518, 566650946, 4142492826, 1728879713, 2882767088, 1783734482, 3629395816, 2517608232, 2874225571, 1861159788, 326777828, 3124490320, 2130389656, 2716951837, 967770486, 1724537150, 2185432712, 2364442137, 1164943284, 2105845187, 998989502, 3765401048, 2244026483, 1075463327, 1455516326, 1322494562, 910128902, 469688178, 1117454909, 936433444, 3490320968, 3675253459, 1240580251, 122909385, 2157517691, 634681816, 4142456567, 3825094682, 3061402683, 2540495037, 79693498, 3249098678, 1084186820, 1583128258, 426386531, 1761308591, 1047286709, 322548459, 995290223, 1845252383, 2603652396, 3431023940, 2942221577, 3202600964, 3727903485, 1712269319, 422464435, 3234572375, 1170764815, 3523960633, 3117677531, 1434042557, 442511882, 3600875718, 1076654713, 1738483198, 4213154764, 2393238008, 3677496056, 1014306527, 4251020053, 793779912, 2902807211, 842905082, 4246964064, 1395751752, 1040244610, 2656851899, 3396308128, 445077038, 3742853595, 3577915638, 679411651, 2892444358, 2354009459, 1767581616, 3150600392, 3791627101, 3102740896, 284835224, 4246832056, 1258075500, 768725851, 2589189241, 3069724005, 3532540348, 1274779536, 3789419226, 2764799539, 1660621633, 3471099624, 4011903706, 913787905, 3497959166, 737222580, 2514213453, 2928710040, 3937242737, 1804850592, 3499020752, 2949064160, 2386320175, 2390070455, 2415321851, 4061277028, 2290661394, 2416832540, 1336762016, 1754252060, 3520065937, 3014181293, 791618072, 3188594551, 3933548030, 2332172193, 3852520463, 3043980520, 413987798, 3465142937, 3030929376, 4245938359, 2093235073, 3534596313, 375366246, 2157278981, 2479649556, 555357303, 3870105701, 2008414854, 3344188149, 4221384143, 3956125452, 2067696032, 3594591187, 2921233993, 2428461, 544322398, 577241275, 1471733935, 610547355, 4027169054, 1432588573, 1507829418, 2025931657, 3646575487, 545086370, 48609733, 2200306550, 1653985193, 298326376, 1316178497, 3007786442, 2064951626, 458293330, 2589141269, 3591329599, 3164325604, 727753846, 2179363840, 146436021, 1461446943, 4069977195, 705550613, 3059967265, 3887724982, 4281599278, 3313849956, 1404054877, 2845806497, 146425753, 1854211946], [1266315497, 3048417604, 3681880366, 3289982499, 290971e4, 1235738493, 2632868024, 2414719590, 3970600049, 1771706367, 1449415276, 3266420449, 422970021, 1963543593, 2690192192, 3826793022, 1062508698, 1531092325, 1804592342, 2583117782, 2714934279, 4024971509, 1294809318, 4028980673, 1289560198, 2221992742, 1669523910, 35572830, 157838143, 1052438473, 1016535060, 1802137761, 1753167236, 1386275462, 3080475397, 2857371447, 1040679964, 2145300060, 2390574316, 1461121720, 2956646967, 4031777805, 4028374788, 33600511, 2920084762, 1018524850, 629373528, 3691585981, 3515945977, 2091462646, 2486323059, 586499841, 988145025, 935516892, 3367335476, 2599673255, 2839830854, 265290510, 3972581182, 2759138881, 3795373465, 1005194799, 847297441, 406762289, 1314163512, 1332590856, 1866599683, 4127851711, 750260880, 613907577, 1450815602, 3165620655, 3734664991, 3650291728, 3012275730, 3704569646, 1427272223, 778793252, 1343938022, 2676280711, 2052605720, 1946737175, 3164576444, 3914038668, 3967478842, 3682934266, 1661551462, 3294938066, 4011595847, 840292616, 3712170807, 616741398, 312560963, 711312465, 1351876610, 322626781, 1910503582, 271666773, 2175563734, 1594956187, 70604529, 3617834859, 1007753275, 1495573769, 4069517037, 2549218298, 2663038764, 504708206, 2263041392, 3941167025, 2249088522, 1514023603, 1998579484, 1312622330, 694541497, 2582060303, 2151582166, 1382467621, 776784248, 2618340202, 3323268794, 2497899128, 2784771155, 503983604, 4076293799, 907881277, 423175695, 432175456, 1378068232, 4145222326, 3954048622, 3938656102, 3820766613, 2793130115, 2977904593, 26017576, 3274890735, 3194772133, 1700274565, 1756076034, 4006520079, 3677328699, 720338349, 1533947780, 354530856, 688349552, 3973924725, 1637815568, 332179504, 3949051286, 53804574, 2852348879, 3044236432, 1282449977, 3583942155, 3416972820, 4006381244, 1617046695, 2628476075, 3002303598, 1686838959, 431878346, 2686675385, 1700445008, 1080580658, 1009431731, 832498133, 3223435511, 2605976345, 2271191193, 2516031870, 1648197032, 4164389018, 2548247927, 300782431, 375919233, 238389289, 3353747414, 2531188641, 2019080857, 1475708069, 455242339, 2609103871, 448939670, 3451063019, 1395535956, 2413381860, 1841049896, 1491858159, 885456874, 4264095073, 4001119347, 1565136089, 3898914787, 1108368660, 540939232, 1173283510, 2745871338, 3681308437, 4207628240, 3343053890, 4016749493, 1699691293, 1103962373, 3625875870, 2256883143, 3830138730, 1031889488, 3479347698, 1535977030, 4236805024, 3251091107, 2132092099, 1774941330, 1199868427, 1452454533, 157007616, 2904115357, 342012276, 595725824, 1480756522, 206960106, 497939518, 591360097, 863170706, 2375253569, 3596610801, 1814182875, 2094937945, 3421402208, 1082520231, 3463918190, 2785509508, 435703966, 3908032597, 1641649973, 2842273706, 3305899714, 1510255612, 2148256476, 2655287854, 3276092548, 4258621189, 236887753, 3681803219, 274041037, 1734335097, 3815195456, 3317970021, 1899903192, 1026095262, 4050517792, 356393447, 2410691914, 3873677099, 3682840055], [3913112168, 2491498743, 4132185628, 2489919796, 1091903735, 1979897079, 3170134830, 3567386728, 3557303409, 857797738, 1136121015, 1342202287, 507115054, 2535736646, 337727348, 3213592640, 1301675037, 2528481711, 1895095763, 1721773893, 3216771564, 62756741, 2142006736, 835421444, 2531993523, 1442658625, 3659876326, 2882144922, 676362277, 1392781812, 170690266, 3921047035, 1759253602, 3611846912, 1745797284, 664899054, 1329594018, 3901205900, 3045908486, 2062866102, 2865634940, 3543621612, 3464012697, 1080764994, 553557557, 3656615353, 3996768171, 991055499, 499776247, 1265440854, 648242737, 3940784050, 980351604, 3713745714, 1749149687, 3396870395, 4211799374, 3640570775, 1161844396, 3125318951, 1431517754, 545492359, 4268468663, 3499529547, 1437099964, 2702547544, 3433638243, 2581715763, 2787789398, 1060185593, 1593081372, 2418618748, 4260947970, 69676912, 2159744348, 86519011, 2512459080, 3838209314, 1220612927, 3339683548, 133810670, 1090789135, 1078426020, 1569222167, 845107691, 3583754449, 4072456591, 1091646820, 628848692, 1613405280, 3757631651, 526609435, 236106946, 48312990, 2942717905, 3402727701, 1797494240, 859738849, 992217954, 4005476642, 2243076622, 3870952857, 3732016268, 765654824, 3490871365, 2511836413, 1685915746, 3888969200, 1414112111, 2273134842, 3281911079, 4080962846, 172450625, 2569994100, 980381355, 4109958455, 2819808352, 2716589560, 2568741196, 3681446669, 3329971472, 1835478071, 660984891, 3704678404, 4045999559, 3422617507, 3040415634, 1762651403, 1719377915, 3470491036, 2693910283, 3642056355, 3138596744, 1364962596, 2073328063, 1983633131, 926494387, 3423689081, 2150032023, 4096667949, 1749200295, 3328846651, 309677260, 2016342300, 1779581495, 3079819751, 111262694, 1274766160, 443224088, 298511866, 1025883608, 3806446537, 1145181785, 168956806, 3641502830, 3584813610, 1689216846, 3666258015, 3200248200, 1692713982, 2646376535, 4042768518, 1618508792, 1610833997, 3523052358, 4130873264, 2001055236, 3610705100, 2202168115, 4028541809, 2961195399, 1006657119, 2006996926, 3186142756, 1430667929, 3210227297, 1314452623, 4074634658, 4101304120, 2273951170, 1399257539, 3367210612, 3027628629, 1190975929, 2062231137, 2333990788, 2221543033, 2438960610, 1181637006, 548689776, 2362791313, 3372408396, 3104550113, 3145860560, 296247880, 1970579870, 3078560182, 3769228297, 1714227617, 3291629107, 3898220290, 166772364, 1251581989, 493813264, 448347421, 195405023, 2709975567, 677966185, 3703036547, 1463355134, 2715995803, 1338867538, 1343315457, 2802222074, 2684532164, 233230375, 2599980071, 2000651841, 3277868038, 1638401717, 4028070440, 3237316320, 6314154, 819756386, 300326615, 590932579, 1405279636, 3267499572, 3150704214, 2428286686, 3959192993, 3461946742, 1862657033, 1266418056, 963775037, 2089974820, 2263052895, 1917689273, 448879540, 3550394620, 3981727096, 150775221, 3627908307, 1303187396, 508620638, 2975983352, 2726630617, 1817252668, 1876281319, 1457606340, 908771278, 3720792119, 3617206836, 2455994898, 1729034894, 1080033504], [976866871, 3556439503, 2881648439, 1522871579, 1555064734, 1336096578, 3548522304, 2579274686, 3574697629, 3205460757, 3593280638, 3338716283, 3079412587, 564236357, 2993598910, 1781952180, 1464380207, 3163844217, 3332601554, 1699332808, 1393555694, 1183702653, 3581086237, 1288719814, 691649499, 2847557200, 2895455976, 3193889540, 2717570544, 1781354906, 1676643554, 2592534050, 3230253752, 1126444790, 2770207658, 2633158820, 2210423226, 2615765581, 2414155088, 3127139286, 673620729, 2805611233, 1269405062, 4015350505, 3341807571, 4149409754, 1057255273, 2012875353, 2162469141, 2276492801, 2601117357, 993977747, 3918593370, 2654263191, 753973209, 36408145, 2530585658, 25011837, 3520020182, 2088578344, 530523599, 2918365339, 1524020338, 1518925132, 3760827505, 3759777254, 1202760957, 3985898139, 3906192525, 674977740, 4174734889, 2031300136, 2019492241, 3983892565, 4153806404, 3822280332, 352677332, 2297720250, 60907813, 90501309, 3286998549, 1016092578, 2535922412, 2839152426, 457141659, 509813237, 4120667899, 652014361, 1966332200, 2975202805, 55981186, 2327461051, 676427537, 3255491064, 2882294119, 3433927263, 1307055953, 942726286, 933058658, 2468411793, 3933900994, 4215176142, 1361170020, 2001714738, 2830558078, 3274259782, 1222529897, 1679025792, 2729314320, 3714953764, 1770335741, 151462246, 3013232138, 1682292957, 1483529935, 471910574, 1539241949, 458788160, 3436315007, 1807016891, 3718408830, 978976581, 1043663428, 3165965781, 1927990952, 4200891579, 2372276910, 3208408903, 3533431907, 1412390302, 2931980059, 4132332400, 1947078029, 3881505623, 4168226417, 2941484381, 1077988104, 1320477388, 886195818, 18198404, 3786409e3, 2509781533, 112762804, 3463356488, 1866414978, 891333506, 18488651, 661792760, 1628790961, 3885187036, 3141171499, 876946877, 2693282273, 1372485963, 791857591, 2686433993, 3759982718, 3167212022, 3472953795, 2716379847, 445679433, 3561995674, 3504004811, 3574258232, 54117162, 3331405415, 2381918588, 3769707343, 4154350007, 1140177722, 4074052095, 668550556, 3214352940, 367459370, 261225585, 2610173221, 4209349473, 3468074219, 3265815641, 314222801, 3066103646, 3808782860, 282218597, 3406013506, 3773591054, 379116347, 1285071038, 846784868, 2669647154, 3771962079, 3550491691, 2305946142, 453669953, 1268987020, 3317592352, 3279303384, 3744833421, 2610507566, 3859509063, 266596637, 3847019092, 517658769, 3462560207, 3443424879, 370717030, 4247526661, 2224018117, 4143653529, 4112773975, 2788324899, 2477274417, 1456262402, 2901442914, 1517677493, 1846949527, 2295493580, 3734397586, 2176403920, 1280348187, 1908823572, 3871786941, 846861322, 1172426758, 3287448474, 3383383037, 1655181056, 3139813346, 901632758, 1897031941, 2986607138, 3066810236, 3447102507, 1393639104, 373351379, 950779232, 625454576, 3124240540, 4148612726, 2007998917, 544563296, 2244738638, 2330496472, 2058025392, 1291430526, 424198748, 50039436, 29584100, 3605783033, 2429876329, 2791104160, 1057563949, 3255363231, 3075367218, 3463963227, 1469046755, 985887462]], fy.prototype.PARRAY = [608135816, 2242054355, 320440878, 57701188, 2752067618, 698298832, 137296536, 3964562569, 1160258022, 953160567, 3193202383, 887688300, 3232508343, 3380367581, 1065670069, 3041331479, 2450970073, 2306472731], fy.prototype.NN = 16, fy.prototype._clean = function(e2) {
      if (e2 < 0) {
        e2 = (2147483647 & e2) + 2147483648;
      }
      return e2;
    }, fy.prototype._F = function(e2) {
      let t2;
      const r2 = 255 & e2, n2 = 255 & (e2 >>>= 8), i2 = 255 & (e2 >>>= 8), s2 = 255 & (e2 >>>= 8);
      return t2 = this.sboxes[0][s2] + this.sboxes[1][i2], t2 ^= this.sboxes[2][n2], t2 += this.sboxes[3][r2], t2;
    }, fy.prototype._encryptBlock = function(e2) {
      let t2, r2 = e2[0], n2 = e2[1];
      for (t2 = 0; t2 < this.NN; ++t2) {
        r2 ^= this.parray[t2], n2 = this._F(r2) ^ n2;
        const e3 = r2;
        r2 = n2, n2 = e3;
      }
      r2 ^= this.parray[this.NN + 0], n2 ^= this.parray[this.NN + 1], e2[0] = this._clean(n2), e2[1] = this._clean(r2);
    }, fy.prototype.encryptBlock = function(e2) {
      let t2;
      const r2 = [0, 0], n2 = this.BLOCKSIZE / 2;
      for (t2 = 0; t2 < this.BLOCKSIZE / 2; ++t2) r2[0] = r2[0] << 8 | 255 & e2[t2 + 0], r2[1] = r2[1] << 8 | 255 & e2[t2 + n2];
      this._encryptBlock(r2);
      const i2 = [];
      for (t2 = 0; t2 < this.BLOCKSIZE / 2; ++t2) i2[t2 + 0] = r2[0] >>> 24 - 8 * t2 & 255, i2[t2 + n2] = r2[1] >>> 24 - 8 * t2 & 255;
      return i2;
    }, fy.prototype._decryptBlock = function(e2) {
      let t2, r2 = e2[0], n2 = e2[1];
      for (t2 = this.NN + 1; t2 > 1; --t2) {
        r2 ^= this.parray[t2], n2 = this._F(r2) ^ n2;
        const e3 = r2;
        r2 = n2, n2 = e3;
      }
      r2 ^= this.parray[1], n2 ^= this.parray[0], e2[0] = this._clean(n2), e2[1] = this._clean(r2);
    }, fy.prototype.init = function(e2) {
      let t2, r2 = 0;
      for (this.parray = [], t2 = 0; t2 < this.NN + 2; ++t2) {
        let n3 = 0;
        for (let t3 = 0; t3 < 4; ++t3) n3 = n3 << 8 | 255 & e2[r2], ++r2 >= e2.length && (r2 = 0);
        this.parray[t2] = this.PARRAY[t2] ^ n3;
      }
      for (this.sboxes = [], t2 = 0; t2 < 4; ++t2) for (this.sboxes[t2] = [], r2 = 0; r2 < 256; ++r2) this.sboxes[t2][r2] = this.SBOXES[t2][r2];
      const n2 = [0, 0];
      for (t2 = 0; t2 < this.NN + 2; t2 += 2) this._encryptBlock(n2), this.parray[t2 + 0] = n2[0], this.parray[t2 + 1] = n2[1];
      for (t2 = 0; t2 < 4; ++t2) for (r2 = 0; r2 < 256; r2 += 2) this._encryptBlock(n2), this.sboxes[t2][r2 + 0] = n2[0], this.sboxes[t2][r2 + 1] = n2[1];
    }, gy.keySize = gy.prototype.keySize = 16, gy.blockSize = gy.prototype.blockSize = 8;
    py = new Map(Object.entries({ tripledes: iy, cast5: ay, twofish: yy, blowfish: gy }));
    dy = /* @__PURE__ */ Object.freeze({ __proto__: null, legacyCiphers: py });
    by = new Uint32Array([4089235720, 1779033703, 2227873595, 3144134277, 4271175723, 1013904242, 1595750129, 2773480762, 2917565137, 1359893119, 725511199, 2600822924, 4215389547, 528734635, 327033209, 1541459225]);
    ky = new Uint8Array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3, 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4, 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8, 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13, 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9, 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11, 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10, 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5, 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3].map((e2) => 2 * e2));
    vy = class {
      constructor(e2, t2, r2, n2) {
        const i2 = new Uint8Array(64);
        this.S = { b: new Uint8Array(Sy), h: new Uint32Array(By / 4), t0: new Uint32Array(2), c: 0, outlen: e2 }, i2[0] = e2, t2 && (i2[1] = t2.length), i2[2] = 1, i2[3] = 1, r2 && i2.set(r2, 32), n2 && i2.set(n2, 48);
        const s2 = new Uint32Array(i2.buffer, i2.byteOffset, i2.length / Uint32Array.BYTES_PER_ELEMENT);
        for (let e3 = 0; e3 < 16; e3++) this.S.h[e3] = by[e3] ^ s2[e3];
        if (t2) {
          const e3 = new Uint8Array(Sy);
          e3.set(t2), this.update(e3);
        }
      }
      update(e2) {
        if (!(e2 instanceof Uint8Array)) throw Error("Input must be Uint8Array or Buffer");
        let t2 = 0;
        for (; t2 < e2.length; ) {
          this.S.c === Sy && (wy(this.S.t0, this.S.c), Ey(this.S, false), this.S.c = 0);
          let r2 = Sy - this.S.c;
          this.S.b.set(e2.subarray(t2, t2 + r2), this.S.c);
          const n2 = Math.min(r2, e2.length - t2);
          this.S.c += n2, t2 += n2;
        }
        return this;
      }
      digest(e2) {
        wy(this.S.t0, this.S.c), this.S.b.fill(0, this.S.c), this.S.c = Sy, Ey(this.S, true);
        const t2 = e2 || new Uint8Array(this.S.outlen);
        for (let e3 = 0; e3 < this.S.outlen; e3++) t2[e3] = this.S.h[e3 >> 2] >> 8 * (3 & e3);
        return this.S.h = null, t2.buffer;
      }
    };
    By = 64;
    Sy = 128;
    Ky = 2;
    Cy = 19;
    Dy = 4294967295;
    Uy = 4;
    Py = 4294967295;
    xy = 8;
    Qy = 4294967295;
    Ry = 8;
    Ty = 4294967295;
    Ly = 4294967295;
    My = 32;
    Ny = 1024;
    Fy = 64;
    Oy = 205 === new Uint8Array(new Uint16Array([43981]).buffer)[0];
    Jy = 1024;
    Yy = 64 * Jy;
    ef = /* @__PURE__ */ Object.freeze({ __proto__: null, default: async () => $y((e2) => Xy(0, 0, "AGFzbQEAAAABKwdgBH9/f38AYAABf2AAAGADf39/AGAJf39/f39/f39/AX9gAX8AYAF/AX8CEwEDZW52Bm1lbW9yeQIBkAiAgAQDCgkCAwAABAEFBgEEBQFwAQICBgkBfwFBkIjAAgsHfQoDeG9yAAEBRwACAkcyAAMFZ2V0TFoABBlfX2luZGlyZWN0X2Z1bmN0aW9uX3RhYmxlAQALX2luaXRpYWxpemUAABBfX2Vycm5vX2xvY2F0aW9uAAgJc3RhY2tTYXZlAAUMc3RhY2tSZXN0b3JlAAYKc3RhY2tBbGxvYwAHCQcBAEEBCwEACs0gCQMAAQtYAQJ/A0AgACAEQQR0IgNqIAIgA2r9AAQAIAEgA2r9AAQA/VH9CwQAIAAgA0EQciIDaiACIANq/QAEACABIANq/QAEAP1R/QsEACAEQQJqIgRBwABHDQALC7ceAgt7A38DQCADIBFBBHQiD2ogASAPav0ABAAgACAPav0ABAD9USIF/QsEACACIA9qIAX9CwQAIAMgD0EQciIPaiABIA9q/QAEACAAIA9q/QAEAP1RIgX9CwQAIAIgD2ogBf0LBAAgEUECaiIRQcAARw0ACwNAIAMgEEEHdGoiAEEQaiAA/QAEcCAA/QAEMCIFIAD9AAQQIgT9zgEgBSAF/Q0AAQIDCAkKCwABAgMICQoLIAQgBP0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgT9USIJQSD9ywEgCUEg/c0B/VAiCSAA/QAEUCIG/c4BIAkgCf0NAAECAwgJCgsAAQIDCAkKCyAGIAb9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIGIAX9USIFQSj9ywEgBUEY/c0B/VAiCCAE/c4BIAggCP0NAAECAwgJCgsAAQIDCAkKCyAEIAT9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIKIAogCf1RIgVBMP3LASAFQRD9zQH9UCIFIAb9zgEgBSAF/Q0AAQIDCAkKCwABAgMICQoLIAYgBv0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgkgCP1RIgRBAf3LASAEQT/9zQH9UCIMIAD9AARgIAD9AAQgIgQgAP0ABAAiBv3OASAEIAT9DQABAgMICQoLAAECAwgJCgsgBiAG/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiBv1RIghBIP3LASAIQSD9zQH9UCIIIABBQGsiAf0ABAAiB/3OASAIIAj9DQABAgMICQoLAAECAwgJCgsgByAH/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiByAE/VEiBEEo/csBIARBGP3NAf1QIgsgBv3OASALIAv9DQABAgMICQoLAAECAwgJCgsgBiAG/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiBiAI/VEiBEEw/csBIARBEP3NAf1QIgQgB/3OASAEIAT9DQABAgMICQoLAAECAwgJCgsgByAH/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiCCAL/VEiB0EB/csBIAdBP/3NAf1QIg0gDf0NAAECAwQFBgcQERITFBUWF/0NCAkKCwwNDg8YGRobHB0eHyIH/c4BIAcgB/0NAAECAwgJCgsAAQIDCAkKCyAKIAr9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIKIAQgBSAF/Q0AAQIDBAUGBxAREhMUFRYX/Q0ICQoLDA0ODxgZGhscHR4f/VEiC0Eg/csBIAtBIP3NAf1QIgsgCP3OASALIAv9DQABAgMICQoLAAECAwgJCgsgCCAI/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiCCAH/VEiB0Eo/csBIAdBGP3NAf1QIgcgCv3OASAHIAf9DQABAgMICQoLAAECAwgJCgsgCiAK/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiDv0LBAAgACAGIA0gDCAM/Q0AAQIDBAUGBxAREhMUFRYX/Q0ICQoLDA0ODxgZGhscHR4fIgr9zgEgCiAK/Q0AAQIDCAkKCwABAgMICQoLIAYgBv0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgYgBSAEIAT9DQABAgMEBQYHEBESExQVFhf9DQgJCgsMDQ4PGBkaGxwdHh/9USIFQSD9ywEgBUEg/c0B/VAiBSAJ/c4BIAUgBf0NAAECAwgJCgsAAQIDCAkKCyAJIAn9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIJIAr9USIEQSj9ywEgBEEY/c0B/VAiCiAG/c4BIAogCv0NAAECAwgJCgsAAQIDCAkKCyAGIAb9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIE/QsEACAAIAQgBf1RIgVBMP3LASAFQRD9zQH9UCIFIA4gC/1RIgRBMP3LASAEQRD9zQH9UCIEIAT9DQABAgMEBQYHEBESExQVFhf9DQgJCgsMDQ4PGBkaGxwdHh/9CwRgIAAgBCAFIAX9DQABAgMEBQYHEBESExQVFhf9DQgJCgsMDQ4PGBkaGxwdHh/9CwRwIAEgBCAI/c4BIAQgBP0NAAECAwgJCgsAAQIDCAkKCyAIIAj9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIE/QsEACAAIAUgCf3OASAFIAX9DQABAgMICQoLAAECAwgJCgsgCSAJ/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiCf0LBFAgACAEIAf9USIFQQH9ywEgBUE//c0B/VAiBSAJIAr9USIEQQH9ywEgBEE//c0B/VAiBCAE/Q0AAQIDBAUGBxAREhMUFRYX/Q0ICQoLDA0ODxgZGhscHR4f/QsEICAAIAQgBSAF/Q0AAQIDBAUGBxAREhMUFRYX/Q0ICQoLDA0ODxgZGhscHR4f/QsEMCAQQQFqIhBBCEcNAAtBACEQA0AgAyAQQQR0aiIAQYABaiAA/QAEgAcgAP0ABIADIgUgAP0ABIABIgT9zgEgBSAF/Q0AAQIDCAkKCwABAgMICQoLIAQgBP0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgT9USIJQSD9ywEgCUEg/c0B/VAiCSAA/QAEgAUiBv3OASAJIAn9DQABAgMICQoLAAECAwgJCgsgBiAG/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiBiAF/VEiBUEo/csBIAVBGP3NAf1QIgggBP3OASAIIAj9DQABAgMICQoLAAECAwgJCgsgBCAE/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiCiAKIAn9USIFQTD9ywEgBUEQ/c0B/VAiBSAG/c4BIAUgBf0NAAECAwgJCgsAAQIDCAkKCyAGIAb9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIJIAj9USIEQQH9ywEgBEE//c0B/VAiDCAA/QAEgAYgAP0ABIACIgQgAP0ABAAiBv3OASAEIAT9DQABAgMICQoLAAECAwgJCgsgBiAG/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiBv1RIghBIP3LASAIQSD9zQH9UCIIIAD9AASABCIH/c4BIAggCP0NAAECAwgJCgsAAQIDCAkKCyAHIAf9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIHIAT9USIEQSj9ywEgBEEY/c0B/VAiCyAG/c4BIAsgC/0NAAECAwgJCgsAAQIDCAkKCyAGIAb9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIGIAj9USIEQTD9ywEgBEEQ/c0B/VAiBCAH/c4BIAQgBP0NAAECAwgJCgsAAQIDCAkKCyAHIAf9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIIIAv9USIHQQH9ywEgB0E//c0B/VAiDSAN/Q0AAQIDBAUGBxAREhMUFRYX/Q0ICQoLDA0ODxgZGhscHR4fIgf9zgEgByAH/Q0AAQIDCAkKCwABAgMICQoLIAogCv0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgogBCAFIAX9DQABAgMEBQYHEBESExQVFhf9DQgJCgsMDQ4PGBkaGxwdHh/9USILQSD9ywEgC0Eg/c0B/VAiCyAI/c4BIAsgC/0NAAECAwgJCgsAAQIDCAkKCyAIIAj9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIIIAf9USIHQSj9ywEgB0EY/c0B/VAiByAK/c4BIAcgB/0NAAECAwgJCgsAAQIDCAkKCyAKIAr9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIO/QsEACAAIAYgDSAMIAz9DQABAgMEBQYHEBESExQVFhf9DQgJCgsMDQ4PGBkaGxwdHh8iCv3OASAKIAr9DQABAgMICQoLAAECAwgJCgsgBiAG/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiBiAFIAQgBP0NAAECAwQFBgcQERITFBUWF/0NCAkKCwwNDg8YGRobHB0eH/1RIgVBIP3LASAFQSD9zQH9UCIFIAn9zgEgBSAF/Q0AAQIDCAkKCwABAgMICQoLIAkgCf0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgkgCv1RIgRBKP3LASAEQRj9zQH9UCIKIAb9zgEgCiAK/Q0AAQIDCAkKCwABAgMICQoLIAYgBv0NAAECAwgJCgsAAQIDCAkKC/3eAUEB/csB/c4BIgT9CwQAIAAgBCAF/VEiBUEw/csBIAVBEP3NAf1QIgUgDiAL/VEiBEEw/csBIARBEP3NAf1QIgQgBP0NAAECAwQFBgcQERITFBUWF/0NCAkKCwwNDg8YGRobHB0eH/0LBIAGIAAgBCAFIAX9DQABAgMEBQYHEBESExQVFhf9DQgJCgsMDQ4PGBkaGxwdHh/9CwSAByAAIAQgCP3OASAEIAT9DQABAgMICQoLAAECAwgJCgsgCCAI/Q0AAQIDCAkKCwABAgMICQoL/d4BQQH9ywH9zgEiBP0LBIAEIAAgBSAJ/c4BIAUgBf0NAAECAwgJCgsAAQIDCAkKCyAJIAn9DQABAgMICQoLAAECAwgJCgv93gFBAf3LAf3OASIJ/QsEgAUgACAEIAf9USIFQQH9ywEgBUE//c0B/VAiBSAJIAr9USIEQQH9ywEgBEE//c0B/VAiBCAE/Q0AAQIDBAUGBxAREhMUFRYX/Q0ICQoLDA0ODxgZGhscHR4f/QsEgAIgACAEIAUgBf0NAAECAwQFBgcQERITFBUWF/0NCAkKCwwNDg8YGRobHB0eH/0LBIADIBBBAWoiEEEIRw0AC0EAIRADQCACIBBBBHQiAGoiASAAIANq/QAEACAB/QAEAP1R/QsEACACIABBEHIiAWoiDyABIANq/QAEACAP/QAEAP1R/QsEACACIABBIHIiAWoiDyABIANq/QAEACAP/QAEAP1R/QsEACACIABBMHIiAGoiASAAIANq/QAEACAB/QAEAP1R/QsEACAQQQRqIhBBwABHDQALCxYAIAAgASACIAMQAiAAIAIgAiADEAILewIBfwF+IAIhCSABNQIAIQogBCAFcgRAIAEoAgQgA3AhCQsgACAJNgIAIAAgB0EBayAFIAQbIAhsIAZBAWtBAEF/IAYbIAIgCUYbaiIBIAVBAWogCGxBACAEG2ogAa0gCiAKfkIgiH5CIIinQX9zaiAHIAhscDYCBCAACwQAIwALBgAgACQACxAAIwAgAGtBcHEiACQAIAALBQBBgAgL", e2), (e2) => Xy(0, 0, "AGFzbQEAAAABPwhgBH9/f38AYAABf2AAAGADf39/AGARf39/f39/f39/f39/f39/f38AYAl/f39/f39/f38Bf2ABfwBgAX8BfwITAQNlbnYGbWVtb3J5AgGQCICABAMLCgIDBAAABQEGBwEEBQFwAQICBgkBfwFBkIjAAgsHfQoDeG9yAAEBRwADAkcyAAQFZ2V0TFoABRlfX2luZGlyZWN0X2Z1bmN0aW9uX3RhYmxlAQALX2luaXRpYWxpemUAABBfX2Vycm5vX2xvY2F0aW9uAAkJc3RhY2tTYXZlAAYMc3RhY2tSZXN0b3JlAAcKc3RhY2tBbGxvYwAICQcBAEEBCwEACssaCgMAAQtQAQJ/A0AgACAEQQN0IgNqIAIgA2opAwAgASADaikDAIU3AwAgACADQQhyIgNqIAIgA2opAwAgASADaikDAIU3AwAgBEECaiIEQYABRw0ACwveDwICfgF/IAAgAUEDdGoiEyATKQMAIhEgACAFQQN0aiIBKQMAIhJ8IBFCAYZC/v///x+DIBJC/////w+DfnwiETcDACAAIA1BA3RqIgUgESAFKQMAhUIgiSIRNwMAIAAgCUEDdGoiCSARIAkpAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAEgESABKQMAhUIoiSIRNwMAIBMgESATKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACAFIBEgBSkDAIVCMIkiETcDACAJIBEgCSkDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgASARIAEpAwCFQgGJNwMAIAAgAkEDdGoiDSANKQMAIhEgACAGQQN0aiICKQMAIhJ8IBFCAYZC/v///x+DIBJC/////w+DfnwiETcDACAAIA5BA3RqIgYgESAGKQMAhUIgiSIRNwMAIAAgCkEDdGoiCiARIAopAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAIgESACKQMAhUIoiSIRNwMAIA0gESANKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACAGIBEgBikDAIVCMIkiETcDACAKIBEgCikDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgAiARIAIpAwCFQgGJNwMAIAAgA0EDdGoiDiAOKQMAIhEgACAHQQN0aiIDKQMAIhJ8IBFCAYZC/v///x+DIBJC/////w+DfnwiETcDACAAIA9BA3RqIgcgESAHKQMAhUIgiSIRNwMAIAAgC0EDdGoiCyARIAspAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAMgESADKQMAhUIoiSIRNwMAIA4gESAOKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACAHIBEgBykDAIVCMIkiETcDACALIBEgCykDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgAyARIAMpAwCFQgGJNwMAIAAgBEEDdGoiDyAPKQMAIhEgACAIQQN0aiIEKQMAIhJ8IBFCAYZC/v///x+DIBJC/////w+DfnwiETcDACAAIBBBA3RqIgggESAIKQMAhUIgiSIRNwMAIAAgDEEDdGoiACARIAApAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAQgESAEKQMAhUIoiSIRNwMAIA8gESAPKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACAIIBEgCCkDAIVCMIkiETcDACAAIBEgACkDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgBCARIAQpAwCFQgGJNwMAIBMgEykDACIRIAIpAwAiEnwgEUIBhkL+////H4MgEkL/////D4N+fCIRNwMAIAggESAIKQMAhUIgiSIRNwMAIAsgESALKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACACIBEgAikDAIVCKIkiETcDACATIBEgEykDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgCCARIAgpAwCFQjCJIhE3AwAgCyARIAspAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAIgESACKQMAhUIBiTcDACANIA0pAwAiESADKQMAIhJ8IBFCAYZC/v///x+DIBJC/////w+DfnwiETcDACAFIBEgBSkDAIVCIIkiETcDACAAIBEgACkDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgAyARIAMpAwCFQiiJIhE3AwAgDSARIA0pAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAUgESAFKQMAhUIwiSIRNwMAIAAgESAAKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACADIBEgAykDAIVCAYk3AwAgDiAOKQMAIhEgBCkDACISfCARQgGGQv7///8fgyASQv////8Pg358IhE3AwAgBiARIAYpAwCFQiCJIhE3AwAgCSARIAkpAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAQgESAEKQMAhUIoiSIRNwMAIA4gESAOKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACAGIBEgBikDAIVCMIkiETcDACAJIBEgCSkDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgBCARIAQpAwCFQgGJNwMAIA8gDykDACIRIAEpAwAiEnwgEUIBhkL+////H4MgEkL/////D4N+fCIRNwMAIAcgESAHKQMAhUIgiSIRNwMAIAogESAKKQMAIhJ8IBFC/////w+DIBJCAYZC/v///x+DfnwiETcDACABIBEgASkDAIVCKIkiETcDACAPIBEgDykDACISfCARQv////8PgyASQgGGQv7///8fg358IhE3AwAgByARIAcpAwCFQjCJIhE3AwAgCiARIAopAwAiEnwgEUL/////D4MgEkIBhkL+////H4N+fCIRNwMAIAEgESABKQMAhUIBiTcDAAvdCAEPfwNAIAIgBUEDdCIGaiABIAZqKQMAIAAgBmopAwCFNwMAIAIgBkEIciIGaiABIAZqKQMAIAAgBmopAwCFNwMAIAVBAmoiBUGAAUcNAAsDQCADIARBA3QiAGogACACaikDADcDACADIARBAXIiAEEDdCIBaiABIAJqKQMANwMAIAMgBEECciIBQQN0IgVqIAIgBWopAwA3AwAgAyAEQQNyIgVBA3QiBmogAiAGaikDADcDACADIARBBHIiBkEDdCIHaiACIAdqKQMANwMAIAMgBEEFciIHQQN0IghqIAIgCGopAwA3AwAgAyAEQQZyIghBA3QiCWogAiAJaikDADcDACADIARBB3IiCUEDdCIKaiACIApqKQMANwMAIAMgBEEIciIKQQN0IgtqIAIgC2opAwA3AwAgAyAEQQlyIgtBA3QiDGogAiAMaikDADcDACADIARBCnIiDEEDdCINaiACIA1qKQMANwMAIAMgBEELciINQQN0Ig5qIAIgDmopAwA3AwAgAyAEQQxyIg5BA3QiD2ogAiAPaikDADcDACADIARBDXIiD0EDdCIQaiACIBBqKQMANwMAIAMgBEEOciIQQQN0IhFqIAIgEWopAwA3AwAgAyAEQQ9yIhFBA3QiEmogAiASaikDADcDACADIARB//8DcSAAQf//A3EgAUH//wNxIAVB//8DcSAGQf//A3EgB0H//wNxIAhB//8DcSAJQf//A3EgCkH//wNxIAtB//8DcSAMQf//A3EgDUH//wNxIA5B//8DcSAPQf//A3EgEEH//wNxIBFB//8DcRACIARB8ABJIQAgBEEQaiEEIAANAAtBACEBIANBAEEBQRBBEUEgQSFBMEExQcAAQcEAQdAAQdEAQeAAQeEAQfAAQfEAEAIgA0ECQQNBEkETQSJBI0EyQTNBwgBBwwBB0gBB0wBB4gBB4wBB8gBB8wAQAiADQQRBBUEUQRVBJEElQTRBNUHEAEHFAEHUAEHVAEHkAEHlAEH0AEH1ABACIANBBkEHQRZBF0EmQSdBNkE3QcYAQccAQdYAQdcAQeYAQecAQfYAQfcAEAIgA0EIQQlBGEEZQShBKUE4QTlByABByQBB2ABB2QBB6ABB6QBB+ABB+QAQAiADQQpBC0EaQRtBKkErQTpBO0HKAEHLAEHaAEHbAEHqAEHrAEH6AEH7ABACIANBDEENQRxBHUEsQS1BPEE9QcwAQc0AQdwAQd0AQewAQe0AQfwAQf0AEAIgA0EOQQ9BHkEfQS5BL0E+QT9BzgBBzwBB3gBB3wBB7gBB7wBB/gBB/wAQAgNAIAIgAUEDdCIAaiIEIAAgA2opAwAgBCkDAIU3AwAgAiAAQQhyIgRqIgUgAyAEaikDACAFKQMAhTcDACACIABBEHIiBGoiBSADIARqKQMAIAUpAwCFNwMAIAIgAEEYciIAaiIEIAAgA2opAwAgBCkDAIU3AwAgAUEEaiIBQYABRw0ACwsWACAAIAEgAiADEAMgACACIAIgAxADC3sCAX8BfiACIQkgATUCACEKIAQgBXIEQCABKAIEIANwIQkLIAAgCTYCACAAIAdBAWsgBSAEGyAIbCAGQQFrQQBBfyAGGyACIAlGG2oiASAFQQFqIAhsQQAgBBtqIAGtIAogCn5CIIh+QiCIp0F/c2ogByAIbHA2AgQgAAsEACMACwYAIAAkAAsQACMAIABrQXBxIgAkACAACwUAQYAICw==", e2)) });
    tf = [0, 1, 3, 7, 15, 31, 63, 127, 255];
    rf = function(e2) {
      this.stream = e2, this.bitOffset = 0, this.curByte = 0, this.hasByte = false;
    };
    rf.prototype._ensureByte = function() {
      this.hasByte || (this.curByte = this.stream.readByte(), this.hasByte = true);
    }, rf.prototype.read = function(e2) {
      for (var t2 = 0; e2 > 0; ) {
        this._ensureByte();
        var r2 = 8 - this.bitOffset;
        if (e2 >= r2) t2 <<= r2, t2 |= tf[r2] & this.curByte, this.hasByte = false, this.bitOffset = 0, e2 -= r2;
        else {
          t2 <<= e2;
          var n2 = r2 - e2;
          t2 |= (this.curByte & tf[e2] << n2) >> n2, this.bitOffset += e2, e2 = 0;
        }
      }
      return t2;
    }, rf.prototype.seek = function(e2) {
      var t2 = e2 % 8, r2 = (e2 - t2) / 8;
      this.bitOffset = t2, this.stream.seek(r2), this.hasByte = false;
    }, rf.prototype.pi = function() {
      var e2, t2 = new Uint8Array(6);
      for (e2 = 0; e2 < t2.length; e2++) t2[e2] = this.read(8);
      return function(e3) {
        return Array.prototype.map.call(e3, (e4) => ("00" + e4.toString(16)).slice(-2)).join("");
      }(t2);
    };
    nf = rf;
    sf = function() {
    };
    sf.prototype.readByte = function() {
      throw Error("abstract method readByte() not implemented");
    }, sf.prototype.read = function(e2, t2, r2) {
      for (var n2 = 0; n2 < r2; ) {
        var i2 = this.readByte();
        if (i2 < 0) return 0 === n2 ? -1 : n2;
        e2[t2++] = i2, n2++;
      }
      return n2;
    }, sf.prototype.seek = function(e2) {
      throw Error("abstract method seek() not implemented");
    }, sf.prototype.writeByte = function(e2) {
      throw Error("abstract method readByte() not implemented");
    }, sf.prototype.write = function(e2, t2, r2) {
      var n2;
      for (n2 = 0; n2 < r2; n2++) this.writeByte(e2[t2++]);
      return r2;
    }, sf.prototype.flush = function() {
    };
    of = sf;
    cf = (af = new Uint32Array([0, 79764919, 159529838, 222504665, 319059676, 398814059, 445009330, 507990021, 638119352, 583659535, 797628118, 726387553, 890018660, 835552979, 1015980042, 944750013, 1276238704, 1221641927, 1167319070, 1095957929, 1595256236, 1540665371, 1452775106, 1381403509, 1780037320, 1859660671, 1671105958, 1733955601, 2031960084, 2111593891, 1889500026, 1952343757, 2552477408, 2632100695, 2443283854, 2506133561, 2334638140, 2414271883, 2191915858, 2254759653, 3190512472, 3135915759, 3081330742, 3009969537, 2905550212, 2850959411, 2762807018, 2691435357, 3560074640, 3505614887, 3719321342, 3648080713, 3342211916, 3287746299, 3467911202, 3396681109, 4063920168, 4143685023, 4223187782, 4286162673, 3779000052, 3858754371, 3904687514, 3967668269, 881225847, 809987520, 1023691545, 969234094, 662832811, 591600412, 771767749, 717299826, 311336399, 374308984, 453813921, 533576470, 25881363, 88864420, 134795389, 214552010, 2023205639, 2086057648, 1897238633, 1976864222, 1804852699, 1867694188, 1645340341, 1724971778, 1587496639, 1516133128, 1461550545, 1406951526, 1302016099, 1230646740, 1142491917, 1087903418, 2896545431, 2825181984, 2770861561, 2716262478, 3215044683, 3143675388, 3055782693, 3001194130, 2326604591, 2389456536, 2200899649, 2280525302, 2578013683, 2640855108, 2418763421, 2498394922, 3769900519, 3832873040, 3912640137, 3992402750, 4088425275, 4151408268, 4197601365, 4277358050, 3334271071, 3263032808, 3476998961, 3422541446, 3585640067, 3514407732, 3694837229, 3640369242, 1762451694, 1842216281, 1619975040, 1682949687, 2047383090, 2127137669, 1938468188, 2001449195, 1325665622, 1271206113, 1183200824, 1111960463, 1543535498, 1489069629, 1434599652, 1363369299, 622672798, 568075817, 748617968, 677256519, 907627842, 853037301, 1067152940, 995781531, 51762726, 131386257, 177728840, 240578815, 269590778, 349224269, 429104020, 491947555, 4046411278, 4126034873, 4172115296, 4234965207, 3794477266, 3874110821, 3953728444, 4016571915, 3609705398, 3555108353, 3735388376, 3664026991, 3290680682, 3236090077, 3449943556, 3378572211, 3174993278, 3120533705, 3032266256, 2961025959, 2923101090, 2868635157, 2813903052, 2742672763, 2604032198, 2683796849, 2461293480, 2524268063, 2284983834, 2364738477, 2175806836, 2238787779, 1569362073, 1498123566, 1409854455, 1355396672, 1317987909, 1246755826, 1192025387, 1137557660, 2072149281, 2135122070, 1912620623, 1992383480, 1753615357, 1816598090, 1627664531, 1707420964, 295390185, 358241886, 404320391, 483945776, 43990325, 106832002, 186451547, 266083308, 932423249, 861060070, 1041341759, 986742920, 613929101, 542559546, 756411363, 701822548, 3316196985, 3244833742, 3425377559, 3370778784, 3601682597, 3530312978, 3744426955, 3689838204, 3819031489, 3881883254, 3928223919, 4007849240, 4037393693, 4100235434, 4180117107, 4259748804, 2310601993, 2373574846, 2151335527, 2231098320, 2596047829, 2659030626, 2470359227, 2550115596, 2947551409, 2876312838, 2788305887, 2733848168, 3165939309, 3094707162, 3040238851, 2985771188]), function() {
      var e2 = 4294967295;
      this.getCRC = function() {
        return ~e2 >>> 0;
      }, this.updateCRC = function(t2) {
        e2 = e2 << 8 ^ af[255 & (e2 >>> 24 ^ t2)];
      }, this.updateCRCRun = function(t2, r2) {
        for (; r2-- > 0; ) e2 = e2 << 8 ^ af[255 & (e2 >>> 24 ^ t2)];
      };
    });
    hf = nf;
    uf = of;
    lf = cf;
    yf = function(e2, t2) {
      var r2, n2 = e2[t2];
      for (r2 = t2; r2 > 0; r2--) e2[r2] = e2[r2 - 1];
      return e2[0] = n2, n2;
    };
    ff = { OK: 0, LAST_BLOCK: -1, NOT_BZIP_DATA: -2, UNEXPECTED_INPUT_EOF: -3, UNEXPECTED_OUTPUT_EOF: -4, DATA_ERROR: -5, OUT_OF_MEMORY: -6, OBSOLETE_INPUT: -7, END_OF_BLOCK: -8 };
    gf = {};
    gf[ff.LAST_BLOCK] = "Bad file checksum", gf[ff.NOT_BZIP_DATA] = "Not bzip data", gf[ff.UNEXPECTED_INPUT_EOF] = "Unexpected input EOF", gf[ff.UNEXPECTED_OUTPUT_EOF] = "Unexpected output EOF", gf[ff.DATA_ERROR] = "Data error", gf[ff.OUT_OF_MEMORY] = "Out of memory", gf[ff.OBSOLETE_INPUT] = "Obsolete (pre 0.9.5) bzip format not supported.";
    pf = function(e2, t2) {
      var r2 = gf[e2] || "unknown error";
      t2 && (r2 += ": " + t2);
      var n2 = new TypeError(r2);
      throw n2.errorCode = e2, n2;
    };
    df = function(e2, t2) {
      this.writePos = this.writeCurrent = this.writeCount = 0, this._start_bunzip(e2, t2);
    };
    df.prototype._init_block = function() {
      return this._get_next_block() ? (this.blockCRC = new lf(), true) : (this.writeCount = -1, false);
    }, df.prototype._start_bunzip = function(e2, t2) {
      var r2 = new Uint8Array(4);
      4 === e2.read(r2, 0, 4) && "BZh" === String.fromCharCode(r2[0], r2[1], r2[2]) || pf(ff.NOT_BZIP_DATA, "bad magic");
      var n2 = r2[3] - 48;
      (n2 < 1 || n2 > 9) && pf(ff.NOT_BZIP_DATA, "level out of range"), this.reader = new hf(e2), this.dbufSize = 1e5 * n2, this.nextoutput = 0, this.outputStream = t2, this.streamCRC = 0;
    }, df.prototype._get_next_block = function() {
      var e2, t2, r2, n2 = this.reader, i2 = n2.pi();
      if ("177245385090" === i2) return false;
      "314159265359" !== i2 && pf(ff.NOT_BZIP_DATA), this.targetBlockCRC = n2.read(32) >>> 0, this.streamCRC = (this.targetBlockCRC ^ (this.streamCRC << 1 | this.streamCRC >>> 31)) >>> 0, n2.read(1) && pf(ff.OBSOLETE_INPUT);
      var s2 = n2.read(24);
      s2 > this.dbufSize && pf(ff.DATA_ERROR, "initial position out of bounds");
      var a2 = n2.read(16), o2 = new Uint8Array(256), c2 = 0;
      for (e2 = 0; e2 < 16; e2++) if (a2 & 1 << 15 - e2) {
        var h2 = 16 * e2;
        for (r2 = n2.read(16), t2 = 0; t2 < 16; t2++) r2 & 1 << 15 - t2 && (o2[c2++] = h2 + t2);
      }
      var u2 = n2.read(3);
      (u2 < 2 || u2 > 6) && pf(ff.DATA_ERROR);
      var l2 = n2.read(15);
      0 === l2 && pf(ff.DATA_ERROR);
      var y2 = new Uint8Array(256);
      for (e2 = 0; e2 < u2; e2++) y2[e2] = e2;
      var f2 = new Uint8Array(l2);
      for (e2 = 0; e2 < l2; e2++) {
        for (t2 = 0; n2.read(1); t2++) t2 >= u2 && pf(ff.DATA_ERROR);
        f2[e2] = yf(y2, t2);
      }
      var g2, p2 = c2 + 2, d2 = [];
      for (t2 = 0; t2 < u2; t2++) {
        var A2, w2, m2 = new Uint8Array(p2), b2 = new Uint16Array(21);
        for (a2 = n2.read(5), e2 = 0; e2 < p2; e2++) {
          for (; (a2 < 1 || a2 > 20) && pf(ff.DATA_ERROR), n2.read(1); ) n2.read(1) ? a2-- : a2++;
          m2[e2] = a2;
        }
        for (A2 = w2 = m2[0], e2 = 1; e2 < p2; e2++) m2[e2] > w2 ? w2 = m2[e2] : m2[e2] < A2 && (A2 = m2[e2]);
        g2 = {}, d2.push(g2), g2.permute = new Uint16Array(258), g2.limit = new Uint32Array(22), g2.base = new Uint32Array(21), g2.minLen = A2, g2.maxLen = w2;
        var k2 = 0;
        for (e2 = A2; e2 <= w2; e2++) for (b2[e2] = g2.limit[e2] = 0, a2 = 0; a2 < p2; a2++) m2[a2] === e2 && (g2.permute[k2++] = a2);
        for (e2 = 0; e2 < p2; e2++) b2[m2[e2]]++;
        for (k2 = a2 = 0, e2 = A2; e2 < w2; e2++) k2 += b2[e2], g2.limit[e2] = k2 - 1, k2 <<= 1, a2 += b2[e2], g2.base[e2 + 1] = k2 - a2;
        g2.limit[w2 + 1] = Number.MAX_VALUE, g2.limit[w2] = k2 + b2[w2] - 1, g2.base[A2] = 0;
      }
      var E2 = new Uint32Array(256);
      for (e2 = 0; e2 < 256; e2++) y2[e2] = e2;
      var v2, I2 = 0, B2 = 0, S2 = 0, K2 = this.dbuf = new Uint32Array(this.dbufSize);
      for (p2 = 0; ; ) {
        for (p2-- || (p2 = 49, S2 >= l2 && pf(ff.DATA_ERROR), g2 = d2[f2[S2++]]), e2 = g2.minLen, t2 = n2.read(e2); e2 > g2.maxLen && pf(ff.DATA_ERROR), !(t2 <= g2.limit[e2]); e2++) t2 = t2 << 1 | n2.read(1);
        ((t2 -= g2.base[e2]) < 0 || t2 >= 258) && pf(ff.DATA_ERROR);
        var C2 = g2.permute[t2];
        if (0 !== C2 && 1 !== C2) {
          if (I2) for (I2 = 0, B2 + a2 > this.dbufSize && pf(ff.DATA_ERROR), E2[v2 = o2[y2[0]]] += a2; a2--; ) K2[B2++] = v2;
          if (C2 > c2) break;
          B2 >= this.dbufSize && pf(ff.DATA_ERROR), E2[v2 = o2[v2 = yf(y2, e2 = C2 - 1)]]++, K2[B2++] = v2;
        } else I2 || (I2 = 1, a2 = 0), a2 += 0 === C2 ? I2 : 2 * I2, I2 <<= 1;
      }
      for ((s2 < 0 || s2 >= B2) && pf(ff.DATA_ERROR), t2 = 0, e2 = 0; e2 < 256; e2++) r2 = t2 + E2[e2], E2[e2] = t2, t2 = r2;
      for (e2 = 0; e2 < B2; e2++) K2[E2[v2 = 255 & K2[e2]]] |= e2 << 8, E2[v2]++;
      var D2 = 0, U2 = 0, P2 = 0;
      return B2 && (U2 = 255 & (D2 = K2[s2]), D2 >>= 8, P2 = -1), this.writePos = D2, this.writeCurrent = U2, this.writeCount = B2, this.writeRun = P2, true;
    }, df.prototype._read_bunzip = function(e2, t2) {
      var r2, n2, i2;
      if (this.writeCount < 0) return 0;
      var s2 = this.dbuf, a2 = this.writePos, o2 = this.writeCurrent, c2 = this.writeCount;
      this.outputsize;
      for (var h2 = this.writeRun; c2; ) {
        for (c2--, n2 = o2, o2 = 255 & (a2 = s2[a2]), a2 >>= 8, 3 == h2++ ? (r2 = o2, i2 = n2, o2 = -1) : (r2 = 1, i2 = o2), this.blockCRC.updateCRCRun(i2, r2); r2--; ) this.outputStream.writeByte(i2), this.nextoutput++;
        o2 != n2 && (h2 = 0);
      }
      return this.writeCount = c2, this.blockCRC.getCRC() !== this.targetBlockCRC && pf(ff.DATA_ERROR, "Bad block CRC (got " + this.blockCRC.getCRC().toString(16) + " expected " + this.targetBlockCRC.toString(16) + ")"), this.nextoutput;
    };
    Af = function(e2) {
      if ("readByte" in e2) return e2;
      var t2 = new uf();
      return t2.pos = 0, t2.readByte = function() {
        return e2[this.pos++];
      }, t2.seek = function(e3) {
        this.pos = e3;
      }, t2.eof = function() {
        return this.pos >= e2.length;
      }, t2;
    };
    wf = function(e2) {
      var t2 = new uf(), r2 = true;
      if (e2) if ("number" == typeof e2) t2.buffer = new Uint8Array(e2), r2 = false;
      else {
        if ("writeByte" in e2) return e2;
        t2.buffer = e2, r2 = false;
      }
      else t2.buffer = new Uint8Array(16384);
      return t2.pos = 0, t2.writeByte = function(e3) {
        if (r2 && this.pos >= this.buffer.length) {
          var t3 = new Uint8Array(2 * this.buffer.length);
          t3.set(this.buffer), this.buffer = t3;
        }
        this.buffer[this.pos++] = e3;
      }, t2.getBuffer = function() {
        if (this.pos !== this.buffer.length) {
          if (!r2) throw new TypeError("outputsize does not match decoded input");
          var e3 = new Uint8Array(this.pos);
          e3.set(this.buffer.subarray(0, this.pos)), this.buffer = e3;
        }
        return this.buffer;
      }, t2._coerced = true, t2;
    };
    mf = { Bunzip: df, Stream: uf, Err: ff, decode: function(e2, t2, r2) {
      for (var n2 = Af(e2), i2 = wf(t2), s2 = new df(n2, i2); !("eof" in n2) || !n2.eof(); ) if (s2._init_block()) s2._read_bunzip();
      else {
        var a2 = s2.reader.read(32) >>> 0;
        if (a2 !== s2.streamCRC && pf(ff.DATA_ERROR, "Bad stream CRC (got " + s2.streamCRC.toString(16) + " expected " + a2.toString(16) + ")"), !r2 || !("eof" in n2) || n2.eof()) break;
        s2._start_bunzip(n2, i2);
      }
      if ("getBuffer" in i2) return i2.getBuffer();
    }, decodeBlock: function(e2, t2, r2) {
      var n2 = Af(e2), i2 = wf(r2), s2 = new df(n2, i2);
      if (s2.reader.seek(t2), s2._get_next_block() && (s2.blockCRC = new lf(), s2.writeCopies = 0, s2._read_bunzip()), "getBuffer" in i2) return i2.getBuffer();
    }, table: function(e2, t2, r2) {
      var n2 = new uf();
      n2.delegate = Af(e2), n2.pos = 0, n2.readByte = function() {
        return this.pos++, this.delegate.readByte();
      }, n2.delegate.eof && (n2.eof = n2.delegate.eof.bind(n2.delegate));
      var i2 = new uf();
      i2.pos = 0, i2.writeByte = function() {
        this.pos++;
      };
      for (var s2 = new df(n2, i2), a2 = s2.dbufSize; !("eof" in n2) || !n2.eof(); ) {
        var o2 = 8 * n2.pos + s2.reader.bitOffset;
        if (s2.reader.hasByte && (o2 -= 8), s2._init_block()) {
          var c2 = i2.pos;
          s2._read_bunzip(), t2(o2, i2.pos - c2);
        } else {
          if (s2.reader.read(32), !r2 || !("eof" in n2) || n2.eof()) break;
          s2._start_bunzip(n2, i2), console.assert(s2.dbufSize === a2, "shouldn't change block size within multistream file");
        }
      }
    } };
    bf = /* @__PURE__ */ t({ __proto__: null }, [mf]);
  }
});

// lib/armor.js
var require_armor = __commonJS({
  "lib/armor.js"(exports2, module2) {
    "use strict";
    var ArmorConstants = {
      SIGNATURE_TEXT: 1,
      SIGNATURE_HEADERS: 2,
      SIGNATURE_ARMOR: 3
    };
    function indexOfArmorDelimiter(text, str, offset) {
      let currentOffset = offset;
      while (currentOffset < text.length) {
        let loc = text.indexOf(str, currentOffset);
        if (loc === -1 || loc === 0 || text.charAt(loc - 1) == "\n") {
          return loc;
        }
        currentOffset = loc + str.length;
      }
      return -1;
    }
    function searchBlankLine(str, then) {
      var offset = str.search(/\n\s*\r?\n/);
      if (offset === -1) {
        return "";
      } else {
        return then(offset);
      }
    }
    function indexOfNewline(str, off, then) {
      var offset = str.indexOf("\n", off);
      if (offset === -1) {
        return "";
      } else {
        return then(offset);
      }
    }
    var Armor = {
      /**
       * Locates offsets bracketing PGP armored block in text,
       * starting from given offset, and returns block type string.
       *
       * @param text:          String - ASCII armored text
       * @param offset:        Number - offset to start looking for block
       * @param indentStr:     String - prefix that is used for all lines (such as "> ")
       * @param beginIndexObj: Object - o.value will contain offset of first character of block
       * @param endIndexObj:   Object - o.value will contain offset of last character of block (newline)
       * @param indentStrObj:  Object - o.value will contain indent of 1st line
       *
       * @return String - type of block found (e.g. MESSAGE, PUBLIC KEY)
       *           If no block is found, an empty String is returned;
       */
      locateArmoredBlock: function(text, offset, indentStr, beginIndexObj, endIndexObj, indentStrObj) {
        beginIndexObj.value = -1;
        endIndexObj.value = -1;
        var beginIndex = indexOfArmorDelimiter(text, indentStr + "-----BEGIN PGP ", offset);
        if (beginIndex == -1) {
          var blockStart = text.indexOf("-----BEGIN PGP ");
          if (blockStart >= 0) {
            var indentStart = text.search(/\n.*-----BEGIN PGP /) + 1;
            indentStrObj.value = text.substring(indentStart, blockStart);
            indentStr = indentStrObj.value;
            beginIndex = indexOfArmorDelimiter(text, indentStr + "-----BEGIN PGP ", offset);
          }
        }
        if (beginIndex == -1)
          return "";
        offset = text.indexOf("\n", beginIndex);
        if (offset == -1)
          return "";
        var endIndex = indexOfArmorDelimiter(text, indentStr + "-----END PGP ", offset);
        if (endIndex == -1)
          return "";
        endIndex = text.indexOf("\n", endIndex);
        if (endIndex == -1) {
          endIndex = text.length - 1;
        }
        var blockHeader = text.substr(beginIndex, offset - beginIndex + 1);
        var blockRegex = new RegExp("^" + indentStr.replace(/[.*+\-?^${}()|[\]\\]/g, "\\$&") + "-----BEGIN PGP (.{1,30})-----\\s*\\r?\\n");
        var matches = blockHeader.match(blockRegex);
        var blockType = "";
        if (matches && matches.length > 1) {
          blockType = matches[1];
        }
        if (blockType == "UNVERIFIED MESSAGE") {
          return Armor.locateArmoredBlock(text, endIndex + 1, indentStr, beginIndexObj, endIndexObj, indentStrObj);
        }
        beginIndexObj.value = beginIndex;
        endIndexObj.value = endIndex;
        return blockType;
      },
      /**
       * locateArmoredBlocks returns an array of ASCII Armor block positions
       *
       * @param text: String - text containing ASCII armored block(s)
       *
       * @return Array of objects with the following structure:
       *        obj.begin:     Number
       *        obj.end:       Number
       *        obj.indent:    String
       *        obj.blocktype: String
       *
       *       if no block was found, an empty array is returned
       */
      locateArmoredBlocks: function(text) {
        var beginObj = {};
        var endObj = {};
        var indentStrObj = {};
        var blocks = [];
        var i2 = 0;
        var b2;
        while ((b2 = Armor.locateArmoredBlock(text, i2, "", beginObj, endObj, indentStrObj)) !== "") {
          blocks.push({
            begin: beginObj.value,
            end: endObj.value,
            indent: indentStrObj.value ? indentStrObj.value : "",
            blocktype: b2
          });
          i2 = endObj.value;
        }
        return blocks;
      },
      extractSignaturePart: function(signatureBlock, part) {
        return searchBlankLine(signatureBlock, function(offset) {
          return indexOfNewline(signatureBlock, offset + 1, function(offset2) {
            var beginIndex = signatureBlock.indexOf("-----BEGIN PGP SIGNATURE-----", offset2 + 1);
            if (beginIndex == -1) {
              return "";
            }
            if (part === ArmorConstants.SIGNATURE_TEXT) {
              return signatureBlock.substr(offset2 + 1, beginIndex - offset2 - 1).replace(/^- -/, "-").replace(/\n- -/g, "\n-").replace(/\r- -/g, "\r-");
            }
            return indexOfNewline(signatureBlock, beginIndex, function(offset3) {
              var endIndex = signatureBlock.indexOf("-----END PGP SIGNATURE-----", offset3);
              if (endIndex == -1) {
                return "";
              }
              var signBlock = signatureBlock.substr(offset3, endIndex - offset3);
              return searchBlankLine(signBlock, function(armorIndex) {
                if (part == ArmorConstants.SIGNATURE_HEADERS) {
                  return signBlock.substr(1, armorIndex);
                }
                return indexOfNewline(signBlock, armorIndex + 1, function(armorIndex2) {
                  if (part == ArmorConstants.SIGNATURE_ARMOR) {
                    return signBlock.substr(armorIndex2, endIndex - armorIndex2).replace(/\s*/g, "");
                  } else {
                    return "";
                  }
                });
              });
            });
          });
        });
      },
      /**
       * Remove all headers from an OpenPGP Armored message and replace them
       * with a set of new headers.
       *
       * @param armorText: String - ASCII armored message
       * @param headers:   Object - key/value pairs of new headers to insert
       *
       * @return String - new armored message
       */
      replaceArmorHeaders: function(armorText, headers) {
        let text = armorText.replace(/\r\n/g, "\n");
        let i2 = text.search(/\n/);
        if (i2 < 0) return armorText;
        let m2 = text.substr(0, i2 + 1);
        for (let j2 in headers) {
          m2 += j2 + ": " + headers[j2] + "\n";
        }
        i2 = text.search(/\n\n/);
        if (i2 < 0) return armorText;
        m2 += text.substr(i2 + 1);
        return m2;
      },
      /**
       * Get a list of all headers found in an armor message
       *
       * @param text String - ASCII armored message
       *
       * @return Object: key/value pairs of headers. All keys are in lowercase.
       */
      getArmorHeaders: function(text) {
        let headers = {};
        let b2 = this.locateArmoredBlocks(text);
        if (b2.length === 0) {
          return headers;
        }
        let msg = text.substr(b2[0].begin);
        let indent = b2[0].indent.replace(/[.*+\-?^${}()|[\]\\]/g, "\\$&");
        let lx = new RegExp("\\n" + indent + "\\r?\\n");
        let hdrEnd = msg.search(lx);
        if (hdrEnd < 0) return headers;
        let lines = msg.substr(0, hdrEnd).split(/\r?\n/);
        let rx = new RegExp("^" + indent + "([^: ]+)(: )(.*)");
        for (let i2 = 1; i2 < lines.length; i2++) {
          let m2 = lines[i2].match(rx);
          if (m2 && m2.length >= 4) {
            headers[m2[1].toLowerCase()] = m2[3];
          }
        }
        return headers;
      },
      /**
       * Split armored blocks into an array of strings
       */
      splitArmoredBlocks: function(keyBlockStr) {
        let myRe = /-----BEGIN PGP (PUBLIC|PRIVATE) KEY BLOCK-----/g;
        let myArray;
        let retArr = [];
        let startIndex = -1;
        while ((myArray = myRe.exec(keyBlockStr)) !== null) {
          if (startIndex >= 0) {
            let s2 = keyBlockStr.substring(startIndex, myArray.index);
            retArr.push(s2);
          }
          startIndex = myArray.index;
        }
        retArr.push(keyBlockStr.substring(startIndex));
        return retArr;
      }
    };
    module2.exports = Armor;
  }
});

// lib/pgpHelper.js
var require_pgpHelper = __commonJS({
  "lib/pgpHelper.js"(exports2, module2) {
    "use strict";
    var crc_table = [
      0,
      8801531,
      25875725,
      17603062,
      60024545,
      51751450,
      35206124,
      44007191,
      128024889,
      120049090,
      103502900,
      112007375,
      70412248,
      78916387,
      95990485,
      88014382,
      264588937,
      256049778,
      240098180,
      248108927,
      207005800,
      215016595,
      232553829,
      224014750,
      140824496,
      149062475,
      166599357,
      157832774,
      200747345,
      191980970,
      176028764,
      184266919,
      520933865,
      529177874,
      512099556,
      503334943,
      480196360,
      471432179,
      487973381,
      496217854,
      414011600,
      405478443,
      422020573,
      430033190,
      457094705,
      465107658,
      448029500,
      439496647,
      281648992,
      273666971,
      289622637,
      298124950,
      324696449,
      333198714,
      315665548,
      307683447,
      392699481,
      401494690,
      383961940,
      375687087,
      352057528,
      343782467,
      359738805,
      368533838,
      1041867730,
      1050668841,
      1066628831,
      1058355748,
      1032471859,
      1024199112,
      1006669886,
      1015471301,
      968368875,
      960392720,
      942864358,
      951368477,
      975946762,
      984451313,
      1000411399,
      992435708,
      836562267,
      828023200,
      810956886,
      818967725,
      844041146,
      852051777,
      868605623,
      860066380,
      914189410,
      922427545,
      938981743,
      930215316,
      904825475,
      896059e3,
      878993294,
      887231349,
      555053627,
      563297984,
      547333942,
      538569677,
      579245274,
      570480673,
      588005847,
      596249900,
      649392898,
      640860153,
      658384399,
      666397428,
      623318499,
      631331096,
      615366894,
      606833685,
      785398962,
      777416777,
      794487231,
      802989380,
      759421523,
      767923880,
      751374174,
      743392165,
      695319947,
      704115056,
      687564934,
      679289981,
      719477610,
      711202705,
      728272487,
      737067676,
      2083735460,
      2092239711,
      2109313705,
      2101337682,
      2141233477,
      2133257662,
      2116711496,
      2125215923,
      2073216669,
      2064943718,
      2048398224,
      2057199467,
      2013339772,
      2022141063,
      2039215473,
      2030942602,
      1945504045,
      1936737750,
      1920785440,
      1929023707,
      1885728716,
      1893966647,
      1911503553,
      1902736954,
      1951893524,
      1959904495,
      1977441561,
      1968902626,
      2009362165,
      2000822798,
      1984871416,
      1992881923,
      1665111629,
      1673124534,
      1656046400,
      1647513531,
      1621913772,
      1613380695,
      1629922721,
      1637935450,
      1688082292,
      1679317903,
      1695859321,
      1704103554,
      1728967061,
      1737211246,
      1720132760,
      1711368291,
      1828378820,
      1820103743,
      1836060105,
      1844855090,
      1869168165,
      1877963486,
      1860430632,
      1852155859,
      1801148925,
      1809650950,
      1792118e3,
      1784135691,
      1757986588,
      1750004711,
      1765960209,
      1774462698,
      1110107254,
      1118611597,
      1134571899,
      1126595968,
      1102643863,
      1094667884,
      1077139354,
      1085643617,
      1166763343,
      1158490548,
      1140961346,
      1149762745,
      1176011694,
      1184812885,
      1200772771,
      1192499800,
      1307552511,
      1298785796,
      1281720306,
      1289958153,
      1316768798,
      1325007077,
      1341561107,
      1332794856,
      1246636998,
      1254647613,
      1271201483,
      1262662192,
      1239272743,
      1230733788,
      1213667370,
      1221678289,
      1562785183,
      1570797924,
      1554833554,
      1546300521,
      1588974462,
      1580441477,
      1597965939,
      1605978760,
      1518843046,
      1510078557,
      1527603627,
      1535847760,
      1494504007,
      1502748348,
      1486784330,
      1478020017,
      1390639894,
      1382365165,
      1399434779,
      1408230112,
      1366334967,
      1375129868,
      1358579962,
      1350304769,
      1430452783,
      1438955220,
      1422405410,
      1414423513,
      1456544974,
      1448562741,
      1465633219,
      1474135352
    ];
    var pgpHelper = {
      /**
       * Convert a string to an Uint8Array
       *
       * @param  str: String with binary data
       * @return Uint8Array
       */
      str2Uint8Array: function(str) {
        var buf = new ArrayBuffer(str.length);
        var bufView = new Uint8Array(buf);
        for (var i2 = 0, strLen = str.length; i2 < strLen; i2++) {
          bufView[i2] = str.charCodeAt(i2);
        }
        return bufView;
      },
      /**
       * Create CRC24 checksum
       *
       * @param input: Uint8Array of input data
       *
       * @return Number
       */
      createcrc24: function(input2) {
        var crc = 11994318;
        var index = 0;
        while (input2.length - index > 16) {
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 1]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 2]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 3]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 4]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 5]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 6]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 7]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 8]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 9]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 10]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 11]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 12]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 13]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 14]) & 255];
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index + 15]) & 255];
          index += 16;
        }
        for (var j2 = index; j2 < input2.length; j2++) {
          crc = crc << 8 ^ crc_table[(crc >> 16 ^ input2[index++]) & 255];
        }
        return crc & 16777215;
      },
      /**
       * Create an ASCII armored string from binary data. The message data is NOT
       * checked for correctness, only the CRC is added at the end.
       *
       * @param {Number} msgType: type of OpenPGP message to create (ARMOR Enum)
       * @param {String|Uint8Array} str: binary OpenPGP message
       *
       * @return String: ASCII armored OpenPGP message
       */
      bytesToArmor: function(msgType, str) {
        const ARMOR_TYPE = {
          multipartSection: 0,
          multipartLast: 1,
          signed: 2,
          message: 3,
          publicKey: 4,
          privateKey: 5,
          signature: 6
        };
        let hdr = "";
        switch (msgType) {
          case ARMOR_TYPE.signed:
          case ARMOR_TYPE.message:
            hdr = "MESSAGE";
            break;
          case ARMOR_TYPE.publicKey:
            hdr = "PUBLIC KEY BLOCK";
            break;
          case ARMOR_TYPE.privateKey:
            hdr = "PRIVATE KEY BLOCK";
            break;
          case ARMOR_TYPE.signature:
            hdr = "SIGNATURE";
            break;
        }
        let crc;
        if (typeof str === "string") {
          crc = pgpHelper.createcrc24(pgpHelper.str2Uint8Array(str));
        } else {
          crc = pgpHelper.createcrc24(str);
        }
        let crcAsc = String.fromCharCode(crc >> 16) + String.fromCharCode(crc >> 8 & 255) + String.fromCharCode(crc & 255);
        let s2 = "-----BEGIN PGP " + hdr + "-----\n\n" + btoa(str).replace(/(.{72})/g, "$1\n") + "\n=" + btoa(crcAsc) + "\n-----END PGP " + hdr + "-----\n";
        return s2;
      },
      signingAlgIdToString: function(id) {
        if (typeof id === "string") {
          return id;
        }
        let EnigmailLocale;
        switch (parseInt(id, 10)) {
          case 1:
          case 2:
          case 3:
            return "RSA";
          case 16:
            return "Elgamal";
          case 17:
            return "DSA";
          case 18:
            return "ECDH";
          case 19:
            return "ECDSA";
          case 20:
            return "ELG";
          case 22:
            return "EDDSA";
          default:
            return String(parseInt(id, 10));
        }
      },
      hashAlgIdToString: function(id) {
        if (typeof id === "string") {
          return id;
        }
        let EnigmailLocale;
        switch (parseInt(id, 10)) {
          case 1:
            return "MD5";
          case 2:
            return "SHA-1";
          case 3:
            return "RIPE-MD/160";
          case 8:
            return "SHA256";
          case 9:
            return "SHA384";
          case 10:
            return "SHA512";
          case 11:
            return "SHA224";
          default:
            return String(parseInt(id, 10));
        }
      }
    };
    module2.exports = pgpHelper;
  }
});

// lib/keys.js
var require_keys = __commonJS({
  "lib/keys.js"(exports2, module2) {
    "use strict";
    var PgpJS = (init_openpgp_min(), __toCommonJS(openpgp_min_exports));
    var LogData2 = require_logData();
    var PgpHelper = require_pgpHelper();
    var Armor = require_armor();
    var Funcs = require_funcs();
    var promptForInputFunc;
    var keys = {
      /**
       * Get a human readable version string
       *
       * @returns {String}
       */
      getVersionString: function() {
        return PgpJS.config.versionString;
      },
      init: function(promptForInputFn) {
        promptForInputFunc = promptForInputFn;
      },
      /**
       * Get a minimal key, possibly reduced to a specific email address
       *
       * @param {String|Object} key: String: armored key data
       *                             Object: OpenPGP.JS Key object
       * @param {String} emailAddr:  (Optional) If set, only filter for UIDs with the emailAddr
       *
       * @return {String} armored key data
       */
      getStrippedKey: async function(key, emailAddr) {
        LogData2.DEBUG("keys.js: getStrippedKey()\n");
        let searchUid = void 0;
        if (emailAddr) {
          if (emailAddr.search(/^<.{1,500}>$/) < 0) {
            searchUid = `<${emailAddr}>`;
          } else searchUid = emailAddr;
        }
        try {
          if (typeof key === "string") {
            let keyList = await PgpJS.readKeys({
              armoredKeys: key
            });
            if (!keyList || keyList.length === 0) {
              return null;
            }
            key = keyList[0];
          }
          let uid = await key.getPrimaryUser(null, searchUid);
          if (!uid || !uid.user) return null;
          let signSubkey = await key.getSigningKey();
          let encSubkey = await key.getEncryptionKey();
          if (signSubkey && "directSignatures" in signSubkey) signSubkey.directSignatures = [];
          if ("otherCertifications" in uid.user) uid.user.otherCertifications = [];
          const primaryKey = key.getKeys()[0];
          let p2 = primaryKey.toPacketList().filterByTag(PgpJS.PublicKeyPacket.tag, PgpJS.SecretKeyPacket.tag);
          p2 = p2.concat(uid.user.toPacketList());
          if (key !== signSubkey) {
            p2 = p2.concat(signSubkey.toPacketList());
          }
          if (key !== encSubkey) {
            p2 = p2.concat(encSubkey.toPacketList());
          }
          return PgpJS.armor(
            PgpJS.enums.armor.publicKey,
            p2.write()
          );
        } catch (ex) {
          LogData2.DEBUG("keys.js: getStrippedKey: ERROR " + ex.message + "\n" + ex.stack + "\n");
        }
        return null;
      },
      getKeysFromKeyBlock: async function(keyBlockStr) {
        LogData2.DEBUG("keys.js: getKeysFromKeyBlock()\n");
        const SIG_TYPE_REVOCATION = 32;
        let keyList = [];
        let key = {};
        let blocks;
        let isBinary = false;
        if (keyBlockStr.search(/-----BEGIN PGP (PUBLIC|PRIVATE) KEY BLOCK-----/) >= 0) {
          blocks = Armor.splitArmoredBlocks(keyBlockStr);
        } else {
          isBinary = true;
          blocks = [PgpHelper.bytesToArmor(PgpJS.enums.armor.publicKey, keyBlockStr)];
        }
        for (let b2 of blocks) {
          let keys2 = await PgpJS.readKeys({
            armoredKeys: b2
          });
          for (let k2 of keys2) {
            keyList.push(k2);
          }
        }
        return keyList;
      },
      getSignaturesFromKey: function(pgpJsKey) {
        const fpr = pgpJsKey.getFingerprint().toUpperCase();
        const keyId = pgpJsKey.getKeyID().toHex().toUpperCase();
        let sigs = [];
        for (let u2 of pgpJsKey.users) {
          if (u2.userID) {
            if (u2.selfCertifications.length > 0) {
              let uid = {
                userId: u2.userID.userID,
                keyId,
                fpr,
                created: u2.selfCertifications[0].created,
                sigList: []
              };
              for (let c2 of u2.selfCertifications) {
                let sig = {
                  created: c2.created,
                  sigType: Number(c2.signatureType).toString(16) + "x",
                  userId: "",
                  fpr: "",
                  sigKnown: false
                };
                if (c2.issuerFingerprint) {
                  sig.signerKeyId = Funcs.arrayToHex(c2.issuerFingerprint);
                } else {
                  sig.signerKeyId = c2.issuerKeyID.toHex().toUpperCase();
                }
                uid.sigList.push(sig);
              }
              for (let c2 of u2.otherCertifications) {
                if (c2.revoked) continue;
                let sig = {
                  created: c2.created,
                  sigType: Number(c2.signatureType).toString(16) + "x",
                  userId: "",
                  fpr: "",
                  sigKnown: false
                };
                if (c2.issuerFingerprint) {
                  sig.signerKeyId = Funcs.arrayToHex(c2.issuerFingerprint);
                } else {
                  sig.signerKeyId = c2.issuerKeyID.toHex().toUpperCase();
                }
                uid.sigList.push(sig);
              }
              sigs.push(uid);
            }
          }
        }
        return sigs;
      },
      /**
       * Decrypt a secret key. If needed, request password via prompt
       *
       * @param {Object} key:    OpenPGP.js key
       * @param {String} reason: Reason code. one of:   
       *                            "KEY_DECRYPT_REASON_DECRYPT_MSG"   // decrypt a message 
       *                            "KEY_DECRYPT_REASON_SIGN_MSG"      // sign a message
       *                            "KEY_DECRYPT_REASON_SIGNCRYPT_MSG" // encrypt and sign an message
       *                            "KEY_DECRYPT_REASON_MODIFY_KEY"    // perform modifications on key (such as add UID, change passwd)
       *
       * @return {Object}: 
       *                   - result: decryptedKey               // if decryption successful
       *                   - needPassword: { fpr, reason }      // password required
       *                   - wrongPassword: { fpr, reason }     // wrong password provided
       */
      decryptSecretKey: async function(key, reason, password) {
        return internalSecretKeyDecryption(key, reason, password);
      },
      generateKey: async function(userId, expiryDate, keyLength, keyType, passphrase) {
        LogData2.DEBUG(`keys.js: generateKey(${userId.join(", ")}, ${expiryDate}, ${keyLength}, ${keyType})
`);
        if (!expiryDate) {
          expiryDate = Infinity;
        }
        let options = {
          userIDs: userId.map((x2) => {
            return { name: x2 };
          }),
          keyExpirationTime: Date(expiryDate),
          passphrase,
          // FIXME?? EnigmailData.convertToUnicode(passphrase, 'utf-8'),
          format: "armored",
          subkeys: [{}]
        };
        switch (keyType) {
          case "ECC":
            options.curve = "curve25519Legacy";
            options.type = "ecc";
            break;
          case "RSA":
            options.type = "rsa";
            options.rsaBits = keyLength || 4096;
            break;
          case "Curve448":
          case "Curve25519":
            options.type = keyType.toLowerCase();
            break;
          default:
            throw Error(`Invalid key type ${keyType}`);
        }
        const {
          privateKey,
          revocationCertificate
        } = await PgpJS.generateKey(options);
        const key = await PgpJS.readPrivateKey({
          armoredKey: privateKey
        });
        LogData2.DEBUG(`keys.js: generateKey: key created
`);
        return {
          privateKey,
          revocationCertificate,
          key
        };
      },
      /**
       * Extract a photo ID from a key, store it as file and return the file object.
       *
       * @param {Object} key:         OpenPGP.js key object
       * @param {Number} photoNumber: number of the photo on the key, starting with 0
       *
       * @return {nsIFile} object or null in case no data / error.
       */
      getPhotoForKey: async function(key, photoNumber) {
        LogData2.DEBUG(`keys.js: getPhotoForKey: (${key.getFingerprint()}, ${photoNumber})
`);
        let currUat = 0;
        for (let i2 in key.users) {
          if (key.users[i2].userAttribute !== null) {
            if (currUat < photoNumber) {
              ++currUat;
              continue;
            }
            if (key.users[i2].userAttribute.attributes.length > 0) {
              return btoa(await key.users[i2].userAttribute.write());
            }
          }
        }
        return null;
      },
      /**
       * Change the expiry time of a key
       *
       * @param {Object} decryptedKey:  OpenPGP.js key
       * @param {Array<Number>} subKeyIdentification:  Subkeys to modify
       *                                   "0" reflects the primary key and should always be set.
       * @param {Date}   expiryDate: new expiry date. Use Inifitiy for no expiry
       * @param {String} passwd:     Password to decrypt key (if needed)
       *                             If null or undefined, prompt for the password
       *
       * @return {Object} modified Key
       */
      changeKeyExpiry: async function(key, subKeyIdentification, expiryDate, passwd = "") {
        LogData2.DEBUG(`keys.js: changeKeyExpiry: (${key.getFingerprint()}, ${expiryDate})
`);
        let decryptedKey;
        let keyIsDecrypted = key.isDecrypted();
        if (!keyIsDecrypted) {
          const r2 = await internalSecretKeyDecryption(key, "KEY_DECRYPT_REASON_MODIFY_KEY", passwd);
          if (!("result" in r2)) {
            return null;
          }
          decryptedKey = r2.result;
          passwd = r2.password;
        } else {
          decryptedKey = key;
        }
        let uids = [];
        let subkeys = decryptedKey.subkeys;
        decryptedKey.subkeys = [];
        if (!Array.isArray(subKeyIdentification)) {
          subKeyIdentification = [];
        }
        for (let i2 = 0; i2 < subkeys.length; i2++) {
          if (subKeyIdentification.length === 0 || subKeyIdentification.indexOf(i2 + 1) >= 0) {
            decryptedKey.subkeys.push(subkeys[i2]);
          }
        }
        for (let uid of decryptedKey.users) {
          if (uid.userID !== null && uid.revocationSignatures.length === 0) uids.push(uid.userID);
        }
        let deltaSeconds = 0;
        if (expiryDate > 0) {
          deltaSeconds = Math.floor(expiryDate - decryptedKey.getCreationTime()) / 1e3;
        }
        let opts = {
          privateKey: decryptedKey,
          keyExpirationTime: deltaSeconds,
          userIDs: uids,
          format: "object"
        };
        let newKey = (await PgpJS.reformatKey(opts)).privateKey;
        for (let i2 = 0; i2 < subkeys.length; i2++) {
          if (subKeyIdentification.indexOf(i2 + 1) < 0) {
            newKey.subkeys.push(subkeys[i2]);
          }
        }
        newKey.revocationSignatures = [];
        let finalKey = await newKey.update(decryptedKey);
        if (!keyIsDecrypted) {
          if (passwd.length > 0) {
            finalKey = await PgpJS.encryptKey({
              privateKey: finalKey,
              passphrase: passwd
            });
          }
        }
        return finalKey;
      },
      /**
       * Sign a key.
       *
       * @param {Object} signingKey: OpenPGP.js key that is used for signing the key
       * @param {Object} keyToSign: OpenPGP.js key to sign
       * @param {Array<String>} uidList: List of UIDs to sign
       * @param {String} password: (optional) password to decrypt key
       *                          If null or undefined, prompt for the password
       *
       * @return {Object}
       *  - {Object} signedKey: the signed key / null in case of error
       *  - {String} errorMsg: In case of error: Error message
       */
      signKey: async function(signingKey, keyToSign, uidList, password) {
        LogData2.DEBUG(`keys.js: signKey: (${signingKey.getFingerprint()}, ${keyToSign.getFingerprint()})
`);
        let r2 = await keys.decryptSecretKey(signingKey, "KEY_DECRYPT_REASON_MODIFY_KEY", password);
        if (!("result" in r2)) {
          return r2;
        }
        if (!Array.isArray(uidList)) {
          uidList = [];
        }
        let signedSomething = false;
        signingKey = r2.result;
        let { status, signing } = await this.getKeyCapabilities(signingKey);
        if (!signing) {
          if (status !== this.KeyStatus.VALID) {
            return LogData2.genError("ERROR_" + status, "Signing key not valid");
          } else {
            return LogData2.genError("ERROR_KEY_CANT_SIGN", "Signing key has no signing capabilites");
          }
        }
        for (let i2 = 0; i2 < keyToSign.users.length; i2++) {
          if (keyToSign.users[i2].userID === null) continue;
          if (uidList.length > 0 && uidList.indexOf(keyToSign.users[i2].userID.userID) < 0) continue;
          const uid = keyToSign.users[i2];
          try {
            await uid.verify(keyToSign.keyPacket);
          } catch (ex) {
            continue;
          }
          try {
            keyToSign.users[i2] = await uid.certify([signingKey], void 0, PgpJS.config);
            signedSomething = true;
          } catch (ex) {
            LogData2.DEBUG(`keys.js: signKey: ERROR: ${ex.toString()}
`);
            return {
              signedKey: null,
              errorMsg: ex.toString()
            };
          }
        }
        if (signedSomething) {
          return {
            result: keyToSign
          };
        } else {
          return LogData2.genError("ERROR_NO_VALID_USERID", "No valid user ID to sign");
        }
      },
      /**
       * Determine if an exception is thrown because the password was wrong
       *
       * @param {Object} exception: the exception thrown by OpenPGP.js
       *
       * @return {Boolean} true if yes, false if no
       */
      isWrongPassword: function(exception) {
        if ("message" in exception) {
          return exception.message.search(/Incorrect key passphrase/) >= 0;
        }
        return false;
      },
      /**
       * Determine if a partially encrypted key has been fully decrypted
       *
       * @param {Object} exception: the exception thrown by OpenPGP.js
       * @param {Object} key: OpenPGP.js key
       *
       * @return {Boolean} true if key is fully decrypted, false if not
       */
      isKeyFullyDecrypted: async function(exception, key) {
        if ("message" in exception && exception.message.search(/Key packet is already decrypted/) >= 0) {
          if (!await key.isDecrypted()) {
            return false;
          }
          for (let sk of key.subkeys) {
            if (!await sk.isDecrypted()) {
              return false;
            }
          }
          return true;
        }
        return false;
      },
      KeyStatus: {
        EXPIRED: "KEY_EXPIRED",
        REVOKED: "KEY_REVOKED",
        VALID: "KEY_VALID",
        UNKNWON: "?"
      },
      getKeyCapabilities: async function(key) {
        let status = this.KeyStatus.UNKNWON, signing = false, encryption = false;
        const NOW = /* @__PURE__ */ new Date();
        let expiryTime = await key.getExpirationTime();
        if (await key.isRevoked()) {
          status = this.KeyStatus.REVOKED;
        } else if (expiryTime < NOW) {
          status = this.KeyStatus.EXPIRED;
        } else {
          status = this.KeyStatus.VALID;
          try {
            if (await key.getSigningKey()) signing = true;
          } catch (x2) {
          }
          try {
            if (await key.getEncryptionKey()) encryption = true;
          } catch (x2) {
          }
        }
        return {
          status,
          signing,
          encryption
        };
      }
    };
    async function internalSecretKeyDecryption(key, reason, password) {
      LogData2.DEBUG(`keys.js: decryptSecretKey(${key.getFingerprint()})
`);
      if (!key.isPrivate()) return LogData2.genError("ERROR_NO_SECRET_KEY", "key is not a secret key");
      if (key.isDecrypted()) return {
        result: key
      };
      const fpr = key.getFingerprint().toUpperCase();
      if (typeof password !== "string") {
        password = await promptPassword(key.getFingerprint().toUpperCase(), reason);
      }
      if (password) {
        try {
          const decryptedKey = await PgpJS.decryptKey({
            privateKey: key,
            passphrase: password
          });
          if (decryptedKey) {
            return {
              result: decryptedKey,
              password
            };
          }
        } catch (ex) {
          if ("message" in ex) {
            if (keys.isWrongPassword(ex)) {
              return {
                wrongPassword: {
                  fpr,
                  reason
                }
              };
            } else if (keys.isKeyFullyDecrypted(ex, key)) {
              return {
                result: key,
                password
              };
            } else if (ex.message.search(/s2k/i) >= 0) {
              return "ERROR_KEY_UNSUPPORTED", "MD5 protected key - not supported";
            }
          } else {
            return {
              wrongPassword: {
                fpr,
                reason
              }
            };
          }
        }
      } else {
        return {
          needPassword: {
            fpr,
            reason
          }
        };
      }
      return null;
    }
    async function promptPassword(fpr, reason) {
      let r2 = await promptForInputFunc("password", { fpr, reason });
      if ("password" in r2) {
        return r2.password;
      }
      return null;
    }
    module2.exports = keys;
  }
});

// lib/keystore.js
var require_keystore = __commonJS({
  "lib/keystore.js"(exports2, module2) {
    "use strict";
    var LogData2 = require_logData();
    var Funcs = require_funcs();
    var PgpJS = (init_openpgp_min(), __toCommonJS(openpgp_min_exports));
    var FileSystem = require("node:fs");
    var Os = require("node:os");
    var Path = require("node:path");
    var Armor = require_armor();
    var Keys2 = require_keys();
    var keyStore2 = {
      _homePath: "",
      setHomePath: function(homePath) {
        const isWin32 = Os.type() == "Windows_NT";
        if (homePath) {
          this._homePath = homePath;
        } else {
          if (process.env.ENIGMAILKEYS) {
            this._homePath = process.env.ENIGMAILKEYS;
          } else {
            if (isWin32) {
              this._homePath = process.env.APPDATA + "\\Enigmail";
            } else {
              this._homePath = process.env.HOME + "/.enigmail";
            }
          }
        }
      },
      /**
      * Determine the path where the key database is stored
      *
      * @return {String}: full path including file name
      */
      getDatabasePath: function() {
        const DBName = "openpgpkeys.json";
        let dbName = this._homePath + Path.sep + DBName;
        return dbName;
      },
      getKeyFlags,
      /**
       * Import key(s) from a string provided
       *
       * @param {String} keyData:  the key data to be imported (ASCII armored)
       * @param {Boolean} minimizeKey: import the minimum key without any 3rd-party signatures
       * @param {Array of String} limitedUid: only import the UID specified
       *
       * @return {Object} or null in case no data / error:
       *   - {Number}          exitCode:        result code (0: OK)
       *   - {Array of String) importedKeys:    imported fingerprints
       *   - {Number}          importSum:       total number of processed keys
       *   - {Number}          importUnchanged: number of unchanged keys
       */
      importKeyData: async function(keyData, minimizeKey, limitedUid) {
        if (minimizeKey) {
          let firstUid = null;
          if (limitedUid && limitedUid.length > 0) {
            firstUid = limitedUid[0];
          }
          keyData = (await Keys2.getStrippedKey(keyData, firstUid, true)).write();
        }
        try {
          let imported = await this.writeKey(keyData);
          return {
            importedFpr: imported
          };
        } catch (ex) {
          return {
            importedFpr: []
          };
        }
      },
      /**
       * Write key(s) into the database.
       *
       * @param {String} keyData: armored or binary key data
       *
       * @return {Promise<Array<String>>} Array of imported fpr
       */
      writeKey: async function(keyData) {
        LogData2.DEBUG("keystore.js: writeKey()\n");
        let keys = [];
        if (typeof keyData === "string") {
          if (keyData.search(/-----BEGIN PGP (PUBLIC|PRIVATE) KEY BLOCK-----/) >= 0) {
            let blocks = Armor.splitArmoredBlocks(keyData);
            for (let b2 of blocks) {
              let res, packetData;
              try {
                packetData = await PgpJS.unarmor(b2);
                if (packetData.type === PgpJS.enums.armor.publicKey || packetData.type === PgpJS.enums.armor.privateKey) {
                  res = await PgpJS.readKeys({
                    binaryKeys: packetData.data,
                    config: {
                      ignoreMalformedPackets: true,
                      ignignoreUnsupportedPackets: true
                    }
                  });
                } else {
                  LogData2.DEBUG(`keystore.js: writeKey: invalid packet type: ${packetData.type}
`);
                }
              } catch (x2) {
                try {
                  const SignaturePacket = PgpJS.SignaturePacket;
                  const packetList = await PgpJS.PacketList.fromBinary(packetData.data, {
                    [SignaturePacket.tag]: SignaturePacket
                  });
                  res = await appendRevocationCert(packetList);
                } catch (x3) {
                  LogData2.DEBUG(`keystore.js: writeKey: error while reading keys: ${x3.toString()}
`);
                }
              }
              if (res) {
                keys = keys.concat(res);
              }
            }
          } else {
            let data = stringToUint8Array(keyData);
            let res = await PgpJS.readKeys({
              binaryKeys: data
            });
            keys = res;
          }
        } else {
          keys = await PgpJS.readKeys({
            binaryKeys: keyData
          });
        }
        let importedFpr = [];
        for (let k2 of keys) {
          try {
            await keyStoreDatabase.writeKeyToDb(k2);
            importedFpr.push(k2.getFingerprint().toUpperCase());
          } catch (x2) {
            LogData2.ERROR(`keystore.js: writeKey: error ${x2.toString()} / ${x2.stack}
`);
          }
        }
        await keyStoreDatabase.writeDatabase();
        return importedFpr;
      },
      /**
       * Read one or more keys from the key store
       *
       * @param {Array<String>} keyArr: [optional] Array of Fingerprints or keyIDs. If not provided, all keys are returned
       *
       * @return {Promise<Array<Object>>} found keys:
       *    fpr: fingerprint
       *    key: OpenPGP.js Key object
       */
      readKeys: async function(keyArr) {
        let rows = await keyStoreDatabase.readKeysFromDb(keyArr);
        let foundKeys = [];
        for (let i2 in rows) {
          foundKeys.push({
            fpr: rows[i2].fpr,
            key: await PgpJS.readKey({
              armoredKey: rows[i2].armoredKey
            })
          });
        }
        return foundKeys;
      },
      /**
       * Read one or more keys from the key store
       *
       * @param {Array<String>} keyArr: [optional] Array of Fingerprints. If not provided, all keys are returned
       *
       * @return {Promise<Array<Object>>} found keys:
       *    object that suits as input for keyObj.contructor
       */
      readKeyMetadata: async function(keyArr = null) {
        LogData2.DEBUG(`keystore.js: readKeyMetadata(${keyArr})
`);
        const now = Date.now() / 1e3;
        let rows = await keyStoreDatabase.readKeysFromDb(keyArr);
        let foundKeys = [];
        for (let i2 in rows) {
          let key = getKeyExpired(rows[i2].metadata);
          if (key.expiryTime < now && key.expiryTime > 0) {
            key.keyUseFor = key.keyUseFor.replace(/[CES]/g, "");
            if (key.keyTrust.search(/^[fu]/) === 0) {
              key.keyTrust = "e";
            }
          }
          foundKeys.push(key);
        }
        return foundKeys;
      },
      readPublicKeys: async function(keyArr) {
        LogData2.DEBUG(`keystore.js: readPublicKeys(${keyArr})
`);
        let keyList = await this.readKeys(keyArr);
        if (keyList.length === 0) {
          return "";
        }
        let packets = new PgpJS.PacketList();
        for (let i2 in keyList) {
          let k2 = await keyList[i2].key.toPublic();
          packets = packets.concat(await k2.toPacketList());
        }
        return PgpJS.armor(PgpJS.enums.armor.publicKey, packets.write());
      },
      readMinimalPubKey: async function(fpr, email) {
        LogData2.DEBUG(`keystore.js: readMinimalPubKey(${fpr})
`);
        let keyList = await this.readKeys([fpr]);
        let ret = {
          exitCode: 0,
          keyData: "",
          errorMsg: ""
        };
        if (keyList.length > 0) {
          let k2 = await keyList[0].key.toPublic();
          k2.toPacketList();
          if (!email) {
            try {
              email = Funcs.stripEmail((await k2.getPrimaryUser()).user);
            } catch (ex) {
            }
          }
          let keyBlob = await Keys2.getStrippedKey(k2, email);
          if (keyBlob) {
            ret.keyData = btoa(String.fromCharCode.apply(null, keyBlob));
          }
        }
        return ret;
      },
      /**
       * Export secret key(s) as ASCII armored data
       *
       * @param {String}  keyArr      Specification by fingerprint or keyID, separate mutliple keys with spaces
       * @param {Boolean} minimalKey  if true, reduce key to minimum required
       *
       * @return {Object}:
       *   - {Number} exitCode:  result code (0: OK)
       *   - {String} keyData:   ASCII armored key data material
       *   - {String} errorMsg:  error message in case exitCode !== 0
       */
      readSecretKeys: async function(keyArr, minimalKey) {
        LogData2.DEBUG(`keystore.js: readSecretKeys(${keyArr})
`);
        let keyList = await this.readKeys(keyArr);
        if (keyList.length === 0) {
          return "";
        }
        let packets = new PgpJS.PacketList();
        for (let k2 of keyList) {
          if (k2.key.isPrivate()) {
            if (minimalKey) {
              packets = packets.concat(await Keys2.getStrippedKey(k2.key, null, true));
            } else {
              packets = packets.concat(await k2.key.toPacketList());
            }
          }
        }
        if (packets.length > 0) {
          return PgpJS.armor(PgpJS.enums.armor.privateKey, packets.write());
        }
        return "";
      },
      /**
       * Delete one or more keys from the key store
       *
       * @param {Array<String>} keyArr: Array of Fingerprints
       *
       * @return undefined
       */
      deleteKeys: async function(keyArr) {
        return keyStoreDatabase.deleteKeysFromDb(keyArr);
      },
      /**
       * Write a key to the database. Unlike writeKey(), this function does not merge
       * an existing key with the new key, but replaces an existing key entirely.
       *
       * @param {Object} keyObj: OpenPGP.js key object
       */
      replaceKey: async function(keyObj) {
        LogData2.DEBUG(`keystore.js: replaceKey(${keyObj.getFingerprint()})
`);
        let error = null;
        await keyStoreDatabase.deleteKeysFromDb([keyObj.getFingerprint().toUpperCase()]);
        await keyStoreDatabase.writeKeyToDb(keyObj);
        await keyStoreDatabase.writeDatabase();
      },
      /**
       * Retrieve the OpenPGP.js key objects for a given set of keyIds.
       *
       * @param {Boolean} secretKeys:     if true, only return secret keys
       * @param {Array<String>} keyIdArr: keyIDs to look up. If null, then all
       *                                  secret or public keys are retrieved
       *
       * @return {Array<Object>}: array of the found key objects
       */
      getKeysForKeyIds: async function(secretKeys, keyIdArr = null) {
        let findKeyArr = [];
        if (secretKeys) {
          let keys2 = await keyStore2.readKeyMetadata(keyIdArr);
          for (let k2 of keys2) {
            if (k2.secretAvailable) {
              findKeyArr.push(k2.fpr);
            }
          }
        } else {
          findKeyArr = keyIdArr;
        }
        let returnArray = [];
        let keys = await keyStore2.readKeys(findKeyArr);
        for (let k2 of keys) {
          if (!secretKeys) {
            let pk = k2.key.toPublic();
            returnArray.push(pk);
          } else {
            returnArray.push(k2.key);
          }
        }
        returnArray.toPacketList = function() {
          let pktList = new PgpJS.PacketList();
          for (let i2 = 0; i2 < this.length; i2++) {
            pktList = pktList.concat(this[i2].toPacketList());
          }
          return pktList;
        };
        return returnArray;
      },
      /**
       * Write a revocation certificate to its default location
       */
      storeRevocationCert: function(key, certificateData) {
        const fpr = key.getFingerprint().toUpperCase();
        LogData2.DEBUG(`keystore.js: storeRevocationCert(${fpr})
`);
        let dbPath = Path.dirname(keyStore2.getDatabasePath());
        dbPath += "/openpgp-revocs.d";
        if (!FileSystem.existsSync(dbPath)) {
          FileSystem.mkdirSync(dbPath, { recursive: true });
        }
        const uid = key.users[0].userID.userID;
        dbPath += `/${fpr}.rev`;
        const data = `This is a revocation certificate for the OpenPGP key:

Key ID:      ${fpr}
uid          ${uid}

A revocation certificate is a kind of "kill switch" to publicly
declare that a key shall not anymore be used.  It is not possible
to retract such a revocation certificate once it has been published.

Use it to revoke this key in case of a compromise or loss of
the secret key.

To avoid an accidental use of this file, a colon has been inserted
before the 5 dashes below.  Remove this colon with a text editor
before importing and publishing this revocation certificate.

:${certificateData}
`;
        FileSystem.writeFileSync(dbPath, data);
      },
      /**
       * Determine the key status and return the corresponding code
       *
       * @param {Object} key: OpenPGP.js key object
       *
       * @return {String} status code
       */
      getKeyStatusCode,
      /**
       * Initialize module
       *
       * @return {Boolean} true if successful
       */
      init: function() {
        LogData2.DEBUG(`keystore.js: init()
`);
        gKeyCache = null;
        this.setHomePath();
        let k2 = keyStoreDatabase.openDatabase();
        return k2 ? true : false;
      },
      getPrimaryUser: function(keyMetadata) {
        for (let uid of keyMetadata.userIds) {
          if (uid.isPrimary) return uid;
        }
        return keyMetadata.userIds[0];
      },
      getKeyMetadata
    };
    var gKeyCache = null;
    var keyStoreDatabase = {
      openDatabase: function() {
        LogData2.DEBUG("keystore.js: openDatabase()\n");
        try {
          const fileName = keyStore2.getDatabasePath();
          if (!gKeyCache) {
            if (FileSystem.existsSync(fileName)) {
              let data = FileSystem.readFileSync(fileName);
              gKeyCache = JSON.parse(data);
            } else {
              FileSystem.mkdirSync(Path.dirname(fileName), { recursive: true });
              gKeyCache = [];
            }
          }
        } catch (ex) {
          LogData2.DEBUG(`keystore.js: openDatabase(): error ${ex}
`);
          gKeyCache = [];
        }
        return gKeyCache;
      },
      /**
       * Store a key in the database
       *
       * @param {Object} key: OpenPGP.js Key object
       *
       * no return value
       */
      writeKeyToDb: async function(key) {
        LogData2.DEBUG(`keystore.js: writeKeyToDb(${key})
`);
        const fpr = key.getFingerprint().toUpperCase();
        let rows = await this.readKeysFromDb([fpr]);
        let metadata;
        if (rows.length === 1) {
          let oldKey = await PgpJS.readKey({
            armoredKey: rows[0].armoredKey
          });
          try {
            key = await key.update(oldKey);
          } catch (x2) {
            try {
              key = await oldKey.update(key);
            } catch (x3) {
              if (oldKey.isPrivate()) key = oldKey;
            }
          }
          metadata = await getKeyMetadata(key);
          rows[0].metadata = metadata;
          rows[0].armoredKey = await key.armor();
          rows[0].fpr = metadata.fpr;
          rows[0].ids = metadata.subKeys.map((sk) => {
            return sk.keyId;
          });
          rows[0].ids.push(fpr);
          rows[0].ids.push(metadata.keyId);
        } else {
          metadata = await getKeyMetadata(key);
          let insObj = {
            fpr,
            metadata,
            armoredKey: await key.armor(),
            ids: metadata.subKeys.map((sk) => {
              return sk.keyId;
            })
          };
          insObj.ids.push(fpr);
          insObj.ids.push(metadata.keyId);
          gKeyCache.push(insObj);
        }
        LogData2.DEBUG(`keystore.js: writeKeyToDb: wrote ${fpr}
`);
      },
      writeDatabase: function() {
        LogData2.DEBUG(`keystore.js: writeDatabase()
`);
        try {
          const fileName = keyStore2.getDatabasePath();
          FileSystem.writeFileSync(fileName, JSON.stringify(gKeyCache));
        } catch (ex) {
          LogData2.DEBUG(`keystore.js: writeDatabase(): error ${ex}
`);
        }
      },
      /**
       * Read one or more keys from the database
       *
       * @param {Array<String>} keyArr: [optional] Array of Fingerprints or keyIDs. If not provided, all keys are returned
       *
       * @return {Array<Key>} List of OpenPGP.js Key objects.
       */
      readKeysFromDb: async function(keyArr = null) {
        LogData2.DEBUG("keystore.js: readKeysFromDb(" + (keyArr ? keyArr.length : "null") + ")\n");
        const rows = [];
        if (keyArr !== null) {
          let allKeys = "." + keyArr.map((key) => {
            return key.replace(/^0x/, "").replace(/[^A-Fa-f0-9]/g, "").toUpperCase();
          }).join(".") + ".";
          for (let key of gKeyCache) {
            for (let i2 of key.ids) {
              if (allKeys.indexOf(`.${i2}.`) >= 0) {
                rows.push(key);
                break;
              }
            }
          }
          return rows;
        }
        return gKeyCache;
      },
      /**
       * Delete one or more keys from the database
       *
       * @param {Array<String>} keyArr: Array of Fingerprints
       */
      deleteKeysFromDb: async function(keyArr = []) {
        LogData2.DEBUG(`keystore.js: deleteKeysFromDb(${keyArr})
`);
        const rows = [], foundKeys = [];
        if (keyArr !== null) {
          let allKeys = "." + keyArr.map((key) => {
            return key.replace(/^0x/, "").replace(/[^A-Fa-f0-9]/g, "").toUpperCase();
          }).join(".") + ".";
          for (let i2 in gKeyCache) {
            for (let j2 in gKeyCache[i2].ids) {
              if (allKeys.indexOf(`.${gKeyCache[i2].ids[j2]}.`) >= 0) {
                rows.push(i2);
                foundKeys.push(gKeyCache[i2].ids[j2]);
                break;
              }
            }
          }
          for (let i2 = rows.length; i2--; i2 >= 0) {
            gKeyCache.splice(rows[i2], 1);
          }
          this.writeDatabase();
        }
        return foundKeys;
      }
    };
    function stringToUint8Array(str) {
      return Uint8Array.from(Array.from(str).map((x2) => {
        return x2.charCodeAt(0);
      }));
    }
    async function getKeyMetadata(key) {
      let keyObj = {};
      let uatNum = 0;
      keyObj.keyId = key.getKeyID().toHex().toUpperCase();
      keyObj.secretAvailable = key.isPrivate();
      keyObj.created = key.getCreationTime();
      try {
        keyObj.expiryTime = await key.getExpirationTime();
      } catch (x2) {
        keyObj.expiryTime = "";
      }
      let keyTrustLevel = await getKeyStatusCode(key);
      if (keyTrustLevel === "f" && keyObj.secretAvailable) keyTrustLevel = "u";
      let keyIsValid = keyTrustLevel.search(/^[uf]$/) === 0;
      let prim = null;
      try {
        prim = (await key.getPrimaryUser()).user;
      } catch (ex) {
        if (key.users.length > 0) {
          prim = key.users[0];
        }
      }
      const keyUse = await getKeyFlags(key);
      keyObj.keyUseFor = (keyUse.cert ? "c" : "") + (keyUse.certValid ? "C" : "") + (keyUse.sign ? "s" : "") + (keyUse.signValid ? "S" : "") + (keyUse.enc ? "e" : "") + (keyUse.encValid ? "E" : "");
      keyObj.algoSym = getAlgorithmDesc(key.getAlgorithmInfo().algorithm);
      keyObj.keySize = key.getAlgorithmInfo().bits;
      keyObj.fpr = key.getFingerprint().toUpperCase();
      keyObj.validity = keyTrustLevel;
      keyObj.userIds = [];
      for (let i2 in key.users) {
        let trustLevel = "f";
        if (keyIsValid) {
          try {
            trustLevel = await getUserStatusCode(key.users[i2], key);
          } catch (x2) {
          }
        } else {
          trustLevel = keyTrustLevel;
        }
        if (key.users[i2].userAttribute !== null) {
          keyObj.userIds.push({
            userId: `UAT:${uatNum}`,
            validity: trustLevel,
            type: "uat"
          });
          ++uatNum;
        } else {
          let uidObj = {
            userId: key.users[i2].userID.userID,
            validity: trustLevel,
            type: "uid",
            isPrimary: key.users[i2] == prim
          };
          keyObj.userIds.push(uidObj);
        }
      }
      keyObj.subKeys = [];
      let sk = key.getSubkeys();
      for (let i2 in sk) {
        let exp = 0;
        try {
          exp = await sk[i2].getExpirationTime();
        } catch (x2) {
        }
        let keyTrust = "f";
        if (keyIsValid) {
          try {
            keyTrust = await getSubKeyStatusCode(sk[i2]);
            if (keyTrust === "f" && keyObj.secretAvailable) keyTrust = "u";
          } catch (x2) {
          }
        } else {
          keyTrust = keyTrustLevel;
        }
        keyObj.subKeys.push({
          keyId: sk[i2].getKeyID().toHex().toUpperCase(),
          fpr: sk[i2].getFingerprint().toUpperCase(),
          created: sk[i2].getCreationTime(),
          expiryTime: exp,
          validity: keyTrust,
          keyUseFor: (sk[i2].getAlgorithmInfo().algorithm.search(/sign/i) ? "s" : "") + (sk[i2].getAlgorithmInfo().algorithm.search(/encrypt/i) ? "e" : ""),
          keySize: sk[i2].getAlgorithmInfo().bits,
          algoSym: getAlgorithmDesc(sk[i2].getAlgorithmInfo().algorithm)
        });
      }
      return keyObj;
    }
    function getKeyExpired(keyObj) {
      const now = /* @__PURE__ */ new Date();
      if (!("keyTrust" in keyObj)) {
        keyObj.keyTrust = "v";
      }
      let keyIsValid = keyObj.keyTrust === "v";
      if (keyIsValid) {
        if (keyObj.expiryTime > 0 && keyObj.expiryTime < now) {
          keyObj.keyTrust = "e";
          for (let i2 in keyObj.subKeys) {
            keyObj.subKeys[i2].keyTrust = "e";
          }
          for (let i2 in keyObj.userIds) {
            keyObj.userIds[i2].keyTrust = "e";
          }
        } else {
          for (let i2 in keyObj.subKeys) {
            if (keyObj.subKeys[i2].expiryTime > 0 && keyObj.subKeys[i2].expiryTime < now) {
              keyObj.subKeys[i2].keyTrust = "e";
            }
          }
        }
      }
      return keyObj;
    }
    async function appendRevocationCert(packetList) {
      LogData2.DEBUG("keystore.js: appendRevocationCert()\n");
      const revCert = packetList.findPacket(PgpJS.SignaturePacket.tag);
      if (revCert.signatureType !== PgpJS.enums.signature.keyRevocation) {
        return null;
      }
      let keyId = getFprFromArray(revCert.issuerFingerprint);
      if (!keyId) {
        keyId = revCert.issuerKeyID.toHex().toUpperCase();
      }
      let foundKeys = await keyStore2.getKeysForKeyIds(false, [keyId]);
      if (foundKeys.length === 1) {
        return [await foundKeys[0].applyRevocationCertificate(await PgpJS.armor(PgpJS.enums.armor.signature, packetList.write()))];
      }
      return null;
    }
    function getFprFromArray(arr) {
      const HEX_TABLE = "0123456789ABCDEF";
      if (!arr) return null;
      let hex = "";
      for (let j2 = 0; j2 < arr.length; j2++) {
        hex += HEX_TABLE.charAt((arr[j2] & 240) >> 4) + HEX_TABLE.charAt(arr[j2] & 15);
      }
      return hex;
    }
    async function getKeyStatusCode(key) {
      let now = /* @__PURE__ */ new Date();
      try {
        if (await key.isRevoked(null, null, now)) {
          return "r";
        } else if (!key.users.some((user) => user.userID && user.selfCertifications.length)) {
          return "i";
        } else {
          const {
            user,
            selfCertification
          } = await key.getPrimaryUser(now, {}) || {};
          if (!user) return "i";
          if (isDataExpired(key.keyPacket, selfCertification, now)) {
            return "e";
          }
        }
      } catch (x2) {
        return "i";
      }
      return "f";
    }
    async function getUserStatusCode(user, key) {
      try {
        if (!user.selfCertifications.length) {
          return "i";
        }
        try {
          if (await user.verify()) {
            return "f";
          }
        } catch (x2) {
        }
        const results = ["i"].concat(
          await Promise.all(user.selfCertifications.map(async function(selfCertification) {
            try {
              if (selfCertification.revoked || await user.isRevoked(key.keyPacket, selfCertification)) {
                return "r";
              }
              if (selfCertification.isExpired()) {
                return "e";
              }
              return "i";
            } catch (x2) {
              return "i";
            }
          }))
        );
        return results.some((status) => status === "f") ? "f" : results.pop();
      } catch (x2) {
        return "i";
      }
    }
    async function getSubKeyStatusCode(key) {
      const dataToVerify = {
        key: null,
        bind: key.keyPacket
      };
      const now = /* @__PURE__ */ new Date();
      try {
        if (await key.verify()) {
          return "f";
        }
      } catch (x2) {
      }
      const bindingSignature = getLatestSignature(key.bindingSignatures, now);
      if (bindingSignature.revoked || await key.isRevoked(key.keyPacket, bindingSignature, null, now)) {
        return "r";
      }
      if (bindingSignature.isExpired(now)) {
        return "e";
      }
      return "i";
    }
    function getLatestSignature(signatures, date = /* @__PURE__ */ new Date()) {
      let signature = signatures[0];
      for (let i2 = 1; i2 < signatures.length; i2++) {
        if (signatures[i2].created >= signature.created && (signatures[i2].created <= date || date === null)) {
          signature = signatures[i2];
        }
      }
      return signature;
    }
    function isDataExpired(keyPacket, signature, date = /* @__PURE__ */ new Date()) {
      const normDate = normalizeDate(date);
      if (normDate !== null) {
        const expirationTime = getExpirationTime(keyPacket, signature);
        return !(keyPacket.created <= normDate && normDate < expirationTime) || signature && signature.isExpired(date);
      }
      return false;
    }
    function getExpirationTime(keyPacket, signature) {
      let expirationTime;
      try {
        if (signature.keyExpirationTime > 0) {
          expirationTime = keyPacket.created.getTime() + signature.keyExpirationTime * 1e3;
        }
        return expirationTime ? new Date(expirationTime) : Infinity;
      } catch (ex) {
        return Infinity;
      }
    }
    function normalizeDate(time = Date.now()) {
      return time === null ? time : new Date(Math.floor(Number(time) / 1e3) * 1e3);
    }
    async function getKeyFlags(key) {
      const keyUse = {
        sign: 0,
        signValid: 0,
        enc: 0,
        encValid: 0,
        cert: 0,
        certValid: 0
      };
      function determineFlags(inputFlags, isVerified) {
        try {
          if (inputFlags & PgpJS.enums.keyFlags.certifyKeys) ++keyUse.cert;
          if (inputFlags & PgpJS.enums.keyFlags.signData) ++keyUse.sign;
          if (inputFlags & PgpJS.enums.keyFlags.encryptCommunication) ++keyUse.enc;
          if (inputFlags & PgpJS.enums.keyFlags.encryptStorage) ++keyUse.enc;
          if (isVerified) {
            if (inputFlags & PgpJS.enums.keyFlags.certifyKeys) ++keyUse.certValid;
            if (inputFlags & PgpJS.enums.keyFlags.signData) ++keyUse.signValid;
            if (inputFlags & PgpJS.enums.keyFlags.encryptCommunication) ++keyUse.encValid;
            if (inputFlags & PgpJS.enums.keyFlags.encryptStorage) ++keyUse.encValid;
          }
        } catch (x2) {
        }
      }
      try {
        await key.verifyPrimaryKey();
        await key.verifyAllUsers();
      } catch (x2) {
      }
      let keyStatusCode = await getKeyStatusCode(key);
      for (let sk of key.getSubkeys()) {
        let sigVerified = false;
        try {
          await sk.verify(key.primaryKey);
          sigVerified = true;
        } catch (x2) {
        }
        for (let sig in sk.bindingSignatures) {
          let skNotExp = !isDataExpired(sk.keyPacket, sk.bindingSignatures[sig]);
          for (let flg in sk.bindingSignatures[sig].keyFlags) {
            determineFlags(sk.bindingSignatures[sig].keyFlags[flg], sigVerified && skNotExp && keyStatusCode === "f");
          }
        }
      }
      for (let usr of key.users) {
        let usrVerified = false;
        try {
          await usr.verify();
          usrVerified = true;
        } catch (x2) {
        }
        for (let sig in usr.selfCertifications) {
          for (let flg in usr.selfCertifications[sig].keyFlags) {
            determineFlags(usr.selfCertifications[sig].keyFlags[flg], usrVerified && keyStatusCode === "f");
          }
        }
      }
      for (let sig in key.directSignatures) {
        for (let flg in key.directSignatures[sig].keyFlags) {
          determineFlags(key.directSignatures[sig].keyFlags[flg], key.directSignatures[sig].verified && keyStatusCode === "f");
        }
      }
      return keyUse;
    }
    function getAlgorithmDesc(algorithm) {
      algorithm = algorithm.toUpperCase();
      if (algorithm.search(/^RSA/) === 0) {
        algorithm = "RSA";
      }
      return algorithm;
    }
    keyStore2.setHomePath(null);
    module2.exports = keyStore2;
  }
});

// lib/encrypt.js
var require_encrypt = __commonJS({
  "lib/encrypt.js"(exports2, module2) {
    "use strict";
    var LogData2 = require_logData();
    var PgpJS = (init_openpgp_min(), __toCommonJS(openpgp_min_exports));
    var Keys2 = require_keys();
    var keyStore2 = require_keystore();
    var Encrypt = {
      /**
       * Encrypt messages
       *
       * @param {String} plainText: data to encrypt
       * @param {Array<String>} recipients: fpr of recipients
       * @param {Array<String>} hiddenRecipients: [OPTIONAL] fpr of hidden recipients (bcc)
       * @param {String} signingKey: [OPTIONAL] fpr of signing key ID. if provided, message is singed and encrypted, otherwise only encrypted
       * @param {String} filename:  [OPTIONAL] filename to use for encryption
       * @param {String} password: [OPTIONAL] password for signing key
       
       *
       * @return {Promise<Object>}:
       *   - result:
       *     - {String} data:        encrypted data
       *   - error:
       */
      encryptMessage: async function(plainText, recipients, hiddenRecipients, signingKey, filename, password) {
        LogData2.DEBUG(`encrypt.js: encryptMessage(${plainText.length}, ${recipients}.join(", "), ${hiddenRecipients}, ${signingKey}, ${plainText.length})
`);
        try {
          if (Array.isArray(hiddenRecipients)) {
            recipients = recipients.concat(hiddenRecipients);
          }
          return await encryptData(recipients, signingKey, plainText, filename, password);
        } catch (ex) {
          LogData2.DEBUG(`encrypt.js: encryptMessage: ERROR: ${ex.toString()}
`);
          LogData2.DEBUG(`${ex.stack}
`);
          return LogData2.genError("ERROR_ENCRYPT", ex.toString());
        }
        return null;
      },
      /**
       * 
       * @param {String} plainText:  data to sign
       * @param {String} signingKey: fpr of signing key ID. if provided, message is singed and encrypted, otherwise only encrypted
       * @param {Boolean} detached:  true: create a detached signature / false: create cleartext signed message
       * @param {String} password:   [OPTIONAL] password for signing key
       */
      signMessage: async function(plainText, signingKey, detached = true, password) {
        return await signData(signingKey, plainText, detached, password);
      }
    };
    async function encryptData(recipientKeyIds, signingKeyId, text, filename, password) {
      LogData2.DEBUG(`encrypt.js: encryptData(${recipientKeyIds.length}, ${signingKeyId})
`);
      let privateKeys = void 0;
      if (recipientKeyIds.length < 1) {
        return LogData2.genError("ERROR_NO_RECIPIENT", `No recipients specified for encryption`);
      }
      if (signingKeyId) {
        privateKeys = await keyStore2.getKeysForKeyIds(true, [signingKeyId]);
        if (privateKeys.length === 0) {
          return LogData2.genError("ERROR_INVALID_SIGNING_KEY", `Could not find key ${signingKeyId}`);
        }
        let r2 = await Keys2.decryptSecretKey(privateKeys[0], "KEY_DECRYPT_REASON_SIGNCRYPT_MSG", password);
        if (!("result" in r2)) {
          return r2;
        }
        privateKeys = r2.result;
        const { status, signing } = await Keys2.getKeyCapabilities(privateKeys);
        if (status !== Keys2.KeyStatus.VALID) {
          return LogData2.genError("ERROR_INVALID_SIGNING_KEY", `Key ${privateKeys.getFingerprint.toUpperCase()} is not valid`);
        }
        if (!signing) {
          return LogData2.genError("ERROR_INVALID_SIGNING_KEY", `Key ${privateKeys.getFingerprint.toUpperCase()} cannot be used for signing`);
        }
      }
      let uniqueKeyIds = [...new Set(recipientKeyIds)];
      let publicKeys = await keyStore2.getKeysForKeyIds(false, uniqueKeyIds);
      if (!filename) filename = "";
      if (publicKeys.length === 0) {
        return LogData2.genError("ERROR_NO_VALID_RECIPIENTS", `Could not find recipient to encrypt message`);
      }
      if (uniqueKeyIds.length > publicKeys.length) {
        let notFound = [];
        for (let req of uniqueKeyIds) {
          let found = false;
          for (let pk of publicKeys) {
            if (pk.getFingerprint().toUpperCase() === req) {
              found = true;
              break;
            }
            if (!found) {
              notFound.push(req);
            }
          }
        }
        return LogData2.genError("ERROR_INVALID_RECIPIENTS", "Could not find all recipents", notFound);
      }
      for (let pk of publicKeys) {
        const { status, encryption } = await Keys2.getKeyCapabilities(pk);
        if (status !== Keys2.KeyStatus.VALID) {
          return LogData2.genError("ERROR_INVALID_RECIPIENT_CERT", `Certificate ${pk.getFingerprint.toUpperCase()} is not valid`);
        }
        if (!encryption) {
          return LogData2.genError("ERROR_INVALID_RECIPIENT_CERT", `Certificate ${pk.getFingerprint.toUpperCase()} cannot be used for encryption`);
        }
      }
      try {
        return {
          result: {
            data: await PgpJS.encrypt({
              message: await PgpJS.createMessage({
                text,
                filename
              }),
              encryptionKeys: publicKeys,
              signingKeys: privateKeys,
              // for signing
              format: "armored"
            })
          }
        };
      } catch (ex) {
        return LogData2.genError("ERROR_ENCRYPTION_FAILED", ex.message);
      }
      ;
    }
    async function signData(signingFpr, text, detachedSignature, password) {
      LogData2.DEBUG(`encrypt.js: signData(${signingFpr})
`);
      if (!signingFpr) {
        return LogData2.genError("ERROR_MISSING_PRIVATE_KEY", "No private key FPR provided");
      }
      let privateKeys = await keyStore2.getKeysForKeyIds(true, [signingFpr]);
      if (privateKeys.length === 0) {
        return LogData2.genError("ERROR_INVALID_SIGNING_KEY", `Could not find key ${signingFpr}`);
      }
      let r2 = await Keys2.decryptSecretKey(privateKeys[0], "KEY_DECRYPT_REASON_SIGN_MSG", password);
      if (!("result" in r2)) {
        return r2;
      }
      privateKeys = r2.result;
      let signedData;
      try {
        if (detachedSignature) {
          signedData = await PgpJS.sign({
            message: await PgpJS.createMessage({
              text
            }),
            signingKeys: privateKeys,
            detached: detachedSignature,
            format: "armored"
          });
        } else {
          signedData = await PgpJS.sign({
            message: await PgpJS.createCleartextMessage({
              text
            }),
            signingKeys: privateKeys,
            detached: detachedSignature,
            format: "armored"
          });
        }
      } catch (ex) {
        return LogData2.genError("ERROR_SIGN_MESSAGE", ex.toString());
      }
      return {
        result: {
          data: signedData
        }
      };
    }
    module2.exports = Encrypt;
  }
});

// lib/decrypt.js
var require_decrypt = __commonJS({
  "lib/decrypt.js"(exports2, module2) {
    "use strict";
    var LogData2 = require_logData();
    var PgpJS = (init_openpgp_min(), __toCommonJS(openpgp_min_exports));
    var Keys2 = require_keys();
    var keyStore2 = require_keystore();
    var DecrypConst = {
      /* Status flags */
      GOOD_SIGNATURE: "GOOD_SIGNATURE",
      BAD_SIGNATURE: "BAD_SIGNATURE",
      UNVERIFIED_SIGNATURE: "UNVERIFIED_SIGNATURE",
      EXPIRED_SIGNATURE: "EXPIRED_SIGNATURE",
      EXPIRED_CERT_SIGNATURE: "EXPIRED_CERT_SIGNATURE",
      EXPIRED_CERT: "EXPIRED_CERT",
      REVOKED_CERT: "REVOKED_CERT",
      NO_CERTIFICATE_FOUND: "NO_CERTIFICATE_FOUND",
      NO_PRIVATE_KEY: "NO_PRIVATE_KEY",
      BAD_PASSPHRASE: "BAD_PASSPHRASE",
      DECRYPTION_FAILED: "DECRYPTION_FAILED",
      DECRYPTION_OKAY: "DECRYPTION_OKAY",
      INVALID_OR_MISSING_MDC: "INVALID_OR_MISSING_MDC",
      VALID_KEY: "VALID_KEY",
      VERIFICATION_FAILED: "VERIFICATION_FAILED",
      INVALID_MESSAGE: "INVALID_MESSAGE"
    };
    var Decrypt = {
      /**
         * Process an OpenPGP message
         *
         * @param {String} encrypted     The encrypted data
         *
         * @return {Promise<Object>} - Return object with decryptedData and status information:
         *
         */
      processPgpMessage: async function(encrypted, password) {
        LogData2.DEBUG(`processPgpMessage(${encrypted.length})
`);
        const retData = getReturnObj();
        try {
          let message;
          if (encrypted.search(/^-----BEGIN PGP/m) >= 0) {
            message = await PgpJS.readMessage({
              armoredMessage: encrypted
            });
          } else {
            let encArr = ensureUint8Array(encrypted);
            message = await PgpJS.readMessage({
              binaryMessage: encArr
            });
          }
          let idx = message.packets.indexOfTag(PgpJS.enums.packet.compressedData);
          if (idx.length > 0 && idx[0] === 0) {
            message = message.unwrapCompressed();
          }
          let pubKeyIds = message.getEncryptionKeyIDs().map((keyId) => {
            return keyId.toHex().toUpperCase();
          });
          if (pubKeyIds.length === 0 && message.getSigningKeyIDs().length > 0) {
            return this.verifyMessage(message, true);
          }
          idx = message.packets.indexOfTag(PgpJS.enums.packet.literalData);
          if (idx.length > 0 && idx[0] === 0) {
            let litDataArr = message.getLiteralData();
            retData.decryptedData = arrayBufferToString(litDataArr);
            return retData;
          }
          return this.decryptMessage(message, pubKeyIds, password);
        } catch (ex) {
          LogData2.DEBUG(`processPgpMessage: ERROR: ${ex.toString()}
${ex.stack}
${encrypted}
`);
          retData.errorMsg = ex.toString();
          retData.decryptionResult.push(DecrypConst.DECRYPTION_FAILED);
        }
        retData.decryptionResult = [...new Set(retData.decryptionResult)];
        return retData;
      },
      decryptMessage: async function(message, pubKeyIds, password) {
        LogData2.DEBUG(`decryptMessage(${pubKeyIds.join(", ")})
`);
        let secretKeys = [];
        const retData = getReturnObj();
        retData.encryptionKeys = pubKeyIds;
        try {
          secretKeys = await keyStore2.getKeysForKeyIds(true, pubKeyIds);
          if (secretKeys.length === 0) {
            retData.decryptionResult.push(DecrypConst.NO_PRIVATE_KEY);
            return retData;
          }
          for (let sk of secretKeys) {
            let r2 = await Keys2.decryptSecretKey(sk, "KEY_DECRYPT_REASON_SIGNCRYPT_MSG", password);
            if (!("result" in r2)) {
              return r2;
            }
            let decryptedSecKey = r2.result;
            if ("subkeys" in decryptedSecKey) {
              decryptedSecKey.revocationSignatures = [];
              let result = await PgpJS.decrypt({
                message,
                format: "binary",
                decryptionKeys: decryptedSecKey
              });
              let verifiation;
              if (result && "data" in result) {
                retData.decryptedData = ensureString(result.data);
                retData.decryptionResult = DecrypConst.DECRYPTION_OKAY;
                if ("signatures" in result && result.signatures.length > 0) {
                  let pkt = new PgpJS.PacketList();
                  for (let sig of result.signatures) {
                    let sigPackets = await sig.signature;
                    pkt = pkt.concat(sigPackets.packets);
                  }
                  verifiation = await this.verifyDetached(result.data, pkt);
                  retData.signatures = verifiation.signatures;
                  retData.errorMsg = verifiation.errorMsg;
                }
              }
              if ("filename" in result) {
                retData.encryptedFileName = result.filename;
              }
              break;
            } else {
              LogData2.DEBUG(`decrypt invalid or no passphrase supplied
`);
              retData.decryptionResult.push(DecrypConst.BAD_PASSPHRASE);
            }
          }
        } catch (ex) {
          if ("message" in ex && ex.message.search(/(Message .*not authenticated|missing MDC|Modification detected)/) > 0) {
            retData.decryptionResult.push(DecrypConst.INVALID_OR_MISSING_MDC);
          } else {
            LogData2.DEBUG(`decryptMessage: ERROR: ${ex.toString()}
`);
            retData.exitCode = 1;
            retData.decryptionResult.push(DecrypConst.DECRYPTION_FAILED);
            retData.errorMsg = ex.toString();
          }
        }
        return retData;
      },
      verify: async function(data, options) {
        LogData2.DEBUG(`verify(${data.length})
`);
        let result = getReturnObj();
        try {
          let msg = await PgpJS.readCleartextMessage({
            cleartextMessage: data
          });
          let binaryData = extractDataFromClearsignedMsg(data);
          if (msg && "signature" in msg) {
            result = await this.verifyDetached(binaryData, msg.signature.armor(), true);
          }
        } catch (ex) {
          if (ex.toString().search(/cleartext signed/) >= 0) {
            return this.processPgpMessage(data);
          }
          LogData2.DEBUG(`verify: ERROR: ${ex.toString()}
`);
          result.errorMsg = ex.toString();
          result.signatures = [getSigErrorObj(DecrypConst.UNVERIFIED_SIGNATURE)];
        }
        return result;
      },
      /**
       * Verify a message and return the signature verification status
       *
       * @param {Object} messageObj: OpenPGP.js Message
       * @param {Boolean} returnData: if true, inculde the verified data in the result
       *
       * @return {Promise<Object>} ResultObj
       */
      verifyMessage: async function(messageObj, returnData = false) {
        LogData2.DEBUG(`verifyMessage()
`);
        const SIG_STATUS = {
          unreadable_sig: 0,
          unknown_key: 1,
          bad_signature: 2,
          good_sig_invalid_key: 3,
          good_sig_expired_key: 4,
          valid_signature: 5
        };
        const result = getReturnObj();
        result.decryptionResult = [];
        let currentKey = null, signatureStatus = -1;
        let keyIds = messageObj.getSigningKeyIDs().map((keyId) => {
          return keyId.toHex().toUpperCase();
        });
        if (keyIds.length === 0) {
          result.signatures = [getSigErrorObj(DecrypConst.INVALID_MESSAGE)];
          return result;
        }
        let armoredPubKeys = await keyStore2.readPublicKeys(keyIds);
        let pubKeys = [];
        if (armoredPubKeys.length > 0) {
          pubKeys = await PgpJS.readKeys({
            armoredKeys: armoredPubKeys
          });
        }
        LogData2.DEBUG(`verifyMessage: found ${pubKeys.length} key(s)
`);
        for (let key of pubKeys) {
          if (await key.isRevoked()) {
            key.revocationSignatures = [];
          }
        }
        let ret = {};
        for (let key of pubKeys) {
          try {
            ret = await PgpJS.verify({
              message: messageObj,
              verificationKeys: key
            });
            LogData2.DEBUG(`verifyMessage: message verification succeeded
`);
          } catch (x2) {
            LogData2.DEBUG(`verifyMessage: message verification failed with ${x2.toString()}
`);
          }
          break;
        }
        try {
          if (returnData && "data" in ret) {
            result.decryptedData = ensureString(ret.data);
          }
          if ("signatures" in ret) {
            for (let sig of ret.signatures) {
              let sigValid = false;
              signatureStatus = -1;
              try {
                sigValid = await sig.verified;
              } catch (ex) {
                LogData2.DEBUG(`verifyMessage: signature verification failed with ${ex.toString()}
`);
              }
              currentKey = null;
              let keyId = sig.keyID.toHex();
              for (let k2 of pubKeys) {
                if (k2.getKeyID().toHex() === keyId) {
                  currentKey = k2;
                  break;
                } else {
                  for (let sk of k2.getSubkeys()) {
                    if (sk.getKeyID().toHex() === keyId) {
                      currentKey = k2;
                      break;
                    }
                  }
                }
              }
              let statusDesc = "";
              if (currentKey === null) {
                LogData2.DEBUG(`verifyMessage: key not found
`);
                signatureStatus = SIG_STATUS.unknown_key;
                statusDesc = "UNKNOWN_CERT";
                result.keyId = keyId.toUpperCase();
              } else {
                if (!sigValid) {
                  signatureStatus = SIG_STATUS.bad_signature;
                  statusDesc = "BAD_SIGNATURE";
                } else {
                  let keyStatus = await getKeyStatusCode(currentKey);
                  switch (keyStatus) {
                    case "i":
                    case "r":
                      statusDesc = "GOOD_SIG_INVALID_CERT";
                      signatureStatus = SIG_STATUS.good_sig_invalid_key;
                      break;
                    case "e":
                      statusDesc = "GOOD_SIG_EXPIRED_CERT";
                      signatureStatus = SIG_STATUS.good_sig_expired_key;
                      break;
                    case "f":
                    case "u":
                      statusDesc = "VALID_SIGNATURE";
                      signatureStatus = SIG_STATUS.valid_signature;
                      break;
                    default:
                      statusDesc = "UNKNOWN_CERT";
                      signatureStatus = SIG_STATUS.unknown_key;
                  }
                }
                const pkt = (await sig.signature).packets[0];
                result.signatures.push({
                  fpr: currentKey.getFingerprint().toUpperCase(),
                  created: pkt.created.toISOString(),
                  signatureExpirationTime: pkt.signatureNeverExpires ? "" : JSON.stringify(pkt.signatureExpirationTime.getTime() / 1e3),
                  pubkeyAlgorithm: pkt.publicKeyAlgorithm,
                  hashAlgorithm: pkt.hashAlgorithm,
                  signatureStatus: statusDesc
                });
              }
            }
          } else {
            result.signatures = [];
            for (let key of keyIds) {
              result.signatures.push(getSigErrorObj(DecrypConst.NO_CERTIFICATE_FOUND, key));
            }
          }
        } catch (ex) {
          LogData2.DEBUG(`verifyMessage: ERROR: ${ex.toString()} ${ex.stack}
`);
        }
        return result;
      },
      /**
       * Verify a message with a detached signature
       *
       * @param {String|Uint8Array} data: the data to verify
       * @param {String} signature: ASCII armored signature
       * @param {Boolean} returnData: if true, inculde the verified data in the result
       *
       * @return {Promise<Object>}: ResultObj
       */
      verifyDetached: async function(data, signature, returnData = false) {
        LogData2.DEBUG(`verifyDetached(${data.length}, ${signature.length})
`);
        try {
          let sigString;
          if (typeof signature === "string") {
            sigString = signature;
          } else {
            sigString = await PgpJS.armor(PgpJS.enums.armor.signature, signature.write());
          }
          let msg;
          if (typeof data === "string") {
            msg = await PgpJS.createMessage({
              text: data
            });
          } else {
            msg = await PgpJS.createMessage({
              binary: data
            });
          }
          await msg.appendSignature(sigString);
          return this.verifyMessage(msg, returnData);
        } catch (ex) {
          LogData2.DEBUG(`verifyDetached: ERROR: ${ex.toString()}
`);
          let result = getReturnObj();
          result.signatures = [getSigErrorObj(DecrypConst.NO_CERTIFICATE_FOUND)];
          result.errorMsg = ex.toString();
          return result;
        }
      }
    };
    function getReturnObj() {
      return {
        decryptedData: "",
        decryptionResult: [DecrypConst.DECRYPTION_FAILED],
        encryptionKeys: [],
        signatures: [],
        errorMsg: ""
      };
    }
    function getSigErrorObj(statusDesc, keyId = "") {
      return {
        fpr: keyId,
        created: (/* @__PURE__ */ new Date(0)).toISOString(),
        signatureExpirationTime: "",
        pubkeyAlgorithm: 0,
        hashAlgorithm: 0,
        signatureStatus: statusDesc
      };
    }
    function ensureString(stringOrUint8Array) {
      if (typeof stringOrUint8Array === "string") {
        return stringOrUint8Array;
      }
      return arrayBufferToString(stringOrUint8Array);
    }
    function ensureUint8Array(stringOrUint8Array) {
      if (typeof stringOrUint8Array === "string") {
        const result = new Uint8Array(stringOrUint8Array.length);
        for (let i2 = 0; i2 < stringOrUint8Array.length; i2++) {
          result[i2] = stringOrUint8Array.charCodeAt(i2);
        }
        return result;
      }
      return stringOrUint8Array;
    }
    function arrayBufferToString(buffer) {
      const MAXLEN = 102400;
      let uArr = new Uint8Array(buffer);
      let ret = "";
      let len = buffer.byteLength;
      for (let j2 = 0; j2 < Math.floor(len / MAXLEN) + 1; j2++) {
        ret += String.fromCharCode.apply(null, uArr.subarray(j2 * MAXLEN, (j2 + 1) * MAXLEN));
      }
      return ret;
    }
    async function getKeyStatusCode(key) {
      let now = /* @__PURE__ */ new Date();
      try {
        if (await key.isRevoked(null, null, now)) {
          return "r";
        } else if (!key.users.some((user) => user.userID && user.selfCertifications.length)) {
          return "i";
        } else {
          const {
            user,
            selfCertification
          } = await key.getPrimaryUser(now, {}) || {};
          if (!user) return "i";
          if (isDataExpired(key.keyPacket, selfCertification, now)) {
            return "e";
          }
        }
      } catch (x2) {
        return "i";
      }
      return "f";
    }
    function isDataExpired(keyPacket, signature, date = /* @__PURE__ */ new Date()) {
      const normDate = normalizeDate(date);
      if (normDate !== null) {
        const expirationTime = getExpirationTime(keyPacket, signature);
        return !(keyPacket.created <= normDate && normDate < expirationTime) || signature && signature.isExpired(date);
      }
      return false;
    }
    function normalizeDate(time = Date.now()) {
      return time === null ? time : new Date(Math.floor(Number(time) / 1e3) * 1e3);
    }
    function getExpirationTime(keyPacket, signature) {
      let expirationTime;
      try {
        if (signature.keyNeverExpires === false) {
          expirationTime = keyPacket.created.getTime() + signature.keyExpirationTime * 1e3;
        }
        return expirationTime ? new Date(expirationTime) : Infinity;
      } catch (ex) {
        return Infinity;
      }
    }
    function extractDataFromClearsignedMsg(dataStr) {
      dataStr = dataStr.replace(/\r?\n/g, "\r\n");
      dataStr = dataStr.replace(/^- /mg, "");
      let start = dataStr.search(/\r\n\r\n/);
      let end = dataStr.search(/^-----BEGIN PGP SIGNATURE-----/m);
      if (start < 0 || end < 0 || end < start) return "";
      return dataStr.substring(start + 4, end - 2);
    }
    module2.exports = Decrypt;
  }
});

// lib/keyModification.js
var require_keyModification = __commonJS({
  "lib/keyModification.js"(exports2, module2) {
    "use strict";
    var LogData2 = require_logData();
    var PgpJS = (init_openpgp_min(), __toCommonJS(openpgp_min_exports));
    var { writeFile } = require("node:fs/promises");
    var keyStore2 = require_keystore();
    var Keys2 = require_keys();
    var KeyModification = {
      /**
       * Generate a revocation certificate
       * @param {String} fpr: The fingerprint of the key for which to create the certificate 
       * @param {String} reasonCode: (Optional) Reason of revocation. One of: COMPROMISED, SUPERSEDED, RETIRED
       * @param {String} reasonText: (Optional) Description of revocation reason
       * @param {String} password: (Optional) Password to decrypt the secret key
       * 
       * @returns result: { revocationCertificate: ASCII-armored certificate }
       */
      genRevokeCert: async function(fpr, reasonCode, reasonText, password) {
        LogData2.DEBUG(`keyModification.js: genRevokeCert: keyId=${fpr}
`);
        let keyList = await keyStore2.getKeysForKeyIds(true, [fpr]);
        if (!keyList || keyList.length === 0) {
          return createError("KEY_NOT_FOUND", `${fpr}`, `Key ${fpr} is not available`);
        }
        let revokeReason = 0;
        switch (reasonCode) {
          case "COMPROMISED":
            revokeReason = PgpJS.enums.reasonForRevocation.key_compromised;
            break;
          case "SUPERSEDED":
            revokeReason = PgpJS.enums.reasonForRevocation.key_superseded;
            break;
          case "RETIRED":
            revokeReason = PgpJS.enums.reasonForRevocation.key_retired;
            break;
          default:
            if (reasonCode) createError("ERROR_INVALID_PARAMETERS", `Invalid revocation reason ${reasonCode}`);
        }
        const res = await Keys2.decryptSecretKey(keyList[0], "KEY_DECRYPT_REASON_MODIFY_KEY", password);
        if (!("result" in res)) {
          return createError("INVALID_PASSWORD", "Could not decrypt key");
        }
        keyList[0] = res.result;
        const revokedKey = await keyList[0].revoke({
          flag: revokeReason,
          string: reasonText
        });
        const revCert = await revokedKey.getRevocationCertificate();
        return createSuccess({ revocationCertificate: revCert });
      },
      /**
       * set the expiration date of the chosen key and subkeys
       *
       * @param  {String}    fpr           fpr of the key to modify
       * @param  {Array}     subKeys       List of Integer values, e.g. [0,1,3]
       *                                   "0" reflects the primary key and should always be set.
       * @param  {Date}      expiryDate    New expiry date (and time) to set. Use Infinity for no expiry
       * @return  {Promise<Object>}
       */
      setKeyExpiration: async function(fpr, subKeys, expiryDate, password) {
        LogData2.DEBUG(`keyModification.js: setKeyExpiration: keyId=${fpr}
`);
        let keyList = await keyStore2.getKeysForKeyIds(true, [fpr]);
        if (!keyList || keyList.length === 0) {
          return createError("KEY_NOT_FOUND", `${fpr}`, `Key ${fpr} is not available`);
        }
        let newKey = await Keys2.changeKeyExpiry(keyList[0], subKeys, expiryDate, password);
        if (newKey) {
          try {
            await keyStore2.replaceKey(newKey);
          } catch (ex) {
            return createError("ERROR_UNKNOWN", ex.message);
          }
        }
        return createSuccess({
          fpr: newKey.getFingerprint().toUpperCase(),
          expiryDate
        });
      },
      /**
       * Change the password of a key
       * 
       * @param {String} fpr: Fingerprint of the key to change
       * @param {String} oldPassword: (Optional) current password. Leave null or "" if the key is not protected
       * @param {String} newPassword: (Optional) new password. Leave null or "" if the key shall not be protected
       * 
       * @returns result: {fpr: Fingerprint of the modified key}
       */
      changePassphrase: async function(fpr, oldPassword = "", newPassword = "") {
        LogData2.DEBUG(`keyModification.js: changePassphrase: keyId=${fpr}
`);
        let keyList = await keyStore2.getKeysForKeyIds(true, [fpr]);
        if (!keyList || keyList.length === 0) {
          return createError("KEY_NOT_FOUND", `${fpr}`, `Key ${fpr} is not available`);
        }
        let key = keyList[0];
        if (!key.isDecrypted()) {
          try {
            key = await PgpJS.decryptKey({
              privateKey: key,
              passphrase: oldPassword
            });
          } catch (ex) {
            if (Keys2.isWrongPassword(ex)) {
              return createError("INVALID_PASSWORD", "The key cannot be decrypted");
            } else if (!Keys2.isKeyFullyDecrypted(ex, key)) {
              return createError("KEY_INCOMPLETELY_DECRYPTED", "Not all subkeys of the key can be decrypted with the given password");
            }
          }
        }
        if (newPassword.length > 0) {
          key = await PgpJS.encryptKey({
            privateKey: key,
            passphrase: newPassword
          });
        }
        await keyStore2.replaceKey(key);
        return createSuccess({ fpr: key.getFingerprint().toUpperCase() });
      },
      /**
         * Sign a certificate
         * 
         * @param {String} signingFpr: FPR of key that is used for signing the certificate
         * @param {String} fprToSign: FPR of certificate to sign
         * @param {Array<String>} uidList: Optional List of UIDs to sign
         * @param {String} passwd: (optional) password to decrypt key
         *                          If null or undefined, prompt for the password
      
         * @returns result: { signedKey: ASCII Armored signed key}
         */
      signKey: async function(signingFpr, fprToSign, signUids, passwd) {
        LogData2.DEBUG(`keyModification.js: signKey (signingFpr= ${signingFpr}, fprToSign=${fprToSign})
`);
        let keyList = await keyStore2.getKeysForKeyIds(true, [signingFpr]);
        if (!keyList || keyList.length === 0) {
          return createError("KEY_NOT_FOUND", `${signingFpr}`, `Signing key ${signingFpr} is not available`);
        }
        const signingKey = keyList[0];
        keyList = await keyStore2.getKeysForKeyIds(false, [fprToSign]);
        if (!keyList || keyList.length === 0) {
          return createError("CERT_NOT_FOUND", `${fprToSign}`, `Certificate to sign ${fprToSign} is not available`);
        }
        const keyToSign = keyList[0];
        try {
          let r2 = await Keys2.signKey(signingKey, keyToSign, signUids, passwd);
          if ("result" in r2) {
            let armoredKey = await r2.result.armor();
            await keyStore2.writeKey(armoredKey);
            return createSuccess({ signedKey: armoredKey });
          } else {
            return r2;
          }
        } catch (ex) {
          return createError("ERROR_SIGNING", ex.message);
        }
      }
    };
    function createError(errorCode, errorMsg) {
      return {
        error: {
          errorMsg,
          errorCode
        }
      };
    }
    function createSuccess(data) {
      if (!data) {
        data = "OK";
      }
      return {
        result: data
      };
    }
    module2.exports = KeyModification;
  }
});

// lib/cryptoFuncs.js
var require_cryptoFuncs = __commonJS({
  "lib/cryptoFuncs.js"(exports2, module2) {
    "use strict";
    var LogData2 = require_logData();
    var keyStore2 = require_keystore();
    var Keys2 = require_keys();
    var Encrypt = require_encrypt();
    var Decrypt = require_decrypt();
    var KeyModification = require_keyModification();
    var CryptoAPI = class {
      constructor() {
        this.api_name = "OpenPGP.js";
      }
      /**
       * Initialize the tools/functions required to run the API
       *
       * @param {Function} promptForInputFn: callback Function to call when input from the caller is needed
       */
      initialize(promptForInputFn) {
        Keys2.init(promptForInputFn);
        return keyStore2.init();
      }
      /**
       * Get a version strings for the tool and the OpenPGP.js library
       *
       * @returns {String}
       */
      getVersionString(toolVersion) {
        this.printResult({
          version: toolVersion,
          libraryVersion: Keys2.getVersionString()
        });
      }
      printResult(data) {
        console.log(JSON.stringify(data) + ";");
      }
      /**
         * Read one or more keys from the database
         *
         * @param {Array<String>} fpr: [optional] Array of Fingerprints or keyIDs. If not provided, all keys are returned
         *
         * @return {Array<Key>} List of OpenPGP.js Key objects.
         */
      async getKeyList(fpr = null) {
        let r2 = await keyStore2.readKeyMetadata(fpr);
        this.printResult({ result: r2 });
      }
      async getCertMetadata(keyBlockStr) {
        let keys = await Keys2.getKeysFromKeyBlock(keyBlockStr);
        let a2 = [];
        for (let k2 of keys) {
          a2.push(await keyStore2.getKeyMetadata(k2));
        }
        this.printResult({ result: a2 });
      }
      /**
       * Import key(s) from a string provided
       *
       * @param {String} keyData:  the key data to be imported (ASCII armored)
       * @param {Boolean} minimizeKey: import the minimum key without any 3rd-party signatures
       * @param {Array of String} limitedUid: only import the UID specified
       *
       * @return {Object}:
       *   - {Array of String) imported:    imported fingerprints
       */
      async importKeyData(keyData, minimizeKey, limitedUid) {
        if (!keyData) {
          this.printResult(LogData2.genError("ERROR_NO_DATA", "missing key data"));
        }
        let r2 = await keyStore2.importKeyData(keyData, minimizeKey, limitedUid);
        this.printResult({
          result: r2
        });
      }
      /**
       * Delete keys from keyring
       *
       * @param {Array<String>} fpr: fingerprint(s) to delete
       *
       * @return {Promise<Object>}:
       *      - {Number} exitCode: 0 if successful, other values indicate error
       *      - {String} errorMsg: error message if deletion not successful
       */
      async deleteKeys(fpr) {
        try {
          let deleted = await keyStore2.deleteKeys(fpr);
          this.printResult({
            result: {
              deleted
            }
          });
          return;
        } catch (ex) {
        }
        this.printResult({
          result: {
            deleted: []
          }
        });
      }
      /**
       * Export public key(s) as ASCII armored data
       *
       * @param {Array<String>}  fprArr     Fingerprints to export
       *
       * @return {Object}:
       *   - {String} armored_certs:        ASCII armored key data material
       */
      async extractPublicKey(fprArr) {
        let keyData = "";
        try {
          keyData = await keyStore2.readPublicKeys(fprArr);
        } catch (ex) {
          LogData2.DIE("ERROR_UNKNOWN", ex.toString());
        }
        this.printResult({
          result: {
            armoredCerts: keyData
          }
        });
      }
      /**
       * Export secret key(s) as ASCII armored data
       *
       * @param {Array<String>}  fprArr      Fingerprint to export
       * @param {Boolean}        minimalKey  if true, reduce key to minimum required
       *
       * @return {Object}:
       *   - {String} armored_keys:          ASCII armored key data material
       */
      async extractSecretKey(fprArr = null, minimalKey) {
        let keyData = "";
        try {
          keyData = await keyStore2.readSecretKeys(fprArr, minimalKey);
        } catch (ex) {
        }
        this.printResult({
          result: {
            armoredKeys: keyData
          }
        });
      }
      /**
       * Export the minimum key for the public key object:
       * public key, user ID, newest encryption subkey
       *
       * @param {String} fpr  : a single FPR
       * @param {String} email: [optional] the email address of the desired user ID.
       *                        If the desired user ID cannot be found or is not valid, use the primary UID instead
       *
       * @return {Promise<Object>}:
       *    - cert: BASE64-encded string of key data
       */
      async getMinimalPubKey(fpr, email) {
        if (!fpr) {
          this.printResult(LogData2.genError("ERROR_NO_DATA", "missing fingerprint"));
          return;
        }
        let r2 = await keyStore2.readMinimalPubKey(fpr, email);
        this.printResult({
          result: {
            armoredCert: r2.keyData
          }
        });
      }
      /**
       * Obtain signatures for a given set of key IDs.
       *
       * @param {String}  fpr:            fingerprint of key
       * @param {Boolean} ignoreUnknownUid: if true, filter out unknown signer's UIDs
       *
       * @return {Promise<Array of Object>}:
       *     - {String} userId
       *     - {String} rawUserId
       *     - {String} keyId
       *     - {String} fpr
       *     - {String} created
       *     - {Array} sigList:
       *            - {String} userId
       *            - {String} created
       *            - {String} signerKeyId
       *            - {String} sigType
       *            - {Boolean} sigKnown
       */
      async getKeySignatures(fpr, ignoreUnknownUid = false) {
        LogData2.DEBUG(`cryptoFuncs.js: getKeySignatures ${fpr}
`);
        if (!fpr) {
          this.printResult(LogData2.genError("ERROR_NO_DATA", "missing fingerprint"));
          return;
        }
        let keys = await keyStore2.readKeys([fpr]);
        let allKeys = await keyStore2.readKeyMetadata();
        let keyList = [];
        for (let k2 of allKeys) {
          keyList[k2.fpr] = k2;
          keyList[k2.keyId] = k2;
        }
        let sigs = [];
        for (let k2 of keys) {
          let uids = Keys2.getSignaturesFromKey(k2.key);
          for (let uid of uids) {
            let foundSigs = [];
            for (let sig of uid.sigList) {
              if (sig.signerKeyId in keyList) {
                sig.sigKnown = true;
                sig.userId = keyStore2.getPrimaryUser(keyList[sig.signerKeyId]).userId;
                sig.fpr = keyList[sig.signerKeyId].fpr;
                foundSigs.push(sig);
              } else if (ignoreUnknownUid) {
                foundSigs.push(sig);
              }
            }
            uid.sigList = foundSigs;
            sigs.push(uid);
          }
        }
        this.printResult({
          result: sigs
        });
      }
      // /** FIXME
      //  * Extract a photo ID from a key, store it as file and return the file object.
      //  *
      //  * @param {String} keyId:       Key ID / fingerprint
      //  * @param {Number} photoNumber: number of the photo on the key, starting with 0
      //  *
      //  * @return {nsIFile} object or null in case no data / error.
      //  */
      // async getPhotoFile(keyId, photoNumber) {
      //   let keys = await keyStore.getKeysForKeyIds(false, [keyId]);
      //   if (keys.length > 0) {
      //     return keys.getPhotoForKey(keys[0], photoNumber);
      //   }
      //   return null;
      // }
      /**
       * Generate a new key pair
       *
       * @param {Array<String>} userId: name and email of key owner
       * @param {String} expiryDate: Key expiry: date and time in ISO 8601 format (as formatted by Date.toJSON())
       * @param {String} keyType:    V4: 'RSA' or 'ECC'  / V6: Curve25519 or Curve448
       * @param {Number} keyLength:  size of key in bytes for RSA keys (e.g 4096)
       * @param {String} passphrase: password; use null if no password
       *
       * @return {Object}: Handle to key creation
       *    - {function} cancel(): abort key creation
       *    - {Promise<exitCode, generatedKeyId>} promise: resolved when key creation is complete
       *                 - {Number} exitCode:       result code (0: OK)
       *                 - {String} generatedKeyId: generated key ID
       */
      async generateKey(userId, expiryDate, keyType, keyLength, passphrase) {
        try {
          let keyData = await Keys2.generateKey(userId, expiryDate, keyLength, keyType, passphrase);
          await keyStore2.writeKey(keyData.privateKey);
          keyStore2.storeRevocationCert(keyData.key, keyData.revocationCertificate);
          this.printResult({
            result: {
              fpr: keyData.key.getFingerprint().toUpperCase()
            }
          });
        } catch (err) {
          LogData2.DIE("ERROR_KEYGEN_FAILED", err.toString());
          reject(err);
        }
        ;
      }
      /**
       * Generic function to decrypt and/or verify an OpenPGP message.
       *
       * @param {String} pgpMessage:   The signed or encrypted OpenPGP message  data
       * @param {String} password:     (Optional) password to decrypt private key
       * @param {Boolean} verifyOnly:  Only verifiy signature(s), do not return any verified data
       *
       * @return {Promise<Object>} - Return object with decryptedData and
       * status information
       *
       */
      async decryptOrVerify(pgpMessage, password, verifyOnly) {
        LogData2.DEBUG(`openpgpg-js.js: decrypt()
`);
        let result;
        if (verifyOnly) {
          result = await Decrypt.verify(pgpMessage, { verifyOnly });
        } else {
          result = await Decrypt.processPgpMessage(pgpMessage, password);
        }
        this.printResult(this.fixPasswdPrompt(result));
      }
      /**
       * Encrypt messages
       *
       * @param {String} plainText: data to encrypt
       * @param {Array<String>} recipients: FPR of recipients
       * @param {Array<String>} hiddenRecipients: [OPTIONAL] keyIDs or email addresses of hidden recipients (bcc), separated by spaces
       * @param {String} signingKey: [OPTIONAL] fpr of signing key ID. if provided, message is singed and encrypted, otherwise only encrypted
       * @param {String} fileName: [OPTIONAL] name of the file
       * @param {String} password: [OPTIONAL] password for signing key
       *
       * @return {Promise<Object>}: 
       *   - result:
       *     - {String} data:        encrypted data
       *   - error:                  standard error data
       */
      async encrypt(plainText, recipients, signingKey, fileName, password) {
        let r2 = await Encrypt.encryptMessage(plainText, recipients, null, signingKey, fileName, password);
        this.printResult(this.fixPasswdPrompt(r2));
      }
      /**
      * Generate a revocation certificate
      * @param {String} fpr: The fingerprint of the key for which to create the certificate 
      * @param {String} reasonCode: (Optional) Reason of revocation. One of: COMPROMISED, SUPERSEDED, RETIRED
      * @param {String} reasonText: (Optional) Description of revocation reason
      * @param {String} password: (Optional) Password to decrypt the secret key
      * 
      * @returns result: { revocationCertificate: ASCII-armored certificate }
      */
      async genRevokeCert(keyId, reasonCode, reasonText, password) {
        let r2 = await KeyModification.genRevokeCert(keyId, reasonCode, reasonText, password);
        this.printResult(this.fixPasswdPrompt(r2));
      }
      /**
      * set the expiration date of the chosen key and subkeys
      *
      * @param  {String}    fpr           fpr of the key to modify
      * @param  {Array}     subKeys       List of Integer values, e.g. [0,1,3]
      *                                   "0" reflects the primary key and should always be set.
      * @param  {Date}      expiryDate    New expiry date (and time) to set. Use Infinity for no expiry
      * @return  {Promise<Object>}
      */
      async setKeyExpiration(fpr, subKeys, expiryDate, password) {
        let r2 = await KeyModification.setKeyExpiration(fpr, subKeys, expiryDate, password);
        this.printResult(this.fixPasswdPrompt(r2));
      }
      /**
      * Change the password of a key
      * 
      * @param {String} fpr: Fingerprint of the key to change
      * @param {String} oldPassword: (Optional) current password. Leave null or "" if the key is not protected
      * @param {String} newPassword: (Optional) new password. Leave null or "" if the key shall not be protected
      * 
      * @returns result: {fpr: Fingerprint of the modified key}
      */
      async changePassphrase(fpr, oldPassword, newPassword) {
        let r2 = await KeyModification.changePassphrase(fpr, oldPassword, newPassword);
        this.printResult(this.fixPasswdPrompt(r2));
      }
      /**
         * Sign a certificate
         * 
         * @param {String} signingFpr: FPR of key that is used for signing the certificate
         * @param {String} fprToSign: FPR of certificate to sign
         * @param {Array<String>} uidList: Optional List of UIDs to sign
         * @param {String} passwd: (optional) password to decrypt key
         *                          If null or undefined, prompt for the password
      
         * @returns result: { signedKey: ASCII Armored signed key}
         */
      async signKey(signingFpr, fprToSign, signUids, passwd) {
        let r2 = await KeyModification.signKey(signingFpr, fprToSign, signUids, passwd);
        this.printResult(this.fixPasswdPrompt(r2));
      }
      fixPasswdPrompt(dataObj) {
        if ("needPassword" in dataObj) {
          dataObj = LogData2.genError("NEED_PASSWORD", `Password for key ${dataObj.needPassword.fpr} needed.`, { fpr: dataObj.needPassword.fpr });
        } else if ("wrongPassword" in dataObj) {
          dataObj = LogData2.genError("WRONG_PASSWORD", `Password for key ${dataObj.wrongPassword.fpr} needed.`, { fpr: dataObj.wrongPassword.fpr });
        } else if (!("result" in dataObj)) {
          dataObj = { result: dataObj };
        }
        return dataObj;
      }
    };
    var cryptoFuncs2 = new CryptoAPI();
    module2.exports = cryptoFuncs2;
  }
});

// lib/api.js
var require_api = __commonJS({
  "lib/api.js"(exports2, module2) {
    "use strict";
    var apiSpec = {
      op: {
        changeExpiry: {
          desc: "Modify the expiration date of a key and its subkeys",
          params: {
            fpr: {
              desc: "Fingerprint of the key to modify",
              type: "String"
            },
            subKeys: {
              desc: "List of Integer values representing the subkey numbers, e.g. [0,1,3]. If not provided, modify  all subkeys. '0' reflects the primary key and should always be set.",
              type: "Array:Number",
              optional: true
            },
            expiryDate: {
              desc: "New expiry date (and time) to set. Use Infinity for no expiry",
              type: "Date"
            },
            password: {
              type: "String",
              desc: "Password for decrypting the signing key. If not provided, and a password is required, then it is prompted on stdout/stdin.",
              optional: true
            }
          },
          returns: {
            result: {
              fpr: "Fingerprint of the modified key",
              expiryDate: "New expiry date & time"
            },
            errors: {
              KEY_NOT_FOUND: "The given fingerprint was not found or is not a key"
            }
          }
        },
        changePassword: {
          desc: "Change the password of a key",
          params: {
            fpr: {
              desc: "Fingerprint of the key to change",
              type: "String"
            },
            oldPassword: {
              desc: "Current password of the key. Leave null or '' if the key is not protected.",
              type: "String",
              optional: true
            },
            newPassword: {
              desc: "New password. Leave null or '' if the key shall not be protected.",
              type: "String",
              optional: true
            }
          },
          returns: {
            result: {
              fpr: "Fingerprint of the updated key"
            },
            errors: {
              KEY_NOT_FOUND: "The given fingerprint was not found or is not a key",
              INVALID_PASSWORD: "The key cannot be decrypted with the provided password",
              KEY_INCOMPLETELY_DECRYPTED: "Not all subkeys of the key could be decrypted with the given password"
            }
          }
        },
        decrypt: {
          desc: "Decrypt and/or verify a message, and return the cleartext data",
          params: {
            cryptoData: {
              type: "String",
              desc: "The ASCII armored data to decrypt or verify"
            },
            password: {
              type: "String",
              desc: "Password for decrypting the signing key. If not provided, and a password is required, then it is prompted on stdout/stdin.",
              optional: true
            }
          },
          returns: {
            result: {
              decryptedData: "The decrypted or verified data",
              signatures: `Array of objects with the following properties:
    - fpr:                       Fingerprint of the signing certificate
    - created:                   Signature reation date
    - signatureExpirationTime:   Expiration time of the signature
    - pubkeyAlgorithm:           Used encryption algorithm
    - hashAlgorithm:             Used hashing algorithm
    - signatureStatus:           Status of the signature. One of: VALID_SIGNATURE, BAD_SIGNATURE, GOOD_SIG_INVALID_CERT, GOOD_SIG_EXPIRED_CERT, UNKNOWN_CERT`,
              decryptionResult: "Array of Strings with all applicable status: DECRYPTION_OKAY, DECRYPTION_FAILED, EXPIRED_CERT, REVOKED_CERT, NO_PRIVATE_KEY, BAD_PASSPHRASE, INVALID_OR_MISSING_MDC, VALID_KEY, VERIFICATION_FAILED, INVALID_MESSAGE",
              encryptionKeys: "Array containing the fingerprints/key ID of all certificates the message was encyrpted to",
              encryptedFileName: "Encrypted Filename"
            },
            errors: {
              WRONG_PASSWORD: "The password provided is wrong, no key could be decrypted",
              NEED_PASSWORD: "There was no password provided, but the key is encrypted"
            }
          }
        },
        delete: {
          desc: "Delete a key or certificate from the database",
          params: {
            fpr: {
              type: "Array:String",
              desc: "The FPR of the keys/certificates to delete"
            }
          },
          returns: {
            result: {
              deleted: "Array of fingerprints of the deleted keys"
            },
            errors: {}
          }
        },
        encrypt: {
          desc: "Encrypt (and optionally sign) data to one or more certificates",
          params: {
            plainText: {
              type: "String",
              desc: "The data to encypt. Must be provided as UTF-8 string"
            },
            recipients: {
              type: "Array:String",
              desc: "FPR of certificates to which the data is encrypted"
            },
            signingKey: {
              type: "String",
              desc: "FPR of key to use for signing the message. If provided, the data is signed and encypted, otherwise only encrypted.",
              optional: true
            },
            filename: {
              type: "String",
              desc: "Name of the file",
              optional: true
            },
            password: {
              type: "String",
              desc: "Password for decrypting the signing key. If not provided, and a password is required, then it is prompted on stdout/stdin.",
              optional: true
            }
          },
          returns: {
            result: {
              data: "Encrypted message (ASCII armored data)"
            },
            errors: {
              ERROR_INVALID_SIGNING_KEY: "The specified signing key is not available, not valid, or cannot be used for signing",
              ERROR_NO_VALID_RECIPIENTS: "Could not find any of the recipient certificates specified",
              ERROR_INVALID_RECIPIENTS: "Some of the recipent certificates were not found",
              ERROR_INVALID_RECIPIENT_CERT: "A certificate is not valid (e.g. expired, revoked), or cannot be used for encryption",
              ERROR_ENCRYPTION_FAILED: "Message encryption failed",
              ERROR_ENCRYPT: "Geneneral encryption error. See message text."
            }
          }
        },
        extractCerts: {
          desc: "Obtain certificates (public key) as ASCII armored data",
          params: {
            fpr: {
              type: "Array:String",
              desc: "FPR of certificates to export"
            }
          },
          returns: {
            result: {
              armoredCerts: "Certificates, formatted as ASCII armored data. Certificates that are not available are omitted"
            }
          }
        },
        extractKeys: {
          desc: "Obtain pricate keys as ASCII armored data",
          params: {
            fpr: {
              type: "Array:String",
              desc: "The FPR of the keys to obtain. Returns all keys if not provided.",
              optional: true
            },
            minimize: {
              type: "Boolean",
              desc: "If true, return a minimal key, consisiting of the public key, the primary user ID, and the newest keys for signing and encryption, and the relevant self-signatures",
              optional: true
            }
          },
          returns: {
            result: {
              armoredKeys: "Private keys, formatted as ASCII armored data. Keys that are not available are omitted."
            }
          }
        },
        extractMinimalCert: {
          desc: "Obtain a minimal certificate consisting of: the public key, a user ID, and the newest encryption subkey, and the relevant self-signatures",
          params: {
            fpr: {
              type: "String",
              desc: "FPR of the certificate to produce"
            },
            email: {
              type: "String",
              desc: "The email address of the requested user ID. If the user ID cannot be found or is not valid, the primary user ID is used instead",
              optional: true
            }
          },
          returns: {
            result: {
              armoredCert: "The certificate, formatted as ASCII armored data. Empty string if no certificate found"
            }
          }
        },
        generateKey: {
          desc: "Create a new key pair",
          params: {
            userId: {
              type: "Array:String",
              desc: "Name and email adressess of key owner (e.g. John Doe <john.doe@some.example>)"
            },
            expiryDate: {
              type: "String",
              desc: "Key expiry date in ISO 8601 format. Leave null if key should not expire.",
              optional: true
            },
            keyType: {
              type: "String",
              desc: "Key type. Use 'ECC' or 'RSA' for OpenPGP v4 keys, and 'Curve25519' or 'Curve448' for v6 keys."
            },
            password: {
              type: "String",
              desc: "Password to protect key with. If '' or not provided, the key will not be protected.",
              optional: true
            },
            keyLength: {
              type: "Number",
              desc: "Key size (for RSA keys only). Default if not provided is 4096.",
              optional: true
            }
          },
          returns: {
            result: {
              "fpr": "Fingerprint of the generated key"
            }
          },
          errors: {
            ERROR_KEYGEN_FAILED: "The key was not created due to an error."
          }
        },
        generateRevocationCert: {
          desc: "Generate a revocation certificate",
          params: {
            fpr: {
              type: "String",
              desc: "FPR of the key for which to generate a revocation certificate"
            },
            reasonCode: {
              type: "String",
              desc: "Reason why the key was revoked. One of: COMPROMISED, SUPERSEDED, RETIRED. Leave out if no reason specified",
              optional: true
            },
            reasonText: {
              type: "String",
              desc: "A description text that is stored in the generated certificate",
              optional: true
            },
            password: {
              type: "String",
              desc: "Password for decrypting the key. If not provided, and a password is required, then it is prompted on stdout/stdin.",
              optional: true
            }
          },
          returns: {
            result: {
              revocationCertificate: "ASCII armored revocation certificate"
            }
          },
          errors: {
            KEY_NOT_FOUND: "The given fingerprint was not found or is not a key",
            INVALID_PASSWORD: "The key cannot be decrypted with the provided password"
          }
        },
        getCertMetadata: {
          desc: "Obtain the metadata of a provided certificate or key",
          params: {
            key: {
              type: "String",
              desc: "The ASCII armored key/certifiace material to analyze"
            }
          },
          returns: {
            result: {
              "(Array of Objects)": "The metadata of the provided certs/keys. See [op:list] for information on the metadata"
            }
          }
        },
        import: {
          desc: "Import a key or certificate",
          params: {
            key: {
              type: "String",
              desc: "The ASCII armored key/certifiace material to import"
            },
            minimize: {
              type: "Boolean",
              desc: "Import the minimum key without any 3rd-party signatures",
              optional: true
            },
            limitUid: {
              type: "Array:String",
              desc: "Only import the user IDs specified",
              optional: true
            }
          },
          returns: {
            result: {
              importedFpr: "Array of strings containing the fingerprints of each imported key"
            }
          }
        },
        list: {
          desc: "Produce a list with all metadata of the certificates and keys in the database",
          params: {
            fpr: {
              type: "Array:String",
              desc: "If provided, only return the cerificates and keys that match the given FPR",
              optional: true
            }
          },
          returns: {
            result: {
              "Array": `Array of objects with the following structure:
   - fpr:                        Certificate fingerprint
   - keyId:                      ID of the certificate (16 hex digits for V4 keys)
   - secretAvailable:            true for secret keys, false for certificates
   - created:                    Creation date & time (ISO string)
   - expiryTime:                 Date & time of expiry (ISO string)
   - keyUseFor:                  Flags to indicate what the key/cert can be used for: c = certify / s = sign / e = encrypt 
   - algoSym:                    Certificate algorithm (RSA, ECC, etc.)
   - validity:                   Validity: u = ultimately valid (secret key) / f = fully valid / e = expired / r = revoked / i = invalid
   - keySize:                    Size of key in bits 
   - userIds:                    Array of objects:
     - userId:                   User ID (such as name and email address)
     - type:                     one of: uid = User ID / uat = User attribute (picture)
     - validity:                 User ID validity. See key validity for possible values
     - isPrimary:                true if this is the primary user ID, false otherwise
   - subKeys:                    Array of objects:
     - keyId:                    Subkey key ID
     - fpr:                      Subkey fingerprint
     - created:                  Creation date & time (ISO string)
     - expiryTime:               Date & time of expiry (ISO string)
     - validity:                 Subkey validity. See key validity for possible values
     - keyUseFor:                Key usage. See key keyUseFor
     - keySize:                  Subkey size in bits
     - algoSym:                  Subkey algorithm (RSA, ECC, etc.)`
            }
          }
        },
        listSig: {
          desc: "Obtain the signatures for a given certificate",
          params: {
            fpr: {
              type: "String",
              desc: "The FPR of the certificate to read"
            },
            ignoreUnknownSig: {
              type: "Boolean",
              desc: "Ignore signatures signed with certificates that are not on the keyring",
              optional: true
            }
          },
          returns: {
            result: {
              "Array": `Array of objects with the following structure:
   - fpr:                        Fingerprint of signed certificate
   - keyId:                      Key ID of signed certificate
   - userId:                     Signed user ID
   - created:                    Timestamp of user ID creation
   - sigList:                    Array of signature objects:
     - created:                  Signature creation timestamp
     - sigType:                  Signature type as hexadecimal value
     - userId:                   Primary user ID of signature issuer
     - fpr:                      Signature issuer fingerprint
     - sigKnown:                 true if fhe signature issuer certificate is on the keyring
     - signerKeyId:              Signature issuer key ID`
            }
          }
        },
        signCert: {
          desc: "Sign a certificate",
          params: {
            signingFpr: {
              desc: "FPR of key that is used for signing the certificate",
              type: "String"
            },
            fprToSign: {
              desc: "FPR of certificate to sign",
              type: "String"
            },
            uidList: {
              desc: "User IDs to sign. User attribtes are not signed",
              type: "Array:String",
              optional: true
            },
            password: {
              type: "String",
              desc: "Password for decrypting the signing key. If not provided, and a password is required, then it is prompted on stdout/stdin.",
              optional: true
            }
          },
          returns: {
            result: {
              signedKey: "ASCII armored key"
            },
            errors: {
              KEY_NOT_FOUND: "Key to use for signing is not found or not a secret key",
              CERT_NOT_FOUND: "Certificate to sign not found",
              ERROR_KEY_CANT_SIGN: "Key to use for signing does not have the capability to sign certificated",
              ERROR_KEY_EXPIRED: "Key to use for signing to use for signing has expired",
              ERROR_KEY_REVOKED: "Key to use for signing has expired",
              ERROR_NO_VALID_USERID: "No user ID found for signing",
              WRONG_PASSWORD: "The password provided is wrong, no key could be decrypted",
              NEED_PASSWORD: "There was no password provided, but the key is encrypted"
            }
          }
        },
        verify: {
          desc: "Verify a message, and do not return the verified data",
          params: {
            cryptoData: {
              type: "String",
              desc: "The ASCII armored data to decrypt or verify"
            }
          },
          returns: {
            result: {
              decryptedData: "The verified data",
              signatures: `Array of objects with the following properties:
    - fpr:                       Fingerprint of the signing certificate
    - created:                   Signature reation date
    - signatureExpirationTime:   Expiration time of the signature
    - pubkeyAlgorithm:           Used encryption algorithm
    - hashAlgorithm:             Used hashing algorithm
    - signatureStatus:           Status of the signature. One of: GOOD_SIGNATURE, BAD_SIGNATURE, UNVERIFIED_SIGNATURE, EXPIRED_SIGNATURE, EXPIRED_CERT_SIGNATURE, EXPIRED_CERT_SIGNATURE, NO_CERTIFICATE_FOUND`
            }
          }
        },
        version: {
          desc: "Get the version of the tool an the used OpenPGP.js library",
          params: {},
          returns: {
            result: {
              version: "App version string",
              libraryVersion: "Version string of the used OpenPGP.js library"
            }
          }
        }
      }
    };
    var LogData2 = require_logData();
    var jsonApi2 = {
      /**
       * Validate if all provided parameters match the required types
       *
       * @param {String} operation: function name
       * @param  {Object} params: ...
       * @param  {Object} subTree: apiSpec subtree
       */
      checkApi: function(operation, params, relaxedMode, subTree) {
        function reportTypeMismatch(type, argName) {
          console.log(LogData2.genError("ERROR_INVALID_PARAMETERS", `Parameter '${argName}' is not type ${type}`));
          return false;
        }
        if (!subTree) {
          subTree = apiSpec;
        }
        if (!apiSpec.op[operation]) {
          console.log(LogData2.genError("ERROR_INVALID_OPERATION", `The operation '${operation}' is not defined`));
          return false;
        }
        let req = apiSpec.op[operation].params;
        if (!req) return true;
        if (!params) {
          params = {};
        }
        if (typeof params !== "object") {
          return reportTypeMismatch("Object", "data");
        }
        for (let arg in req) {
          let p2 = req[arg], subtype;
          let optional = p2.optional ? true : false;
          let type = p2.type;
          if (!(arg in params)) {
            if (!optional) {
              console.log(LogData2.genError("ERROR_INVALID_PARAMETERS", `Mandatory argument '${arg}' not provided for operation '${operation}'`));
              return false;
            } else continue;
          }
          if (type.startsWith("Array")) {
            subtype = type.substring(6);
            type = "Array";
          }
          switch (type) {
            case "Array":
              if (!Array.isArray(params[arg])) return reportTypeMismatch(type, arg);
              for (let i2 in params[arg]) {
                if (typeof params[arg][i2] !== subtype.toLowerCase()) {
                  return reportTypeMismatch(subtype, `${arg}[${i2}]`);
                }
              }
              break;
            case "String":
            case "Number":
            case "Boolean":
              if (type.toLowerCase() !== typeof params[arg]) return reportTypeMismatch(type, arg);
              break;
          }
        }
        if (!relaxedMode) {
          for (let arg in params) {
            if (!(arg in req)) {
              console.log(LogData2.genError("ERROR_INVALID_PARAMETERS", `Parameter '${arg}' is not available for operation '${operation}'`));
              return false;
            }
          }
        }
        return true;
      },
      describeOperations: function() {
        console.log(`This tool expects a JSON object with a request and responds with
a JSON object. The property  "op" is mandatory and its string 
value selects the operation. Parameters for the operation can be
provided via a "data" attribute. All requests must be terminated 
with a semicolon (;) followed by a new line.

All "op" commands produce a JSON object with a "result" property that 
contains the return values of the operation. In case of an error, an
"error" property is returned instead.

Supported values for "op" are:
`);
        for (let op in apiSpec.op) {
          console.log(`  ${op.padEnd(19)} ${apiSpec.op[op].desc}`);
        }
        console.log(`
Example: 
  {
    "op": "generateKey", 
    "data": {
      "userId": ["John doe <john.doe@some.example>"],
      "keyType": "ECC"
    }
  };

Command line options:
--help [OPERATION]  Print this help. If OPERATION is provided, print 
                    help on the specific operation.
--cmd OPERATION     Execute OPERATION from the command line instead
                    of reading from stdin
--home PATH         Set the path where the keyring is stored
--interactive       Use interactive mode
--relaxed           Don't check for unknown parameters of operations
--version           Obtain version information`);
      },
      fitTerminalWidth: function(str, indentation) {
        if (process.stdout.columns) {
          const cols = process.stdout.columns;
          let s2 = "";
          let strArr = str.split(/[\r\n]/);
          for (let l2 in strArr) {
            let line = strArr[l2], i2 = cols;
            if (l2 > 0) {
              s2 = s2 + "\n";
            }
            while (line.length > cols) {
              if (line[i2] === " ") {
                s2 = s2 + line.substring(0, i2) + "\n";
                line = " ".padEnd(indentation) + line.substring(i2 + 1);
                i2 = cols;
              } else {
                i2--;
              }
            }
            s2 = s2 + line;
          }
          return s2;
        } else {
          return str;
        }
      },
      describeSingleOperation: function(operation) {
        if (!apiSpec.op[operation]) {
          console.log(`The operation "${operation}" does not exist`);
          this.describeOperations();
          return;
        }
        console.log(this.fitTerminalWidth(`op: "${operation}":   ${apiSpec.op[operation].desc}`, 33));
        console.log("\nParameters:");
        const params = apiSpec.op[operation].params;
        for (let par in params) {
          console.log(this.fitTerminalWidth(` - ${(par + " {" + params[par].type + "}").padEnd(28)}  ${params[par].desc}`, 33));
        }
        if ("returns" in apiSpec.op[operation]) {
          console.log(`
Result object:`);
          const returns = apiSpec.op[operation].returns;
          for (let r2 in returns.result) {
            console.log(this.fitTerminalWidth(` - ${r2.padEnd(28)}  ${returns.result[r2]}`, 33));
          }
          if ("errors" in returns) {
            console.log(`
Error codes:`);
            for (let e2 in returns.errors) {
              console.log(this.fitTerminalWidth(` - ${e2.padEnd(28)}  ${returns.errors[e2]}`, 33));
            }
          }
        }
      },
      describeApi: function(operation) {
        if (!operation) {
          this.describeOperations();
        } else this.describeSingleOperation(operation);
      }
    };
    module2.exports = jsonApi2;
  }
});

// npgp.js
var LogData = require_logData();
var cryptoFuncs = require_cryptoFuncs();
var keyStore = require_keystore();
var Keys = require_keys();
var jsonApi = require_api();
var rlp = require("node:readline");
var { stdin: input, stdout: output } = require("node:process");
var NPGPJS_VERSION = "0.1";
var ReadLine;
var gCmdQueue = [];
var gResolve = null;
var gReadLineOpen = false;
var gRelaxedMode = false;
function promptForInput(type, data) {
  const cmd = {
    input: type,
    data
  };
  if (gReadLineOpen) {
    console.log(JSON.stringify(cmd) + ";");
  }
  return new Promise((resolve, reject2) => {
    let r2;
    if (!gReadLineOpen) {
      resolve({ password: "" });
      return;
    }
    if (gCmdQueue.length > 0) {
      let str = gCmdQueue[0];
      gCmdQueue.splice(0, 1);
      try {
        r2 = JSON.parse(str);
        resolve(r2);
      } catch (x2) {
        resolve({});
      }
    } else {
      gResolve = resolve;
    }
  });
}
async function processInputData(data) {
  let input2;
  try {
    input2 = JSON.parse(data);
  } catch (ex) {
    if (data.length > 200) {
      LogData.DIE("ERROR_INVALID_INPUT", ex + data.substring(0, 100) + " ... " + data.substring(-100, 100));
    } else {
      LogData.DIE("ERROR_INVALID_INPUT", ex + data);
    }
    return 1;
  }
  if ("op" in input2) {
    if (!jsonApi.checkApi(input2.op, input2.data, gRelaxedMode)) {
      return 0;
    }
    switch (input2.op) {
      case "import":
        await cryptoFuncs.importKeyData(input2.data.key, input2.data.minimize, input2.data.limitUid);
        break;
      case "delete":
        await cryptoFuncs.deleteKeys(input2.data.fpr);
        break;
      case "list":
        await cryptoFuncs.getKeyList("data" in input2 ? input2.data.fpr : null);
        break;
      case "listSig":
        await cryptoFuncs.getKeySignatures(input2.data.fpr, input2.data.ignoreUnknownSig);
        break;
      case "extractCerts":
        await cryptoFuncs.extractPublicKey(input2.data.fpr);
        break;
      case "extractMinimalCert":
        await cryptoFuncs.getMinimalPubKey(input2.data.fpr, input2.data.email);
        break;
      case "extractKeys":
        if ("data" in input2) {
          await cryptoFuncs.extractSecretKey(input2.data.fpr, input2.data.minimize);
        } else {
          await cryptoFuncs.extractSecretKey();
        }
        break;
      case "encrypt":
        await cryptoFuncs.encrypt(input2.data.plainText, input2.data.recipients, input2.data.signingKey, input2.data.filename, input2.data.password);
        break;
      case "decrypt":
        await cryptoFuncs.decryptOrVerify(input2.data.cryptoData, input2.data.password);
        break;
      case "generateRevocationCert":
        await cryptoFuncs.genRevokeCert(input2.data.fpr, input2.data.reasonCode, input2.data.reasonText, input2.data.password);
        break;
      case "verify":
        await cryptoFuncs.decryptOrVerify(input2.data.cryptoData, null, true);
        break;
      case "generateKey":
        await cryptoFuncs.generateKey(input2.data.userId, input2.data.expiryDate, input2.data.keyType, input2.data.keyLength, input2.data.password);
        break;
      case "changeExpiry":
        await cryptoFuncs.setKeyExpiration(input2.data.fpr, input2.data.subKeys, input2.data.expiryDate, input2.data.password);
        break;
      case "changePassword":
        await cryptoFuncs.changePassphrase(input2.data.fpr, input2.data.oldPassword, input2.data.newPassword);
        break;
      case "signCert":
        await cryptoFuncs.signKey(input2.data.signingFpr, input2.data.fprToSign, input2.data.uidList, input2.data.password);
        break;
      case "getCertMetadata":
        await cryptoFuncs.getCertMetadata(input2.data.key);
        break;
      case "version":
        cryptoFuncs.getVersionString(NPGPJS_VERSION);
        break;
      default:
        LogData.DIE("ERROR_INVALID_OPERATION", `Invalid operation: '${input2.op}'`);
    }
  } else if ("help" in input2) {
    jsonApi.describeApi(input2.help);
  } else {
    LogData.DIE("ERROR_INVALID_OPERATION", `Invalid JSON, no 'op' parameter given`);
  }
  return 0;
}
function getDataFromStdin() {
  let data = "", processing = false, closed = false;
  return new Promise((resolve, reject2) => {
    ReadLine.on("close", () => {
      closed = true;
    });
    ReadLine.on("SIGINT", () => {
      process.exit(0);
    });
    ReadLine.on("line", async (line) => {
      let i2;
      data += line + "\n";
      while ((i2 = data.search(/;\W*\n/)) >= 0) {
        gCmdQueue.push(data.substring(0, i2));
        data = data.substring(i2 + 1);
      }
      if (!processing) {
        while (gCmdQueue.length > 0 && !processing) {
          processing = true;
          let cmd = gCmdQueue[0];
          gCmdQueue.splice(0, 1);
          try {
            await processInputData(cmd);
          } catch (ex) {
            LogData.ERROR("ERROR_UNKNOWN", ex.toString());
          }
          processing = false;
        }
        if (closed) {
          resolve(0);
        }
      } else if (gResolve) {
        let cmd;
        try {
          let cmdStr = gCmdQueue[0];
          cmd = JSON.parse(cmdStr);
        } catch (x2) {
          cmd = {};
        }
        gCmdQueue.splice(0, 1);
        let cbResole = gResolve;
        gResolve = null;
        cbResole(cmd);
      }
    });
    gReadLineOpen = true;
  });
}
async function main() {
  let i2 = 2, cmd = null, interactive = false;
  while (i2 < process.argv.length) {
    switch (process.argv[i2]) {
      case "--version":
        console.log(`npgp.js ${NPGPJS_VERSION}`);
        console.log(`${Keys.getVersionString()}`);
        return 0;
      case "-h":
      case "-?":
      case "--help":
        console.log(`npgp.js ${NPGPJS_VERSION}`);
        console.log(`${Keys.getVersionString()}
`);
        if (i2 == process.argv.length - 1) {
          jsonApi.describeApi();
        } else {
          jsonApi.describeApi(process.argv[i2 + 1]);
        }
        return 0;
      case "-c":
      case "--cmd":
        ++i2;
        cmd = process.argv[i2];
        break;
      case "-i":
      case "--interactive":
        interactive = true;
        break;
      case "--home":
        ++i2;
        keyStore.setHomePath(process.argv[i2]);
        break;
      case "--relaxed":
        gRelaxedMode = true;
        break;
      default:
        LogData.DIE("ERROR_INVALID_CMDLINE_PARAM", `Invalid parameter ${process.argv[i2]}`);
    }
    i2++;
  }
  ReadLine = rlp.createInterface({ input, output, autoCommit: true, terminal: interactive });
  await cryptoFuncs.initialize(promptForInput);
  if (!cmd) {
    return getDataFromStdin();
  } else {
    try {
      await processInputData(cmd);
      ReadLine.close();
    } catch (ex) {
      LogData.ERROR("ERROR_UNKNOWN", ex.toString());
      return 1;
    }
    return 0;
  }
}
main().then((r2) => {
}).catch((e2) => {
  LogData.DIE("ERROR_UNKNOWN", e2.message + "\n" + e2.stack);
});
/*! OpenPGP.js v6.1.0 - 2025-01-30 - this is LGPL licensed code, see LICENSE/our website https://openpgpjs.org/ for more information. */
/*! noble-ciphers - MIT License (c) 2023 Paul Miller (paulmillr.com) */
/*! noble-hashes - MIT License (c) 2022 Paul Miller (paulmillr.com) */
/*! noble-curves - MIT License (c) 2022 Paul Miller (paulmillr.com) */
